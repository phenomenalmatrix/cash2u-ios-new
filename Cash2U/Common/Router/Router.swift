//
//  Router.swift
//  cash2u
//
//  Created by Eldar Akkozov on 28/3/22.
//

import Foundation
import UIKit

protocol RouterBase {
    var navigationController: UINavigationController? { get set }
    var builder: BuilderProtocol? { get set }
    
    func initialSplashViewContrller()
}

protocol RouterAuthorization {
    func showNumber()
    func showSMS(number: String)
    func showPinCode()
    func showValidPinCode()
    func showRepeatPinCode()
    func showReferalEditCode()
}

protocol RouterHome {
    func showHomeGuest()
    func showHomeRegistered()
    func showHomeLoanAccepted()
    
    func showHomeRouter()
}

protocol RouterEntrance {
    func showOnBoard()
    func showLanguageSelection()
}

protocol RouterBaseNavigation {
    func showBack()
}

typealias RouterProtocol = RouterBase & RouterAuthorization & RouterHome & RouterBaseNavigation & RouterEntrance

// MARK: RouterBase

class Router: RouterBase {

    var navigationController: UINavigationController?
    var builder: BuilderProtocol?
    
    init(navigationController: UINavigationController, builder: BuilderProtocol) {
        self.navigationController = navigationController
        self.builder = builder
    }
    
    func initialSplashViewContrller() {
        guard let navigationController  = navigationController else { return }
        guard let splashModules = builder?.createSplashModule(router: self) else { return }
        
        navigationController.viewControllers = [splashModules]
    }
}

// MARK: RouterEntrance

extension Router: RouterEntrance {
    func showLanguageSelection() {
        guard let navigationController  = navigationController else { return }
        guard let languageSelectionModules = builder?.createLanguageSelectionModule(router: self) else { return }
        
        navigationController.pushViewController(languageSelectionModules, animated: true)
    }
    
    func showOnBoard() {
        guard let navigationController  = navigationController else { return }
        guard let onBoardModules = builder?.createOnBoardModule(router: self) else { return }
        
        navigationController.pushViewController(onBoardModules, animated: true)
    }
}

// MARK: RouterHome

extension Router: RouterHome {
    func showHomeGuest() {
        guard let navigationController  = navigationController else { return }
        guard let guestModules = builder?.createGuestModule(router: self) else { return }
        
        navigationController.pushViewController(guestModules, animated: true)
    }
    
    func showHomeRegistered() {
        guard let navigationController  = navigationController else { return }
        guard let registeredModules = builder?.createRegisteredModule(router: self) else { return }
        
        navigationController.pushViewController(registeredModules, animated: true)
    }
    
    func showHomeLoanAccepted() {
        
    }
    
    func showHomeRouter() {
        
    }
}

// MARK: RouterAuthorization

extension Router: RouterAuthorization {
    func showValidPinCode() {
        guard let navigationController  = navigationController else { return }
        guard let validPinCodeModules = builder?.createValidPinCodeModule(router: self) else { return }
        
        navigationController.pushViewController(validPinCodeModules, animated: true)
    }
    
    func showNumber() {
        guard let navigationController  = navigationController else { return }
        guard let authorizationModules = builder?.createNumberModule(router: self) else { return }
        
        navigationController.pushViewController(authorizationModules, animated: true)
    }
    
    func showSMS(number: String) {
        guard let navigationController  = navigationController else { return }
        guard let smsModules = builder?.createSmsModule(router: self, number: number) else { return }
        
        navigationController.pushViewController(smsModules, animated: true)
    }
    
    func showPinCode() {
        guard let navigationController  = navigationController else { return }
        guard let pinCodeModules = builder?.createPinCodeModule(router: self) else { return }
        
        navigationController.pushViewController(pinCodeModules, animated: true)
    }
    
    func showRepeatPinCode() {
        guard let navigationController  = navigationController else { return }
        guard let repeatPinCodeModules = builder?.createRepeatPinCodeModule(router: self) else { return }
        
        navigationController.pushViewController(repeatPinCodeModules, animated: true)
    }
    
    func showReferalEditCode() {
        guard let navigationController  = navigationController else { return }
        guard let repeatPinCodeModules = builder?.createRefetalEditModule(router: self) else { return }
        
        navigationController.pushViewController(repeatPinCodeModules, animated: true)
    }
}

// MARK: RouterBaseNavigation

extension Router: RouterBaseNavigation {
    func showBack() {
        guard let navigationController  = navigationController else { return }

        navigationController.popViewController(animated: true)
    }
}
