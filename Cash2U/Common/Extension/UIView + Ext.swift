//
//  UIView + Ext.swift
//  cash2u
//
//  Created by Eldar Akkozov on 2/4/22.
//

import UIKit
import SnapKit

extension UIView {
    func asImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
}

extension UIView {
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}

extension UIView {
    
    func showClick() {
        UIView.animate(withDuration: 0.1,
            animations: {
                self.transform = CGAffineTransform(scaleX: 0.90, y: 0.90)
            },
            completion: { _ in
                UIView.animate(withDuration: 0.1) {
                    self.transform = CGAffineTransform.identity
                }
            }
        )
    }
    
    func showUp() {
        DispatchQueue.main.async { [self] in
            isHidden = false
            
            self.transform = CGAffineTransform(translationX: 0, y: frame.height)
            
            UIView.animate(withDuration: 0.6) {
                self.transform = CGAffineTransform(translationX: 0, y: 0)
            }
        }
    }
    
    func showDown() {
        DispatchQueue.main.async { [self] in
            UIView.animate(withDuration: 0.6, animations: {
                self.transform = CGAffineTransform(translationX: 0, y: frame.height)
            }, completion: {(_ completed: Bool) -> Void in
                
                self.isHidden = true
            })
        }
    }
    
    func showZoomIn() {
        UIView.animate(withDuration: 2, delay: 0, options: [.curveEaseIn], animations: {
            self.center.y -= self.bounds.height
            
            self.layoutIfNeeded()
        }, completion: nil)
        
        self.isHidden = false
    }
    
    func showZoomOut() {
        UIView.animate(withDuration: 2, delay: 0, options: [.curveLinear], animations: {
            self.center.y += self.bounds.height
            
            self.layoutIfNeeded()
        }, completion: {(_ completed: Bool) -> Void in
            
            self.isHidden = true
        })
    }
    
    func showHidden(hidden: Bool, withDuration: Double = 0.3) {
        if hidden {
            UIView.animate(withDuration: withDuration, animations: {
                self.alpha = 0
            }) { (finished) in
                self.isHidden = finished
            }
        } else {
            self.alpha = 1
            
            UIView.transition(with: self, duration: withDuration, options: .transitionCrossDissolve, animations: {
                self.isHidden = hidden
            })
        }
    }
    
    var safeArea: ConstraintBasicAttributesDSL {
        #if swift(>=3.2)
            if #available(iOS 11.0, *) {
                return self.safeAreaLayoutGuide.snp
            }
            return self.snp
        #else
            return self.snp
        #endif
    }
    
    /// Border color of view; also inspectable from Storyboard.
    var borderColor: UIColor? {
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
        set {
            guard let color = newValue else {
                layer.borderColor = nil
                return
            }
            layer.borderColor = color.cgColor
        }
    }
    
    var maskedCorners: CACornerMask {
        get { layer.maskedCorners }
        set { layer.maskedCorners = newValue }
    }

    
    /// Border width of view; also inspectable from Storyboard.
    var borderWidth: CGFloat {
        get { layer.borderWidth }
        set { layer.borderWidth = newValue }
    }
    
    /// Corner radius of view; also inspectable from Storyboard.
    var cornerRadius: CGFloat {
        get { layer.cornerRadius }
        set {
            clipsToBounds = true
            layer.cornerRadius = abs(CGFloat(Int(newValue * 100)) / 100)
        }
    }
    
    func addSubviews(_ subviews: [UIView]) {
        subviews.forEach { addSubview($0) }
    }
    
    var parentViewController: UIViewController? {
        if let nextResponder = self.next as? UIViewController {
            return nextResponder
        } else if let nextResponder = self.next as? UIView {
            return nextResponder.parentViewController
        } else {
            return nil
        }
    }
    
    func addShadow(ofColor color: UIColor = UIColor(named: "gray_shdw") ?? .lightGray , radius: CGFloat, offset: CGSize = .zero, opacity: Float = 0.5) {
        layer.masksToBounds = false
        layer.shadowOffset = offset
        layer.shadowColor = color.cgColor
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
        
        let backgroundCGColor = backgroundColor?.cgColor
        backgroundColor = nil
        layer.backgroundColor =  backgroundCGColor
    }
    
    func addTopBorder(with color: UIColor?, andWidth borderWidth: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        border.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
        border.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: borderWidth)
        addSubview(border)
    }
    
    func addBottomBorder(with color: UIColor?, andWidth borderWidth: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        border.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
        border.frame = CGRect(x: 0, y: frame.size.height - borderWidth, width: frame.size.width, height: borderWidth)
        addSubview(border)
    }
    
    func addLeftBorder(with color: UIColor?, andWidth borderWidth: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        border.frame = CGRect(x: 0, y: 0, width: borderWidth, height: frame.size.height)
        border.autoresizingMask = [.flexibleHeight, .flexibleRightMargin]
        addSubview(border)
    }
    
    func addRightBorder(with color: UIColor?, andWidth borderWidth: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        border.autoresizingMask = [.flexibleHeight, .flexibleLeftMargin]
        border.frame = CGRect(x: frame.size.width - borderWidth, y: 0, width: borderWidth, height: frame.size.height)
        addSubview(border)
    }
    
    // OUTPUT 1
      func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 1

        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
      }

      // OUTPUT 2
      func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius

        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
      }
    
    func createGradientBlur() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [
            UIColor.clear.withAlphaComponent(0).cgColor,
            UIColor.black.withAlphaComponent(0.5).cgColor
        ]
        let effectView = UIVisualEffectView()
        effectView.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.size.height)
        gradientLayer.frame = effectView.bounds
        gradientLayer.locations = [0, 1]
        effectView.autoresizingMask = [.flexibleHeight]
        effectView.layer.mask = gradientLayer
        effectView.backgroundColor = UIColor.init(hex: "#3530FA")
        effectView.isUserInteractionEnabled = false //Use this to pass touches under this blur effect
        effectView.clipsToBounds = true
        addSubview(effectView)
    }
    
    func createGradientBlurGray(color: UIColor, gradientType: GradienType = .up, alpha: Double) {
        // you need use layoutIfNeeded on init (commonInit)
        let gradientLayer = CAGradientLayer()
        switch gradientType {
        case .up:
            gradientLayer.colors = [
                UIColor.clear.withAlphaComponent(alpha).cgColor,
                UIColor.black.withAlphaComponent(0).cgColor
            ]
        case .down:
            gradientLayer.colors = [
                UIColor.clear.withAlphaComponent(0).cgColor,
                UIColor.black.withAlphaComponent(alpha).cgColor
            ]
        }
        let effectView = UIVisualEffectView()
        effectView.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.size.height)
        gradientLayer.frame = effectView.bounds
        gradientLayer.locations = [0, 1]
        effectView.autoresizingMask = [.flexibleHeight]
        effectView.layer.mask = gradientLayer
        effectView.backgroundColor = color
        effectView.isUserInteractionEnabled = false //Use this to pass touches under this blur effect
        effectView.clipsToBounds = true
        addSubview(effectView)
    
    }
}

enum GradienType {
    case up
    case down
}

extension UIStackView {
    
    func removeAllArrangedSubviews() {
        
        let removedSubviews = arrangedSubviews.reduce([]) { (allSubviews, subview) -> [UIView] in
            self.removeArrangedSubview(subview)
            return allSubviews + [subview]
        }
        
        // Deactivate all constraints
        NSLayoutConstraint.deactivate(removedSubviews.flatMap({ $0.constraints }))
        
        // Remove the views from self
        removedSubviews.forEach({ $0.removeFromSuperview() })
    }
}
