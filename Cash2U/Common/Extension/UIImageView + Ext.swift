//
//  UIImageView + Ext.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/5/22.
//

import UIKit
import Kingfisher

extension UIImageView {
    func setImage(withLoader: Bool = true, with url: String, withPlaceholder: Bool = false) {
        guard let url = URL(string: url) else {return}
        self.kf.indicatorType = withPlaceholder ? .none : (withLoader ? .activity : .none)
        
        DispatchQueue.main.async {
            self.kf.setImage(with: url, placeholder: withPlaceholder ?  UIImage(named: "placeholder") : nil, options: [.loadDiskFileSynchronously, .transition(.fade(0.3))])}
    }
    
    func setImageWithAuth(withLoader: Bool = true, with url: String, withPlaceholder: Bool = false) {
        let modifier = AnyModifier { request in
            var r = request
            if let token = KeychainService.shared.accessToken {
                r.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            }
            return r
        }
        guard let url = URL(string: url) else {return}
        self.kf.indicatorType = withPlaceholder ? .none : (withLoader ? .activity : .none)
        
        DispatchQueue.main.async {
            self.kf.setImage(with: url, placeholder: withPlaceholder ?  UIImage(named: "placeholder") : nil, options: [.loadDiskFileSynchronously, .transition(.fade(0.3)), .requestModifier(modifier)]) { result in
                switch result {
                case .success(_):
                    break
                case .failure(let error):
                    print("\n[🏞]Error load img with auth: \(url) \n Error: \(error.localizedDescription)")
                    break
                }
            }
        }
        }
    }
    

