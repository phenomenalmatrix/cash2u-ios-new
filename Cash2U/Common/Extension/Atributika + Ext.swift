//
//  Atributika + Ext.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/5/22.
//

import Foundation
import Atributika
import UIKit

extension Style {
    
    static func mainTextStyle(
        size: CGFloat,
        fontType: UIFont.Weight = .regular,
        color: UIColor? = nil,
        alignment: NSTextAlignment = .left,
        letterSpacing: Double? = nil,
        lineHeight: CGFloat? = nil,
        lineBreakMode: NSLineBreakMode? = nil,
        _ type: StyleType = .normal
    ) -> Style {
        let font = UIFont.systemFont(ofSize: size, weight: fontType)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.0
        if let lineHeight = lineHeight {
            paragraphStyle.minimumLineHeight = lineHeight
            paragraphStyle.maximumLineHeight = lineHeight
        } else {
            paragraphStyle.minimumLineHeight = font.lineHeight
            paragraphStyle.maximumLineHeight = font.lineHeight
        }
        paragraphStyle.alignment = alignment
        if let lineBreakMode = lineBreakMode {
            paragraphStyle.lineBreakMode = lineBreakMode
        }
        var style = Style
            .font(font)
            .paragraphStyle(paragraphStyle)
        
        if let color = color {
            style = style.foregroundColor(color)
        }
        if let kern = letterSpacing {
            style = style.kern(Float(kern))
        }
        
        return style
    }
    
    func mainTextStyle(
        size: CGFloat,
        fontType: UIFont.Weight = .regular,
        color: UIColor? = nil,
        alignment: NSTextAlignment = .left,
        letterSpacing: Double? = nil,
        lineHeight: CGFloat? = nil,
        lineBreakMode: NSLineBreakMode? = nil,
        _ type: StyleType = .normal
    ) -> Style {
        let font = UIFont.systemFont(ofSize: size, weight: fontType)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.0
        if let lineHeight = lineHeight {
            paragraphStyle.minimumLineHeight = lineHeight
            paragraphStyle.maximumLineHeight = lineHeight
        } else {
            paragraphStyle.minimumLineHeight = font.lineHeight
            paragraphStyle.maximumLineHeight = font.lineHeight
        }
        paragraphStyle.alignment = alignment
        if let lineBreakMode = lineBreakMode {
            paragraphStyle.lineBreakMode = lineBreakMode
        }
        var style = Style
            .font(font)
            .paragraphStyle(paragraphStyle)
        
        if let color = color {
            style = style.foregroundColor(color)
        }
        if let kern = letterSpacing {
            style = style.kern(Float(kern))
        }
        
        return style
    }
    
    static func mainTextStyle(
        tag: String,
        size: CGFloat,
        fontType: UIFont.Weight = .regular,
        color: UIColor? = nil,
        alignment: NSTextAlignment = .left,
        letterSpacing: Double? = nil,
        lineHeight: CGFloat? = nil,
        lineBreakMode: NSLineBreakMode? = nil,
        _ type: StyleType = .normal
    ) -> Style {
        let font = UIFont.systemFont(ofSize: size, weight: fontType)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.0
        if let lineHeight = lineHeight {
            paragraphStyle.minimumLineHeight = lineHeight
            paragraphStyle.maximumLineHeight = lineHeight
        } else {
            paragraphStyle.minimumLineHeight = font.lineHeight
            paragraphStyle.maximumLineHeight = font.lineHeight
        }
        paragraphStyle.alignment = alignment
        if let lineBreakMode = lineBreakMode {
            paragraphStyle.lineBreakMode = lineBreakMode
        }
        var style = Style(tag)
            .font(font)
            .paragraphStyle(paragraphStyle)
        
        if let color = color {
            style = style.foregroundColor(color)
        }
        if let kern = letterSpacing {
            style = style.kern(Float(kern))
        }
        
        return style
    }
    
    func mainTextStyle(
        tag: String,
        size: CGFloat,
        fontType: UIFont.Weight = .regular,
        color: UIColor? = nil,
        alignment: NSTextAlignment = .left,
        letterSpacing: Double? = nil,
        lineHeight: CGFloat? = nil,
        lineBreakMode: NSLineBreakMode? = nil,
        _ type: StyleType = .normal
    ) -> Style {
        let font = UIFont.systemFont(ofSize: size, weight: fontType)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.0
        if let lineHeight = lineHeight {
            paragraphStyle.minimumLineHeight = lineHeight
            paragraphStyle.maximumLineHeight = lineHeight
        } else {
            paragraphStyle.minimumLineHeight = font.lineHeight
            paragraphStyle.maximumLineHeight = font.lineHeight
        }
        paragraphStyle.alignment = alignment
        if let lineBreakMode = lineBreakMode {
            paragraphStyle.lineBreakMode = lineBreakMode
        }
        var style = Style(tag)
            .font(font)
            .paragraphStyle(paragraphStyle)
        
        if let color = color {
            style = style.foregroundColor(color)
        }
        if let kern = letterSpacing {
            style = style.kern(Float(kern))
        }
        
        return style
    }
}
