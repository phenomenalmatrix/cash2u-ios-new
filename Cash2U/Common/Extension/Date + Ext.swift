//
//  Date + Ext.swift
//  Cash2U
//
//  Created by talgar osmonov on 22/7/22.
//

import Foundation
import UIKit

enum DateFormatterType: String {
    case mmdd = "MM.dd"
    case mmss = "mm:ss"
    case ddMMyyyy = "dd.MM.yyyy"
    case yyyyMMddTHHmmssSSS  = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    case yyyyMMddTHHmmss  = "yyyy-MM-dd'T'HH:mm:ss"
    case yyyyMMddTHHmmssSSSZ  = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    case yyyyMMddTHHmmssSSZ  = "yyyy-MM-dd'T'HH:mm:ss.SSZ"
}

extension Date {
    
    func isToday() -> Bool {
        return Calendar.current.isDateInToday(self)
    }
    
    func wasYesterday() -> Bool {
        return Calendar.current.isDateInYesterday(self)
    }
    
    func isInSameDayOf(date: Date) -> Bool {
        return Calendar.current.isDate(self, inSameDayAs:date)
    }
    
    func currentTimeMillis() -> Double {
        return Double(self.timeIntervalSince1970)
    }
    
    static func getStringDate(stringDate: String, getFormat: DateFormatterType = .ddMMyyyy, isUTC: Bool = false) -> String {
        let dateFormatter: DateFormatter = DateFormatter()
        if isUTC {
            dateFormatter.timeZone = TimeZone(identifier: "UTC")
        }
        var date: Date?
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        if dateFormatter.date(from: stringDate) != nil {
            date = dateFormatter.date(from: stringDate)!
        }
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        if dateFormatter.date(from: stringDate) != nil {
            date = dateFormatter.date(from: stringDate)!
        }
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if dateFormatter.date(from: stringDate) != nil {
            date = dateFormatter.date(from: stringDate)!
        }
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        if dateFormatter.date(from: stringDate) != nil {
            date = dateFormatter.date(from: stringDate)!
        }
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSZ"
        if dateFormatter.date(from: stringDate) != nil {
            date = dateFormatter.date(from: stringDate)!
        }
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss:ss"
        if dateFormatter.date(from: stringDate) != nil {
            date = dateFormatter.date(from: stringDate)!
        }
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss:SSS"
        if dateFormatter.date(from: stringDate) != nil {
            date = dateFormatter.date(from: stringDate)!
        }
        dateFormatter.dateFormat = "yyyy-mm-dd'T'hh:mm:ss.s+zzzzzz"
        if dateFormatter.date(from: stringDate) != nil {
            date = dateFormatter.date(from: stringDate)!
        }
        dateFormatter.dateFormat = "yyyy-mm-dd'T'hh:mm:ss-hh:mm"
        if dateFormatter.date(from: stringDate) != nil {
            date = dateFormatter.date(from: stringDate)!
        }
        dateFormatter.dateFormat = "yyyy-mm-dd'T'hh:mm:ss+hh:mm"
        if dateFormatter.date(from: stringDate) != nil {
            date = dateFormatter.date(from: stringDate)!
        }
        dateFormatter.dateFormat = "yyyy-mm-dd'T'hh:mm:ss[.SSS]"
        if dateFormatter.date(from: stringDate) != nil {
            date = dateFormatter.date(from: stringDate)!
        }
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS:Z"
        if dateFormatter.date(from: stringDate) != nil {
            date = dateFormatter.date(from: stringDate)!
        }
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss+hh:mm"
        if dateFormatter.date(from: stringDate) != nil {
            date = dateFormatter.date(from: stringDate)!
        }
        dateFormatter.dateFormat = "yyyy-mm-dd'T'HH:mm:ss"
        if dateFormatter.date(from: stringDate) != nil {
            date = dateFormatter.date(from: stringDate)!
        }
        dateFormatter.dateFormat = "yyyy-mm-dd'T'HH:mm:ss+hh:mm"
        if dateFormatter.date(from: stringDate) != nil {
            date = dateFormatter.date(from: stringDate)!
        }
        let finalFormatter: DateFormatter = DateFormatter()
        finalFormatter.dateFormat = getFormat.rawValue
        
        return finalFormatter.string(from: date!)
    }
    
    static func getStringToDate(stringDate: String, getFormat: DateFormatterType = .mmss, isUTC: Bool = false) -> Date {
        let dateFormatter: DateFormatter = DateFormatter()
        if isUTC {
            dateFormatter.timeZone = TimeZone(identifier: "UTC")
        }
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSZ"
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss+hh:mm"
        
        return dateFormatter.date(from: stringDate)!
    }
    
    static func getDateToString(date: Date, getFormat: DateFormatterType = .yyyyMMddTHHmmssSSSZ, isUTC: Bool = false) -> String {
        
        let finalFormatter: DateFormatter = DateFormatter()
        if isUTC {
            finalFormatter.timeZone = TimeZone(identifier: "UTC")
        }
        finalFormatter.dateFormat = getFormat.rawValue
        
        return finalFormatter.string(from: date)
    }
}

extension NumberFormatter {
    static let `default`: NumberFormatter = {
        let instance = NumberFormatter()
        instance.numberStyle = .decimal
        instance.groupingSize = 3
        instance.secondaryGroupingSize = 3
        instance.groupingSeparator = " "
        instance.maximumFractionDigits = 1
        return instance
    }()
}
