//
//  String +Ext.swift
//  cash2u
//
//  Created by Eldar Akkozov on 2/4/22.
//

import Foundation
import UIKit
import TTTAttributedLabel
import Atributika

extension String {
    
    static var empty: String { return "" }
    
    static var newLine: String { return "\n" }
    
    static func whitespace(count: Int = 1) -> String {
        return String(repeating: " ", count: count)
    }
    
    var isContainsLettersOnly: Bool {
        let nsRange = NSRange(location: 0, length: (self as NSString).length)
        let pattern = "[a-z]"

        let unescapedRegex = try? NSRegularExpression(
            pattern: pattern, options: []
        )

        return unescapedRegex?.matches(in: self, options: [], range: nsRange)
            .count == nsRange.length
    }
    
    var isContainsNumberOnly: Bool {
        let nsRange = NSRange(location: 0, length: (self as NSString).length)
        let pattern = "[0-9]"

        let unescapedRegex = try? NSRegularExpression(
            pattern: pattern, options: []
        )

        return unescapedRegex?.matches(in: self, options: [], range: nsRange)
            .count == nsRange.length
    }
}

extension NSAttributedString {
    func withLineSpacing(_ spacing: CGFloat) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(attributedString: self)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.lineSpacing = spacing
        attributedString.addAttribute(.paragraphStyle,
                                      value: paragraphStyle,
                                      range: NSRange(location: 0, length: string.count))
        return NSAttributedString(attributedString: attributedString)
    }
}

extension TTTAttributedLabel {
    /// Настроить ссылки по AttributedText
    func setupLinks(
        attributedText: AttributedText,
        tagNames: [String] = ["a"]
    ) {
        for detection in attributedText.detections {
            guard
                case .tag(let tag) = detection.type,
                tagNames.contains(tag.name)
            else { continue }
            addLink(
                toTransitInformation: tag.attributes,
                with: NSRange(
                    detection.range,
                    in: attributedText.string
                )
            )
        }
    }
}

extension NSAttributedString {
    
    func getHeight(withConstrainedWidth width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(
            width: width,
            height: .greatestFiniteMagnitude
        )
        let boundingBox = boundingRect(
            with: constraintRect,
            options: .usesLineFragmentOrigin,
            context: nil
        )
        return ceil(boundingBox.height)
    }
    
    func getWidth(withConstrainedHeight height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(
            width: .greatestFiniteMagnitude,
            height: height
        )
        let boundingBox = boundingRect(
            with: constraintRect,
            options: .usesLineFragmentOrigin,
            context: nil
        )

        return ceil(boundingBox.width)
    }
}

