//
//  BaseRepository.swift
//  cash2u
//
//  Created by Eldar Akkozov on 2/4/22.
//

import Foundation

class BaseRepository<Service: AnyObject> {
    
    var service: Service
    
    required init(service: Service) {
        self.service = service
    }
}
