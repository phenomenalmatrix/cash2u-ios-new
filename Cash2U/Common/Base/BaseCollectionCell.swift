//
//  BaseCollectionCell.swift
//  cash2u
//
//  Created by Eldar Akkozov on 4/4/22.
//

import Foundation
import UIKit

class BaseCollectionCell: UICollectionViewCell {
    
    var isCreate = false
    
    override func layoutSubviews() {
        if !isCreate {
            isCreate = true
            
            setupCell()
            setupSubViews()
        }
    }
    
    open func setupCell() {}
    open func setupSubViews() {}
}
