//
//  BaseController.swift
//  cash2u
//
//  Created by Eldar Akkozov on 28/3/22.
//

import Foundation
import UIKit

class BaseController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        setupKeyboard()

        setupConstraint()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        view.backgroundColor = .init(named: "BackgroundWindowColor")
        
        setupUI()
    }
    
    public func setupUI() {

    }
    
    public func setupConstraint() {
        
    }
    
    private func setupKeyboard() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        hideKeyboard()
    }
    
    @objc func keyBoardWillShow(notification: NSNotification) {
        showKeyBoard(height: (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.height)
    }
    
    
    @objc func keyBoardWillHide(notification: NSNotification) {
        hideKeyBoard(height: (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.height)
    }
    
    open func showKeyBoard(height: CGFloat) {}
    
    open func hideKeyBoard(height: CGFloat) {}
    
    func hideKeyboard() {
        view.endEditing(true)
    }
    
    convenience init() {
        self.init(nibName: nil, bundle: nil)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

