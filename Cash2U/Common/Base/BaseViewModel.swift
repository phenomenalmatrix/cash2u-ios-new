//
//  BaseViewModel.swift
//  cash2u
//
//  Created by Eldar Akkozov on 2/4/22.
//

import Foundation
import UIKit

class BaseViewModel<View> {
    
    var delegate: View? = nil
    
    var router: RouterProtocol
    
    required init(view: View, router: RouterProtocol) {
        self.delegate = view
        self.router = router
    }
    
    func back() {
        router.showBack()
    }
}
