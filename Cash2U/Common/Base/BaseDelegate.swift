//
//  BaseDelegate.swift
//  cash2u
//
//  Created by Eldar Akkozov on 2/4/22.
//

import Foundation

protocol BaseDelegate: AnyObject {
    func showProgress()
    func hideProgress()
    
    func showAlert(_ title: String, _ message: String)
}
