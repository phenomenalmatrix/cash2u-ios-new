//
//  BaseModelController.swift
//  cash2u
//
//  Created by Eldar Akkozov on 2/4/22.
//

import Foundation
import UIKit

class BaseModelController<T>: BaseController {
    var viewModel: T!
}
