//
//  BaseNavigationController.swift
//  cash2u
//
//  Created by Eldar Akkozov on 28/3/22.
//

import Foundation
import UIKit

class BaseNavigationController: UINavigationController {
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setUpNavBar()
        setUpUI()
    }
    
    func setUpUI() {
        view.backgroundColor = .white
    }
    
    func setUpNavBar() {
        isNavigationBarHidden = true
    }
}

