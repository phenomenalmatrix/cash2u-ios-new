//
//  BaseServiceViewModel.swift
//  cash2u
//
//  Created by Eldar Akkozov on 2/4/22.
//

import Foundation

class BaseRepositoryViewModel<View, Repository: AnyObject>: BaseViewModel<View> {
    
    var repository: Repository
    
    required init(view: View, router: RouterProtocol, repository: Repository) {
        self.repository = repository

        super.init(view: view, router: router)
    }
    
    required init(view: View, router: RouterProtocol) {
        fatalError("init(view:router:) has not been implemented")
    }
}
