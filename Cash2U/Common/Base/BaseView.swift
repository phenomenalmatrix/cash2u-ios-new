//
//  BaseView.swift
//  cash2u
//
//  Created by Eldar Akkozov on 4/4/22.
//

import Foundation
import UIKit

class BaseView: UIView {
    
    private var isCreate = false
    private var setupClick = true

    private var onClick: (UIView) -> Void = {_ in }
    
    override func layoutSubviews() {
        if !isCreate {
            isCreate = true
    
            setupSubViews()
            setupView()
        }
    }
    
    open func setupSubViews() {}
    open func setupView() {}
    
    func onClickTouch() { }
    
    func onClickListener(onClick: @escaping (UIView) -> Void) {
        if setupClick {
            setupClick = false
            
            let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.checkAction))
            addGestureRecognizer(gesture)
        }
        
        self.onClick = onClick
    }
    
    @objc func checkAction(sender: UITapGestureRecognizer) {
        self.onClick(self)
        
        onClickTouch()
    }
}
