//
//  UserDefault.swift
//  cash2u
//
//  Created by Eldar Akkozov on 28/3/22.
//

import Foundation

enum Key: String {
    case TOKEN = "TOKEN"
    case IS_FIRST_OPEN = "IS_FIRST_OPEN"
}

@propertyWrapper
class UserDefault<T> {
    private var key: Key
    private var defaultValue: T?
    
    var wrappedValue: T? {
        get {
            if let value = UserDefaults.standard.object(forKey: key.rawValue) as? T {
                return value
            }
            
            return defaultValue
        }
        set {
            UserDefaults.standard.set(newValue, forKey: key.rawValue)
            UserDefaults.standard.synchronize()
        }
    }
    
    init(key: Key, defaultValue: T? = nil) {
        self.key = key
        self.defaultValue = defaultValue
    }
}

