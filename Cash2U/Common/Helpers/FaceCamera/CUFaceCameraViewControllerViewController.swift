//
//  CUFaceCameraViewControllerViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 31/10/22.
//  
//

import UIKit
import AVFoundation

protocol CUFaceCameraViewControllerViewControllerDelegate: AnyObject {
    func selfiePhotoResult(image: UIImage)
}

final class CUFaceCameraViewControllerViewController: CUViewController, AVCapturePhotoCaptureDelegate {
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    weak var delegate: CUFaceCameraViewControllerViewControllerDelegate? = nil
    
    private lazy var captureSession = AVCaptureSession()
    private lazy var stillImageOutput = AVCapturePhotoOutput()
    private lazy var videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
    
    private let cameraThread = DispatchQueue(label: "com.cash2u.fronCamera")
    
    
    private lazy var dimmedView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var previewView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var takePhotoButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Отложить селфи", backgroundColor: .mainDarkColor, borderWidth: 1, borderColor: .grayColor)
        view.addTarget(self, action: #selector(onTakePhotoTapped), for: .touchUpInside)
        return view
    }()
    
    override func loadView() {
        super.loadView()
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
        
        let width: CGFloat = view.frame.size.width
        let height: CGFloat = view.frame.size.height

        let path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height), cornerRadius: 0)
        let rect = CGRect(x: 50 , y: view.safeAreaInsets.top + ((ScreenHelper.getWidth - 10) - 300), width: ScreenHelper.getWidth - 100, height: ScreenHelper.getWidth - 10)
        let circlePath = UIBezierPath(ovalIn: rect)
        path.append(circlePath)
        path.usesEvenOddFillRule = true

        let fillLayer = CAShapeLayer()
        fillLayer.path = path.cgPath
        fillLayer.fillRule = .evenOdd
        fillLayer.fillColor = UIColor.redColor?.cgColor
        fillLayer.opacity = 0.3
        
        let path1 = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height), cornerRadius: 0)
        let rect1 = CGRect(x: width / 2 - (ScreenHelper.getWidth / 2.5) / 2, y: ((ScreenHelper.getWidth / 2.5) - 50) + ((ScreenHelper.getWidth - 10)), width: ScreenHelper.getWidth / 2.5, height: (ScreenHelper.getWidth / 2.5) - 50)
        let circlePath1 = UIBezierPath(roundedRect: rect1, cornerRadius: 10)
        path1.append(circlePath1)
        path1.usesEvenOddFillRule = true

        let fillLayer1 = CAShapeLayer()
        fillLayer1.path = path1.cgPath
        fillLayer1.fillRule = .evenOdd
        fillLayer1.fillColor = UIColor.redColor?.cgColor
        fillLayer1.opacity = 0.3
        
        dimmedView.layer.addSublayer(fillLayer)
        dimmedView.layer.addSublayer(fillLayer1)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        captureSession.sessionPreset = .medium
        
        guard let backCamera = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: .front)
            else {
                print("Unable to access back camera!")
                return
        }
        
        do {
            let input = try AVCaptureDeviceInput(device: backCamera)
            
            if captureSession.canAddInput(input) && captureSession.canAddOutput(stillImageOutput) {
                captureSession.addInput(input)
                captureSession.addOutput(stillImageOutput)
                setupLivePreview()
            }
        }
        catch let error  {
            print("Error Unable to initialize back camera:  \(error.localizedDescription)")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.captureSession.stopRunning()
    }
    
    private func setupLivePreview() {
        
        videoPreviewLayer.videoGravity = .resizeAspectFill
        videoPreviewLayer.connection?.videoOrientation = .portrait
        previewView.layer.addSublayer(videoPreviewLayer)
        
        cameraThread.async { //[weak self] in
            self.captureSession.startRunning()
            DispatchQueue.main.async {
                self.videoPreviewLayer.frame = self.previewView.bounds
            }
        }
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        
        guard let imageData = photo.fileDataRepresentation()
            else { return }
        
        if let image = UIImage(data: imageData) {
            delegate?.selfiePhotoResult(image: image)
            self.dismiss(animated: true)
        }
    }
    
    @objc private func onTakePhotoTapped() {
        let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
        stillImageOutput.capturePhoto(with: settings, delegate: self)
    }
    
}

extension CUFaceCameraViewControllerViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        
        view.addSubview(previewView)
        view.addSubview(dimmedView)
        view.addSubview(takePhotoButton)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        previewView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        dimmedView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        takePhotoButton.snp.makeConstraints { make in
            make.bottom.equalTo(view.safeArea.bottom).offset(-16)
            make.leading.trailing.equalToSuperview().inset(20)
            make.height.equalTo(ScreenHelper.mainButtonHeight)
        }
    }
}
