//
//  DispatchQueueHelper.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/5/22.
//

import Foundation

let dbThreadQueue = DispatchQueue(label: "kg.client.mobile.cash2u.db_thread")

extension DispatchQueue {
    static var dbThread: DispatchQueue {
        dbThreadQueue
    }
}
