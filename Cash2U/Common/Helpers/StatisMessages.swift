//
//  StatisMessages.swift
//  Cash2U
//
//  Created by talgar osmonov on 4/11/22.
//

import Foundation


enum IdentificationMessages {
    
    static let currentUser = CUSender(senderId: "currentUser", displayName: "Talgar Osmonov")
    static let cash2uUser = CUSender(senderId: "cash2uUser", displayName: "cash2u")
    
    static var ready0: IdentificationMessageRealm {
        return IdentificationMessageRealm.createModel(
            text: "🤟 Ура! Вы сохранили все фотки!\nОсталось совсем немного",
            type: .text,
            id: "ready0",
            sender: cash2uUser,
            status: .create
        )
    }
    
    static var ready1: IdentificationMessageRealm {
        return IdentificationMessageRealm.createModel(
            text: "👌 Идет проверка данных Пожалуйста, подождите немного",
            type: .text,
            id: "ready1",
            sender: cash2uUser,
            status: .create
        )
    }
    
    static var selfiePhoto0: IdentificationMessageRealm {
        return IdentificationMessageRealm.createModel(
            text: "😎 Вы почти у цели!\nВы сохранили фотоданные паспорта",
            type: .text,
            id: "selfiePhoto0",
            sender: cash2uUser,
            status: .create
        )
    }
    
    static var selfiePhoto1: IdentificationMessageRealm {
        return IdentificationMessageRealm.createModel(
            text: "1.  Сделайте селфи с паспортом",
            type: .text,
            id: "selfiePhoto1",
            sender: cash2uUser,
            status: .create
        )
    }
    
    static var backPhoto0: IdentificationMessageRealm {
        return IdentificationMessageRealm.createModel(
            text: "🤩 Отличное начало!\nВы сохранили лицевую сторону паспорта",
            type: .text,
            id: "backPhoto0",
            sender: cash2uUser,
            status: .create
        )
    }
    
    static var backPhoto1: IdentificationMessageRealm {
        return IdentificationMessageRealm.createModel(
            text: "1.  Загрузите фото обратной\n     стороны паспорта",
            type: .text,
            id: "backPhoto1",
            sender: cash2uUser,
            status: .create
        )
    }
    
    static var backPhoto2: IdentificationMessageRealm {
        return IdentificationMessageRealm.createModel(
            photo: "frunze_test",
            type: .photo,
            id: "backPhoto2",
            sender: cash2uUser,
            status: .create
        )
    }
    
    static var frontPhoto0: IdentificationMessageRealm {
        return IdentificationMessageRealm.createModel(
            text: "1.  Загрузите фото лицевой\n     стороны паспорта",
            type: .text,
            id: "frontPhoto0",
            sender: cash2uUser,
            status: .create
        )
    }
    
    static var frontPhoto1: IdentificationMessageRealm {
        return IdentificationMessageRealm.createModel(
            photo: "frunze_test",
            type: .photo,
            id: "frontPhoto1",
            sender: cash2uUser,
            status: .create
        )
    }
    
    static var city0: IdentificationMessageRealm {
        return IdentificationMessageRealm.createModel(
            text: "Введите адрес проживания.\n<b>(город/село, улица, дом/офис/строение)</b>",
            type: .text,
            id: "city0",
            sender: cash2uUser,
            status: .create
        )
    }
    
    static var inn0: IdentificationMessageRealm {
        return IdentificationMessageRealm.createModel(
            text: "Отлично, начнем! 😉",
            type: .text,
            id: "inn0",
            sender: cash2uUser,
            status: .create
        )
    }
    
    static var inn1: IdentificationMessageRealm {
        return IdentificationMessageRealm.createModel(
            text: "Для начала заполните ваши паспортные данные.\n(ИНН, номер паспорта)",
            type: .text,
            id: "inn1",
            sender: cash2uUser,
            status: .create
        )
    }
    
    static var greetings0: IdentificationMessageRealm {
        return IdentificationMessageRealm.createModel(
            text: "👋 <b>Привет!</b>\nДля использования всех\nвозможностей приложения\n<b>cash2u</b>, подтвердите свою\nличность, это займет\nпримерно 5 минут 😉",
            type: .text,
            id: "greetings0",
            sender: cash2uUser,
            status: .create
        )
    }
    
    static var greetings1: IdentificationMessageRealm {
        return IdentificationMessageRealm.createModel(
            text: "<b>Как это сделать:</b>\n\n1.  Заполните паспортные данные\n\n2.  Введите адрес проживания",
            type: .text,
            id: "greetings1",
            sender: cash2uUser,
            status: .create
        )
    }
    
    static var greetings2: IdentificationMessageRealm {
        return IdentificationMessageRealm.createModel(
            text: "3.  Загрузить фото лицевой\n     стороны паспорта",
            type: .text,
            id: "greetings2",
            sender: cash2uUser,
            status: .create
        )
    }
    
    static var greetings3: IdentificationMessageRealm {
        return IdentificationMessageRealm.createModel(
            photo: "frunze_test",
            type: .photo,
            id: "greetings3",
            sender: cash2uUser,
            status: .create
        )
    }
    
    static var greetings4: IdentificationMessageRealm {
        return IdentificationMessageRealm.createModel(
            text: "4.  Загрузить фото обратной\n     стороны паспорта",
            type: .text,
            id: "greetings4",
            sender: cash2uUser,
            status: .create
        )
    }
    
    static var greetings5: IdentificationMessageRealm {
        return IdentificationMessageRealm.createModel(
            photo: "frunze_test",
            type: .photo,
            id: "greetings5",
            sender: cash2uUser,
            status: .create
        )
    }
    
    static var greetings6: IdentificationMessageRealm {
        return IdentificationMessageRealm.createModel(
            text: "5.  Сделать селфи с паспортом",
            type: .text,
            id: "greetings6",
            sender: cash2uUser,
            status: .create
        )
    }
    
    static var greetings7: IdentificationMessageRealm {
        return IdentificationMessageRealm.createModel(
            photo: "frunze_test",
            type: .photo,
            id: "greetings7",
            sender: cash2uUser,
            status: .create
        )
    }
    
    static var greetings8: IdentificationMessageRealm {
        return IdentificationMessageRealm.createModel(
            text: "6.  И дождаться видеозвонка\n     от специалиста",
            type: .text,
            id: "greetings8",
            sender: cash2uUser,
            status: .create
        )
    }
    
    static var greetings9: IdentificationMessageRealm {
        return IdentificationMessageRealm.createModel(
            photo: "frunze_test",
            type: .photo,
            id: "greetings9",
            sender: cash2uUser,
            status: .create
        )
    }
}
