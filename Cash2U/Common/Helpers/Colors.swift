//
//  Colors.swift
//  Cash2U
//
//  Created by talgar osmonov on 18/7/22.
//

import Foundation
import UIKit

extension UIColor {
    static let mainColor = UIColor(named: "main_color")
    static let mainTextColor = UIColor(named: "main_text_color")
    static let grayColor = UIColor(named: "gray_color")
    static let inputColor = UIColor(named: "input_color")
    static let mainButtonColor = UIColor(named: "main_button_color")
    static let redColor = UIColor(named: "red_color")
    static let mainDarkColor = UIColor(named: "main_dark_color")
    static let darkBlueColor = UIColor(named: "dark_blue_color")
    static let mainWhiteColor = UIColor(named: "main_white_color")
    static let mainBlueColor = UIColor(named: "main_blue_color")
    static let inputDarkColor = UIColor(named: "input_dark_color")
    static let secondaryColor = UIColor(named: "secondary_color")
    static let onBoardCardColor = UIColor(named: "on_board_card_color")
    static let secondBlueColor = UIColor(named: "second_blue_color")
    static let brownColor = UIColor(named: "brown_color")
    static let thirdaryColor = UIColor(named: "thirdary_color")
    static let mainSecondaryColor = UIColor(named: "main_secondary_color")
//    static let secondaryColor = UIColor(named: "secondary_color")
//    static let darkBlueColor = UIColor(named: "dark_blue_color")
//    static let mainWhiteColor = UIColor(named: "main_white_color")
//    static let mainBlueColor = UIColor(named: "main_blue_color")
}
