//
//  NotificationCenterHelper.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/5/22.
//

import Foundation

extension Notification.Name {
    static let termsOfUseCheckBox = Notification.Name(rawValue: "termsOfUseCheckBox")
    static let phoneBecomeFirstResponder = Notification.Name(rawValue: "phoneBecomeFirstResponder")
    static let installmentPageSelected = Notification.Name(rawValue: "installmentPageSelected")
    static let callResponder = Notification.Name(rawValue: "callResponder")
}
