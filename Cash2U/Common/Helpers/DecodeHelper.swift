//
//  DecodeHelper.swift
//  Cash2U
//
//  Created by Oroz on 31/10/22.
//

import Foundation

class DecodeHelper {
  static func decodeDataToObject<T: Codable>(data : Data?)->T?{
        if let dt = data{
            do{
                return try JSONDecoder().decode(T.self, from: dt)
            }  catch let DecodingError.dataCorrupted(context) {
                print(context)
            } catch let DecodingError.keyNotFound(key, context) {
                print("Key '\(key)' not found:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch let DecodingError.valueNotFound(value, context) {
                print("Value '\(value)' not found:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch let DecodingError.typeMismatch(type, context)  {
                print("Type '\(type)' mismatch:", context.debugDescription)
                print("codingPath:", context.codingPath)
            } catch {
                print("error: ", error)
            }
        }
        return nil
    }
    
    static func encodeObjectToData<T: Codable>(model: T) -> Data? {
        let jsonData = try? JSONEncoder().encode(model).toJSON()?.compactMapValues{ $0 }
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: jsonData,
            options: []) {
            let theJSONText = String(data: theJSONData,encoding: .ascii)
            return Data(theJSONData)
        }
        return nil
    }
}
