//
//  Screen.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/5/22.
//

import Foundation
import UIKit

enum ScreenHelper {
    static let getWidth = UIScreen.main.bounds.width
    static let getHeight = UIScreen.main.bounds.height
    static let getScreenBounds = UIScreen.main.bounds
    
    static let mainButtonHeight: CGFloat = isSmallButton ? 52 : 62
    
    static var isSmallButton: Bool {
        switch Device.version() {
        case .iPhone5, .iPhone5C, .iPhone5S, .iPhoneSE, .iPhone6, .iPhone6S, .iPodTouch4Gen, .iPodTouch5Gen, .iPodTouch6Gen, .iPodTouch7Gen:
            return true
        case .simulator:
            return false
        case .unknown:
            return false
        default:
            return false
        }
    }
    
    static var isSmallScreen: Bool {
        switch Device.version() {
        case .iPhone5, .iPhone5C, .iPhone5S, .iPhoneSE, .iPhone6, .iPhone6S, .iPodTouch4Gen, .iPodTouch5Gen, .iPodTouch6Gen, .iPodTouch7Gen:
            return true
        case .iPhone7, .iPhone8, .iPhoneSE2:
            return true
        case .iPhone11Pro_Max, .iPhoneXS_Max, .iPhone12Pro_Max, .iPhone13Pro_Max, .iPhone6Plus, .iPhone6SPlus, .iPhone7Plus, .iPhoneXR, .iPhone8Plus, .iPhoneX, .iPhoneXS, .iPhone11, .iPhone11Pro, .iPhone12, .iPhone12Pro, .iPhone13, .iPhone13Pro, .iPhone12Mini, .iPhone13Mini:
            return false
            
        case .iPadPro9_7Inch, .iPadPro12_9Inch, .iPadPro10_5Inch, .iPadPro12_9Inch2, .iPadPro11_0Inch2, .iPadPro11_0Inch3, .iPadPro12_9Inch3, .iPadPro12_9Inch4, .iPadPro12_9Inch5, .iPadPro11_0Inch, .iPadMini4, .iPadMini5, . iPadMini6, .iPad5, .iPad6, .iPad7, .iPad8, .iPad9, .iPadAir3, .iPadAir4:
            return false
        case .simulator:
            return false
        case .unknown:
            return false
        default:
            return false
        }
    }
    
    static var isEyebrowsScreen: Bool {
        switch Device.version() {
        case .iPhone5, .iPhone5C, .iPhone5S, .iPhoneSE, .iPhone6, .iPhone6S, .iPodTouch4Gen, .iPodTouch5Gen, .iPodTouch6Gen, .iPodTouch7Gen:
            return false
        case .iPhone7, .iPhone8, .iPhoneSE2, .iPhone6Plus, .iPhone6SPlus, .iPhone7Plus, .iPhone8Plus:
            return false
        case .iPhone11Pro_Max, .iPhoneXS_Max, .iPhone12Pro_Max, .iPhone13Pro_Max, .iPhoneXR, .iPhoneX, .iPhoneXS, .iPhone11, .iPhone11Pro, .iPhone12, .iPhone12Pro, .iPhone13, .iPhone13Pro, .iPhone12Mini, .iPhone13Mini:
            return true
        case .iPadPro9_7Inch, .iPadPro12_9Inch, .iPadPro10_5Inch, .iPadPro12_9Inch2, .iPadPro11_0Inch2, .iPadPro11_0Inch3, .iPadPro12_9Inch3, .iPadPro12_9Inch4, .iPadPro12_9Inch5, .iPadPro11_0Inch, .iPadMini4, .iPadMini5, . iPadMini6, .iPad5, .iPad6, .iPad7, .iPad8, .iPad9, .iPadAir3, .iPadAir4:
            return false
        case .simulator:
            return true
        case .unknown:
            return true
        default:
            return false
        }
    }
    
    static var noFaceID: Bool {
        switch Device.version() {
        case .iPhone5, .iPhone5C, .iPhone5S, .iPhoneSE, .iPhone6, .iPhone6S, .iPodTouch4Gen, .iPodTouch5Gen, .iPodTouch6Gen, .iPodTouch7Gen,.iPhone7, .iPhone8, .iPhoneSE2, .iPhone6Plus, .iPhone6SPlus, .iPhone7Plus, .iPhone8Plus:
            return true
        case .iPhone11Pro_Max, .iPhoneXS_Max, .iPhone12Pro_Max, .iPhone13Pro_Max, .iPhoneXR, .iPhoneX, .iPhoneXS, .iPhone11, .iPhone11Pro, .iPhone12, .iPhone12Pro, .iPhone13, .iPhone13Pro, .iPhone12Mini, .iPhone13Mini:
            return false
        case .iPadPro9_7Inch, .iPadPro12_9Inch, .iPadPro10_5Inch, .iPadPro12_9Inch2, .iPadPro11_0Inch2, .iPadPro11_0Inch3, .iPadPro12_9Inch3, .iPadPro12_9Inch4, .iPadPro12_9Inch5, .iPadPro11_0Inch, .iPadMini4, .iPadMini5, . iPadMini6, .iPad5, .iPad6, .iPad7, .iPad8, .iPad9, .iPadAir3, .iPadAir4:
            return false
        case .simulator:
            return false
        case .unknown:
            return false
        default:
            return false
        }
    }
}
