//
//  ModuleBuilder.swift
//  cash2u
//
//  Created by Eldar Akkozov on 28/3/22.
//

import Foundation
import UIKit

protocol BuilderHome {
    func createGuestModule(router: RouterProtocol) -> UIViewController
    func createRegisteredModule(router: RouterProtocol) -> UIViewController

    
    func createGuidePanelModel(router: RouterProtocol) -> UIViewController
}

protocol BuilderAuthorization {
    func createNumberModule(router: RouterProtocol) -> UIViewController
    func createSmsModule(router: RouterProtocol, number: String) -> UIViewController
    func createPinCodeModule(router: RouterProtocol) -> UIViewController
    func createValidPinCodeModule(router: RouterProtocol) -> UIViewController
    func createRepeatPinCodeModule(router: RouterProtocol) -> UIViewController
    func createRefetalEditModule(router: RouterProtocol) -> UIViewController
}

protocol BuilderEntrance {
    func createOnBoardModule(router: RouterProtocol) -> UIViewController
    func createLanguageSelectionModule(router: RouterProtocol) -> UIViewController
}

protocol BuilderBaseProtocol {
    func createSplashModule(router: RouterProtocol) -> UIViewController
}

typealias BuilderProtocol = BuilderBaseProtocol & BuilderAuthorization & BuilderEntrance & BuilderHome

class ModuleBuilder: BuilderBaseProtocol {
    
    func createSplashModule(router: RouterProtocol) -> UIViewController {
        let controller = SplashController()
        let service = SplashService()
        let repository = SplashRepository(service: service)
        
        let viewModel = SplashViewModel(view: controller, router: router, repository: repository)
        
        controller.viewModel = viewModel
        
        return controller
    }
}

// MARK: BuilderAuthorization

extension ModuleBuilder: BuilderAuthorization {
    func createValidPinCodeModule(router: RouterProtocol) -> UIViewController {
        let controller = ValidPinController()
        let viewModel = ValidPinViewModel(view: controller, router: router)
        
        controller.viewModel = viewModel
        
        return controller
    }
    
    func createRefetalEditModule(router: RouterProtocol) -> UIViewController {
        let controller = ReferalEditController()
        let viewModel = ReferalEditViewModel(view: controller, router: router)
        
        controller.viewModel = viewModel
        
        return controller
    }
    
    
    func createPinCodeModule(router: RouterProtocol) -> UIViewController {
        let controller = PinCodeController()
        let viewModel = PinCodeViewModel(view: controller, router: router)
        
        controller.viewModel = viewModel
        
        return controller
    }
    
    func createRepeatPinCodeModule(router: RouterProtocol) -> UIViewController {
        let controller = RepeatPicCodeController()
        let viewModel = RepeatPicCodeViewModel(view: controller, router: router)
        
        controller.viewModel = viewModel
        
        return controller
    }
    
    func createSmsModule(router: RouterProtocol, number: String) -> UIViewController {
        let controller = SmsController()
        let viewModel = SmsViewModel(number: number, view: controller, router: router)
        
        controller.viewModel = viewModel
        
        return controller
    }
    
    func createNumberModule(router: RouterProtocol) -> UIViewController {
        let controller = NumberController()
        let viewModel = NumberViewModel(view: controller, router: router)
        
        controller.viewModel = viewModel
        
        return controller
    }
}

// MARK: BuilderEntrance

extension ModuleBuilder: BuilderEntrance {
    func createLanguageSelectionModule(router: RouterProtocol) -> UIViewController {
        let controller = LanguageSelectionController()
        let viewModel = LanguageSelectionViewModel(view: controller, router: router)
        
        controller.viewModel = viewModel
        
        return controller
    }
    
    func createOnBoardModule(router: RouterProtocol) -> UIViewController {
        let controller = OnBoardController()
        let viewModel = OnBoardViewModel(view: controller, router: router)
        
        controller.viewModel = viewModel
        
        return controller
    }
}

// MARK: BuilderHome

extension ModuleBuilder: BuilderHome {
    func createRegisteredModule(router: RouterProtocol) -> UIViewController {
        let controller = RegisteredController()
        let viewModel = RegisteredViewModel(view: controller, router: router)
        
        controller.viewModel = viewModel
        
        return controller
    }
    
    func createGuestModule(router: RouterProtocol) -> UIViewController {
        let controller = GuestController()
        let service = GuestService()
        let repository = GuestRepository(service: service)
        
        let viewModel = GuestViewModel(view: controller, router: router, repository: repository)
        
        controller.viewModel = viewModel
        
        return controller
    }
    
    func createGuidePanelModel(router: RouterProtocol) -> UIViewController {
        return GuideViewController()
    }
}
