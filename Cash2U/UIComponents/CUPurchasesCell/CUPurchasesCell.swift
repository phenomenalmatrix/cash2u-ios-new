//
//  CUPurchasesCell.swift
//  Cash2U
//
//  Created talgar osmonov on 4/10/22.
//


import UIKit

final class CUPurchasesCell: CUCollectionViewTapAnimationCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var image: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "frunze_test")
        view.contentMode = .scaleAspectFill
        view.cornerRadius = 12
        return view
    }()
    
    private lazy var nameLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(size: 16, numberOfLines: 2, fontType: .medium, titleColor: .grayColor)
        return view
    }()
    
    private lazy var amountLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(size: 16, numberOfLines: 1, fontType: .medium, titleColor: .grayColor)
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUPurchasesCell {
    /// Установка
    func setup() {
        nameLabel.changeText(title: "Nike Shop KG")
        amountLabel.changeText(title: "-2 200,00 c")
    }
}

private extension CUPurchasesCell {
    // Общий инициализатор
    func commonInit() {
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(image)
        contentView.addSubview(nameLabel)
        contentView.addSubview(amountLabel)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        image.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.centerY.equalToSuperview()
            make.size.equalTo(65)
        }
        
        nameLabel.snp.makeConstraints { make in
            make.leading.equalTo(image.snp.trailing).offset(16)
            make.trailing.equalToSuperview().offset(60)
            make.centerY.equalToSuperview()
        }
        
        amountLabel.snp.makeConstraints { make in
            make.trailing.equalToSuperview()
            make.centerY.equalToSuperview()
        }
    }
}
