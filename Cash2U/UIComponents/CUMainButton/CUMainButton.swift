//
//  CUMainButton.swift
//  Cash2U
//
//  Created talgar osmonov on 19/7/22.
//


import UIKit
import Atributika

final class CUMainButton: UIControl {
    
    private var isConstraintsInstalled: Bool = false
    
    private var animator = UIViewPropertyAnimator()
    
    private lazy var titleLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isUserInteractionEnabled = false
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
    
    @objc private func touchDown() {
        animator = UIViewPropertyAnimator(duration: 0.4, dampingRatio: 1, animations: {
            self.alpha = 0.5
        })
        animator.startAnimation()
    }
    
    @objc private func touchUp() {
        animator = UIViewPropertyAnimator(duration: 0.4, dampingRatio: 1, animations: {
            self.alpha = 1
        })
        animator.startAnimation()
    }
}

extension CUMainButton {
    
    func isActive(_ isActive: Bool) {
        if isActive {
            self.backgroundColor = .mainButtonColor
        } else {
            self.backgroundColor = .grayColor
        }
    }
    
    /// Установка
    func setup(title: String, titleSize: CGFloat = 17, fontType:  UIFont.Weight = .bold, titleColor: UIColor? = .mainTextColor, backgroundColor: UIColor? = .mainButtonColor, borderWidth: CGFloat = 0, borderColor: UIColor? = .clear, cornerRadius: CGFloat = 17) {
        self.cornerRadius = cornerRadius
        self.backgroundColor = backgroundColor
        self.borderWidth = borderWidth
        self.borderColor = borderColor
        titleLabel.setup(title: title, size: titleSize, fontType: fontType, titleColor: titleColor, alignment: .center)
    }
    
    func changeText(text: String) {
        titleLabel.changeText(title: text)
    }
    
    func getTitleWidth(withConstrainedHeight: CGFloat) -> CGFloat {
        return titleLabel.getWidth(withConstrainedHeight: withConstrainedHeight)
    }
}

private extension CUMainButton {
    // Общий инициализатор
    func commonInit() {
        createViewHierarchy()
        setupConstraintsIfNeeded()
        
        addTarget(self, action: #selector(touchDown), for: [.touchDown, .touchDragEnter])
        addTarget(self, action: #selector(touchUp), for: [.touchUpInside, .touchDragExit, .touchCancel])
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        addSubview(titleLabel)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        titleLabel.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
