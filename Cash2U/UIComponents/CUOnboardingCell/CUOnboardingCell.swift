//
//  CUOnboardingCell.swift
//  Cash2U
//
//  Created talgar osmonov on 20/9/22.
//


import UIKit
import AVFoundation

final class CUOnboardingCell: UICollectionViewCell {
    
    private var isConstraintsInstalled: Bool = false
    
    var player: AVQueuePlayer?
    var playerLooper: AVPlayerLooper?
    
    private lazy var playerLayer: AVPlayerLayer = {
        let view = AVPlayerLayer()
        view.videoGravity = .resizeAspectFill
        view.cornerRadius = 18
        view.masksToBounds = true
        return view
    }()
    
    private lazy var titleLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(size: 28, numberOfLines: 2, fontType: .bold, isSizeToFit: true)
        return view
    }()
    
    private lazy var descriptionLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(size: 20, numberOfLines: 3, fontType: .medium, isSizeToFit: true)
        return view
    }()
    
    private lazy var subdescriptionLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(size: 22, numberOfLines: 0, fontType: .medium, lineHeight: 40, isSizeToFit: true)
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUOnboardingCell {
    /// Установка
    func setup(model: OnboardingModel, isMuted: Bool) {
        DispatchQueue.main.async {
            guard let path = Bundle.main.path(forResource: model.video, ofType:"MP4") else {return}
            self.player = AVQueuePlayer(url: URL(fileURLWithPath: path))
            self.player?.volume = isMuted ? 0 : 1
            self.playerLooper = AVPlayerLooper(player: self.player!, templateItem: self.player!.currentItem!)
            self.playerLayer.player = self.player
            
            self.titleLabel.changeText(title: model.text)
            self.descriptionLabel.changeText(title: model.description)
            self.subdescriptionLabel.changeText(title: model.subdescription ?? "")
        }
    }
}

private extension CUOnboardingCell {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .mainColor
        clipsToBounds = true
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.layer.addSublayer(playerLayer)
        contentView.addSubview(subdescriptionLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(titleLabel)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        playerLayer.frame = contentView.frame.inset(by: .init(top: 0, left: 20, bottom: 0, right: 20))
        
        subdescriptionLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(40)
            make.bottom.equalToSuperview().offset(-30)
        }
        
        descriptionLabel.snp.makeConstraints { make in
            make.bottom.equalTo(subdescriptionLabel.snp.top).offset(-10)
            make.leading.trailing.equalToSuperview().inset(40)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.bottom.equalTo(descriptionLabel.snp.top).offset(-20)
            make.leading.trailing.equalToSuperview().inset(40)
        }
    }
}
