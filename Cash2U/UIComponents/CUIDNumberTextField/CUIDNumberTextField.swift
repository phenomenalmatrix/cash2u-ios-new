//
//  CUIDNumberTextField.swift
//  Cash2U
//
//  Created talgar osmonov on 28/9/22.
//


import UIKit

final class CUIDNumberTextField: UIView {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var idMaskLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "ID", size: 20, fontType: .medium)
        return view
    }()
    
    private lazy var titleLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Номер паспорта", size: 14, fontType: .regular, titleColor: .grayColor)
        return view
    }()
    
    lazy var numberField: UITextField = {
        let view = UITextField()
        view.attributedPlaceholder = NSAttributedString(
            string: "0000000",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.grayColor ?? .darkText]
        )
        view.delegate = self
        view.keyboardType = .numberPad
        view.font = .systemFont(ofSize: 20, weight: .medium)
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUIDNumberTextField {
    /// Установка
    func setup() {
        
    }
}

extension CUIDNumberTextField: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false }
        let newString = (text as NSString).replacingCharacters(in: range, with: string)
        textField.text = format(with: "XXXXXXX", phone: newString)
        return false
    }
    
    func format(with mask: String, phone: String) -> String {
        let numbers = phone.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
        var result = ""
        var index = numbers.startIndex

        for ch in mask where index < numbers.endIndex {
            if ch == "X" {
                result.append(numbers[index])
                index = numbers.index(after: index)

            } else {
                result.append(ch)
            }
        }
        
        if result.count == 7 {
            numberField.endEditing(true)
//            delegate?.isPhoneNumberFull(isFull: true)
        } else {
//            delegate?.isPhoneNumberFull(isFull: false)
        }
        
        return result
    }
}

private extension CUIDNumberTextField {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .thirdaryColor 
        layer.borderWidth = 0.5
        layer.borderColor = UIColor.grayColor?.cgColor
        cornerRadius = 18
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        addSubview(titleLabel)
        addSubview(idMaskLabel)
        addSubview(numberField)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(14)
            make.leading.equalToSuperview().offset(20)
        }
        
        idMaskLabel.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-13)
            make.leading.equalToSuperview().offset(20)
        }
        
        numberField.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-12)
            make.leading.equalToSuperview().offset(50)
        }
    }
}
