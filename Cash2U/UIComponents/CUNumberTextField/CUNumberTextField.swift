//
//  CUNumberTextField.swift
//  Cash2U
//
//  Created talgar osmonov on 22/7/22.
//


import UIKit

protocol CUNumberTextFieldDelegate: AnyObject {
    func isPhoneNumberFull(isFull: Bool)
}


final class CUNumberTextField: UIView {
    
    private var isConstraintsInstalled: Bool = false
    
    weak var delegate: CUNumberTextFieldDelegate? = nil
    
    private lazy var numberMaskLabel = UILabelView("    +996", font: .systemFont(ofSize: 24, weight: .bold), textColor: .grayColor)
    
    lazy var numberField: UITextField = {
        let view = UITextField()
        view.attributedPlaceholder = NSAttributedString(
            string: "000 00 00 00",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.grayColor ?? .darkText]
        )
        view.delegate = self
        view.keyboardType = .numberPad
        view.font = .systemFont(ofSize: 24, weight: .bold)
        return view
    }()
    
    private lazy var separator: UIView = {
        let view = UIView()
        view.backgroundColor = .grayColor
        return view
    }()
    
    private lazy var contener = UIView()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUNumberTextField {
    /// Установка
    func setup() {
        
    }
}


extension CUNumberTextField: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        numberMaskLabel.textColor = .white
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text?.count == 0 {
            numberMaskLabel.textColor = .grayColor
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false }
        let newString = (text as NSString).replacingCharacters(in: range, with: string)
        textField.text = format(with: "XXX XX XX XX", phone: newString)
        return false
    }
    
    func format(with mask: String, phone: String) -> String {
        let numbers = phone.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
        var result = ""
        var index = numbers.startIndex

        for ch in mask where index < numbers.endIndex {
            if ch == "X" {
                result.append(numbers[index])
                index = numbers.index(after: index)

            } else {
                result.append(ch)
            }
        }
        
        if result.count == 12 {
            numberField.endEditing(true)
            delegate?.isPhoneNumberFull(isFull: true)
        } else {
            delegate?.isPhoneNumberFull(isFull: false)
        }
        
        return result
    }
}


private extension CUNumberTextField {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .inputColor
        cornerRadius = 12
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        addSubview(contener)
        contener.addSubview(numberMaskLabel)
        contener.addSubview(separator)
        contener.addSubview(numberField)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        contener.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.bottom.equalToSuperview()
        }
        
        numberMaskLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.top.equalToSuperview().offset(12)
            make.bottom.equalToSuperview().offset(-12)
        }
        
        separator.snp.makeConstraints { make in
            make.leading.equalTo(numberMaskLabel.snp.trailing).offset(8)
            make.width.equalTo(1)
            make.top.equalToSuperview().offset(18)
            make.bottom.equalToSuperview().offset(-18)
        }
        
        numberField.snp.makeConstraints { make in
            make.leading.equalTo(separator.snp.trailing).offset(8)
            make.top.equalToSuperview().offset(12)
            make.bottom.equalToSuperview().offset(-12)
            make.width.equalTo(180)
            make.trailing.equalToSuperview()
        }
    }
}
