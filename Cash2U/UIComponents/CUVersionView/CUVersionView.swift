//
//  CUVersionView.swift
//  Cash2U
//
//  Created talgar osmonov on 4/8/22.
//


import UIKit
import Atributika

final class CUVersionView: UICollectionViewCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var titleLabel: UILabel = {
        
        var attribitedTitle: NSAttributedString {
            let style = Style.mainTextStyle(
                size: 16,
                fontType: .regular,
                color: .grayColor,
                alignment: .center,
                lineHeight: 21,
                lineBreakMode: .byTruncatingTail
            )
            return "Версия приложеньки: \(ProjectConfig.appVersion).\(ProjectConfig.appBuild)".styleAll(style).attributedString
        }
        
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 2
        view.attributedText = attribitedTitle
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUVersionView {
    /// Установка
    func setup() {
        
    }
}

private extension CUVersionView {
    // Общий инициализатор
    func commonInit() {
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(titleLabel)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        titleLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
        }
    }
}
