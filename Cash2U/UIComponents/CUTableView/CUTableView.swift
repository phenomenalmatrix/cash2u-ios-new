//
//  CUTableView.swift
//  Cash2U
//
//  Created talgar osmonov on 16/8/22.
//


import UIKit

class CUTableView: UITableView {
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        self.delaysContentTouches = false
        self.tableFooterView = UIView()
        self.backgroundColor = .mainColor
        self.showsHorizontalScrollIndicator = false
        self.showsVerticalScrollIndicator = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
