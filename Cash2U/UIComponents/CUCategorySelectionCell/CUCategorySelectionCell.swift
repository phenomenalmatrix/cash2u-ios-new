//
//  CUCategorySelectionCell.swift
//  Cash2U
//
//  Created talgar osmonov on 26/8/22.
//


import UIKit

final class CUCategorySelectionCell: CUCollectionViewTapAnimationCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var title: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "", size: 18, numberOfLines: 1, fontType: .medium)
        return view
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .grayColor
        view.alpha = 0.3
        return view
    }()
    
    private lazy var icon: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "WhiteArrowRight")
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUCategorySelectionCell {
    /// Установка
    func setup(text: String, isFirst: Bool = false, isLast: Bool = false, isExpanded: Bool) {
        title.changeText(title: text)
        let transform = CGAffineTransform.init(rotationAngle: isExpanded ? CGFloat.pi / 2.0 : 0)
        icon.transform = transform
        if isFirst && !isLast {
            icon.isHidden = false
            cornerRadius = 10
            layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
            lineView.isHidden = true
            return
        } else if isLast && !isFirst {
            icon.isHidden = true
            cornerRadius = 10
            layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
            lineView.isHidden = true
            return
        } else if isLast && isFirst {
            icon.isHidden = false
            cornerRadius = 10
            layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMinYCorner]
            lineView.isHidden = true
            return
        } else {
            icon.isHidden = true
            cornerRadius = 0
            lineView.isHidden = false
        }
    }
}

private extension CUCategorySelectionCell {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .thirdaryColor
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubviews([title, lineView, icon])
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        
        title.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-60)
        }
        
        lineView.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.height.equalTo(1)
        }
        
        icon.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().offset(-20)
            make.size.equalTo(15)
        }
    }
}
