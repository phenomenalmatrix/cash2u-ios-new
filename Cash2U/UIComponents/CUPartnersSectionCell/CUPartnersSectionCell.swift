//
//  CUPartnersSectionCell.swift
//  Cash2U
//
//  Created talgar osmonov on 25/8/22.
//


import UIKit

final class CUPartnersSectionCell: CUCollectionViewTapAnimationCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var nol3: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "0/0/3", titleSize: 13, backgroundColor: .mainBlueColor, borderWidth: 0.5, borderColor: .mainTextColor, cornerRadius: 25 / 2)
        view.isUserInteractionEnabled = false
        view.setHeight(25)
        return view
    }()
    
    private lazy var nol6: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "0/0/6", titleSize: 13, backgroundColor: .systemPurple, borderWidth: 0.5, borderColor: .mainTextColor, cornerRadius: 25 / 2)
        view.isUserInteractionEnabled = false
        view.setHeight(25)
        return view
    }()
    
    private lazy var nolStack: UIStackView = {
        let view = UIStackView(arrangedSubviews: [nol3, nol6])
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .vertical
        view.alignment = .fill
        view.distribution = .fill
        view.spacing = 5
        return view
    }()
    
    private lazy var image: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleToFill
        view.cornerRadius = 18
        return view
    }()
    
    private lazy var title: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "", size: 16, numberOfLines: 1, fontType: .semibold, titleColor: .mainTextColor, alignment: .left)
        return view
    }()
    
    private lazy var subtitle: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "", size: 15, numberOfLines: 2, fontType: .regular, titleColor: .grayColor, alignment: .left)
        return view
    }()
    
    private lazy var raitingView: CURaitingView = {
        let view = CURaitingView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.cornerRadius = 25 / 2
        view.setup()
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUPartnersSectionCell {
    /// Установка
    func setup() {
        image.setImage(with: "https://images.unsplash.com/photo-1565992441121-4367c2967103?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=627&q=80", withPlaceholder: true)
        title.changeText(title: "Top Sport")
        subtitle.changeText(title: "Одежда и товары для спорта")
    }
}

private extension CUPartnersSectionCell {
    // Общий инициализатор
    func commonInit() {
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubviews([image, title, subtitle, nolStack, raitingView])
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        image.snp.makeConstraints { make in
            make.leading.trailing.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(-80)
        }
        
        title.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(image.snp.bottom).offset(10)
        }
        
        subtitle.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(title.snp.bottom).offset(5)
        }
        
        nolStack.snp.makeConstraints { make in
            make.leading.top.equalToSuperview().offset(12)
            make.width.equalTo(55)
        }
        
        raitingView.snp.makeConstraints { make in
            make.trailing.bottom.equalTo(image).offset(-12)
            make.width.equalTo(85)
            make.height.equalTo(25)
        }
    }
}
