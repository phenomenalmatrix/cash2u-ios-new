//
//  CUSearchView.swift
//  Cash2U
//
//  Created talgar osmonov on 19/8/22.
//


import UIKit

protocol CUSearchViewDelegate: AnyObject {
    func filterButtonTapped()
}

final class CUSearchView: UICollectionViewCell {
    
    private var isConstraintsInstalled: Bool = false
    
    weak var delegate: CUSearchViewDelegate? = nil
    
    private lazy var title: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Я ищу...", size: 17, titleColor: .gray)
        return view
    }()
    
    private lazy var image: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "search_icon")
        return view
    }()
    
    private lazy var filterButton: CUIconButton = {
        let view = CUIconButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(image: UIImage(named: "FilterIcon"), backgroundColor: .clear)
        view.addTarget(self, action: #selector(onFilterButtonTapped), for: .touchUpInside)
        return view
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = .grayColor
        view.alpha = 0.2
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
    
    @objc private func onFilterButtonTapped() {
        delegate?.filterButtonTapped()
    }
}

extension CUSearchView {
    /// Установка
    func setup() {
        
    }
}

private extension CUSearchView {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .thirdaryColor
        cornerRadius = 10
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(image)
        contentView.addSubview(title)
        contentView.addSubview(filterButton)
        contentView.addSubview(lineView)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        image.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.centerY.equalToSuperview()
            make.size.equalTo(20)
        }
        
        title.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(51)
            make.centerY.equalToSuperview()
        }
        
        filterButton.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-5)
            make.centerY.equalToSuperview()
            make.height.equalTo(45)
            make.width.equalTo(45)
        }
        
        lineView.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-55)
            make.top.bottom.equalToSuperview()
            make.width.equalTo(1)
        }
    }
}
