//
//  CUSwitchWithTitleView.swift
//  Cash2U
//
//  Created talgar osmonov on 6/10/22.
//


import UIKit

final class CUSwitchWithTitleView: UICollectionViewCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var switchLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Автоплатеж")
        return view
    }()
    
    private lazy var switchToggle: UISwitch = {
        let view = UISwitch()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isOn = UserDefaultsService.shared.isAutoPayEnabled
        view.addTarget(self, action: #selector(onSwitch(_:)), for: .valueChanged)
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
    
    // MARK: - @objc
    
    @objc private func onSwitch(_ sender: UISwitch) {
        UserDefaultsService.shared.isAutoPayEnabled = sender.isOn
    }
}

extension CUSwitchWithTitleView {
    /// Установка
    func setup() {
        
    }
}

private extension CUSwitchWithTitleView {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .secondaryColor
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(switchLabel)
        contentView.addSubview(switchToggle)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        switchLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.centerY.equalToSuperview()
        }
        
        switchToggle.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-20)
            make.centerY.equalToSuperview()
        }
    }
}
