//
//  CUPartnerPageTopView.swift
//  Cash2U
//
//  Created talgar osmonov on 29/8/22.
//


import UIKit

final class CUPartnerPageTopView: UICollectionViewCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var image: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleToFill
        view.cornerRadius = 120 / 2
        return view
    }()
    
    private lazy var raitingView: CURaitingView = {
        let view = CURaitingView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup()
        return view
    }()
    
    private lazy var title: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Top Sport", size: 24, numberOfLines: 2, fontType: .bold, alignment: .center)
        return view
    }()
    
    private lazy var subtitle: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Одежда, обувь, спорт", size: 18, numberOfLines: 2, fontType: .medium, titleColor: .grayColor, alignment: .center)
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUPartnerPageTopView {
    /// Установка
    func setup() {
        image.setImage(with: "https://images.unsplash.com/photo-1565992441121-4367c2967103?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=627&q=80")
    }
}

private extension CUPartnerPageTopView {
    // Общий инициализатор
    func commonInit() {
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubviews([image, raitingView, title, subtitle])
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        image.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.centerX.equalToSuperview()
            make.size.equalTo(120)
        }
        
        raitingView.snp.makeConstraints { make in
            make.top.equalTo(image.snp.bottom).offset(10)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(30)
        }
        
        title.snp.makeConstraints { make in
            make.top.equalTo(raitingView.snp.bottom).offset(10)
            make.leading.trailing.equalToSuperview()
        }
        
        subtitle.snp.makeConstraints { make in
            make.top.equalTo(title.snp.bottom).offset(0)
            make.leading.trailing.equalToSuperview()
        }
    }
}
