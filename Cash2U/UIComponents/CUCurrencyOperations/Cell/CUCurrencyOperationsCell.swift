//
//  CUCurrencyOperationsCell.swift
//  Cash2U
//
//  Created talgar osmonov on 7/8/22.
//


import UIKit

final class CUCurrencyOperationsCell: UICollectionViewCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var background: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .thirdaryColor
        view.cornerRadius = 10
        return view
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .inputColor
        return view
    }()
    
    private lazy var prices: CUCurrencyFiatView = {
        let view = CUCurrencyFiatView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var secondPrices: CUCurrencyFiatView = {
        let view = CUCurrencyFiatView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUCurrencyOperationsCell {
    /// Установка
    func setup() {
        
    }
}

private extension CUCurrencyOperationsCell {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .secondaryColor
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(background)
        contentView.addSubview(lineView)
        contentView.addSubview(prices)
        contentView.addSubview(secondPrices)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        background.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        lineView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(40)
            make.trailing.equalToSuperview().offset(-40)
            make.centerY.equalToSuperview()
            make.height.equalTo(1)
        }
        
        prices.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(0)
            make.trailing.equalToSuperview().offset(0)
            make.bottom.equalTo(lineView.snp.top).offset(-10)
        }
        
        secondPrices.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(0)
            make.trailing.equalToSuperview().offset(0)
            make.top.equalTo(lineView.snp.bottom).offset(10)
        }
    }
}
