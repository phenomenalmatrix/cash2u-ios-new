//
//  CUCurrencyFiatView.swift
//  Cash2U
//
//  Created talgar osmonov on 8/8/22.
//


import UIKit

final class CUCurrencyFiatView: UIView {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var fiatLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "USD", size: 16, fontType: .semibold, titleColor: .grayColor)
        return view
    }()
    
    private lazy var buyLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "82,11", size: 16, fontType: .semibold)
        return view
    }()
    
    private lazy var sellLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "82,88", size: 16, fontType: .semibold)
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUCurrencyFiatView {
    /// Установка
    func setup() {
        
    }
}

private extension CUCurrencyFiatView {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .clear
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        addSubviews([fiatLabel, buyLabel, sellLabel])
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        fiatLabel.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.leading.equalToSuperview().offset(45)
        }
        
        buyLabel.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.trailing.equalToSuperview().offset(-45)
        }
        
        sellLabel.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.trailing.equalTo(buyLabel.snp.leading).offset(-40)
        }
    }
}
