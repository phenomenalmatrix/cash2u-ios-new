//
//  CUCurrencyOperationsView.swift
//  Cash2U
//
//  Created talgar osmonov on 4/8/22.
//


import UIKit

protocol CUCurrencyOperationsViewDelegate: AnyObject {
    
}

final class CUCurrencyOperationsView: UICollectionViewCell {
    
    private var isConstraintsInstalled: Bool = false
    
    weak var delegate: CUCurrencyOperationsViewDelegate? = nil
    
    private lazy var titleView: CUTitleWithArrow = {
        let view = CUTitleWithArrow()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var fiatLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "ВАЛЮТА", size: 11, fontType: .bold)
        return view
    }()
    
    private lazy var buyLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "ПОКУПКА", size: 11, fontType: .bold)
        return view
    }()
    
    private lazy var sellLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "ПРОДАЖА", size: 11, fontType: .bold)
        return view
    }()
    
    private lazy var operationsCollection: CUCollectionView = {
        let layout = SnappingCollectionViewLayout()
        layout.scrollDirection = .horizontal
        let view = CUCollectionView(frame: .zero, collectionViewLayout: layout)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.delegate = self
        view.dataSource = self
        view.register(CUCurrencyOperationsCell.self)
        view.backgroundColor = .secondaryColor
        view.decelerationRate = .fast
        view.alwaysBounceHorizontal = true
        return view
    }()
    
    private lazy var pageControl: UIPageControl = {
        let view = UIPageControl()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfPages = 2
        view.addTarget(self, action: #selector(onPageControlChanged), for: .valueChanged)
        view.currentPageIndicatorTintColor = .mainTextColor?.withAlphaComponent(0.5)
        view.pageIndicatorTintColor = .thirdaryColor
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
    
    @objc private func onPageControlChanged() {
        showItem(index: pageControl.currentPage)
    }
    
    private func showItem(index: Int) {
        pageControl.currentPage = index
        let indexPath = IndexPath(item: index, section: 0)
        operationsCollection.scrollToItem(at: indexPath, at: [.centeredHorizontally, .centeredVertically], animated: true)
    }
}

extension CUCurrencyOperationsView {
    /// Установка
    func setup(isArrowHidden: Bool) {
        titleView.setup(titleText: "Валютные операции", numberOfLines: 1, isShowArrow: !isArrowHidden)
    }
}

extension CUCurrencyOperationsView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(cellType: CUCurrencyOperationsCell.self, for: indexPath)
        cell.setup()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 6
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.size.width
        let page = Int(floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1)
        pageControl.currentPage = page
    }
    
}

private extension CUCurrencyOperationsView {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .secondaryColor
        cornerRadius = 18
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(titleView)
        contentView.addSubviews([fiatLabel, buyLabel, sellLabel])
        contentView.addSubview(operationsCollection)
        contentView.addSubview(pageControl)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        titleView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-5)
            make.top.equalToSuperview().offset(20)
        }
        
        fiatLabel.snp.makeConstraints { make in
            make.top.equalTo(titleView.snp.bottom).offset(20)
            make.leading.equalToSuperview().offset(35)
        }
        
        buyLabel.snp.makeConstraints { make in
            make.top.equalTo(titleView.snp.bottom).offset(20)
            make.trailing.equalToSuperview().offset(-35)
        }
        
        sellLabel.snp.makeConstraints { make in
            make.top.equalTo(titleView.snp.bottom).offset(20)
            make.trailing.equalTo(buyLabel.snp.leading).offset(-25)
        }
        
        operationsCollection.snp.makeConstraints { make in
            make.top.equalTo(fiatLabel.snp.bottom).offset(12)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview().offset(-30)
        }
        
        pageControl.snp.makeConstraints { make in
            make.top.equalTo(operationsCollection.snp.bottom)
            make.leading.trailing.equalToSuperview()
        }
    }
}
