//
//  CUOTPView.swift
//  Cash2U
//
//  Created talgar osmonov on 19/7/22.
//


import UIKit
import SnapKit
import Atributika

protocol CUOTPViewDelegate: AnyObject {
    func dpOTPViewAddText(_ text: String, position: Int)
    func dpOTPViewRemoveText(_ text: String, position: Int)
    func dpOTPViewChangePositionAt(_ position: Int)
    func dpOTPViewBecomeFirstResponder()
    func dpOTPViewResignFirstResponder()
}

protocol FaceIDDelegate: AnyObject {
    func faceIdTapped()
}

protocol ReturnButtonDelegate: AnyObject {
    func returnButtonTapped()
}

protocol MainButtonDelegate: AnyObject {
    func mainButtonTapped()
}

final class CUOTPView: UIView {
    
    weak var delegate: CUOTPViewDelegate? = nil
    weak var faceIDdelegate: FaceIDDelegate? = nil
    weak var returnButtondelegate: ReturnButtonDelegate? = nil
    weak var mainButtondelegate: MainButtonDelegate? = nil
    
    private var isConstraintsInstalled: Bool = false
    
    private var errorTitleHeightConstraint: Constraint?
    
    lazy var otpView: DPOTPView = {
        let view = DPOTPView()
        view.dismissOnLastEntry = true
        view.isCursorHidden = true
        view.count = 6
        view.dpOTPViewDelegate = self
        view.spacing = 8
        view.fontTextField = .systemFont(ofSize: 24, weight: .bold)
        view.placeholder = "000000"
        view.placeholderTextColor = .grayColor ?? .black
        view.backGroundColorTextField = .inputColor ?? .black
        view.textColorTextField = .mainTextColor ?? .black
        view.cornerRadiusTextField = 11
        return view
    }()
    
    private lazy var errorTitleLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "", size: 17, numberOfLines: 2, fontType: .bold, titleColor: .redColor, alignment: .center)
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUOTPView {
    /// Установка
    func setup(count: Int? = 6, placeholder: String? = "000000") {
        otpView.count = count ?? 6
        otpView.placeholder = placeholder ?? "000000"
    }
    
    func setError(title: String) {
        var attribitedText: NSAttributedString {
            let style = Style.mainTextStyle(
                size: 17,
                fontType: .bold,
                color: .redColor,
                alignment: .center,
                lineBreakMode: .byTruncatingTail
            )
            return title.styleAll(style).attributedString
        }
        self.errorTitleLabel.changeText(title: title)
        self.errorTitleHeightConstraint?.update(offset: 20)
    }
    
    func unsetError() {
        self.errorTitleLabel.changeText(title: "")
        self.errorTitleHeightConstraint?.update(offset: 0)
    }
}


extension CUOTPView: DPOTPViewDelegate {
    
    func mainButtonTapped() {
        mainButtondelegate?.mainButtonTapped()
    }
    
    func faceIdTapped() {
        faceIDdelegate?.faceIdTapped()
    }
    
    func returnButtonTapped() {
        returnButtondelegate?.returnButtonTapped()
    }
    
    func dpOTPViewAddText(_ text: String, at position: Int) {
        delegate?.dpOTPViewAddText(text, position: position)
    }
    
    func dpOTPViewRemoveText(_ text: String, at position: Int) {
        delegate?.dpOTPViewRemoveText(text, position: position)
    }
    
    func dpOTPViewChangePositionAt(_ position: Int) {
        delegate?.dpOTPViewChangePositionAt(position)
    }
    
    func dpOTPViewBecomeFirstResponder() {
        delegate?.dpOTPViewBecomeFirstResponder()
    }
    
    func dpOTPViewResignFirstResponder() {
        delegate?.dpOTPViewResignFirstResponder()
    }
    
    
}

private extension CUOTPView {
    // Общий инициализатор
    func commonInit() {
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        addSubview(otpView)
        addSubview(errorTitleLabel)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        otpView.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
            make.height.equalTo(80)
        }
        
        errorTitleLabel.snp.makeConstraints { make in
            make.top.equalTo(otpView.snp.bottom)
            make.leading.trailing.equalToSuperview()
            errorTitleHeightConstraint = make.height.equalTo(0).constraint
        }
    }
}
