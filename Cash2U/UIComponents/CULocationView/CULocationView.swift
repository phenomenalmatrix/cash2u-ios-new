//
//  CULocationView.swift
//  Cash2U
//
//  Created talgar osmonov on 22/8/22.
//


import UIKit

final class CULocationView: UICollectionViewCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var selectCity: CUSelectedCityView = {
        let view = CUSelectedCityView()
        view.setup(title: "Бишкек")
        view.cornerRadius = 44 / 2
        return view
    }()
    
    private lazy var switchLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Рядом")
        return view
    }()
    
    private lazy var switchToggle: UISwitch = {
        let view = UISwitch()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isOn = UserDefaultsService.shared.hasFaceAndTouchPermission
        view.addTarget(self, action: #selector(onSwitch(_:)), for: .valueChanged)
        view.onTintColor = .mainBlueColor
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
    
    // MARK: - UIActions
    
    @objc private func onSwitch(_ sender: UISwitch) {
//        UserDefaultsService.shared.hasFaceAndTouchPermission = sender.isOn
        print("Switch")
    }
}

extension CULocationView {
    /// Установка
    func setup(type: Int) {
        if type == 1 {
            switchToggle.isHidden = false
            switchLabel.isHidden = false
        } else {
            switchToggle.isHidden = true
            switchLabel.isHidden = true
        }
    }
}

private extension CULocationView {
    // Общий инициализатор
    func commonInit() {
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(selectCity)
        contentView.addSubview(switchToggle)
        contentView.addSubview(switchLabel)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        selectCity.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.centerY.equalToSuperview()
            make.width.equalTo(200)
            make.height.equalTo(44)
        }
        
        switchToggle.snp.makeConstraints { make in
            make.trailing.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        
        switchLabel.snp.makeConstraints { make in
            make.trailing.equalTo(switchToggle.snp.leading).offset(-10)
            make.centerY.equalToSuperview()
        }
    }
}
