//
//  CUPartnerPageButtonViewCell.swift
//  Cash2U
//
//  Created talgar osmonov on 29/8/22.
//


import UIKit

final class CUPartnerPageButtonViewCell: CUCollectionViewTapAnimationCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private var model: CUPartnerPageButtonModel?
    
    private lazy var icon: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        view.setDimensions(height: 20, width: 20)
        return view
    }()
    
    private lazy var title: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var stack: UIStackView = {
        let view = UIStackView(arrangedSubviews: [icon, title])
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .vertical
        view.distribution = .fill
        view.alignment = .fill
        view.spacing = 5
        return view
    }()
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                title.alpha = 1
                icon.alpha = 1
                title.setup(title: model?.name ?? "", size: 12, numberOfLines: 1, fontType: .regular, titleColor: .mainTextColor, alignment: .center)
                icon.image = UIImage(named: model?.icon ?? "")?.withTintColor(.mainTextColor ?? .white, renderingMode: .alwaysOriginal)
            } else {
                title.alpha = 0.6
                icon.alpha = 0.6
                title.setup(title: model?.name ?? "", size: 12 , numberOfLines: 1, fontType: .regular, titleColor: .grayColor, alignment: .center)
                icon.image = UIImage(named: model?.icon ?? "")?.withTintColor(.grayColor ?? .white, renderingMode: .alwaysOriginal)
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUPartnerPageButtonViewCell {
    /// Установка
    func setup(model: CUPartnerPageButtonModel) {
        self.model = model
        
        if model.isSelected {
            title.alpha = 1
            icon.alpha = 1
            title.setup(title: model.name, size: 12, numberOfLines: 1, fontType: .regular, titleColor: .mainTextColor, alignment: .center)
            icon.image = UIImage(named: model.icon)?.withTintColor(.mainTextColor ?? .white, renderingMode: .alwaysOriginal)
        } else {
            title.alpha = 0.6
            icon.alpha = 0.6
            title.setup(title: model.name, size: 12, numberOfLines: 1, fontType: .regular, titleColor: .grayColor, alignment: .center)
            icon.image = UIImage(named: model.icon)?.withTintColor(.grayColor ?? .white, renderingMode: .alwaysOriginal)
        }
    }
}

private extension CUPartnerPageButtonViewCell {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .thirdaryColor
        cornerRadius = 18
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(stack)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        stack.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.leading.trailing.equalToSuperview()
        }
    }
}
