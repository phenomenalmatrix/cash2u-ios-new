//
//  CUPartnerPageButtonView.swift
//  Cash2U
//
//  Created talgar osmonov on 29/8/22.
//


import UIKit
import Atributika

enum CUPartnerPageButtonType: String {
    case info = "Info"
    case goods = "Goods"
    case reviews = "Reviews"
    case contacts = "Contacts"
    case purchases = "Purchases"
    case repayment = "Repayment"
    case history = "History"
}

struct CUPartnerPageButtonModel {
    let type: CUPartnerPageButtonType
    let name: String
    let icon: String
    var isSelected: Bool
}

protocol CUPartnerPageButtonViewDelegate: AnyObject {
    func didSelect(type: CUPartnerPageButtonType)
}

final class CUPartnerPageButtonView: UICollectionViewCell {
    
    private var isConstraintsInstalled: Bool = false
    
    weak var delegate: CUPartnerPageButtonViewDelegate? = nil
    
    private var tabsHeight: CGFloat?
    
    private var data: [CUPartnerPageButtonModel] = []
    
    private var partnersPageData = [
        CUPartnerPageButtonModel(type: .info, name: "Инфо", icon: "info_icon", isSelected: true),
        CUPartnerPageButtonModel(type: .goods, name: "Товары", icon: "goods_icon", isSelected: false),
        CUPartnerPageButtonModel(type: .reviews, name: "Отзывы", icon: "reviews_icon", isSelected: false),
        CUPartnerPageButtonModel(type: .contacts, name: "Контакты", icon: "contacts_icon", isSelected: false)
    ]
    
    private var installmentData = [
        CUPartnerPageButtonModel(type: .purchases, name: "Покупки", icon: "info_icon", isSelected: true),
        CUPartnerPageButtonModel(type: .repayment, name: "Погашения", icon: "goods_icon", isSelected: false),
        CUPartnerPageButtonModel(type: .history, name: "История", icon: "reviews_icon", isSelected: false)
    ]
    
    private lazy var mainCollection: CUCollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        let view = CUCollectionView(
            frame: CGRect.zero,
            collectionViewLayout: flowLayout
        )
        view.translatesAutoresizingMaskIntoConstraints = false
        view.delegate = self
        view.dataSource = self
        view.decelerationRate = .normal
        view.alwaysBounceHorizontal = false
        view.isScrollEnabled = false
        view.register(CUPartnerPageButtonViewCell.self)
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUPartnerPageButtonView {
    /// Установка
    func setup(type: Int = 0, tabsHeight: CGFloat? = nil) {
        
        if let tabsHeight {
            self.tabsHeight = tabsHeight
        }
        
        if type == 0 {
            data = partnersPageData
        } else if type == 1 {
            data = installmentData
        }
        mainCollection.reloadData()
    }
}

extension CUPartnerPageButtonView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didSelect(type: data[indexPath.row].type)
        DispatchQueue.main.async {
            let generator = UIImpactFeedbackGenerator(style: .medium)
            generator.impactOccurred()
        }
        data[indexPath.row].isSelected = true
        for i in data.indices {
            if data[i].name != data[indexPath.row].name {
                data[i].isSelected = false
            }
        }
        partnersPageData = data
        installmentData = data
        mainCollection.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(cellType: CUPartnerPageButtonViewCell.self, for: indexPath)
        cell.setup(model: data[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if let tabsHeight {
            let itemPerRow: CGFloat = CGFloat(data.count)
            let paddingWidth = 20 * (itemPerRow + 1) - 60
            let awailableWidth = collectionView.frame.width - paddingWidth
            let widthPerItem = awailableWidth / itemPerRow
            return CGSize(width: widthPerItem, height: tabsHeight)
        } else {
            let itemPerRow: CGFloat = CGFloat(data.count)
            let paddingWidth = 20 * (itemPerRow + 1) - 70
            let awailableWidth = collectionView.frame.width - paddingWidth
            let widthPerItem = awailableWidth / itemPerRow
            return CGSize(width: widthPerItem, height: widthPerItem)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
}

private extension CUPartnerPageButtonView {
    // Общий инициализатор
    func commonInit() {
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(mainCollection)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        mainCollection.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
