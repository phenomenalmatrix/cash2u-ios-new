//
//  CUOffersAndPromotionsView.swift
//  Cash2U
//
//  Created talgar osmonov on 4/8/22.
//


import UIKit

struct OffersTest {
    var data = [1,1,1,1,1,1,1,1]
    var loading = false
}

final class CUOffersAndPromotionsView: UICollectionViewCell {
    
    private var isConstraintsInstalled: Bool = false
    
    var data = OffersTest()
    
    private lazy var mainCollection: CUCollectionView = {
        let flowLayout = SnappingCollectionViewLayout()
        flowLayout.scrollDirection = .horizontal
        let view = CUCollectionView(
            frame: CGRect.zero,
            collectionViewLayout: flowLayout
        )
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentInset = .init(top: 0, left: 20, bottom: 0, right: 20)
        view.delegate = self
        view.dataSource = self
        view.decelerationRate = .fast
        view.alwaysBounceHorizontal = true
        view.register(CUOffersAndPromotionsCell.self)
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUOffersAndPromotionsView {
    /// Установка
    func setup(isLoading: Bool = false) {
        data.loading = isLoading
        mainCollection.reloadData()
    }
}

extension CUOffersAndPromotionsView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(cellType: CUOffersAndPromotionsCell.self, for: indexPath)
        self.isUserInteractionEnabled = !data.loading
        cell.setup(isLoading: data.loading)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width ) - 40, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
}

private extension CUOffersAndPromotionsView {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .mainColor
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(mainCollection)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        mainCollection.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
