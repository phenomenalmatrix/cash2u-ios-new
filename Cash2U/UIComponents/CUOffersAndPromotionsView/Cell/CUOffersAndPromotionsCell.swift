//
//  CUOffersAndPromotionsCell.swift
//  Cash2U
//
//  Created talgar osmonov on 4/8/22.
//


import UIKit

final class CUOffersAndPromotionsCell: CUCollectionViewTapAnimationCell {
    
    private var isConstraintsInstalled: Bool = false
    
     private lazy var titleView: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(size: 18, numberOfLines: 1, fontType: .semibold, titleColor: .mainColor)
         view.isSkeletonable = true
        return view
    }()
    
    private lazy var subtitleView: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(size: 22, numberOfLines: 2, fontType: .bold, titleColor: .mainColor)
        view.isSkeletonable = true
        return view
    }()
    
    private lazy var image: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.cornerRadius = 50 / 2
        view.contentMode = .scaleAspectFill
        view.isSkeletonable = true
        return view
    }()
    
    private lazy var backView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isSkeletonable = true
        view.backgroundColor = .mainColor
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUOffersAndPromotionsCell {
    /// Установка
    func setup(isLoading: Bool = true) {
        if isLoading {
            self.backView.showSkeleton()
            self.titleView.showAnimatedGradientSkeleton()
            self.subtitleView.showAnimatedGradientSkeleton()
            self.image.showAnimatedGradientSkeleton()
        } else {
            self.backView.backgroundColor = .init(hex: "#FFECAA")
            self.image.setImage(with: "https://images.unsplash.com/photo-1659605199215-83f8b3a8b5a2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1365&q=80")
            self.titleView.changeText(title: "Zara")
            self.subtitleView.changeText(title: "Скидка 80% на все коллекции 2021")
            
            self.titleView.hideSkeleton()
            self.subtitleView.hideSkeleton()
            self.image.hideSkeleton()
            self.backView.hideSkeleton()
        }
    }
}

private extension CUOffersAndPromotionsCell {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .mainColor
        self.cornerRadius = 18
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(backView)
        contentView.addSubview(titleView)
        contentView.addSubview(subtitleView)
        contentView.addSubview(image)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        backView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        image.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.size.equalTo(50)
        }
        titleView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-80)
            make.top.equalToSuperview().offset(20)
        }
        
        subtitleView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-80)
            make.bottom.equalToSuperview().offset(-20)
        }
    }
}
