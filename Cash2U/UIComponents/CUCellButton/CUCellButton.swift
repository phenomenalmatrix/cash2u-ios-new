//
//  CUCellButton.swift
//  Cash2U
//
//  Created talgar osmonov on 5/10/22.
//


import UIKit

final class CUCellButton: CUCollectionViewTapAnimationCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var button: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isUserInteractionEnabled = false
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUCellButton {
    /// Установка
    func setup(title: String, titleSize: CGFloat = 17, fontType:  UIFont.Weight = .bold, titleColor: UIColor? = .mainTextColor, backgroundColor: UIColor? = .mainButtonColor, borderWidth: CGFloat = 0, borderColor: UIColor? = .clear, cornerRadius: CGFloat = 17) {
        button.setup(title: title, titleSize: titleSize, fontType: fontType, titleColor: titleColor, backgroundColor: backgroundColor, borderWidth: borderWidth, borderColor: borderColor, cornerRadius: cornerRadius)
    }
}

private extension CUCellButton {
    // Общий инициализатор
    func commonInit() {
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(button)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        button.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
