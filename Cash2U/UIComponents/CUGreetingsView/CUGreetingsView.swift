//
//  CUChooseCityView.swift
//  Cash2U
//
//  Created talgar osmonov on 5/8/22.
//


import UIKit

protocol CUGreetingsViewDelegate: AnyObject {
    func settingsTapped()
}

final class CUGreetingsView: UICollectionViewCell {
    
    private var isConstraintsInstalled: Bool = false
    
    weak var delegate: CUGreetingsViewDelegate? = nil
    
    private lazy var greetingView: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var settingsButton: CUIconButton = {
        let view = CUIconButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(image: UIImage(named: "settings_icon"), backgroundColor: .thirdaryColor, cornerRadius: 8)
        view.addTarget(self, action: #selector(onSettingsTapped), for: .touchUpInside)
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
    
    @objc private func onSettingsTapped() {
        delegate?.settingsTapped()
    }
}

extension CUGreetingsView {
    /// Установка
    func setup(title: String, type: Int) {
        self.settingsButton.isHidden = type == 0 ? true : false
        greetingView.setup(title: title, size: 25, fontType: .bold, isSizeToFit: true)
    }
}

private extension CUGreetingsView {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .mainColor
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubviews([greetingView, settingsButton])
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        greetingView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-60)
            make.top.bottom.equalToSuperview()
        }
        
        settingsButton.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-20)
            make.centerY.equalTo(greetingView)
            make.size.equalTo(40)
        }
    }
}
