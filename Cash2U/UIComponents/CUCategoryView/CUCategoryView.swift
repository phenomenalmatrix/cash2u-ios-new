//
//  CUCategoryView.swift
//  Cash2U
//
//  Created talgar osmonov on 5/9/22.
//


import UIKit

final class CUCategoryView: CUCollectionViewTapAnimationCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var gradient = UIView()
    
    private lazy var image: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFill
        view.image = UIImage(named: "hearts")
        return view
    }()
    
    private lazy var title: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Спец. оборудование", size: 16, numberOfLines: 2, fontType: .bold, isSizeToFit: true)
        return view
    }()
    
    private lazy var partnersCountLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "23 партнера", size: 12, numberOfLines: 2, fontType: .medium, alignment: .center, isSizeToFit: true)
        
        return view
    }()
    
    private lazy var bottomView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .mainDarkColor
        view.alpha = 0.4
        view.cornerRadius = 30 / 2
        view.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        self.gradient.createGradientBlurGray(color: UIColor.init(hex: "#1E1E1E"), gradientType: .up, alpha: 0.5)

    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
    
}

extension CUCategoryView {
    /// Установка
    func setup(model: CategoryModelItemIG) {
        backgroundColor = UIColor(hex: model.backgroundColor)
        title.changeText(title: model.name)
        partnersCountLabel.changeText(title: model.partnerCount.description)
        image.setImageWithAuth(with: model.logo)
    }
}

private extension CUCategoryView {
    // Общий инициализатор
    func commonInit() {
        cornerRadius = 18
        createViewHierarchy()
        setupConstraintsIfNeeded()
        layoutIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(image)
        contentView.addSubview(gradient)
        contentView.addSubview(title)
        contentView.addSubview(bottomView)
        contentView.addSubview(partnersCountLabel)

    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        image.snp.makeConstraints { make in
            make.trailing.equalToSuperview()
            make.top.equalToSuperview().offset(40)
            make.size.equalTo(60)
        }
        
        gradient.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.trailing.leading.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
        title.snp.makeConstraints { make in
            make.top.leading.equalToSuperview().offset(10)
            make.trailing.equalToSuperview().offset(-5)
        }
        
        bottomView.snp.makeConstraints { make in
            make.trailing.bottom.equalToSuperview()
            make.width.equalTo(partnersCountLabel.getWidth(withConstrainedHeight: 35) + 20)
            make.height.equalTo(30)
        }
        
        partnersCountLabel.snp.makeConstraints { make in
            make.edges.equalTo(bottomView).inset(1)
        }
    }
}
