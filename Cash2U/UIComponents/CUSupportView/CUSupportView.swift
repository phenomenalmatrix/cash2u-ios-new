//
//  CUSupportView.swift
//  Cash2U
//
//  Created talgar osmonov on 4/8/22.
//


import UIKit

final class CUSupportView: CUCollectionViewTapAnimationCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var title: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var subtitle: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var image: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "support_robot_dog")
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private lazy var background: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFill
        view.image = UIImage(named: "support_background")
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUSupportView {
    /// Установка
    func setup(title: String, subtitle: String, image: String, backgroundColor: UIColor? = .mainColor, supportBackground: Bool = false) {
        self.image.image = UIImage(named: image)
        self.title.setup(title: title, size: 24, numberOfLines: 1, fontType: .bold, titleColor: .white, isSizeToFit: true)
        self.subtitle.setup(title: subtitle, size: 16, numberOfLines: 3, fontType: .medium, titleColor: .white, isSizeToFit: true)
        self.backgroundColor = backgroundColor
        self.background.isHidden = !supportBackground
    }
}

private extension CUSupportView {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .mainColor
        self.cornerRadius = 18
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubviews([background, title, subtitle, image])
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        background.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        title.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(25)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-105)
        }
        
        subtitle.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-25)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-105)
        }
        
        image.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-10)
            make.centerY.equalToSuperview()
            make.height.equalToSuperview().dividedBy(1.4)
            make.width.equalTo(image.snp.height)
            
        }
    }
}
