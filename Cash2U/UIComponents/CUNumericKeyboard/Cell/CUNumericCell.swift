//
//  CUNumericCell.swift
//  Cash2U
//
//  Created talgar osmonov on 11/8/22.
//


import UIKit

final class CUNumericCell: CUCollectionViewTapAnimationCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var title: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var image: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUNumericCell {
    /// Установка
    func setup(item: CUNumericKeyboardModel) {
        self.isHidden = item.isHidden
        self.backgroundColor = item.backgroundColor
        if let image = item.image {
            self.image.image = UIImage(named: image)
        } else {
            self.title.setup(title: item.title, size: 22, alignment: .center)
        }
    }
}

private extension CUNumericCell {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .thirdaryColor
        self.cornerRadius = 5
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(title)
        contentView.addSubview(image)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        title.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(5)
        }
        
        image.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(22)
        }
    }
}
