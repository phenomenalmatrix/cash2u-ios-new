//
//  CUNumericKeyboard.swift
//  Cash2U
//
//  Created talgar osmonov on 11/8/22.
//

import UIKit
import SnapKit

// MARK: - UITextInput extension

extension UITextInput {
    var selectedRange: NSRange? {
        guard let textRange = selectedTextRange else { return nil }
        
        let location = offset(from: beginningOfDocument, to: textRange.start)
        let length = offset(from: textRange.start, to: textRange.end)
        return NSRange(location: location, length: length)
    }
}

enum CUNumericKeyboardType {
    case one, two ,three, four, five, six, seven, eight, nine, zero, delete, faceId
}

struct CUNumericKeyboardModel {
    let title: String
    let image: String?
    let backgroundColor: UIColor?
    let isHidden: Bool
    let type: CUNumericKeyboardType
}

protocol CUNumericKeyboardDelegate: AnyObject {
    func mainButtonTapped()
    func faceIdTapped()
}

open class CUNumericKeyboard: UIView {
    
    weak var delegate: CUNumericKeyboardDelegate? = nil
    
    weak var target: (UIKeyInput & UITextInput)? = nil
    
    var isMainButtonHidden = true {
        didSet {
            buttonConstraint?.update(offset: isMainButtonHidden ? 0 : ScreenHelper.mainButtonHeight)
        }
    }
    
    var isFaceIdHidden = true {
        didSet {
            mainCollection.reloadData()
        }
    }
    
    private var isConstraintsInstalled: Bool = false
    
    private var buttonConstraint: Constraint?
    
    private lazy var buttons = [
        CUNumericKeyboardModel(title: "1", image: nil, backgroundColor: .thirdaryColor, isHidden: false, type: .one),
        CUNumericKeyboardModel(title: "2", image: nil, backgroundColor: .thirdaryColor, isHidden: false, type: .two),
        CUNumericKeyboardModel(title: "3", image: nil, backgroundColor: .thirdaryColor, isHidden: false, type: .three),
        CUNumericKeyboardModel(title: "4", image: nil, backgroundColor: .thirdaryColor, isHidden: false, type: .four),
        CUNumericKeyboardModel(title: "5", image: nil, backgroundColor: .thirdaryColor, isHidden: false, type: .five),
        CUNumericKeyboardModel(title: "6", image: nil, backgroundColor: .thirdaryColor, isHidden: false, type: .six),
        CUNumericKeyboardModel(title: "7", image: nil, backgroundColor: .thirdaryColor, isHidden: false, type: .seven),
        CUNumericKeyboardModel(title: "8", image: nil, backgroundColor: .thirdaryColor, isHidden: false, type: .eight),
        CUNumericKeyboardModel(title: "9", image: nil, backgroundColor: .thirdaryColor, isHidden: false, type: .nine),
        CUNumericKeyboardModel(title: "", image: ScreenHelper.noFaceID ? "touch_id" : "face_id", backgroundColor: .clear, isHidden: isFaceIdHidden, type: .faceId),
        CUNumericKeyboardModel(title: "0", image: nil, backgroundColor: .thirdaryColor, isHidden: false, type: .zero),
        CUNumericKeyboardModel(title: "", image: "delete_icon", backgroundColor: .clear, isHidden: false, type: .delete)
    ]
    
    private lazy var mainCollection: CUCollectionView = {
        let layout = UICollectionViewFlowLayout()
        let view = CUCollectionView(frame: .zero, collectionViewLayout: layout)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isScrollEnabled = false
        view.backgroundColor = .secondaryColor
        view.delegate = self
        view.dataSource = self
        view.contentInset = .init(top: 8, left: 8, bottom: 8, right: 8)
        view.register(CUNumericCell.self)
        return view
    }()
    
    lazy var mainButton: CUMainButton = {
        let view = CUMainButton()
        view.setup(title: "Далее")
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(onButtonTapped), for: .touchUpInside)
        return view
    }()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
    
    @objc private func onButtonTapped() {
        delegate?.mainButtonTapped()
    }
    
    private func insertText(_ string: String) {
        guard let range = target?.selectedRange else { return }
        
        if let textField = target as? UITextField, textField.delegate?.textField?(textField, shouldChangeCharactersIn: range, replacementString: string) == false {
            return
        }
        
        if let textView = target as? UITextView, textView.delegate?.textView?(textView, shouldChangeTextIn: range, replacementText: string) == false {
            return
        }
        
        target?.insertText(string)
    }
}

extension CUNumericKeyboard {
    /// Установка
    func setup() {
        
    }
}

extension CUNumericKeyboard: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch self.buttons[indexPath.row].type {
        case .delete:
            self.target?.deleteBackward()
        case .faceId:
            self.delegate?.faceIdTapped()
        default:
            self.insertText(self.buttons[indexPath.row].title)
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return buttons.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(cellType: CUNumericCell.self, for: indexPath)
        cell.setup(item: buttons[indexPath.row])
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemPerRow: CGFloat = 3
        let paddingWidth = 14 * (itemPerRow + 1) - 26
        let awailableWidth = collectionView.frame.width - paddingWidth
        let widthPerItem = awailableWidth / itemPerRow
        return CGSize(width: widthPerItem, height: widthPerItem / 2.5)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 7
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 7
    }
    
}

private extension CUNumericKeyboard {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .secondaryColor
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }
    
    // Создать иерархию вью
    func createViewHierarchy() {
        addSubview(mainCollection)
        addSubview(mainButton)
    }
    
    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        mainCollection.snp.makeConstraints { make in
            make.leading.top.trailing.equalToSuperview()
            make.bottom.equalTo(safeArea.bottom).offset(-20)
        }
        
        mainButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.bottom.equalTo(safeArea.bottom).offset(-16)
            buttonConstraint = make.height.equalTo(ScreenHelper.mainButtonHeight).constraint
        }
        
    }
}
