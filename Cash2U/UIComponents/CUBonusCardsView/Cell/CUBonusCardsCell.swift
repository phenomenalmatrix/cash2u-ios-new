//
//  CUBonusCardsCell.swift
//  Cash2U
//
//  Created talgar osmonov on 4/8/22.
//


import UIKit

final class CUBonusCardsCell: CUCollectionViewTapAnimationCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var image: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFill
        view.isSkeletonable = true
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUBonusCardsCell {
    /// Установка
    func setup(isLoading: Bool = true) {
        if isLoading {
            self.image.showAnimatedGradientSkeleton()
        } else {
            self.image.image = UIImage(named: "frunze_test")
            self.image.hideSkeleton()
        }
    }
}

private extension CUBonusCardsCell {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .mainColor
        cornerRadius = 18
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(image)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        image.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
