//
//  CUIconButton.swift
//  Cash2U
//
//  Created talgar osmonov on 5/8/22.
//


import UIKit

final class CUIconButton: UIControl {
    
    private var isConstraintsInstalled: Bool = false
    
    private var animator = UIViewPropertyAnimator()
    
    private var insets: Int = 11
    
    private lazy var icon: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
    
    @objc private func touchDown() {
        animator = UIViewPropertyAnimator(duration: 0.4, dampingRatio: 1, animations: {
            self.alpha = 0.5
        })
        animator.startAnimation()
    }
    
    @objc private func touchUp() {
        animator = UIViewPropertyAnimator(duration: 0.4, dampingRatio: 1, animations: {
            self.alpha = 1
        })
        animator.startAnimation()
    }
}

extension CUIconButton {
    /// Установка
    func setup(image: UIImage?, backgroundColor: UIColor? = .mainButtonColor, borderWidth: CGFloat = 0, borderColor: UIColor? = .clear, cornerRadius: CGFloat = 0, insets: Int = 11) {
        self.insets = insets
        self.cornerRadius = cornerRadius
        icon.image = image?.withTintColor(.mainTextColor ?? .white, renderingMode: .alwaysOriginal)
        self.backgroundColor = backgroundColor
        
        setupConstraintsIfNeeded()
    }
}

private extension CUIconButton {
    // Общий инициализатор
    func commonInit() {
        
        clipsToBounds = true
        
        createViewHierarchy()
        
        addTarget(self, action: #selector(touchDown), for: [.touchDown, .touchDragEnter])
        addTarget(self, action: #selector(touchUp), for: [.touchUpInside, .touchDragExit, .touchCancel])
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        addSubview(icon)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        icon.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(insets)
        }
    }
}
