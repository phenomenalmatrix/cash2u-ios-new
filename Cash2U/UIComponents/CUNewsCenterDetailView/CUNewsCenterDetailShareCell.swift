//
//  CUNewsCenterDetailShareCell.swift
//  Cash2U
//
//  Created by Oroz on 27/9/22.
//

import Foundation
import UIKit

final class CUNewsCenterDetailShareCell: UICollectionViewCell {
    private var isConstraintsInstalled: Bool = false
    private var likeIsPressed = false
    
    private lazy var likeIcon: UIButton = {
        let view = UIButton(type: .system)
        view.setImage(UIImage(systemName: "hand.thumbsup"),for: .normal)
        view.addTarget(self, action: #selector(likePressEvent), for: .touchUpInside)
        return view
    }()
    
    private lazy var likeTitle: CULabelView = {
        let view = CULabelView()
        return view
    }()
    
    private lazy var shareIcon: UIButton = {
        let view = UIButton()
        view.isUserInteractionEnabled = false
        view.setImage(UIImage(systemName: "square.and.arrow.up"), for: .normal)
        return view
    }()
    
    private lazy var shareTitle: UIButton = {
        let view = UIButton(type: .system)
        view.setTitle("Поделиться", for: .normal)
        view.addTarget(self, action: #selector(shareEvent), for: .touchUpInside)
        return view
    }()
    
    private lazy var viewsIcon: UIImageView = {
        let view = UIImageView(image: UIImage(named: "View"))
        return view
    }()

    private lazy var viewsTitle: CULabelView = {
        let view = CULabelView()
        
        return view
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = .grayColor
        view.alpha = 0.3
       return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
    
}

extension CUNewsCenterDetailShareCell{
    /// Установка
    func setup() {
        likeTitle.setup(title: "11 784", size: 14, fontType: .medium, titleColor: .white)
        viewsTitle.setup(title: "11 784", size: 14, fontType: .medium, titleColor: .white)
    }
    
    @objc func shareEvent(){
        let data = ["Text, Image and url", UIImage(named: "frunze_test")!, "url"] as [Any]
        UIApplication.share(data)
    }
    
    @objc func likePressEvent(){
        likeIsPressed.toggle()
        likeIsPressed ? likeIcon.setImage(UIImage(systemName: "hand.thumbsup.fill"), for: .normal) : likeIcon.setImage(UIImage(systemName: "hand.thumbsup"), for: .normal)
    }
}

private extension CUNewsCenterDetailShareCell {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .mainColor
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubviews([likeIcon, likeTitle, viewsIcon, viewsTitle, shareTitle, shareIcon, lineView])
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        likeIcon.snp.makeConstraints { make in
            make.height.width.equalTo(22)
            make.leading.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        
        likeTitle.snp.makeConstraints { make in
            make.leading.equalTo(likeIcon.snp.trailing).offset(6)
            make.centerY.equalToSuperview().offset(2)
        }

        viewsIcon.snp.makeConstraints { make in
            make.height.width.equalTo(22)
            make.leading.equalTo(likeTitle.snp.trailing).offset(15)
            make.centerY.equalToSuperview()
        }

        viewsTitle.snp.makeConstraints { make in
            make.leading.equalTo(viewsIcon.snp.trailing).offset(6)
            make.centerY.equalToSuperview().offset(2)
        }

        shareTitle.snp.makeConstraints { make in
            make.trailing.equalToSuperview()
            make.centerY.equalToSuperview().offset(2)
        }

        shareIcon.snp.makeConstraints { make in
            make.height.width.equalTo(22)
            make.trailing.equalTo(shareTitle.snp.leading).offset(-6)
            make.centerY.equalToSuperview()
        }
        
        lineView.snp.makeConstraints { make in
            make.height.equalTo(1)
            make.width.equalToSuperview()
            make.bottom.equalToSuperview()
        }
   
    }
}
