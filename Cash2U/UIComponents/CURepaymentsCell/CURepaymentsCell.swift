//
//  CURepaymentsCell.swift
//  Cash2U
//
//  Created talgar osmonov on 4/10/22.
//

 
import UIKit

final class CURepaymentsCell: CUCollectionViewTapAnimationCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var backView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var dateLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "До 5-го мая:", size: 18, numberOfLines: 1, fontType: .medium)
        return view
    }()
    
    private lazy var totalAmountLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "1720,00 с", size: 22, numberOfLines: 1, fontType: .medium, alignment: .right, isSizeToFit: true)
        return view
    }()
    
    private lazy var contributedLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Уже внесено:", size: 14, numberOfLines: 1, fontType: .medium)
        return view
    }()
    
    private lazy var contributedAmountLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "0,00 с", size: 16, numberOfLines: 1, fontType: .medium, alignment: .right, isSizeToFit: true)
        return view
    }()
    
    private lazy var moreButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Подробнее", titleColor: .grayColor, backgroundColor: .secondaryColor, borderWidth: 1, borderColor: .grayColor, cornerRadius: 40 / 2)
        view.isUserInteractionEnabled = false
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CURepaymentsCell {
    /// Установка
    func setup(title: String, isExpired: Bool) {
        
        if isExpired {
            layer.borderWidth = 1
            layer.borderColor = UIColor.redColor?.cgColor
        } else {
            layer.borderWidth = 0
            layer.borderColor = nil
        }
        
        self.dateLabel.changeText(title: title)
    }

}

private extension CURepaymentsCell {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .secondaryColor
        cornerRadius = 18
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(dateLabel)
        contentView.addSubview(totalAmountLabel)
        
        contentView.addSubview(contributedLabel)
        contentView.addSubview(contributedAmountLabel)
        
        contentView.addSubview(moreButton)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        dateLabel.snp.makeConstraints { make in
            make.top.leading.equalToSuperview().offset(20)
        }
        
        totalAmountLabel.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-20)
            make.top.equalToSuperview().offset(18)
        }
        
        contributedLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.top.equalTo(dateLabel.snp.bottom).offset(20)
        }
        
        contributedAmountLabel.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-20)
            make.top.equalTo(totalAmountLabel.snp.bottom).offset(16)
        }
        
        moreButton.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-20)
            make.leading.trailing.equalToSuperview().inset(20)
            make.height.equalTo(40)
        }
    }
}
