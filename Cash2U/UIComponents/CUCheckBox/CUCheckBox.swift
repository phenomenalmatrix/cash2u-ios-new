//
//  CUCheckBox.swift
//  Cash2U
//
//  Created talgar osmonov on 10/8/22.
//


import UIKit

protocol CUCheckBoxDelegate: AnyObject {
    func isSelected(_ isSelected: Bool)
}

final class CUCheckBox: UIControl {
    
    weak var delegate: CUCheckBoxDelegate? = nil
    
    private var isConstraintsInstalled: Bool = false
    
    private var animator = UIViewPropertyAnimator()
    
    private lazy var checkmark: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "flat_checkmark")
        view.isHidden = !checkmarkIsSelected
        return view
    }()
    
    private lazy var backColor: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.cornerRadius = 4
        view.backgroundColor = .clear
        view.isUserInteractionEnabled = false
        return view
    }()
    
    var checkmarkIsSelected = false {
        didSet {
            checkmark.isHidden = !checkmarkIsSelected
            backColor.backgroundColor = !checkmarkIsSelected ? .clear : .mainTextColor
            delegate?.isSelected(checkmarkIsSelected)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
 
    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
    
    @objc private func touchDown() {
        animator = UIViewPropertyAnimator(duration: 0.4, dampingRatio: 1, animations: {
            self.transform = CGAffineTransform(scaleX: 0.96, y: 0.96)
            self.alpha = 0.5
        })
        animator.startAnimation()
    }
    
    @objc private func touchUp() {
        animator = UIViewPropertyAnimator(duration: 0.4, dampingRatio: 1, animations: {
            self.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.alpha = 1
            self.checkmarkIsSelected.toggle()
        })
        animator.startAnimation()
    }
}

extension CUCheckBox {
    /// Установка
    func setup() {
        
    }
}

private extension CUCheckBox {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .clear
        self.backColor.layer.borderWidth = 1
        self.backColor.layer.borderColor = UIColor.mainTextColor?.cgColor
        self.cornerRadius = 4
        createViewHierarchy()
        setupConstraintsIfNeeded()
        
        addTarget(self, action: #selector(touchDown), for: [.touchDown, .touchDragEnter])
        addTarget(self, action: #selector(touchUp), for: [.touchUpInside, .touchDragExit, .touchCancel])
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        addSubview(backColor)
        backColor.addSubview(checkmark)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        backColor.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(8)
        }
        checkmark.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(5)
        }
    }
}
