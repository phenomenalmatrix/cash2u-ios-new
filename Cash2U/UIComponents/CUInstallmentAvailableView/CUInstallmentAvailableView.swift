//
//  CUInstallmentAvailableView.swift
//  Cash2U
//
//  Created talgar osmonov on 29/8/22.
//


import UIKit

final class CUInstallmentAvailableView: UICollectionViewCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var title: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Доступна рассрочка", size: 16)
        return view
    }()
    
    private lazy var nol3: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "0/0/3", titleSize: 13, backgroundColor: .mainBlueColor, borderWidth: 0.5, borderColor: .mainTextColor, cornerRadius: 25 / 2)
        view.isUserInteractionEnabled = false
        view.setHeight(25)
        view.setWidth(55)
        return view
    }()
    
    private lazy var nol6: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "0/0/6", titleSize: 13, backgroundColor: .systemPurple, borderWidth: 0.5, borderColor: .mainTextColor, cornerRadius: 25 / 2)
        view.isUserInteractionEnabled = false
        view.setHeight(25)
        view.setWidth(55)
        return view
    }()
    
    private lazy var nolStack: UIStackView = {
        let view = UIStackView(arrangedSubviews: [nol3, nol6])
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .horizontal
        view.alignment = .fill
        view.distribution = .fill
        view.spacing = 8
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUInstallmentAvailableView {
    /// Установка
    func setup() {
        
    }
}

private extension CUInstallmentAvailableView {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .secondaryColor 
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(title)
        contentView.addSubview(nolStack)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        title.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.centerY.equalToSuperview()
        }
        
        nolStack.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-20)
            make.centerY.equalToSuperview()
        }
    }
}
