//
//  CUDoubleAmountLabelView.swift
//  Cash2U
//
//  Created Oroz on 4/10/22.
//


import UIKit

final class CUDoubleAmountLabelView: UIView {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var bonusTitleCount = UILabel()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUDoubleAmountLabelView {
    /// Установка
    /// Пример использования:
    func setup(summ: Double, colorForInteger: UIColor ,colorForTheRest: UIColor, fontForInteger: UIFont, fontForTheRemainder: UIFont, currencySymbol: String? = nil, currencySymbolFont: UIFont? = nil, currencySymbolColor: UIColor? = nil) {
        let doubleSumm = summ.description.components(separatedBy: ".")[1]
        let integerSumm = summ.description.components(separatedBy: ".")[0]
        let name = NSAttributedString(string: doubleSumm, attributes: [.foregroundColor: colorForTheRest, .font: fontForTheRemainder])
        let greeting = NSMutableAttributedString(string: "\(integerSumm),", attributes: [.foregroundColor: colorForInteger, .font: fontForInteger])
        greeting.append(name)
        if currencySymbol != nil {
            let allTextWithCurrency = NSMutableAttributedString(string: " ")
            let currency = NSAttributedString(string: currencySymbol ?? "", attributes: [.foregroundColor: currencySymbolColor ?? UIColor(), .font: currencySymbolFont ?? UIFont(), .underlineStyle: NSUnderlineStyle.single.rawValue])
            allTextWithCurrency.append(currency)
            greeting.append(allTextWithCurrency)
        }
        bonusTitleCount.attributedText = greeting
        
    }
}

private extension CUDoubleAmountLabelView {
    // Общий инициализатор
    func commonInit() {
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        addSubview(bonusTitleCount)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        bonusTitleCount.snp.makeConstraints { make in
            make.top.bottom.leading.trailing.equalToSuperview()
        }
    }
}
