//
//  CUSettingsPageCell.swift
//  Cash2U
//
//  Created talgar osmonov on 10/8/22.
//


import UIKit

protocol CUSettingsPageCellDelegate: AnyObject {
    func onActionTapped(indexPath: IndexPath)
}

final class CUSettingsPageCell: UITableViewCell {
    
    weak var delegate: CUSettingsPageCellDelegate? = nil
    
    private var isConstraintsInstalled: Bool = false
    
    var indexPath = IndexPath(row: 0, section: 0)
    
    private lazy var icon: CUIconButton = {
        let view = CUIconButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(onTapped), for: .touchUpInside)
        return view
    }()
    
    private lazy var title: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var background: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .secondaryColor
        view.cornerRadius = 14
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
    
    @objc private func onTapped() {
        delegate?.onActionTapped(indexPath: indexPath)
    }
}

extension CUSettingsPageCell {
    /// Установка
    func setup(title: String, image: String) {
        self.title.setup(title: title, size: 18, fontType: .semibold)
        self.icon.setup(image: UIImage(named: image), backgroundColor: .clear)
    }
}

private extension CUSettingsPageCell {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .clear
        selectionStyle = .none
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        addSubview(background)
        contentView.addSubview(icon)
        addSubview(title)
//        contentView.addSubview(moveImage)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        background.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(5)
            make.bottom.equalToSuperview().offset(-5)
            make.leading.trailing.equalToSuperview()
        }
        
        icon.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(10)
            make.size.equalTo(45)
            make.centerY.equalToSuperview()
        }
        
        title.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(60)
            make.trailing.equalToSuperview().offset(-60)
            make.top.bottom.equalToSuperview()
        }
        
//        moveImage.snp.makeConstraints { make in
//            make.trailing.equalToSuperview().offset(-10)
//            make.size.equalTo(45)
//            make.centerY.equalToSuperview()
//        }
    }
}
