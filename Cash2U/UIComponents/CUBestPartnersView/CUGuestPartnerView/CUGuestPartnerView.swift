//
//  CUGuestPartnerView.swift
//  Cash2U
//
//  Created talgar osmonov on 7/8/22.
//


import UIKit

final class CUGuestPartnerView: CUCollectionViewTapAnimationCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var title: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Партнеры", size: 24, numberOfLines: 1, fontType: .bold, titleColor: .mainTextColor)
        return view
    }()
    
    private lazy var subtitle: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Каталог магазинов и услуг", size: 14, numberOfLines: 1, fontType: .medium, titleColor: .grayColor)
        return view
    }()
    
    private lazy var partnerCount: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "1000+", titleSize: 13, titleColor: .mainTextColor, backgroundColor: .thirdaryColor, cornerRadius: 30 / 2)
        view.isUserInteractionEnabled = false
        return view
    }()
    
    private lazy var partnersCollection: CUCollectionView = {
        let layout = SnappingCollectionViewLayout()
        layout.scrollDirection = .horizontal
        let view = CUCollectionView(frame: .zero, collectionViewLayout: layout)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isScrollEnabled = false
        view.delegate = self
        view.dataSource = self
        view.alwaysBounceHorizontal = true
        view.register(CUGuestBestPartnersCell.self)
        view.backgroundColor = .secondaryColor
        view.isUserInteractionEnabled = false
        return view
    }()
    
    private var items: [PartnersItemModelIG]? = nil
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUGuestPartnerView {
    /// Установка
    func setup(items: [PartnersItemModelIG]) {
        self.items = items
        partnersCollection.reloadData()
    }
}

extension CUGuestPartnerView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(cellType: CUGuestBestPartnersCell.self, for: indexPath)
        cell.setup(item: items?[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemPerRow: CGFloat = 5
        let paddingWidth = 12 * (itemPerRow + 1) - 48
        let awailableWidth = collectionView.frame.width - paddingWidth
        let widthPerItem = awailableWidth / itemPerRow
        return CGSize(width: widthPerItem, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 6
    }
    
}

private extension CUGuestPartnerView {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .secondaryColor
        self.cornerRadius = 18
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubviews([title, subtitle])
        contentView.addSubview(partnerCount)
        contentView.addSubview(partnersCollection)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        title.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(20)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-105)
        }
        
        subtitle.snp.makeConstraints { make in
            make.top.equalTo(title.snp.bottom).offset(5)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-105)
        }
        
        partnerCount.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-20)
            make.centerY.equalTo(title)
            make.width.equalTo(70)
            make.height.equalTo(30)
        }
        
        partnersCollection.snp.makeConstraints { make in
            make.top.equalTo(subtitle.snp.bottom).offset(20)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.bottom.equalToSuperview().offset(-15)
        }
    }
}
