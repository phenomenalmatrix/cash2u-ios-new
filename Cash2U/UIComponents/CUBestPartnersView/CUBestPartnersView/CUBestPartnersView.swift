//
//  CUBestPartnersCell.swift
//  Cash2U
//
//  Created talgar osmonov on 4/8/22.
//


import UIKit

protocol CUBestPartnersViewDelegate: AnyObject {
    
}

final class CUBestPartnersView: UICollectionViewCell {
    
    weak var delegate: PartnerSelectionDelegate? = nil
    private var isConstraintsInstalled: Bool = false
    
    var data = OffersTest()
    
    private lazy var mainCollection: CUCollectionView = {
        let flowLayout = SnappingCollectionViewLayout()
        flowLayout.scrollDirection = .horizontal
        let view = CUCollectionView(
            frame: CGRect.zero,
            collectionViewLayout: flowLayout
        )
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentInset = .init(top: 0, left: 20, bottom: 0, right: 20)
        view.delegate = self
        view.dataSource = self
        view.decelerationRate = .fast
        view.alwaysBounceHorizontal = true
        view.register(CUBestPartnersCell.self)
        return view
    }()
    
    private var items: [PartnersItemModelIG]? = nil
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUBestPartnersView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let items {
            delegate?.didSelectPartner(partnerId: items[indexPath.row].id)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return data.data.count
        return items?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(cellType: CUBestPartnersCell.self, for: indexPath)
        self.isUserInteractionEnabled = !data.loading
        if let items {
            cell.setup(isLoading: data.loading, parter: items[indexPath.row])
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemPerRow: CGFloat = 4
        let paddingWidth = 20 * (itemPerRow + 1) - 30
        let awailableWidth = collectionView.frame.width - paddingWidth
        let widthPerItem = awailableWidth / itemPerRow
        return CGSize(width: widthPerItem, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
}

extension CUBestPartnersView {
    /// Установка
    func setup(isLoading: Bool = false, items: [PartnersItemModelIG]) {
        data.loading = isLoading
        self.items = items
        mainCollection.reloadData()
    }
}

private extension CUBestPartnersView {
    // Общий инициализатор
    func commonInit() {
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(mainCollection)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        mainCollection.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
