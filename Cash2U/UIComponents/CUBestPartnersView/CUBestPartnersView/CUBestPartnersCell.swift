//
//  CUBestPartnersCell.swift
//  Cash2U
//
//  Created talgar osmonov on 4/8/22.
//


import UIKit

final class CUBestPartnersCell: CUCollectionViewTapAnimationCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var title: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isSkeletonable = true
        return view
    }()
    
    private lazy var image: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFill
        view.cornerRadius = 12
        view.isSkeletonable = true
        return view
    }()
    
    private lazy var raitingView: CURaitingView = {
        let view = CURaitingView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isSkeletonable = true
        view.cornerRadius = 25 / 2
        view.setup()
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUBestPartnersCell {
    /// Установка
    func setup(isLoading: Bool = true,parter: PartnersItemModelIG) {
        if isLoading {
            self.image.showAnimatedGradientSkeleton()
            self.raitingView.showAnimatedGradientSkeleton()
        } else {
            self.title.setup(title: parter.name ?? "", size: 16, numberOfLines: 1, fontType: .regular, alignment: .center)
//            self.image.setImage(with: "https://images.unsplash.com/photo-1538805060514-97d9cc17730c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=987&q=80")
            self.image.setImageWithAuth(withLoader: true, with: parter.logo ?? "")
            self.image.hideSkeleton()
            self.raitingView.hideSkeleton()
        }
    }
}

private extension CUBestPartnersCell {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .clear
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubviews([image, title, raitingView])
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        image.snp.makeConstraints { make in
            make.leading.trailing.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(-55)
        }
        
        title.snp.makeConstraints { make in
            make.top.equalTo(image.snp.bottom).offset(8)
            make.leading.trailing.equalToSuperview()
        }
        
        raitingView.snp.makeConstraints { make in
            make.top.equalTo(title.snp.bottom).offset(0)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(25)
        }
    }
}
