//
//  CUDiscountCell.swift
//  Cash2U
//
//  Created talgar osmonov on 19/8/22.
//


import UIKit

final class CUDiscountCell: CUCollectionViewTapAnimationCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var gradient: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.4
        return view
    }()
    
    private lazy var logo: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFill
        view.cornerRadius = 45 / 2
        return view
    }()
    
    private lazy var title: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var image: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFill
        view.cornerRadius = 18
        return view
    }()
    
    private lazy var subtitle: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUDiscountCell {
    /// Установка
    func setup(items: DiscountItemsModelIG?) {
//        logo.setImage(with: "https://images.unsplash.com/photo-1542000551557-3fd0ad0eb15f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1471&q=80")
//        title.setup(title: "Young and Cool", size: 18, numberOfLines: 1, fontType: .bold)
//        image.setImage(with: "https://images.unsplash.com/photo-1528502668750-88ba58015b2f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80")
//        subtitle.setup(title: "🛍 Скидка 20% при оплате с помощью cash2u!🌟⭐️", numberOfLines: 2, fontType: .bold)
        if let items {
            logo.setImageWithAuth(with: items.partner?.logo ?? "")
            title.setup(title: items.title ?? "", size: 18, numberOfLines: 1, fontType: .bold)
            image.setImageWithAuth(with: items.cover ?? "")
        }
    }
}

private extension CUDiscountCell {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .mainColor
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(logo)
        contentView.addSubview(title)
        contentView.addSubview(image)
        contentView.addSubview(subtitle)
        image.addSubview(gradient)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        logo.snp.makeConstraints { make in
            make.top.leading.equalToSuperview()
            make.size.equalTo(45)
        }
        
        title.snp.makeConstraints { make in
            make.leading.equalTo(logo.snp.trailing).offset(16)
            make.trailing.equalToSuperview()
            make.centerY.equalTo(logo)
        }
        
        image.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(logo.snp.bottom).offset(16)
            make.bottom.equalToSuperview()
        }
        
        subtitle.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.bottom.equalToSuperview().offset(-15)
        }
        
        gradient.snp.makeConstraints { make in
            make.top.equalTo(subtitle.snp.top).offset(-20)
            make.leading.trailing.bottom.equalTo(image)
        }
        
        
    }
}
