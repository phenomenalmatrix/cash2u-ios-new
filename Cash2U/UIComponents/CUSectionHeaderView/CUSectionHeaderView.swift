//
//  CUSectionHeaderView.swift
//  Cash2U
//
//  Created talgar osmonov on 4/8/22.
//


import UIKit
import Atributika

protocol CUSectionHeaderViewDelegate: AnyObject {
    func viewAllTapped()
}


final class CUSectionHeaderView: UICollectionViewCell {
    
    private var isConstraintsInstalled: Bool = false
    
    weak var delegate: CUSectionHeaderViewDelegate? = nil
    
    private lazy var title: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isSkeletonable = true
        return view
    }()
    
    private lazy var rightArrow: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Все", titleSize: 14, titleColor: .mainTextColor, backgroundColor: .thirdaryColor, cornerRadius: 30 / 2)
        view.addTarget(self, action: #selector(onViewAllTapped), for: .touchUpInside)
        view.isSkeletonable = true
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
    
    @objc private func onViewAllTapped() {
        delegate?.viewAllTapped()
    }
}

extension CUSectionHeaderView {
    /// Установка
    func setup(titleText: String, titleSize: CGFloat = 24, isRightButtonHidden: Bool = false, isLoading: Bool = false) {
        self.rightArrow.isHidden = isRightButtonHidden
        if isLoading {
            self.title.showAnimatedGradientSkeleton()
            self.rightArrow.showAnimatedGradientSkeleton()
        } else {
            self.title.setup(title: titleText, size: titleSize, fontType: .bold)
            self.title.hideSkeleton()
            self.rightArrow.hideSkeleton()
        }
    }
}

private extension CUSectionHeaderView {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .mainColor
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(title)
        contentView.addSubview(rightArrow)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        title.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(20)
            make.top.bottom.equalToSuperview()
            make.trailing.equalToSuperview().offset(-70)
        }
        
        rightArrow.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-10)
            make.centerY.equalTo(title)
            make.width.equalTo(60)
            make.height.equalTo(30)
        }
    }
}
