//
//  CUSectionTitleHeaderView.swift
//  Cash2U
//
//  Created talgar osmonov on 6/10/22.
//


import UIKit

final class CUSectionTitleHeaderView: UICollectionViewCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var leftTitle: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isSkeletonable = true
        return view
    }()
    
    private lazy var rightTitle: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isSkeletonable = true
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUSectionTitleHeaderView {
    /// Установка
    func setup(
        leftTitleText: String,
        rightTitleText: String = "",
        leftTitleSize: CGFloat = 19,
        rightTitleSize: CGFloat = 19,
        leftTitleColor: UIColor? = .grayColor,
        rightTitleColor: UIColor? = .grayColor,
        isLoading: Bool = false) {
        if isLoading {
            self.leftTitle.showAnimatedGradientSkeleton()
            self.rightTitle.showAnimatedGradientSkeleton()
        } else {
            self.leftTitle.setup(title: leftTitleText, size: leftTitleSize, fontType: .semibold, titleColor: leftTitleColor)
            self.rightTitle.setup(title: rightTitleText, size: rightTitleSize, fontType: .semibold, titleColor: rightTitleColor)
            self.leftTitle.hideSkeleton()
            self.rightTitle.hideSkeleton()
        }
    }
}

private extension CUSectionTitleHeaderView {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .mainColor
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(leftTitle)
        contentView.addSubview(rightTitle)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        leftTitle.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(20)
            make.top.bottom.equalToSuperview()
            make.trailing.equalToSuperview().offset(-100)
        }
        
        rightTitle.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-20)
            make.top.bottom.equalToSuperview()
        }
    }
}
