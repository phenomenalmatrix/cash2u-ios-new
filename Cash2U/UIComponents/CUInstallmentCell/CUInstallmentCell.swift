//
//  CUInstallmentCell.swift
//  Cash2U
//
//  Created talgar osmonov on 7/10/22.
//


import UIKit

final class CUInstallmentCell: CUCollectionViewTapAnimationCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var titleLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var rightImage: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private lazy var bottomDescription: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var backView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFill
        return view
    }()
    
    private lazy var detailedButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Подробнее", titleSize: 16, fontType: .medium)
        view.isUserInteractionEnabled = false
        return view
    }()
    
    private lazy var connectedTitle: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Подключено", size: 15, numberOfLines: 1, fontType: .bold, isSizeToFit: true)
        return view
    }()
    
    private lazy var connectedImage: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "done_icon")
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUInstallmentCell {
    /// Установка
    func setup(type: InstallmentType, isConnected: Bool = false, isGuest: Bool = false) {
        detailedButton.isHidden = !isGuest
        connectedImage.isHidden = !isConnected
        connectedTitle.isHidden = !isConnected
        bottomDescription.isHidden = isConnected
        switch type {
        case .nol3:
            rightImage.image = UIImage(named: "nol_3_icon")
            backView.image = UIImage(named: "nol_3_gradient")
            titleLabel.setup(title: "0/0/3", size: 42, numberOfLines: 1, fontType: .medium, isSizeToFit: true)
            bottomDescription.setup(title: "0 процентов ・ 0 переплат ・ 3 месяцев", numberOfLines: 1, fontType: .medium, isSizeToFit: true)
            detailedButton.setup(title: "Подробнее", titleSize: 14, fontType: .medium, cornerRadius: 32 / 2)
        case .nol6:
            titleLabel.setup(title: "0/0/6", size: 42, numberOfLines: 1, fontType: .medium, isSizeToFit: true)
            rightImage.image = UIImage(named: "nol_6_icon")
            backView.image = UIImage(named: "nol_6_gradient")
            bottomDescription.setup(title: "0 процентов ・ 0 переплат ・ 6 месяцев", numberOfLines: 1, fontType: .medium, isSizeToFit: true)
            detailedButton.setup(title: "Подробнее", titleSize: 14, fontType: .medium, backgroundColor: .systemPurple, cornerRadius: 32 / 2)
        case .fuel:       
            titleLabel.setup(title: "Топливная\nкарта", size: 27, numberOfLines: 2, fontType: .semibold, isSizeToFit: true)
            rightImage.image = UIImage(named: "fuel_icon")
            backView.image = UIImage(named: "fuel_gradient")
            bottomDescription.setup(title: "Заправься сейчас ・ плати потом", numberOfLines: 2, fontType: .medium, isSizeToFit: true)
            detailedButton.setup(title: "Подробнее", titleSize: 14, fontType: .medium, cornerRadius: 32 / 2)
        case .product:
            titleLabel.setup(title: "Продуктовая\nкарта", size: 27, numberOfLines: 2, fontType: .semibold, isSizeToFit: true)
            rightImage.image = UIImage(named: "product_icon")
            backView.image = UIImage(named: "product_gradient")
            bottomDescription.setup(title: "Оплачивайте покупку продуктов частями", numberOfLines: 2, fontType: .medium, isSizeToFit: true)
            detailedButton.setup(title: "Подробнее", titleSize: 14, fontType: .medium, backgroundColor: .systemGreen, cornerRadius: 32 / 2)
        }
    }
}

private extension CUInstallmentCell {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .clear
        cornerRadius = 18
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(backView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(rightImage)
        contentView.addSubview(connectedImage)
        contentView.addSubview(connectedTitle)
        contentView.addSubview(bottomDescription)
        contentView.addSubview(detailedButton)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        backView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.width.equalToSuperview().dividedBy(2.1)
            make.top.equalToSuperview().offset(20)
        }
        
        rightImage.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-20)
            make.top.equalToSuperview().offset(20)
            make.height.equalToSuperview().dividedBy(1.6)
            make.width.equalTo(rightImage.snp.height)
        }
        
        connectedImage.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-20)
            make.top.equalTo(rightImage.snp.bottom).offset(3)
            make.size.equalTo(25)
        }
        
        connectedTitle.snp.makeConstraints { make in
            make.centerY.equalTo(connectedImage)
            make.trailing.equalTo(connectedImage.snp.leading).offset(-8)
            make.leading.equalTo(rightImage.snp.leading)
        }
        
        bottomDescription.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(20)
            make.bottom.equalToSuperview().offset(-16)
        }
        
        detailedButton.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(22)
            make.centerX.equalTo(rightImage)
            make.height.equalTo(32)
            make.width.equalTo(rightImage.snp.width)
        }
    }
}
