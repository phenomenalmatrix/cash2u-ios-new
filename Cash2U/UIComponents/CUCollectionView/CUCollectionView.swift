//
//  CUCollectionView.swift
//  Cash2U
//
//  Created talgar osmonov on 16/8/22.
//


import UIKit

class CUCollectionView: UICollectionView {

    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        self.delaysContentTouches = false
        self.backgroundColor = .mainColor
        self.showsHorizontalScrollIndicator = false
        self.showsVerticalScrollIndicator = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
