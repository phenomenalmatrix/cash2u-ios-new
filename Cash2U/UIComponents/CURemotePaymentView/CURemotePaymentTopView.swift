//
//  CURemotePaymentTopView.swift
//  Cash2U
//
//  Created talgar osmonov on 13/10/22.
//


import UIKit

final class CURemotePaymentTopView: UICollectionViewCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var icon: CUIconButton = {
        let view = CUIconButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(image: UIImage(named: "wallet_icon"), backgroundColor: .systemGreen, cornerRadius: 40 / 2, insets: 8)
        view.isUserInteractionEnabled = false
        return view
    }()
    
    private lazy var titleLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Деньги готовы для снятия", size: 20, numberOfLines: 1, fontType: .semibold, isSizeToFit: true)
        return view
    }()
    
    private lazy var dateLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "до: 15.08.2022", size: 16, numberOfLines: 1, fontType: .medium, titleColor: .grayColor, isSizeToFit: true)
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CURemotePaymentTopView {
    /// Установка
    func setup() {
        
    }
}

private extension CURemotePaymentTopView {
    // Общий инициализатор
    func commonInit() {
        cornerRadius = 18
        backgroundColor = .secondaryColor
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubviews([icon, titleLabel, dateLabel])
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        icon.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.centerY.equalToSuperview()
            make.size.equalTo(40)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview().offset(-11)
            make.leading.equalTo(icon.snp.trailing).offset(16)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        dateLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview().offset(11)
            make.leading.equalTo(icon.snp.trailing).offset(16)
            make.trailing.equalToSuperview().offset(-20)
        }
    }
}
