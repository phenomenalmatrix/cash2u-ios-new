//
//  CURemotePaymentView.swift
//  Cash2U
//
//  Created talgar osmonov on 13/10/22.
//


import UIKit

protocol CURemotePaymentViewDelegate: AnyObject {
    func onButtonTapped(type: Int)
}

final class CURemotePaymentView: UICollectionViewCell {
    
    weak var delegate: CURemotePaymentViewDelegate? = nil
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var titleLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "KROSSBOX", size: 22, numberOfLines: 1, fontType: .bold, isSizeToFit: true)
        return view
    }()
    
    private lazy var requestLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "запрашивает", size: 22, numberOfLines: 1, fontType: .bold, isSizeToFit: true)
        return view
    }()
    
    private lazy var amountLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "1850 с", size: 26, numberOfLines: 1, fontType: .semibold, isSizeToFit: true)
        return view
    }()
    
    private lazy var descriptionLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "👟 За кроссовки Puma Suede 43 размер", size: 15, numberOfLines: 2, fontType: .medium, titleColor: .grayColor, isSizeToFit: true)
        return view
    }()
    
    private lazy var imageView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFill
        view.cornerRadius = 80 / 2
        return view
    }()
    
    private lazy var sendButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Отправить")
        view.addTarget(self, action: #selector(onSendButtonTapped), for: .touchUpInside)
        return view
    }()
    
    private lazy var declineutton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Отклонить", backgroundColor: .thirdaryColor)
        view.addTarget(self, action: #selector(onDeclineButtonTapped), for: .touchUpInside)
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
    
    // MARK: - UIActions
    
    @objc private func onSendButtonTapped() {
        delegate?.onButtonTapped(type: 0)
    }
    
    @objc private func onDeclineButtonTapped() {
        delegate?.onButtonTapped(type: 1)
    }
}

extension CURemotePaymentView {
    /// Установка
    func setup() {
        imageView.setImage(with: "https://images.unsplash.com/photo-1528502668750-88ba58015b2f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80")
    }
}

private extension CURemotePaymentView {
    // Общий инициализатор
    func commonInit() {
        cornerRadius = 18
        backgroundColor = .secondaryColor
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubviews([titleLabel, requestLabel, amountLabel, descriptionLabel, imageView, sendButton, declineutton])
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        titleLabel.snp.makeConstraints { make in
            make.top.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-100)
        }
        
        requestLabel.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-100)
        }
        
        amountLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.top.equalTo(requestLabel.snp.bottom).offset(10)
            make.trailing.equalToSuperview().offset(-100)
        }
        
        descriptionLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.top.equalTo(amountLabel.snp.bottom).offset(10)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        imageView.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-20)
            make.top.equalToSuperview().offset(20)
            make.size.equalTo(80)
        }
        
        sendButton.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-20)
            make.leading.equalToSuperview().offset(20)
            make.height.equalTo(52)
            make.width.equalToSuperview().dividedBy(2.45)
        }
        
        declineutton.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-20)
            make.trailing.equalToSuperview().offset(-20)
            make.height.equalTo(52)
            make.width.equalToSuperview().dividedBy(2.45)
        }
    }
}
