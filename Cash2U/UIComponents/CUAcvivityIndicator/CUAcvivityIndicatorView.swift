//
//  CUAcvivityIndicatorView.swift
//  Cash2U
//
//  Created talgar osmonov on 26/7/22.
//


import UIKit
import JGProgressHUD

final class CUAcvivityIndicatorView: UIView {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var progressView: JGProgressHUD = {
        let view = JGProgressHUD()
        view.style = .dark
        view.backgroundColor = .clear
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
    
    func showLoader(_ isShown: Bool) {
        if isShown {
            progressView.show(in: self)
        } else {
            progressView.dismiss(animated: true)
        }
    }
}

extension CUAcvivityIndicatorView {
    /// Установка
    func setup() {
        
    }
}

private extension CUAcvivityIndicatorView {
    // Общий инициализатор
    func commonInit() {
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
    }
}
