//
//  CUReviewsView.swift
//  Cash2U
//
//  Created talgar osmonov on 31/8/22.
//


import UIKit

final class CUReviewsView: CUCollectionViewTapAnimationCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var backColor: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .secondaryColor
        view.cornerRadius = 18
        return view
    }()
    private lazy var raitingView: CURaitingView = {
        let view = CURaitingView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(raiting: "5.0", titleSize: 18, backColor: .clear)
        return view
    }()
    
    private lazy var title: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Бектемир Тойчуганов", size: 18, numberOfLines: 1, fontType: .medium)
        return view
    }()
    
    private lazy var subtitle: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "", size: 16, numberOfLines: 0, fontType: .regular, titleColor: .grayColor)
        return view
    }()
    
    private lazy var date: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "08.07", size: 14, numberOfLines: 1, fontType: .regular, titleColor: .grayColor)
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUReviewsView {
//    /// Установка
//    func setup(text: String) {
//        subtitle.changeText(title: text)
//    }
    /// Установка
    func setup(item: ReviewsItemModelIG) {
        subtitle.changeText(title: item.comment)
        raitingView.setup(raiting: "\(item.rate)", titleSize: 18, backColor: .clear)
        date.setup(title: "\(Date.getStringDate(stringDate: item.createDateTime, getFormat: .mmdd))", size: 14, numberOfLines: 1, fontType: .regular, titleColor: .grayColor)
    }
}

private extension CUReviewsView {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .clear
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(backColor)
        contentView.addSubview(raitingView)
        contentView.addSubview(title)
        contentView.addSubview(date)
        contentView.addSubview(subtitle)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        backColor.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalToSuperview().offset(5)
            make.bottom.equalToSuperview().offset(-5)
        }
        
        raitingView.snp.makeConstraints { make in
            make.leading.top.bottom.equalToSuperview()
            make.width.equalTo(70)
        }
        
        title.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(80)
            make.top.equalToSuperview().offset(18)
            make.trailing.equalToSuperview().offset(-55)
        }
        
        date.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(22)
            make.trailing.equalToSuperview().offset(-10)
        }
        
        subtitle.snp.makeConstraints { make in
            make.top.equalTo(title.snp.bottom).offset(5)
            make.leading.equalToSuperview().offset(80)
            make.trailing.equalToSuperview().offset(-20)
        }
    }
}
