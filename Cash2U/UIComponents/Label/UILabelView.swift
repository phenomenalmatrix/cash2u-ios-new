//
//  UITextView.swift
//  cash2u
//
//  Created by Eldar Akkozov on 4/4/22.
//

import Foundation
import UIKit

class UILabelView: UILabel {

    private var onClick: (UIView) -> Void = {_ in }
    
    private var isSetupClick = true
    
    init(_ text: String? = nil, font: UIFont, textColor: UIColor? = .mainTextColor ?? .darkText) {
        super.init(frame: .zero)
        
        self.text = text
        
        self.font = font
        self.numberOfLines = 0
        self.textColor = textColor
    }
    
    @objc func checkAction(sender: UITapGestureRecognizer) {
        self.onClick(self)
    }
    
    func onClickListener(onClick: @escaping (UIView) -> Void) {
        if (isSetupClick) {
            isSetupClick = false
     
            let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.checkAction))
            isUserInteractionEnabled = true
            addGestureRecognizer(gesture)
        }
        
        self.onClick = onClick
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

