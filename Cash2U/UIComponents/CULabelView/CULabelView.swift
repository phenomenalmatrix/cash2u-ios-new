//
//  CULabelView.swift
//  Cash2U
//
//  Created talgar osmonov on 5/8/22.
//


import UIKit
import Atributika
import TTTAttributedLabel

final class CULabelView: UIView {
    
    private var isConstraintsInstalled: Bool = false
    
    private var style: Style?
    
    private lazy var label: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isSkeletonable = true
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CULabelView {
    /// Установка
    func setup(title: String = "", size: CGFloat = 16, numberOfLines: Int = 0, fontType: UIFont.Weight = .regular, titleColor: UIColor? = .mainTextColor, alignment: NSTextAlignment = .left, lineHeight: CGFloat = 0, boldTitleColor: UIColor? = .mainDarkColor, isSizeToFit: Bool = false) {
        
        if isSizeToFit {
            self.label.sizeToFit()
            self.label.adjustsFontSizeToFitWidth = true
            self.label.minimumScaleFactor = 0.5
        }
        
        var attribitedTitle: NSAttributedString {
            let style = Style.mainTextStyle(
                size: size,
                fontType: fontType,
                color: titleColor,
                alignment: alignment,
                lineHeight: lineHeight,
                lineBreakMode: .byTruncatingTail
            )
            
            let boldStyle = Style.mainTextStyle(
                tag: "b",
                size: size,
                fontType: .bold,
                color: boldTitleColor,
                alignment: alignment,
                lineHeight: lineHeight,
                lineBreakMode: .byTruncatingTail
            )
            
            self.style = style
            return title.style(tags: [boldStyle]).styleAll(style).attributedString
        }
        self.label.numberOfLines = numberOfLines
        self.label.attributedText = attribitedTitle
    }
    
    func changeText(title: String) {
        if let style = style {
            self.label.attributedText = title.styleAll(style).attributedString
        }
    }
    
    func getWidth(withConstrainedHeight: CGFloat) -> CGFloat {
        guard let text = label.attributedText else {return 0}
        return text.getWidth(withConstrainedHeight: withConstrainedHeight)
    }
    
    func getHeight(withConstrainedWidth: CGFloat) -> CGFloat {
        guard let text = label.attributedText else {return 0}
        return text.getHeight(withConstrainedWidth: withConstrainedWidth)
    }
}

private extension CULabelView {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .clear
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }
    
    // Создать иерархию вью
    func createViewHierarchy() {
        addSubview(label)
    }
    
    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        label.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
