//
//  CUViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 20/5/22.
//

import Foundation
import UIKit

extension CUViewControllerDelegate {
    func didDismiss(action: String = "") {
        return didDismiss(action: action)
    }
}

protocol CUViewControllerDelegate: AnyObject {
    func didDismiss(action: String)
}

class CUViewController: UIViewController {
    
    weak var didDismissDelegate: CUViewControllerDelegate? = nil
    
    private var isVisible = false
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = .mainColor
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: String(), style: .plain, target: nil, action: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - UIActions
    
    @objc func onOpenQR() {
        let vc = PayQRModuleBuilder.build()
        self.navigationController?.presentPanModal(vc)
    }
    
    @objc func dismissKeyboard() {
        hideKeyboard()
    }
    
    @objc func keyBoardWillShow(notification: NSNotification) {
        isVisible = true
        showKeyBoard(height: (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.height)
    }
    
    
    @objc func keyBoardWillHide(notification: NSNotification) {
        isVisible = false
        hideKeyBoard(height: (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.height)
    }
    
    open func showKeyBoard(height: CGFloat) {}
    
    open func hideKeyBoard(height: CGFloat) {}
    
    func hideKeyboard(completion: (() -> Void)? = nil) {
        if isVisible {
            view.endEditing(true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                if let completion = completion {
                    completion()
                }
            }
        } else {
            if let completion = completion {
                completion()
            }
        }
    }
}
