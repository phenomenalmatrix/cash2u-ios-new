//
//  CUBellViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/8/22.
//

import UIKit

class CUBellViewController: CUViewController {

    override func loadView() {
        super.loadView()
        navigationItem.rightBarButtonItem = .init(image: UIImage(systemName: "bell"), style: .plain, target: self, action: #selector(onBellTapped))
    }
    
    // MARK: - UIActions
    
    @objc private func onBellTapped() {
        let vc = NewsCenterModuleConfigurator.build()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
