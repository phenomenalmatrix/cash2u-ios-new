//
//  CUInstallmentHistoryCell.swift
//  Cash2U
//
//  Created talgar osmonov on 5/10/22.
//


import UIKit

final class CUInstallmentHistoryCell: UICollectionViewCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var image: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "double_check_icon")
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private lazy var nameLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(size: 16, numberOfLines: 1, fontType: .medium)
        return view
    }()
    
    private lazy var amountLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(size: 16, numberOfLines: 1, fontType: .medium, titleColor: .systemGreen)
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUInstallmentHistoryCell {
    /// Установка
    func setup() {
        nameLabel.changeText(title: "Погашение")
        amountLabel.changeText(title: "2 200,00 c")
    }
}

private extension CUInstallmentHistoryCell {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .secondaryColor
        cornerRadius = 18
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(image)
        contentView.addSubview(nameLabel)
        contentView.addSubview(amountLabel)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        image.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.centerY.equalToSuperview()
            make.size.equalTo(45)
        }
        
        nameLabel.snp.makeConstraints { make in
            make.leading.equalTo(image.snp.trailing).offset(16)
            make.trailing.equalToSuperview().offset(60)
            make.centerY.equalToSuperview()
        }
        
        amountLabel.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-20)
            make.centerY.equalToSuperview()
        }
    }
}
