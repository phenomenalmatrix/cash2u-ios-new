//
//  CUHasReferralCodeView.swift
//  Cash2U
//
//  Created talgar osmonov on 27/7/22.
//


import UIKit

final class CUHasReferralCodeView: UIView {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var title: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 2
        view.text = "Есть ли у вас \nреферальный код?"
        view.textAlignment = .left
        view.textColor = UserDefaultsService.shared.isDarkMode ? .mainWhiteColor : .mainDarkColor
        view.font = .systemFont(ofSize: 20, weight: .bold)
        return view
    }()
    
    private lazy var image: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "stars")
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private lazy var confirmButton: CUMainButton = {
        let view = CUMainButton()
        view.setup(title: "Да", titleColor:  UserDefaultsService.shared.isDarkMode ? .mainWhiteColor : .mainDarkColor, backgroundColor: UserDefaultsService.shared.isDarkMode ? .mainBlueColor : .mainDarkColor)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(onConfirmButtonClick), for: .touchUpInside)
        return view
    }()
    
    private lazy var declineButton: CUMainButton = {
        let view = CUMainButton()
        view.setup(title: "Нет", titleColor: UserDefaultsService.shared.isDarkMode ? .mainWhiteColor : .mainDarkColor, backgroundColor: UserDefaultsService.shared.isDarkMode ? .inputDarkColor : .mainWhiteColor, borderWidth: 1, borderColor: .grayColor)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(onDeclineButtonClick), for: .touchUpInside)
        return view
    }()
    
    var confirmButtonClick: (() -> Void) = {}
    var declineButtonClick: (() -> Void) = {}
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
    
    // MARK: - UIActions
    
    @objc private func onConfirmButtonClick() {
        confirmButtonClick()
    }
    
    @objc private func onDeclineButtonClick() {
        declineButtonClick()
    }
}

extension CUHasReferralCodeView {
    /// Установка
    func setup() {
        
    }
}

private extension CUHasReferralCodeView {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = UserDefaultsService.shared.isDarkMode ? .mainSecondaryColor : .mainWhiteColor
        cornerRadius = 17
        clipsToBounds = true
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        addSubviews([title, image, confirmButton, declineButton])
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        title.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(20)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        image.snp.makeConstraints { make in
            make.top.equalTo(title.snp.bottom).offset(32)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(110)
        }
        
        confirmButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.bottom.equalToSuperview().offset(-20)
            make.height.equalTo(54)
        }
        
        declineButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.bottom.equalTo(confirmButton.snp.top).offset(-10)
            make.height.equalTo(54)
        }
    }
}
