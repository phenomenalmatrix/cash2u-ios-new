//
//  CURegistrationSuccessView.swift
//  Cash2U
//
//  Created talgar osmonov on 26/7/22.
//


import UIKit

final class CURegistrationSuccessView: UIView {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var image: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private lazy var title: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 2
        view.textAlignment = .center
        view.textColor = UserDefaultsService.shared.isDarkMode ? .mainWhiteColor : .mainDarkColor
        view.font = .systemFont(ofSize: 20, weight: .bold)
        return view
    }()
    
    private lazy var nextButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(onNextButtonClick), for: .touchUpInside)
        return view
    }()
    
    var nextButtonClick: (() -> Void) = {}
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
    
    // MARK: - UIActions
    
    @objc private func onNextButtonClick() {
        nextButtonClick()
    }
}

extension CURegistrationSuccessView {
    /// Установка
    func setup(title: String, image: UIImage? = UIImage(named: "check_mark"), buttonTitle: String = "Продолжить") {
        self.title.text = title
        self.image.image = image
        self.nextButton.setup(title: buttonTitle, titleColor: UserDefaultsService.shared.isDarkMode ? .mainWhiteColor : .mainDarkColor, backgroundColor: UserDefaultsService.shared.isDarkMode ? .mainBlueColor : .mainDarkColor)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.2) {
            self.nextButtonClick()
        }
    }
}

private extension CURegistrationSuccessView {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = UserDefaultsService.shared.isDarkMode ? .mainSecondaryColor : .mainWhiteColor
        cornerRadius = 17
        clipsToBounds = true
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        addSubview(image)
        addSubview(title)
//        addSubview(nextButton)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        image.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(32)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.height.equalTo(60)
        }
        
        title.snp.makeConstraints { make in
            make.top.equalTo(image.snp.bottom).offset(32)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
//        nextButton.snp.makeConstraints { make in
//            make.leading.equalToSuperview().offset(20)
//            make.trailing.equalToSuperview().offset(-20)
//            make.bottom.equalToSuperview().offset(-20)
//            make.height.equalTo(54)
//        }
    }
}
