//
//  CUWarningView.swift
//  Cash2U
//
//  Created Oroz on 7/8/22.
//


import UIKit

final class CUWarningView: UIView {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var titleView: CULabelView = {
        let view = CULabelView()
        return view
    }()
    
    private lazy var iconView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUWarningView {
    /// Установка
    func setup(title: String, image: String) {
        titleView.setup(title: title, size: 20, numberOfLines: 3, fontType: .bold, titleColor: UserDefaultsService.shared.isDarkMode ? .mainWhiteColor : .mainDarkColor, alignment: .center, isSizeToFit: true)
        self.iconView.image = UIImage(named: image)
    }
}

private extension CUWarningView {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .mainSecondaryColor
        cornerRadius = 18
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        addSubview(titleView)
        addSubview(iconView)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        iconView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(32)
            make.size.equalTo(64)
            make.centerX.equalToSuperview()
        }
        
        titleView.snp.makeConstraints { make in
            make.top.equalTo(iconView.snp.bottom).offset(20)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.bottom.equalToSuperview().offset(-32)
        }
    }
}
