//
//  CUFiltersCategoryCell.swift
//  Cash2U
//
//  Created talgar osmonov on 13/9/22.
//


import UIKit
import SnapKit

final class CUFiltersCategoryCell: CUTableViewTapAnimationCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private var titleViewLeadingConstraint: Constraint?
    
    private lazy var titleView: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(size: 16, numberOfLines: 1, fontType: .medium, titleColor: .mainTextColor, isSizeToFit: true)
        return view
    }()
    
    lazy var checkbox: CUCheckBox = {
        let view = CUCheckBox()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isUserInteractionEnabled = false
        return view
    }()
    
    lazy var icon: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "WhiteArrowRight")
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUFiltersCategoryCell {
    /// Установка
    func setup(item: TreeItem<OutlineItem>, isFirst: Bool = false, isLast: Bool = false) {
        
        var left = 20
        var textColor: UIColor? = .mainTextColor
        
        if item.level == 0 {
            left = 20
            textColor = .mainTextColor
        } else if item.level == 1 {
            left = 20
            textColor = .grayColor
        } else {
            left += 10 * item.level
            textColor = .grayColor
        }
        
        titleViewLeadingConstraint?.update(offset: left)
        
        if item.subitems.isEmpty {
            titleView.setup(title: item.value.title, size: 16, numberOfLines: 1, fontType: .semibold, titleColor: .grayColor, isSizeToFit: true)
            checkbox.isHidden = false
            icon.isHidden = true
            checkbox.checkmarkIsSelected = item.value.isSelected
        } else {
            titleView.setup(title: item.value.title, size: 16, numberOfLines: 1, fontType: .semibold, titleColor: textColor, isSizeToFit: true)
            checkbox.isHidden = true
            icon.isHidden = false
        }
        
        let transform = CGAffineTransform.init(rotationAngle: item.isExpanded ? CGFloat.pi / 2.0 : 0)
        icon.transform = transform
        
        if isFirst && !isLast {
            cornerRadius = 10
            layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
            return
        } else if isLast && !isFirst {
            cornerRadius = 10
            layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
            return
        } else if isLast && isFirst {
            cornerRadius = 10
            layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else {
            cornerRadius = 0
        }
    }
}

private extension CUFiltersCategoryCell {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .thirdaryColor
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(titleView)
        contentView.addSubview(checkbox)
        contentView.addSubview(icon)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        titleView.snp.makeConstraints { make in
            titleViewLeadingConstraint = make.leading.equalToSuperview().offset(20).constraint
            make.trailing.equalToSuperview().offset(-50)
            make.top.bottom.equalToSuperview()
        }
        
        checkbox.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-10)
            make.centerY.equalToSuperview()
            make.size.equalTo(35)
        }
        
        icon.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-20)
            make.centerY.equalToSuperview()
            make.size.equalTo(15)
        }
    }
}
