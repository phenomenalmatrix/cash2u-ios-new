//
//  CUStarsView.swift
//  Cash2U
//
//  Created talgar osmonov on 26/8/22.
//


import UIKit

final class CURaitingView: UIView {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var title: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "5.5(100)", size: 13, numberOfLines: 1, titleColor: .white)
        return view
    }()
    
    private lazy var star: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "star")
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CURaitingView {
    /// Установка
    func setup(raiting: String = "5.5(100)", titleSize: CGFloat = 13, backColor: UIColor? = .mainColor) {
        backgroundColor = backColor
        title.setup(title: raiting, size: titleSize, numberOfLines: 1, titleColor: .white)
    }
}

private extension CURaitingView {
    // Общий инициализатор
    func commonInit() {
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        addSubview(title)
        addSubview(star)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        title.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.centerX.equalToSuperview().offset(10)
        }
        
        star.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalTo(title.snp.leading).offset(-4)
            make.size.equalTo(15)
        }
    }
}
