//
//  CUButtonSectionView.swift
//  Cash2U
//
//  Created talgar osmonov on 7/8/22.
//


import UIKit

protocol CUButtonSectionViewDelegate: AnyObject {
    func buttonTapped()
}

final class CUButtonSectionView: UICollectionViewCell {
    
    private var isConstraintsInstalled: Bool = false
    
    weak var delegate: CUButtonSectionViewDelegate? = nil
    
    private lazy var button: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(onButtonTapped), for: .touchUpInside)
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
    
    @objc private func onButtonTapped() {
        delegate?.buttonTapped()
    }
}

extension CUButtonSectionView {
    /// Установка
    func setup(title: String) {
        button.setup(title: title)
    }
}

private extension CUButtonSectionView {
    // Общий инициализатор
    func commonInit() {
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(button)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        button.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
