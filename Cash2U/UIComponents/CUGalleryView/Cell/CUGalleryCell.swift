//
//  CUGalleryCell.swift
//  Cash2U
//
//  Created talgar osmonov on 30/8/22.
//


import UIKit

final class CUGalleryCell: CUCollectionViewTapAnimationCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var image: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFill
        view.cornerRadius = 18
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUGalleryCell {
    /// Установка
    func setup() {
        image.setImage(with: "https://images.unsplash.com/photo-1528502668750-88ba58015b2f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80")
    }
}

private extension CUGalleryCell {
    // Общий инициализатор
    func commonInit() {
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(image)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        image.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
