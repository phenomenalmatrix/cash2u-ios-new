//
//  UIViewPager.swift
//  cash2u
//
//  Created by Eldar Akkozov on 13/4/22.
//

import Foundation
import UIKit

class UIViewPager: BaseView {
    
    lazy var collection: UICollectionView = {
        let flow = UICollectionViewFlowLayout()
        flow.scrollDirection = .horizontal
        
        let view = UICollectionView(frame: .zero, collectionViewLayout: flow)
        view.delegate = self
        view.dataSource = self
        view.isScrollEnabled = false
        view.isPagingEnabled = true
        
        return view
    }()
    
    override func setupSubViews() {
        addSubview(collection)
        collection.snp.makeConstraints { make in
            make.top.left.right.bottom.equalToSuperview()
        }
    }
    
    private var count = 0
    
    private var viewHolders: (UICollectionView, IndexPath) -> UICollectionViewCell = { _, _ in return UICollectionViewCell() }
    
    func setViewHolders(_ viewHolders: @escaping (UICollectionView, IndexPath) -> UICollectionViewCell, countCell: Int) {
        self.viewHolders = viewHolders
        self.count = countCell
        
        collection.reloadData()

    }
    
    func scrollToPage(index: Int) {
        collection.setContentOffset(CGPoint(x: self.collection.frame.width * CGFloat(index), y: 0), animated: false)

        collection.cellForItem(at: IndexPath(row: index, section: 0))?.showHidden(hidden: false, withDuration: 0.2)
    }
}

extension UIViewPager: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width, height: frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return viewHolders(collectionView, indexPath)
    }
}
