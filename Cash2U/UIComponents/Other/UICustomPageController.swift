//
//  UICustomPageController.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation
import SnapKit

class UICustomPageController: BaseView {
    
    private lazy var stackView: UIStackView = {
       let view = UIStackView()
        view.axis = .vertical
        view.distribution = .fillProportionally
        view.spacing = 4
        return view
    }()
    
    private lazy var scrollView = UIScrollView()
    
    private lazy var containerScroll = UIView()
    
    private var pageSize = 0
    
    
    open var currentPage: (Int) -> Void = { _ in }
    
    override func setupView() {
        checkCurrentPage()
    }
    
    private func checkCurrentPage() {
        currentPage = { page in
            print("currentPage: \(self.stackView.arrangedSubviews.index(after: page))")
            let view = self.stackView.subviews[page]
            view.backgroundColor = .init(hex: "#21222B")
            self.checkColors(page: page)
            self.checkChangePosition(page: page)
        }
    }
    
    private func checkChangePosition(page: Int) {
        let height = (stackView.subviews[0].layer.frame.height + 4) * CGFloat(page)
        if (stackView.subviews.count > 3 && page >= 2 && page < (stackView.subviews.count - 2)) {
            scrollView.setContentOffset(CGPoint(x: 0, y: height), animated: true)
        }
        if (stackView.subviews.count > 3 && page <= 2) {
            scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }
    }
    
    private func checkColors(page: Int) {
        for i in 1...stackView.subviews.count {
            if(i-1 != page){
                print("i: \(i)")
                let view = self.stackView.subviews[i-1]
                view.backgroundColor = .init(hex: "#C19E00")
            }
        }
    }
    
    override func setupSubViews() {
        if pageSize > 3 {
            addSubview(scrollView)
            scrollView.snp.makeConstraints { make in
                make.bottom.equalToSuperview()
                make.right.left.equalToSuperview()
                make.top.equalToSuperview()
            }
            
            scrollView.addSubview(containerScroll)
            containerScroll.snp.makeConstraints { make in
                make.top.equalToSuperview()
                make.bottom.equalToSuperview()
                make.width.equalToSuperview()
            }
            
            containerScroll.addSubview(stackView)
            stackView.snp.makeConstraints { make in
                make.height.equalTo(150)
                make.width.equalTo(3)
                make.top.equalToSuperview()
                make.bottom.equalToSuperview()
                make.left.equalToSuperview()
                make.right.equalToSuperview()
            }
            
        } else {
            addSubview(stackView)
            stackView.snp.makeConstraints { make in
                make.height.equalTo(150)
                make.width.equalTo(3)
                make.top.equalToSuperview()
                make.bottom.equalToSuperview()
                make.left.equalToSuperview()
                make.right.equalToSuperview()
            }
        }
    }
    
    private func createDots(dotsName: String){
            let dotsName = UIView()
        dotsName.layer.cornerRadius = 3
        dotsName.backgroundColor = .black
        stackView.addArrangedSubview(dotsName)
    }
    
    init(pageSize: Int) {
        super.init(frame: .zero)
        self.pageSize = pageSize
        for i in 1...pageSize {
            createDots(dotsName: "dot\(i)")
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
