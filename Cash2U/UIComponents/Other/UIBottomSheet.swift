//
//  UIBottomSheet.swift
//  cash2u
//
//  Created by Eldar Akkozov on 4/4/22.
//

import Foundation
import UIKit

class UIBottomSheet: BaseView {
    
    lazy var view = BaseView()
    lazy var shadowView = BaseView()
    lazy var viewContaner = UIView()

    private lazy var imageBottomView = UIImageView(image: UIImage(named: "BottomSheetLine"))
    
    func show() {
        isHidden = false
        
        shadowView.showHidden(hidden: false, withDuration: 0.6)
        
        view.showUp()
        
    }
    
    func dismiss() {
        shadowView.showHidden(hidden: true, withDuration: 0.8)

//        view.showDown {
//            self.isHidden = true
//        }
    }
    
    func setController(controller: UIViewController) {
        controller.view.addSubview(self)
        snp.makeConstraints { make in
            make.left.right.bottom.top.equalToSuperview()
        }
        
        setupBottonSheet(controller)
    }
    
    func setupConstrants() { }
    
    func setupBottonSheet(_ controller: UIViewController) {
        view.clipsToBounds = false
        view.backgroundColor = .init(named: "BackgroundBottomSheet")
        view.layer.cornerRadius = 32
        view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        shadowView.backgroundColor = .init(hex: "#000000", alpha: 0.5)
        
        addSubview(shadowView)
        shadowView.snp.makeConstraints { make in
            make.top.left.right.bottom.equalToSuperview()
        }
        
        addSubview(view)
        view.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.right.left.equalToSuperview()
        }
        
        view.addSubview(viewContaner)
        viewContaner.snp.makeConstraints { make in
            make.top.right.left.equalToSuperview()
            make.bottom.equalTo(controller.view.safeArea.bottom)
        }
        
        viewContaner.addSubview(imageBottomView)
        imageBottomView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(8)
            make.centerX.equalToSuperview()
            make.width.equalTo(48)
            make.height.equalTo(8)
        }
        
        shadowView.onClickListener { view in self.dismiss() }

        setupConstrants()
        
        shadowView.isHidden = true
        view.isHidden = true
        isHidden = true
    }
}
