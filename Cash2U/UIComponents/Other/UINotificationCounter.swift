//
//  UINotificationCounter.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation
import UIKit

class UINotificationCounter: BaseView {
    
    private var notificationsCount = 453
    
    private lazy var labelForConter: UILabel = {
        let view = UILabel()
        view.text = String(notificationsCount)
        view.textColor = .white
        view.font = UIFont.systemFont(ofSize: 10, weight: .regular)
        return view
    }()
    
    private lazy var counter: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 5
        view.borderWidth = 1
        view.borderColor = .white
        view.backgroundColor = .red
        return view
    }()
    
    private lazy var bell: UIImageView = {
       let view = UIImageView()
        view.image = UIImage(named: "Bell")
        return view
    }()
    
    override func setupView() {
        addSubview(bell)
        bell.snp.makeConstraints { make in
            make.height.equalTo(20)
            make.width.equalTo(18)
            make.centerY.equalToSuperview()
            make.centerX.equalToSuperview()
        }
        
        addSubview(counter)
        counter.snp.makeConstraints { make in
            make.height.equalTo(11)
            make.width.equalTo(24)
            make.centerY.equalTo(bell.snp.top)
            make.centerX.equalTo(bell.snp.right)
        }

        counter.addSubview(labelForConter)
        labelForConter.snp.makeConstraints { make in
            make.centerY.centerX.equalToSuperview()
        }
    }
}
