//
//  UICheckBox.swift
//  cash2u
//
//  Created by Eldar Akkozov on 5/4/22.
//

import Foundation
import UIKit

protocol UICheckBoxDelegate: AnyObject {
    func isSelected(_ isSelected: Bool)
}

class UICheckBox: BaseView {
    
    weak var delegate: UICheckBoxDelegate? = nil
    
    private lazy var checkBoxImage = UIImageView()
    
    var isSelect = false
    
    private let switchOn: UIImage?
    private let switchOff: UIImage?
    
    init(switchOn: UIImage?, switchOff: UIImage?) {
        self.switchOn = switchOn
        self.switchOff = switchOff
        
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setupSubViews() {
        addSubview(checkBoxImage)
        checkBoxImage.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        checkBoxImage.image = isSelect ? switchOn : switchOff
    }
    
    override func setupView() {
        onClickListener { [self] view in
            if isSelect {
                unSelect()
            } else {
                select()
            }
            delegate?.isSelected(isSelect)
        }
    }
    
    func select() {
        isSelect = true
        
        checkBoxImage.image = switchOn
    }
    
    func unSelect() {
        isSelect = false
        
        checkBoxImage.image = switchOff
    }
}
