//
//  UITabsView.swift
//  cash2u
//
//  Created by Eldar Akkozov on 12/4/22.
//

import Foundation
import UIKit
import SnapKit

class UITabsView: BaseView {
    
    private lazy var homeTab = UITabView(title: "Главная", image: "TabMain", imageEnable: "TabMainEnable")
    private lazy var partnersTab = UITabView(title: "Партнеры", image: "TabPartners", imageEnable: "TabPartnersEnable")
    private lazy var moreTab = UITabView(title: "Еще", image: "TabMore", imageEnable: "TabMoreEnable")

    private lazy var separator: UIView = {
        let view = UIView()
        view.backgroundColor = .init(hex: "#B3B3B3")
        return view
    }()
    
    private var onTabClick: (Int) -> Void = {_ in }
    
    func onTabClickListener(onTabClick: @escaping (Int) -> Void) {
        self.onTabClick = onTabClick
    }
    
    override func setupView() {
        backgroundColor = .init(hex: "#FBFBFB")
        
        homeTab.onClickListener { [self] _ in
            disableAll()
            
            onTabClick(0)
            
            homeTab.enable()
        }
        
        partnersTab.onClickListener { [self] _ in
            disableAll()
            
            onTabClick(1)
            
            partnersTab.enable()
        }
        
        moreTab.onClickListener { [self] _ in
            disableAll()
            
            onTabClick(2)
            
            moreTab.enable()
        }
    }
    
    private func disableAll() {
        homeTab.disable()
        partnersTab.disable()
        moreTab.disable()
    }
    
    override func setupSubViews() {
        homeTab.enable()
        
        snp.makeConstraints { make in
            make.height.equalTo(56)
        }
        
        addSubview(homeTab)
        homeTab.snp.makeConstraints { make in
            make.top.bottom.left.equalToSuperview()
            make.width.equalToSuperview().dividedBy(3)
        }
        
        addSubview(partnersTab)
        partnersTab.snp.makeConstraints { make in
            make.left.equalTo(homeTab.snp.right)
            make.top.bottom.equalToSuperview()
            make.width.equalToSuperview().dividedBy(3)
        }
        
        addSubview(moreTab)
        moreTab.snp.makeConstraints { make in
            make.top.bottom.right.equalToSuperview()
            make.width.equalToSuperview().dividedBy(3)
        }
        
        addSubview(separator)
        separator.snp.makeConstraints { make in
            make.left.right.top.equalToSuperview()
            make.height.equalTo(1)
        }
    }
}

private class UITabView: BaseView {
    
    private lazy var imageView = UIImageView()
    private lazy var titleView = UILabelView(font: .systemFont(ofSize: 10), textColor: .init(hex: "#A3A7B1"))
    
    private lazy var imageCircle = UIImageView(image: UIImage(named: "TabCircle"))
    
    private var imageDisable: String
    private var imageEnable: String
    
    init(title: String, image: String, imageEnable: String) {
        self.imageDisable = image
        self.imageEnable = imageEnable
        
        super.init(frame: .zero)
        
        imageView.image = UIImage(named: image)
        titleView.text = title
        
        imageCircle.isHidden = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func enable() {
        showHidden(hidden: false)
        
        imageCircle.isHidden = false
        imageView.image = UIImage(named: imageEnable)

        titleView.textColor = .init(hex: "#000000")
    }
    
    func disable() {
        showHidden(hidden: false)

        imageCircle.isHidden = true
        imageView.image = UIImage(named: imageDisable)

        titleView.textColor = .init(hex: "#A3A7B1")
    }
    
    override func setupSubViews() {
        addSubview(imageCircle)
        addSubview(imageView)
        
        imageView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(8)
            make.width.height.equalTo(23)
        }
        
        imageCircle.snp.makeConstraints { make in
            make.width.height.equalTo(28)
            make.center.equalTo(imageView)
        }
        
        addSubview(titleView)
        titleView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-9)

        }
    }
}
