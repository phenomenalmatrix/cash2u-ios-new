//
//  CUPartnersHeaderView.swift
//  Cash2U
//
//  Created talgar osmonov on 20/5/22.
//


import UIKit

final class CUPartnersHeaderView: UICollectionReusableView {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var mallsTitle: CUTitleWithArrow = {
        let view = CUTitleWithArrow()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(titleText: "Malls AKSJh  IAUSHD iuh", isShowArrow: true)
        return view
    }()
    
    private lazy var catalogTitle: CUTitleWithArrow = {
        let view = CUTitleWithArrow()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(titleText: "Malls AKSJh  IAUSHD iuh", isShowArrow: false)
        return view
    }()
    
    private lazy var mallsCollectionView: CUCollectionView = {
        let flowLayout = SnappingCollectionViewLayout()
        flowLayout.scrollDirection = .horizontal
        
        let view = CUCollectionView(frame: .zero, collectionViewLayout: flowLayout)
        view.contentInset = .init(top: 0, left: 20, bottom: 0, right: 20)
        view.delegate = self
        view.dataSource = self
        view.decelerationRate = .fast
        view.alwaysBounceHorizontal = true
        view.translatesAutoresizingMaskIntoConstraints = false
        view.register(PartnersMallCell.self)
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUPartnersHeaderView {
    /// Установка
    func setup() {
        
    }
}

extension CUPartnersHeaderView: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return 5
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(cellType: PartnersMallCell.self, for: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width - 40, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}

private extension CUPartnersHeaderView {
    // Общий инициализатор
    func commonInit() {
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        addSubview(mallsTitle)
        addSubview(mallsCollectionView)
        addSubview(catalogTitle)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        mallsTitle.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-10)
            make.top.equalToSuperview()
        }
        
        mallsCollectionView.snp.makeConstraints { make in
            make.top.equalTo(mallsTitle.snp.bottom).offset(10)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview().offset(-60)
        }
        
        catalogTitle.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.bottom.equalToSuperview()
        }
    }
}
