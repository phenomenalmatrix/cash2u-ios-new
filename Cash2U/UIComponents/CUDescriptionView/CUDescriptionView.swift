//
//  CUDescriptionView.swift
//  Cash2U
//
//  Created talgar osmonov on 30/8/22.
//


import UIKit
import SnapKit

protocol CUDescriptionViewDelegate: AnyObject {
    func moreButtonTapped()
}

final class CUDescriptionView: UICollectionViewCell {
    
    private var isConstraintsInstalled: Bool = false
    
    weak var delegate: CUDescriptionViewDelegate? = nil
    
    private var moreButtonWidthConstraint: Constraint?
    
    private lazy var descriptionView: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "", size: 18, numberOfLines: 0, fontType: .regular)
        return view
    }()
    
    private lazy var moreButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "", titleSize: 18, fontType: .regular, titleColor: .grayColor, backgroundColor: .clear)
        view.addTarget(self, action: #selector(onMoreButtonTapped), for: .touchUpInside)
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
    
    @objc private func onMoreButtonTapped() {
        delegate?.moreButtonTapped()
    }
}

extension CUDescriptionView {
    /// Установка
    func setup(text: String, isExpand: Bool) {
        descriptionView.changeText(title: text)
        moreButtonWidthConstraint?.update(offset: isExpand ? 90 : 60)
        moreButton.changeText(text: isExpand ? "Скрыть" : "Еще")
    }
}

private extension CUDescriptionView {
    // Общий инициализатор
    func commonInit() {
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(descriptionView)
        contentView.addSubview(moreButton)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        descriptionView.snp.makeConstraints { make in
            make.leading.trailing.top.equalToSuperview()
            make.bottom.equalToSuperview().offset(-40)
        }
        
        moreButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(-15)
            make.bottom.equalToSuperview()
            make.height.equalTo(40)
            moreButtonWidthConstraint = make.width.equalTo(60).constraint
        }
    }
}
