//
//  CUCollectionViewCell.swift
//  Cash2U
//
//  Created talgar osmonov on 9/8/22.
//


import UIKit

class CUCollectionViewTapAnimationCell: UICollectionViewCell {
    
    private var animator = UIViewPropertyAnimator()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.isSkeletonable = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var isHighlighted: Bool {
        didSet {
            shrink(down: isHighlighted)
        }
    }

    func shrink(down: Bool) {
        
        animator = UIViewPropertyAnimator(duration: 0.3, dampingRatio: 1, animations: {
            if down {
                self.alpha = 0.5
            } else {
                self.alpha = 1
            }
        })
        animator.startAnimation()
    }
}

class CUTableViewTapAnimationCell: UITableViewCell {
    
    private var animator = UIViewPropertyAnimator()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.isSkeletonable = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var isHighlighted: Bool {
        didSet {
            shrink(down: isHighlighted)
        }
    }

    func shrink(down: Bool) {
        
        animator = UIViewPropertyAnimator(duration: 0.3, dampingRatio: 1, animations: {
            if down {
                self.alpha = 0.5
            } else {
                self.alpha = 1
            }
        })
        animator.startAnimation()
    }
}
