//
//  CUContactsTitleView.swift
//  Cash2U
//
//  Created talgar osmonov on 1/9/22.
//


import UIKit

final class CUContactsTitleView: UICollectionViewCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var title: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "", size: 22, numberOfLines: 3, fontType: .medium, isSizeToFit: true)
        return view
    }()
    
    private lazy var subtitle: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "", size: 18, numberOfLines: 3, fontType: .regular, titleColor: .grayColor, isSizeToFit: true)
        return view
    }()
    
    private lazy var toGis: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "2GIS", titleSize: 18, fontType: .bold, titleColor: .black, backgroundColor: .white, cornerRadius: 35 / 2)
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUContactsTitleView {
    /// Установка
    func setup(title: String, subtitle: String, isFirst: Bool = false, isLast: Bool = false) {
        self.title.changeText(title: title)
        self.subtitle.changeText(title: subtitle)
        if isFirst && !isLast {
            cornerRadius = 10
            layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
            return
        } else if isLast && !isFirst {
            cornerRadius = 10
            layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
            return
        } else {
            cornerRadius = 0
        }
    }
}

private extension CUContactsTitleView {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .secondaryColor
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(title)
        contentView.addSubview(subtitle)
        contentView.addSubview(toGis)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        
        title.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.top.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-80)
        }
        
        subtitle.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.top.equalTo(title.snp.bottom).offset(5)
            make.trailing.equalToSuperview().offset(-80)
        }
        
        toGis.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-20)
            make.top.equalToSuperview().offset(20)
            make.width.equalTo(70)
            make.height.equalTo(35)
        }
    }
}
