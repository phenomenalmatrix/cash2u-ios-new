//
//  CUBlankView.swift
//  Cash2U
//
//  Created talgar osmonov on 5/10/22.
//


import UIKit

final class CUBlankView: UICollectionViewCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var placeholderLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var textLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .grayColor
        view.alpha = 0.25
        return view
    }()
    
    private lazy var topLineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .grayColor
        view.alpha = 0.25
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUBlankView {
    /// Установка
    func setup(
        placeholder: String,
        text: String,
        placeholderSize: CGFloat = 14,
        textSize: CGFloat = 14,
        placeholderColor: UIColor? = .grayColor,
        textColor: UIColor? = .mainTextColor,
        isBottomLineHidden: Bool = false,
        isTopLineHidden: Bool = true
    ) {
        placeholderLabel.setup(title: placeholder, size: placeholderSize, numberOfLines: 1, fontType: .medium, titleColor: placeholderColor)
        textLabel.setup(title: text, size: textSize, numberOfLines: 2, fontType: .medium, titleColor: textColor, alignment: .right)
        lineView.isHidden = isBottomLineHidden
        topLineView.isHidden = isTopLineHidden
    }
}

private extension CUBlankView {
    // Общий инициализатор
    func commonInit() {
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(placeholderLabel)
        contentView.addSubview(textLabel)
        contentView.addSubview(lineView)
        contentView.addSubview(topLineView)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        placeholderLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        
        textLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(100)
            make.trailing.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        
        lineView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
            make.height.equalTo(1)
        }
        
        topLineView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalToSuperview()
            make.height.equalTo(1)
        }
    }
}
