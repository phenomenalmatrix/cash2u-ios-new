//
//  CUSelectedCityView.swift
//  Cash2U
//
//  Created talgar osmonov on 5/8/22.
//


import UIKit
import Atributika

final class CUSelectedCityView: UIControl {
    
    private var isConstraintsInstalled: Bool = false
    
    private var animator = UIViewPropertyAnimator()
    
    private lazy var icon: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "checkmark_pin")
        return view
    }()
    
    private lazy var title: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 2
        return view
    }()
    
    private lazy var downArrow: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "arrow_down")?.withTintColor(.mainTextColor ?? .white, renderingMode: .alwaysOriginal)
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
    
    @objc private func touchDown() {
        animator = UIViewPropertyAnimator(duration: 0.4, dampingRatio: 1, animations: {
            self.alpha = 0.5
        })
        animator.startAnimation()
    }
    
    @objc private func touchUp() {
        animator = UIViewPropertyAnimator(duration: 0.4, dampingRatio: 1, animations: {
            self.alpha = 1
        })
        animator.startAnimation()
    }
}

extension CUSelectedCityView {
    /// Установка
    func setup(title: String) {
        var attribitedTitle: NSAttributedString {
            let style = Style.mainTextStyle(
                size: 16,
                fontType: .medium,
                color: .mainTextColor,
                alignment: .left,
                lineBreakMode: .byTruncatingTail
            )
            return title.styleAll(style).attributedString
        }
        self.title.attributedText = attribitedTitle
    }
}

private extension CUSelectedCityView {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .secondaryColor
        clipsToBounds = true
        createViewHierarchy()
        setupConstraintsIfNeeded()
        
        addTarget(self, action: #selector(touchDown), for: [.touchDown, .touchDragEnter])
        addTarget(self, action: #selector(touchUp), for: [.touchUpInside, .touchDragExit, .touchCancel])
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        addSubviews([icon, title, downArrow])
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        icon.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.top.equalToSuperview().offset(8)
            make.bottom.equalToSuperview().offset(-8)
        }
        
        title.snp.makeConstraints { make in
            make.leading.equalTo(icon.snp.trailing).offset(16)
            make.top.equalToSuperview().offset(8)
            make.bottom.equalToSuperview().offset(-8)
        }
        
        downArrow.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-16)
            make.top.equalToSuperview().offset(8)
            make.bottom.equalToSuperview().offset(-8)
        }
    }
}
