//
//  CUDateSelectorView.swift
//  Cash2U
//
//  Created Oroz on 4/10/22.
//


import UIKit

final class CUDateSelectorView: UIView {
    
    private var isConstraintsInstalled: Bool = false
    
//    private lazy var container: UIView = {
//        let view = UIView()
//        view.cornerRadius = 8
//        view.borderWidth = 1
//        view.borderColor = UIColor.init(hex: "#2C3347")
//        return view
//    }()
    
    //    private lazy var dateTitle: CULabelView = {
    //        let view = CULabelView()
    //        view.setup(size: 17, fontType: .medium)
    //       return view
    //    }()
    
    lazy var dateTitle: UITextField = {
        let view = UITextField()
        view.cornerRadius = 8
        view.borderWidth = 1
        view.borderColor = UIColor.init(hex: "#2C3347")
        view.setLeftPaddingPoints(12)
        return view
    }()
    
    private let iconArrowDown = UIImageView(image: UIImage(named: "FilledArrowDown"))
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUDateSelectorView {
    /// Установка
    func setup(type: WichDataType, date: Date) {
        let dateForLabel = Date.getDateToString(date: date, getFormat: DateFormatterType.mmdd, isUTC: true)
        if type == WichDataType.FROM {
            dateTitle.text = "c \(dateForLabel)"
        } else {
            dateTitle.text =  "по \(dateForLabel)"
        }
    }
}

private extension CUDateSelectorView {
    // Общий инициализатор
    func commonInit() {
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
//        addSubview(container)
//        container.addSubviews(dateTitle)
//        container.addSubviews(iconArrowDown)
        addSubview(dateTitle)
        dateTitle.addSubviews(iconArrowDown)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
//        container.snp.makeConstraints { make in
//            make.top.bottom.leading.trailing.equalToSuperview()
//        }
        
//        dateTitle.snp.makeConstraints { make in
//            make.centerY.equalToSuperview()
//            make.leading.equalToSuperview().offset(10)
//        }
//
//        iconArrowDown.snp.makeConstraints { make in
//            make .centerY.equalToSuperview()
//            make.trailing.equalToSuperview().offset(-12)
//        }
        
        dateTitle.snp.makeConstraints { make in
            make.top.bottom.leading.trailing.equalToSuperview()
        }
        
        iconArrowDown.snp.makeConstraints { make in
            make .centerY.equalToSuperview()
            make.trailing.equalToSuperview().offset(-12)
        }
    }
}

enum WichDataType {
    case FROM
    case TO
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
