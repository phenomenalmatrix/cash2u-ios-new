//
//  CUTitleWithRow.swift
//  Cash2U
//
//  Created talgar osmonov on 20/5/22.
//


import UIKit
import Atributika

final class CUTitleWithArrow: UIView {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var title: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var rightArrow: CUIconButton = {
        let view = CUIconButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(image: UIImage(named: "WhiteArrowRight"), backgroundColor: .clear)
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUTitleWithArrow {
    /// Установка
    func setup(titleText: String, numberOfLines: Int = 2, isShowArrow: Bool) {
        self.title.setup(title: titleText, size: 22, numberOfLines: numberOfLines, fontType: .bold, isSizeToFit: true)
        self.rightArrow.isHidden = !isShowArrow
    }
}

extension CUTitleWithArrow {
    // Общий инициализатор
    func commonInit() {
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        addSubview(title)
        addSubview(rightArrow)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        title.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview().offset(-50)
            make.top.bottom.equalToSuperview()
        }
        
        rightArrow.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(1)
            make.centerY.equalTo(title)
            make.height.equalTo(38)
            make.width.equalTo(45)
        }
    }
}
