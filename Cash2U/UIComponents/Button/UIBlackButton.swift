//
//  UIBlackButton.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation
import SnapKit

class UIBlackButton: BaseView {

    private let titleView: UILabel = {
       let view = UILabel()
        view.textColor = .white
        return view
    }()

    override func setupView() {
        backgroundColor = .black
        layer.cornerRadius = 14
    }
    
    override func setupSubViews() {
        
        addSubview(titleView)
        titleView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
    }
    
    init(title: String) {
        super.init(frame: .zero)
        titleView.text = title
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
