//
//  UIChangebleButtonView.swift
//  cash2u
//
//  Created by jojo on 14/4/22.
//

import Foundation
import UIKit
import SnapKit

class UIChangebleButtonView: BaseView {
    
    private lazy var titleText = ""
    private lazy var titleSize = 0
    private lazy var titleColor = ""
    
    private lazy var corners = 0
    
    private lazy var bgColor = ""
    
    lazy var titleLabel = UILabelView(titleText, font: UIFont.systemFont(ofSize: CGFloat(titleSize), weight: .medium), textColor: .init(named: titleColor)!)

    override func setupView() {
        titleLabel.numberOfLines = 0
        backgroundColor = UIColor.init(named: bgColor)
        layer.cornerRadius = CGFloat(corners)
    }
    
    override func setupSubViews() {
        
        addSubview(titleLabel)
        titleLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.centerX.equalToSuperview()
        }
    }
    
    init(title: String,titleSize: Int, titleColor: String, corners: Int, bgColor: String) {
        super.init(frame: .zero)
        titleText = title
        self.titleColor = titleColor
        self.corners = corners
        self.bgColor = bgColor
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
