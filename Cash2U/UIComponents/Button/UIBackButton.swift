//
//  UIBackButton.swift
//  cash2u
//
//  Created by Eldar Akkozov on 6/4/22.
//

import Foundation
import UIKit

class UIBackButton: BaseView {
    
    private lazy var image = UIImageView(image: UIImage(named: "Back"))
    private lazy var title = UILabelView("Назад", font: .systemFont(ofSize: 15))
    
    override func setupSubViews() {
        addSubview(image)
        image.snp.makeConstraints { make in
            make.height.width.equalTo(24)
            make.top.bottom.left.equalToSuperview()
        }
        
        addSubview(title)
        title.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview()
            make.left.equalTo(image.snp.right).offset(4)
        }
    }
}
