//
//  UIBlueButton.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation
import SnapKit

class UIBlueButton: BaseView {

    private let titleView: UILabel = {
       let view = UILabel()
        view.textColor = .white
        view.font = UIFont.boldSystemFont(ofSize: 17)
        return view
    }()

    override func setupView() {
        backgroundColor = .blue
        layer.cornerRadius = 14
    }
    
    override func setupSubViews() {
        
        addSubview(titleView)
        titleView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
    }
    
    init(title: String) {
        super.init(frame: .zero)
        titleView.text = title
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
