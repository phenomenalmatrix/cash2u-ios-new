//
//  UIButtonView.swift
//  cash2u
//
//  Created by Eldar Akkozov on 4/4/22.
//

import Foundation
import UIKit

class UIButtonView: BaseView {
    
    lazy var titleLabel = UILabelView(font: UIFont.systemFont(ofSize: 17, weight: .bold), textColor: .init(named: "ButtonTtitleColor")!)
    
    init(
        _ title: String,
        _ font: UIFont = UIFont.systemFont(ofSize: 17, weight: .bold),
        _ cornerRadius: CGFloat = 16,
        _ backgroundColor: UIColor? = .mainButtonColor,
        _ textColor: UIColor? =  .white
    ) {
        super.init(frame: .zero)
    
        titleLabel.text = title
        titleLabel.textColor = textColor
        titleLabel.font = font
        self.backgroundColor = backgroundColor
        layer.cornerRadius = cornerRadius
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func onClickTouch() {  }
    
    override func setupSubViews() {
        addSubview(titleLabel)
        titleLabel.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        
        snp.makeConstraints { make in
            make.height.equalTo(64)
        }
    }
}
