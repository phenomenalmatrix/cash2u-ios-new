//
//  CUPaymentScheduleCell.swift
//  Cash2U
//
//  Created talgar osmonov on 5/10/22.
//


import UIKit

final class CUPaymentScheduleCell: UICollectionViewCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var amountLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(size: 28, numberOfLines: 1, fontType: .bold)
        return view
    }()
    
    private lazy var titleLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(size: 14, numberOfLines: 1, fontType: .medium, titleColor: .grayColor, alignment: .right)
        return view
    }()
    
    private lazy var dateLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(size: 14, numberOfLines: 1, fontType: .medium, titleColor: .grayColor, alignment: .right)
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUPaymentScheduleCell {
    /// Установка
    func setup() {
        titleLabel.changeText(title: "Платежи 2 из 3")
        dateLabel.changeText(title: "05 мая 2022")
        amountLabel.changeText(title: "733,00 c")
    }
}

private extension CUPaymentScheduleCell {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .thirdaryColor
        cornerRadius = 18
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(dateLabel)
        contentView.addSubview(amountLabel)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        titleLabel.snp.makeConstraints { make in
            make.leading.top.equalToSuperview().inset(20)
        }
        
        dateLabel.snp.makeConstraints({ make in
            make.trailing.equalToSuperview().offset(-20)
            make.top.equalToSuperview().offset(20)
        })
        
        amountLabel.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(10)
            make.leading.equalToSuperview().offset(20)
        }
    }
}
