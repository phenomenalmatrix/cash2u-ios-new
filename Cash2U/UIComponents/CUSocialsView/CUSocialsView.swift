//
//  CUSocialsView.swift
//  Cash2U
//
//  Created talgar osmonov on 1/9/22.
//


import UIKit

final class CUSocialsView: CUCollectionViewTapAnimationCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var icon: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "contacts_icon")
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private lazy var title: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "", size: 19, numberOfLines: 1, fontType: .regular, titleColor: .grayColor)
        return view
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .grayColor
        view.alpha = 0.3
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUSocialsView {
    /// Установка
    func setup(title: String, isFirst: Bool = false, isLast: Bool = false) {
        self.title.changeText(title: title)
        if isFirst && !isLast {
            cornerRadius = 10
            layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
            lineView.isHidden = false
            return
        } else if isLast && !isFirst {
            cornerRadius = 10
            layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
            lineView.isHidden = true
            return
        } else {
            layer.cornerRadius = 0
            lineView.isHidden = false
            
        }
    }
    
    
}

private extension CUSocialsView {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .secondaryColor
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubviews([icon, title, lineView])
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        icon.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.centerY.equalToSuperview()
            make.height.equalTo(20)
            make.width.equalTo(20)
        }
        
        title.snp.makeConstraints { make in
            make.leading.equalTo(icon.snp.trailing).offset(10)
            make.top.bottom.equalToSuperview()
            make.trailing.equalToSuperview().offset(-10)
        }
        
        lineView.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.height.equalTo(0.5)
        }
    }
}
