//
//  CUNewsCell.swift
//  Cash2U
//
//  Created talgar osmonov on 4/8/22.
//


import UIKit

final class CUNewsCell: CUCollectionViewTapAnimationCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var logo: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFill
        view.cornerRadius = 45 / 2
        return view
    }()
    
    private lazy var title: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var image: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFill
        view.cornerRadius = 18
        return view
    }()
    
    private lazy var subtitle: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUNewsCell {
    /// Установка
    func setup() {
        logo.setImage(with: "https://images.unsplash.com/photo-1659307773059-1a9a7d283e9b?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1015&q=80")
        title.setup(title: "cash2u・0 / 0 / 3", size: 18, numberOfLines: 1, fontType: .bold)
        image.setImage(with: "https://images.unsplash.com/photo-1659605199215-83f8b3a8b5a2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1365&q=80")
        subtitle.setup(title: "cash2u + TEAMS BARBERSHOP запускают грандиозную акцию рассчина", numberOfLines: 2, fontType: .bold)
    }
}

private extension CUNewsCell {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .mainColor
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubviews([logo, title, image, subtitle])
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        logo.snp.makeConstraints { make in
            make.top.leading.equalToSuperview()
            make.size.equalTo(45)
        }
        
        title.snp.makeConstraints { make in
            make.leading.equalTo(logo.snp.trailing).offset(16)
            make.trailing.equalToSuperview()
            make.centerY.equalTo(logo)
        }
        
        image.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(logo.snp.bottom).offset(16)
            make.bottom.equalToSuperview().offset(-50)
        }
        
        subtitle.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(image.snp.bottom).offset(10)
            make.bottom.equalToSuperview()
        }
    }
}
