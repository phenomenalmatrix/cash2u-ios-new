//
//  CUPartnerPageHeaderView.swift
//  Cash2U
//
//  Created talgar osmonov on 7/6/22.
//


import UIKit
import Atributika

final class CUPartnerPageHeaderView: UICollectionReusableView {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var mainCollectionView: CUCollectionView = {
        let flowLayout = SnappingCollectionViewLayout()
        flowLayout.scrollDirection = .horizontal
        
        let view = CUCollectionView(frame: .zero, collectionViewLayout: flowLayout)
        view.contentInset = .init(top: 0, left: 20, bottom: 0, right: 20)
        view.delegate = self
        view.dataSource = self
        view.decelerationRate = .fast
        view.alwaysBounceHorizontal = true
        view.translatesAutoresizingMaskIntoConstraints = false
        view.register(PartnersMallCell.self)
        return view
    }()
    
    private lazy var partnerImageView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = .init(named: "SwitchOn")
        view.cornerRadius = 55 / 2
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        return view
    }()
    private lazy var partnerNameLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 1
        return view
    }()
    
    private lazy var partnerCategoryLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 1
        return view
    }()
    
    private lazy var partnerDescriptionLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 2
        return view
    }()
    
    private lazy var partnerLabels: UIStackView = {
        let view = UIStackView(arrangedSubviews: [partnerNameLabel, partnerCategoryLabel])
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .vertical
        view.distribution = .fillEqually
        return view
    }()
    
    private lazy var pageSegment: UISegmentedControl = {
        let view = UISegmentedControl(items: ["Ассортимент", "Филиалы"])
        view.translatesAutoresizingMaskIntoConstraints = false
        view.selectedSegmentIndex = 0
        view.selectedSegmentTintColor = .black
        view.setTitleColor(.white, state: .selected)
        view.setTitleColor(.black, state: .normal)
        view.setTitleFont(.systemFont(ofSize: 16, weight: .medium), state: .selected)
        view.setTitleFont(.systemFont(ofSize: 14, weight: .medium), state: .normal)
        view.addTarget(self, action: #selector(selectedPage), for: .valueChanged)
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUPartnerPageHeaderView {
    /// Установка
    func setup(partnerName: String, partnerCategory: String, partnerDescription: String) {
        
        var attribitedNameText: NSAttributedString {
            let style = Style.mainTextStyle(
                size: 23,
                fontType: .bold,
                color: .black,
                alignment: .left,
                lineBreakMode: .byTruncatingTail
            )
            return partnerName.styleAll(style).attributedString
        }
        
        var attribitedCategoryText: NSAttributedString {
            let style = Style.mainTextStyle(
                size: 18,
                fontType: .medium,
                color: .lightGray,
                alignment: .left,
                lineBreakMode: .byTruncatingTail
            )
            return partnerCategory.styleAll(style).attributedString
        }
        
        var attribitedDescriptionText: NSAttributedString {
            let style = Style.mainTextStyle(
                size: 18,
                fontType: .regular,
                color: .black,
                alignment: .left,
                lineBreakMode: .byTruncatingTail
            )
            return partnerDescription.styleAll(style).attributedString
        }
        
        partnerNameLabel.attributedText = attribitedNameText
        partnerCategoryLabel.attributedText = attribitedCategoryText
        partnerDescriptionLabel.attributedText = attribitedDescriptionText
    }
    
    @objc private func selectedPage() {
        
    }
}

extension CUPartnerPageHeaderView: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return 5
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(cellType: PartnersMallCell.self, for: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width - 40, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}

private extension CUPartnerPageHeaderView {
    // Общий инициализатор
    func commonInit() {
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        
        addSubview(mainCollectionView)
        addSubview(partnerImageView)
        addSubview(partnerLabels)
        addSubview(partnerDescriptionLabel)
        addSubview(pageSegment)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        mainCollectionView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview().offset(-220)
        }
        
        partnerImageView.snp.makeConstraints { make in
            make.top.equalTo(mainCollectionView.snp.bottom).offset(20)
            make.leading.equalToSuperview().offset(20)
            make.size.equalTo(55)
        }
        
        partnerLabels.snp.makeConstraints { make in
            make.leading.equalTo(partnerImageView.snp.trailing).offset(12)
            make.trailing.equalToSuperview().offset(-20)
            make.top.equalTo(partnerImageView.snp.top)
            make.bottom.equalTo(partnerImageView.snp.bottom)
        }
        
        partnerDescriptionLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.top.equalTo(partnerImageView.snp.bottom).offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        pageSegment.snp.makeConstraints { make in
            make.top.equalTo(partnerDescriptionLabel.snp.bottom).offset(20)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.height.equalTo(45)
        }
    }
}
