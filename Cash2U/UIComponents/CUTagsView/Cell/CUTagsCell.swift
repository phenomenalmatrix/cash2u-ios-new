//
//  CUTagsView.swift
//  Cash2U
//
//  Created talgar osmonov on 24/8/22.
//


import UIKit

final class CUTagsCell: CUCollectionViewTapAnimationCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var title: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "", size: 18, numberOfLines: 1, fontType: .medium, titleColor: .grayColor, alignment: .center)
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUTagsCell {
    /// Установка
    func setup(text: String) {
        title.changeText(title: text)
    }
}

private extension CUTagsCell {
    // Общий инициализатор
    func commonInit() {
        cornerRadius = 35 / 2
        backgroundColor = .clear
        borderColor = .grayColor
        borderWidth = 1
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(title)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        title.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
