//
//  CUTagsView.swift
//  Cash2U
//
//  Created talgar osmonov on 25/8/22.
//


import UIKit
import Atributika

final class CUTagsView: UICollectionViewCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private var data = ["Детская одежда", "Строительные материалы", "Одежда", "Женская одежда", "Компьютеры и комплектующие"]
    
    private lazy var mainCollection: CUCollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        let view = CUCollectionView(
            frame: CGRect.zero,
            collectionViewLayout: flowLayout
        )
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentInset = .init(top: 0, left: 20, bottom: 0, right: 20)
        view.delegate = self
        view.dataSource = self
        view.decelerationRate = .normal
        view.alwaysBounceHorizontal = true
        view.register(CUTagsCell.self)
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUTagsView {
    /// Установка
    func setup() {
        
    }
}

extension CUTagsView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(cellType: CUTagsCell.self, for: indexPath)
        cell.setup(text: data[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let tag = data[indexPath.row]
        
        var attribitedTitle: NSAttributedString {
            let style = Style.mainTextStyle(
                size: 18,
                fontType: .regular,
                color: .mainTextColor,
                alignment: .left
            )
            return tag.styleAll(style).attributedString
        }
        
        return CGSize(width: attribitedTitle.getWidth(withConstrainedHeight: collectionView.frame.height) + 35, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
}

private extension CUTagsView {
    // Общий инициализатор
    func commonInit() {
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(mainCollection)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        mainCollection.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
