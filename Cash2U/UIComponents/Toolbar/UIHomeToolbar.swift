//
//  UIToolbar.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation
import UIKit

class UIHomeToolbar: BaseView{
    
    private var countryText = "Бишкек"
    
    private lazy var notificationCounter: UINotificationCounter = {
        let view = UINotificationCounter()
        return view
    }()
    
    private lazy var checkMarkIcon: UIImageView = {
       let view = UIImageView()
        view.image = UIImage(named: "CheckMark")
        return view
    }()

    private lazy var arrowDown: UIImageView = {
       let view = UIImageView()
        view.image = UIImage(named: "ArrowDown")
        return view
    }()
    
    private lazy var countryTitle: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        view.textColor = .black
        view.text = countryText
        return view
    }()
    
    private lazy var separator: UIView = {
        let view = UIView()
        view.backgroundColor = .init(hex: "#F1F1F1")
        return view
    }()
    
    override func setupView() {
        
    }
    
    override func setupSubViews() {
        addSubview(checkMarkIcon)
        checkMarkIcon.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(24)
            make.centerY.equalToSuperview()
        }
        
        addSubview(countryTitle)
        countryTitle.snp.makeConstraints { make in
            make.left.equalTo(checkMarkIcon.snp.right).offset(9)
            make.centerY.equalToSuperview()
        }
        
        addSubview(arrowDown)
        arrowDown.snp.makeConstraints { make in
            make.left.equalTo(countryTitle.snp.right).offset(31)
            make.centerY.equalToSuperview()
        }
        
        addSubview(notificationCounter)
        notificationCounter.snp.makeConstraints { make in
            make.height.equalTo(21)
            make.width.equalTo(31)
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-20)
        }
        
        addSubview(separator)
        separator.snp.makeConstraints { make in
            make.bottom.left.right.equalToSuperview()
            make.height.equalTo(2)
        }
    }
    
}
