//
//  CULogoSectionView.swift
//  Cash2U
//
//  Created talgar osmonov on 8/8/22.
//


import UIKit

final class CULogoSectionView: UICollectionViewCell {
    
    private var isConstraintsInstalled: Bool = false
    
    lazy var logoImageView: UIImageView = {
        let view = UIImageView(image: UIImage(named: "Cash2uLogo"))
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CULogoSectionView {
    /// Установка
    func setup() {
        
    }
}

private extension CULogoSectionView {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .mainColor
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(logoImageView)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        logoImageView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
