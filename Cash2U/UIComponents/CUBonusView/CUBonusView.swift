//
//  CUBonusView.swift
//  Cash2U
//
//  Created Oroz on 4/10/22.
//


import UIKit

final class CUBonusView: UICollectionViewCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var bonusAmout: CUDoubleAmountLabelView = {
        let view = CUDoubleAmountLabelView()
        return view
    }()
    
    private lazy var bonusType: CULabelView = {
        let view = CULabelView()
        view.setup(size: 17, fontType: .medium, titleColor: .gray)
        return view
    }()
    
    private lazy var timeView: CULabelView = {
        let view = CULabelView()
        view.setup(size: 13, fontType: .medium, titleColor: .white)
        return view
    }()
    
    private lazy var comment: CULabelView = {
        let view = CULabelView()
        view.setup(size: 18, titleColor: .gray)
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUBonusView {
    /// Установка
    func setup() {
        bonusType.changeText(title: "Получено")
        bonusAmout.setup(
            summ: 50.00,
            colorForInteger: .white,
            colorForTheRest: .gray,
            fontForInteger: UIFont.systemFont(ofSize: 24, weight: .bold),
            fontForTheRemainder: UIFont.systemFont(ofSize: 24, weight: .bold),
            currencySymbol: "б",
            currencySymbolFont: UIFont.systemFont(ofSize: 24, weight: .bold),
            currencySymbolColor: .white
        )
        timeView.changeText(title: "14:20")
        comment.changeText(title: "За то, что вы с нами уже 6 месяцев😘")
    }
}

private extension CUBonusView {
    // Общий инициализатор
    func commonInit() {
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        addSubviews([bonusType, bonusAmout, timeView, comment])
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        bonusType.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.equalToSuperview()
        }
        
        bonusAmout.snp.makeConstraints { make in
            make.top.equalTo(bonusType.snp.bottom).offset(6)
            make.leading.equalToSuperview()
        }
        
        comment.snp.makeConstraints { make in
            make.top.equalTo(bonusAmout.snp.bottom).offset(10)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
        }
        
        timeView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(30)
            make.trailing.equalToSuperview()
        }
    }
}
