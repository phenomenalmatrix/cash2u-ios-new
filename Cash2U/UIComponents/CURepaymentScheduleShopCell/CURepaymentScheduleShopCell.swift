//
//  CURepaymentScheduleShopCell.swift
//  Cash2U
//
//  Created talgar osmonov on 6/10/22.
//


import UIKit

final class CURepaymentScheduleShopCell: UICollectionViewCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var titleLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(size: 22, numberOfLines: 2, fontType: .semibold)
        return view
    }()
    
    private lazy var image: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFill
        view.cornerRadius = 50 / 2
        return view
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .grayColor
        view.alpha = 0.25
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CURepaymentScheduleShopCell {
    /// Установка
    func setup(text: String, image: String, isBottomLineHidden: Bool = false) {
        titleLabel.changeText(title: text)
        self.image.setImage(with: image)
        lineView.isHidden = isBottomLineHidden
    }
}

private extension CURepaymentScheduleShopCell {
    // Общий инициализатор
    func commonInit() {
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(image)
        contentView.addSubview(lineView)
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        titleLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview().offset(-70)
            make.top.bottom.equalToSuperview().inset(5)
        }
        
        image.snp.makeConstraints { make in
            make.trailing.equalToSuperview()
            make.centerY.equalToSuperview()
            make.size.equalTo(50)
        }
        
        lineView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
            make.height.equalTo(1)
        }
    }
}
