//
//  CUScheduleView.swift
//  Cash2U
//
//  Created talgar osmonov on 1/9/22.
//


import UIKit

final class CUScheduleView: UICollectionViewCell {
    
    private var isConstraintsInstalled: Bool = false
    
    private lazy var start: CUDetailScheduleView = {
        let view = CUDetailScheduleView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup()
        return view
    }()
    
    private lazy var clockIcon: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "clock_icon")
        view.contentMode = .scaleAspectFit
        view.isHidden = true
        return view
    }()
    
    private lazy var end: CUDetailScheduleView = {
        let view = CUDetailScheduleView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup()
        return view
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .grayColor
        view.alpha = 0.3
        return view
    }()

    private lazy var bottomLineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .grayColor
        view.alpha = 0.3
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    /// Обновление констрейнтов
    override public func updateConstraints() {
        setupConstraintsIfNeeded()
        super.updateConstraints()
    }
}

extension CUScheduleView {
    /// Установка
    func setup() {
        
    }
}

private extension CUScheduleView {
    // Общий инициализатор
    func commonInit() {
        backgroundColor = .secondaryColor
        createViewHierarchy()
        setupConstraintsIfNeeded()
    }

    // Создать иерархию вью
    func createViewHierarchy() {
        contentView.addSubviews([lineView, start, clockIcon, end, bottomLineView])
    }

    // Установка констрейнтов
    func setupConstraintsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        lineView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(5)
            make.bottom.equalToSuperview().offset(-25)
            make.width.equalTo(1)
        }
        
        start.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(0)
            make.trailing.equalTo(lineView.snp.leading).offset(-20)
            make.bottom.equalToSuperview().offset(-20)
        }
        
        clockIcon.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.top.equalToSuperview().offset(5)
            make.size.equalTo(25)
        }
        
        end.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(20)
            make.top.equalToSuperview().offset(0)
            make.leading.equalTo(lineView.snp.trailing).offset(20)
            make.bottom.equalToSuperview().offset(-20)
        }
        
        bottomLineView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(20)
            make.bottom.equalToSuperview().offset(-10)
            make.height.equalTo(1)
        }
        
    }
}
