//
//  IdentityApplicationInteractor.swift
//  Cash2U
//
//  Created by talgar osmonov on 20/9/22.
//  
//

import Foundation

final class IdentityApplicationInteractor {
    weak var presenter: IdentityApplicationInteractorToPresenterProtocol?
}

extension IdentityApplicationInteractor: IdentityApplicationInteractorProtocol {
}
