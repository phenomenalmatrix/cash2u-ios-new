//
//  IdentityApplicationViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 20/9/22.
//  
//

import UIKit
import IQKeyboardManagerSwift
import Atributika
import InputBarAccessoryView
import RealmSwift

final class IdentityApplicationViewController: MessagesViewController {
    
    var presenter: IdentityApplicationViewToPresenterProtocol?
    private var notificationToken: NotificationToken?
    
//    private var inn: String?
//    private var IDNumber: String?
//    private var city: String?
//    private var street: String?
//    private var house: String?
//    private var frontImage: UIImage?
//    private var backImage: UIImage?
//    private var selfieImage: UIImage?
    
    private var identificationModel = UserDefaultsService.shared.identificationModel {
        didSet {
            UserDefaultsService.shared.identificationModel = identificationModel
        }
    }
    
    private var inputType: InputBarType = UserDefaultsService.shared.identificationInputType {
        didSet {
            UserDefaultsService.shared.identificationInputType = inputType
        }
    }
    
    private var savedInputType: InputBarType = UserDefaultsService.shared.savedIdentificationInputType {
        didSet {
            UserDefaultsService.shared.savedIdentificationInputType = savedInputType
        }
    }
    
    private var editInputType: InputBarType? = UserDefaultsService.shared.editIdentificationInputType {
        didSet {
            UserDefaultsService.shared.editIdentificationInputType = editInputType
        }
    }
    
    private var newEditInputType: InputBarType?
    
    private func onNextInputType() {
        if inputType != .videoCall {
            inputType = InputBarType(rawValue: inputType.rawValue + 1)!
        }
    }
    
    private func onNextSavedInputType() {
        if savedInputType != .videoCall {
            savedInputType = InputBarType(rawValue: savedInputType.rawValue + 1)!
        }
    }
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private let currentUser = IdentificationMessages.currentUser
    private let cash2uUser = IdentificationMessages.cash2uUser
    
    private lazy var buttonInputBar: CUButtonInputBar = {
        let view = CUButtonInputBar()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.myDelegate = self
        return view
    }()
    
    private lazy var messagesList: [CUMessage] = []
    private var allRealmMessages: Results<IdentificationMessageRealm>?
    
    override func loadView() {
        super.loadView()
        title = "Подтверждение личности"
        view.backgroundColor = .mainColor
        navigationItem.rightBarButtonItem = .init(image: UIImage(systemName: "bell"), style: .plain, target: self, action: #selector(onSupportTapped))
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout = messagesCollectionView.collectionViewLayout as? MessagesCollectionViewFlowLayout
        layout?.sectionInset = UIEdgeInsets(top: 3 , left: 8, bottom: 3, right: 8)

        layout?.setMessageOutgoingAvatarSize(.zero)
        layout?
          .setMessageOutgoingMessageTopLabelAlignment(LabelAlignment(
            textAlignment: .right,
            textInsets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)))
        layout?
          .setMessageOutgoingMessageBottomLabelAlignment(LabelAlignment(
            textAlignment: .right,
            textInsets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)))
        
        layout?.setMessageIncomingAccessoryViewSize(CGSize(width: 0, height: 0))
        layout?.setMessageOutgoingAccessoryViewSize(CGSize(width: 40, height: 40))
        layout?.setMessageOutgoingAccessoryViewPadding(HorizontalEdgeInsets(left: 0, right: 8))
        layout?.setMessageOutgoingAccessoryViewPosition(.messageBottom)
        
        messagesCollectionView.backgroundColor = .mainColor
        messagesCollectionView.showsVerticalScrollIndicator = false
        messagesCollectionView.register(MessageDateReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader)
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate =  self
        messagesCollectionView.messagesDisplayDelegate = self
        messagesCollectionView.messageCellDelegate = self
        scrollsToLastItemOnKeyboardBeginsEditing = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(willTerminateNotification), name: UIApplication.willTerminateNotification, object: nil)
        
        self.messageInputBar = self.buttonInputBar
        fetchMessages()
    }
    
    deinit {
        notificationToken = nil
    }

    // MARK: - Observres

    @objc private func willTerminateNotification() {
        if inputType == .empty {
            changeInputType()
            onNextSavedInputType()
        }
    }
    
    private func fetchMessages() {
        IdentificationRealmService.shared.fetchMyList { result in
            switch result {
            case .success(let success):
                if let success {
                    self.manageFirstMessages(data: success)
                }
            case .failure(let failure):
                break
            }
        }
    }
    
    private func manageFirstMessages(data: Results<IdentificationMessageRealm>) {
        self.allRealmMessages = data
        self.observeMessages()
        if data.count == 0 {
            DispatchQueue.main.async {
                self.sendTypingMessage(messages: [IdentificationMessages.greetings0]) { [weak self] in
                    guard let self else {return}
                    self.sendTypingMessage(messages: [IdentificationMessages.greetings1]) { [weak self] in
                        guard let self else {return}
                        self.sendTypingMessage(messages: [IdentificationMessages.greetings2, IdentificationMessages.greetings3]) { [weak self] in
                            guard let self else {return}
                            self.sendTypingMessage(messages: [IdentificationMessages.greetings4, IdentificationMessages.greetings5]) { [weak self] in
                                guard let self else {return}
                                self.sendTypingMessage(messages: [IdentificationMessages.greetings6, IdentificationMessages.greetings7]) { [weak self] in
                                    guard let self else {return}
                                    self.sendTypingMessage(messages: [IdentificationMessages.greetings8, IdentificationMessages.greetings9], changeInputType: true)
                                    self.onNextSavedInputType()
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    private func observeMessages() {
        notificationToken = allRealmMessages?.observe({ [weak self] (changes: RealmCollectionChange) in
            switch changes {
            case .initial(_):
                self?.insertMessages()
            case .update(_, deletions: _, insertions: let insertions, modifications: let modifications):
                for index in insertions {
                    self?.insertMessage(index: index)
                }
                
                for index in modifications {
                    self?.updateMessage(index: index)
                }
            case .error(let error):
                print("error \(error.localizedDescription)")
            }
        })
    }
    
    // MARK: - UIActions
    
    @objc private func onSupportTapped () {
        let vc = NewsCenterModuleConfigurator.build()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - Helpers
    
    func sendMessage(text: String? = nil, photoName: String? = nil, messageId: String = UUID().uuidString, sender: CUSender) {
        let model = IdentificationMessageRealm()
        model.id = messageId
        model.status = IdentificationMessageStatus.create.rawValue
        model.date = Date()
        model.senderName = sender.displayName
        model.senderId = sender.senderId
        if let text {
            model.type = SendMessageType.text.rawValue
            model.message = text
        }
        if let photoName {
            model.type = SendMessageType.photo.rawValue
            model.pictureUrl = photoName
        }
        sendMessageToRealm(message: model)
    }
    
    func sendMessageToRealm(message: IdentificationMessageRealm) {
        IdentificationRealmService.shared.addToMyList(model: message) { result in
            switch result {
            case .success(let success):
                print("AA")
            case .failure(let failure):
                print("bb")
            }
        }
    }
}

extension IdentityApplicationViewController: IdentityApplicationViewProtocol {
}
 
extension IdentityApplicationViewController: CUButtonInputBarDelegate {
    func onButtonPressed(type: ButtonInputBarType, inputType: InputBarType) {
        switch type {
        case .start:
            startMessage()
        case .takePhoto:
            switch inputType {
            case .frontPasport:
                idCardPhotoMessage()
            case .backPasport:
                idCardPhotoMessage()
            case .selfie:
                selfiePhotoMessage()
            default:
                break
            }
        case .fromGallery:
            idCardPhotoMessage()
        case .send:
            break
        case .sendApplication:
            IdentificationService.shared.sendIdentificationRequest(model: identificationModel)
        case .ready:
            break
        }
    }
    
    func innData(inn: String, an: String) {
        self.identificationModel.documentType = "ID"
        self.identificationModel.pin = inn
        self.identificationModel.documentNumber = an
        let text = "ИНН: \(inn)\nНомер паспорта: \(an)"
        if let _ = newEditInputType {
            IdentificationRealmService.shared.updateModel(id: "innMessage", message: text) { result in
                switch result {
                case .success(let success):
                    print(success)
                    self.changeInputType(isAfterEditType: true, scrollToBottom: false)
                case .failure(let failure):
                    print(failure)
                }
            }
        } else {
            self.sendMessage(text: text, messageId: "innMessage", sender: self.currentUser)
            self.onNextSavedInputType()
            sendTypingMessage(messages: [IdentificationMessages.city0], changeInputType: true)
        }
    }
    
    func cityData(city: String, street: String, house: String) {
//        self.city = city
//        self.street = street
//        self.house = house
        
        let text = "\(city), \(street) \(house)"
        if let _ = newEditInputType {
            IdentificationRealmService.shared.updateModel(id: "cityMessage", message: text) { result in
                switch result {
                case .success(let success):
                    print(success)
                    self.changeInputType(isAfterEditType: true, scrollToBottom: false)
                case .failure(let failure):
                    print(failure)
                }
            }
        } else {
            self.sendMessage(text: text, messageId: "cityMessage", sender: self.currentUser)
            onNextSavedInputType()
            sendTypingMessage(messages: [IdentificationMessages.frontPhoto0, IdentificationMessages.frontPhoto1], changeInputType: true)
        }
    }
}

extension IdentityApplicationViewController: PhotoResultViewControllerDelegate {
    func retakePhoto() {
        switch inputType {
        case .frontPasport:
            idCardPhotoMessage()
        case .backPasport:
            idCardPhotoMessage()
        case .selfie:
            selfiePhotoMessage()
        default:
            break
        }
    }
    
    func sendPhoto(image: UIImage) {
        if let edit = newEditInputType {
            switch edit {
            case .frontPasport:
                FileManagerService.shared.saveImage(fileName: "frontPasport", image: image)
                IdentificationService.shared.sendIdentificationImage(image: image, imageType: .frontPassport) { _ in
                    IdentificationRealmService.shared.updateModel(id: "frontPasportMessage", photo: "frontPasport") { result in
                        switch result {
                        case .success(let success):
                            self.changeInputType(isAfterEditType: true, scrollToBottom: false)
                        case .failure(let failure):
                            print(failure)
                        }
                    }
                }
            case .backPasport:
                FileManagerService.shared.saveImage(fileName: "backPasport", image: image)
                IdentificationService.shared.sendIdentificationImage(image: image, imageType: .backPassport) { _ in
                    IdentificationRealmService.shared.updateModel(id: "backPasportMessage", photo: "backPasport") { result in
                        switch result {
                        case .success(let success):
                            self.changeInputType(isAfterEditType: true, scrollToBottom: false)
                        case .failure(let failure):
                            print(failure)
                        }
                    }
                }
            case .selfie:
                FileManagerService.shared.saveImage(fileName: "selfie", image: image)
                IdentificationService.shared.sendIdentificationImage(image: image, imageType: .selfiePassport) { _ in
                    IdentificationRealmService.shared.updateModel(id: "selfieMessage", photo: "selfie") { result in
                        switch result {
                        case .success(let success):
                            self.changeInputType(isAfterEditType: true, scrollToBottom: false)
                        case .failure(let failure):
                            print(failure)
                        }
                    }
                }
            default:
                break
            }
        } else {
            
            switch inputType {
            case .frontPasport:
                FileManagerService.shared.saveImage(fileName: "frontPasport", image: image)
                IdentificationService.shared.sendIdentificationImage(image: image, imageType: .frontPassport) { _ in
                    self.sendMessage(photoName: "frontPasport", messageId: "frontPasportMessage", sender: self.currentUser)
                    self.onNextSavedInputType()
                    self.sendTypingMessage(messages: [IdentificationMessages.backPhoto0]) { [weak self] in
                        guard let self else {return}
                        self.sendTypingMessage(messages: [IdentificationMessages.backPhoto1, IdentificationMessages.backPhoto2], changeInputType: true)
                    }
                }
                
            case .backPasport:
                FileManagerService.shared.saveImage(fileName: "backPasport", image: image)
                IdentificationService.shared.sendIdentificationImage(image: image, imageType: .backPassport) { _ in
                    self.sendMessage(photoName: "backPasport", messageId: "backPasportMessage", sender: self.currentUser)
                    self.onNextSavedInputType()
                    self.sendTypingMessage(messages: [IdentificationMessages.selfiePhoto0]) { [weak self] in
                        guard let self else {return}
                        self.sendTypingMessage(messages: [IdentificationMessages.selfiePhoto1], changeInputType: true)
                    }
                }
                
            case .selfie:
                FileManagerService.shared.saveImage(fileName: "selfie", image: image)
                IdentificationService.shared.sendIdentificationImage(image: image, imageType: .selfiePassport) { _ in
                    self.sendMessage(photoName: "selfie", messageId: "selfieMessage", sender: self.currentUser)
                    self.onNextSavedInputType()
                    self.sendTypingMessage(messages: [IdentificationMessages.ready0]) { [weak self] in
                        guard let self else {return}
                        self.sendTypingMessage(messages: [IdentificationMessages.ready1], changeInputType: true)
                    }
                }
                
            default:
                break
            }
            
        }
    }
}
    
extension IdentityApplicationViewController: CardDetectionViewControllerDelegate, CUFaceCameraViewControllerViewControllerDelegate {
    func cardDetectionViewController(_ viewController: CardDetectionViewController, didDetectCard image: CGImage, withSettings settings: CardDetectionSettings) {
        let vc = PhotoResultViewController()
        vc.delegate = self
        vc.photo.image = UIImage(cgImage: image)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func selfiePhotoResult(image: UIImage) {
        let vc = PhotoResultViewController()
        vc.delegate = self
        vc.photo.image = image
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

private extension IdentityApplicationViewController {
    //MARK: - Nachalo
    func idCardPhotoMessage() {
        let controller = CardDetectionViewController()
        controller.modalPresentationStyle = .fullScreen
        controller.delegate = self
        self.present(controller, animated: true)
    }
    
    func selfiePhotoMessage() {
        
        let controller = CUFaceCameraViewControllerViewController()
        controller.modalPresentationStyle = .fullScreen
        controller.delegate = self
        self.present(controller, animated: true)
    }
    
    //MARK: - Nachalo
    func startMessage() {
        self.sendMessage(text: "Я хочу подтвердить личность", messageId: "approveIdentity", sender: self.currentUser)
        onNextSavedInputType()
        sendTypingMessage(messages: [IdentificationMessages.inn0]) { [weak self] in
            guard let self else {return}
            self.sendTypingMessage(messages: [IdentificationMessages.inn1], changeInputType: true)
        }
    }
    
    //MARK: - Helpers
    
    func changeInputType(isFirstInput: Bool = false, editInputType: InputBarType? = nil, newEditInputType: InputBarType? = nil, isAfterEditType: Bool = false, isAnimatedScroll: Bool = true, scrollToBottom: Bool = true) {
        
        if let newEditInputType {
            self.newEditInputType = newEditInputType
            DispatchQueue.main.asyncAfter(deadline: .now()) {
                self.buttonInputBar.changeInputType(type: newEditInputType)
            }
            return
        } else {
            self.newEditInputType = nil
        }
        
        if let editInputType {
            self.editInputType = editInputType
            DispatchQueue.main.asyncAfter(deadline: .now()) {
                self.buttonInputBar.changeInputType(type: editInputType)
                if scrollToBottom {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        self.scrollToBottom(isAnimated: isAnimatedScroll)
                    }
                }
            }
        } else {
            if !isFirstInput && !isAfterEditType {
                self.onNextInputType()
            }
            DispatchQueue.main.asyncAfter(deadline: .now()) {
                self.buttonInputBar.changeInputType(type: self.inputType)
                if scrollToBottom {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        self.scrollToBottom(isAnimated: isAnimatedScroll)
                    }
                }
            }
        }
        
        
        
    }
    
    func insertMessage(index: Int) {
        if let allRealmMessages {
            self.createMessage(message: allRealmMessages[index])
            messagesCollectionView.performBatchUpdates({
              messagesCollectionView.insertSections([messagesList.count - 1])
              if messagesList.count >= 2 {
                messagesCollectionView.reloadSections([messagesList.count - 2])
              }
            }, completion: {_ in})
            self.scrollToBottom()
        }
    }
    
    func updateMessage(index: Int) {
        if let allRealmMessages {
            let message = CUMessage(message: allRealmMessages[index])
            messagesList[index] = message
            messagesCollectionView.reloadData()
        }
    }
    
    func insertMessages() {
        DispatchQueue.main.async { [weak self] in
            guard let self else {return}
            if let allRealmMessages = self.allRealmMessages {
                for message in allRealmMessages {
                    self.createMessage(message: message)
                }
                self.messagesCollectionView.reloadData()
                self.scrollToBottom(isAnimated: false)
//                if let input = self.editInputType {
//                    self.changeInputType(editInputType: input)
//                } else {
//                    self.changeInputType(isFirstInput: true)
//                }
//                self.changeInputType(isFirstInput: true)
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    self.buttonInputBar.changeInputType(type: self.savedInputType)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        self.scrollToBottom(isAnimated: false)
                    }
                }
            }
        }
    }
    
    private func createMessage(message: IdentificationMessageRealm) {
        messagesList.append(CUMessage(message: message))
    }
    
    private func scrollToBottom(isAnimated: Bool = true) {
        self.messagesCollectionView.scrollToLastItem(animated: isAnimated)
    }
    
    func sendTypingMessage(messages: [IdentificationMessageRealm], changeInputType: Bool = false, editInputType: InputBarType? = nil, completion: (() -> Void)? = nil) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            self.setTypingIndicatorViewHidden(false, animated: true, completion:  { _ in
                self.scrollToBottom()
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.setTypingIndicatorViewHidden(true, animated: false, completion:  { _ in
                        messages.forEach { model in
                            self.sendMessageToRealm(message: model)
                        }
                        
                        if let editInputType {
                            self.editInputType = editInputType
                            self.changeInputType(editInputType: editInputType)
                        } else {
                            if let _ = self.editInputType {
                                self.changeInputType(editInputType: self.inputType)
                                self.editInputType = nil
                            } else {
                                if changeInputType {
                                    self.changeInputType()
                                }
                            }
                            
                        }
                        
                        if let completion {
                            completion()
                        }
                    })
                }
            })
        }
    }
    
    func getMessageText(text: String) -> NSAttributedString {
        var title: NSAttributedString {
            let style = Style.mainTextStyle(
                size: 18,
                fontType: .regular,
                color: .mainTextColor,
                alignment: .left,
                lineHeight: 21,
                lineBreakMode: .byTruncatingTail
            )
            
            let boldStyle = Style.mainTextStyle(
                tag: "b",
                size: 18,
                fontType: .bold,
                color: .mainTextColor,
                alignment: .left,
                lineHeight: 21,
                lineBreakMode: .byTruncatingTail
            )
            return text.style(tags: [boldStyle]).styleAll(style).attributedString
        }
        
        return title
    }
}

extension IdentityApplicationViewController: MessagesDataSource {
    func currentSender() -> SenderType {
        currentUser
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        messagesList[indexPath.section]
    }
    
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        messagesList.count
    }
    
    func nextMessage(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        guard indexPath.section + 1 < messagesList.count else {
            return messagesList[indexPath.section]
        }
        return messagesList[indexPath.section + 1]
    }
}

extension IdentityApplicationViewController: MessageCellDelegate {
    func didTapAccessoryView(in cell: MessageCollectionViewCell) {
        guard
          let indexPath = messagesCollectionView.indexPath(for: cell),
          let message = messagesCollectionView.messagesDataSource?.messageForItem(at: indexPath, in: messagesCollectionView)
        else { return }
        
        buttonInputBar.changeTouchDisabling(true)
        messagesCollectionView.scrollToItem(indexPath: indexPath)
        if message.messageId == "cityMessage" {
            changeInputType(newEditInputType: .city)
        } else if message.messageId == "innMessage" {
            changeInputType(newEditInputType: .inn)
        } else if message.messageId == "frontPasportMessage" {
            changeInputType(newEditInputType: .frontPasport)
        } else if message.messageId == "backPasportMessage" {
            changeInputType(newEditInputType: .backPasport)
        } else if message.messageId == "selfieMessage" {
            changeInputType(newEditInputType: .selfie)
        }
    }
}

// MARK: - MessagesLayoutDelegate

extension IdentityApplicationViewController: MessagesLayoutDelegate {
    func messageTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in _: MessagesCollectionView) -> CGFloat {
        return !isPreviousMessageSameSender(at: indexPath) ? 10 : 0
    }
}

// MARK: - MessagesDisplayDelegate

extension IdentityApplicationViewController: MessagesDisplayDelegate {
    
    func configureMediaMessageImageView(_ imageView: UIImageView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        if case MessageKind.photo(let media) = message.kind{
            if let url = media.url?.absoluteString {
                imageView.setImage(with: url)
            } else if let image = media.image {
                imageView.image = image
            }
        }
    }
    
    func messageStyle(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
        if isNextMessageSameSender(at: indexPath) {
            if isFromCurrentSender(message: message) {
                return .bubble
            } else {
                return .bubble
            }
        } else {
            if isFromCurrentSender(message: message) {
                return .bubbleTail(.bottomRight, .curved)
            } else {
                return .bubbleTail(.bottomLeft, .curved)
            }
        }
    }
    
    func backgroundColor(for message: MessageType, at  indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return !isFromCurrentSender(message: message) ? .thirdaryColor! : .mainBlueColor!
    }
    
    func textColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return .mainTextColor ?? .white
    }
    
    func shouldDisplayHeader(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> Bool {

        if indexPath.section == 0 {
            return true
        }
        let previousIndexPath = IndexPath(row: 0, section: indexPath.section - 1)
        let previousMessage = messageForItem(at: previousIndexPath, in: messagesCollectionView)

        if message.sentDate.isInSameDayOf(date: previousMessage.sentDate){
            return false
        }

        return true
    }
    
    func headerViewSize(for section: Int, in messagesCollectionView: MessagesCollectionView) -> CGSize {
        let size = CGSize(width: messagesCollectionView.frame.width, height: 75)
        if section == 0 {
            return size
        }

        let currentIndexPath = IndexPath(row: 0, section: section)
        let lastIndexPath = IndexPath(row: 0, section: section - 1)
        let lastMessage = messageForItem(at: lastIndexPath, in: messagesCollectionView)
        let currentMessage = messageForItem(at: currentIndexPath, in: messagesCollectionView)

        if currentMessage.sentDate.isInSameDayOf(date: lastMessage.sentDate) {
            return .zero
        }

        return size
    }
    
    func messageHeaderView(
        for indexPath: IndexPath,
        in messagesCollectionView: MessagesCollectionView
    ) -> MessageReusableView {
        let messsage = messageForItem(at: indexPath, in: messagesCollectionView)
        let header = messagesCollectionView.dequeueReusableHeaderView(MessageDateReusableView.self, for: indexPath)
        header.label.text = MessageKitDateFormatter.shared.string(from: messsage.sentDate)
        return header
    }
    
    func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        avatarView.set(avatar: Avatar(image: UIImage(named: "som_logo")))
        avatarView.isHidden =  isFromCurrentSender(message: message) ? true : isNextMessageSameSender(at: indexPath)
    }
    
    func configureAccessoryView(_ accessoryView: UIView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        
        if isFromCurrentSender(message: message) && message.messageId != "approveIdentity" {
            accessoryView.subviews.forEach { $0.removeFromSuperview() }
            accessoryView.backgroundColor = .clear
            
            let button = UIButton(type: .infoLight)
            button.tintColor = .gray
            accessoryView.addSubview(button)
            button.frame = accessoryView.bounds
            button.isUserInteractionEnabled = false
            accessoryView.layer.cornerRadius = accessoryView.frame.height / 2
            accessoryView.backgroundColor = UIColor.gray.withAlphaComponent(0.3)
        } else {
            accessoryView.subviews.forEach { $0.removeFromSuperview() }
            accessoryView.backgroundColor = .clear
        }
    }
}

private extension IdentityApplicationViewController {
    func isLastSectionVisible() -> Bool {
      guard !messagesList.isEmpty else { return false }

      let lastIndexPath = IndexPath(item: 0, section: messagesList.count - 1)

      return messagesCollectionView.indexPathsForVisibleItems.contains(lastIndexPath)
    }
    
    func isPreviousMessageSameSender(at indexPath: IndexPath) -> Bool {
      guard indexPath.section - 1 >= 0 else { return false }
        return messagesList[indexPath.section].sender.senderId == messagesList[indexPath.section - 1].sender.senderId
    }

    func isNextMessageSameSender(at indexPath: IndexPath) -> Bool {
      guard indexPath.section + 1 < messagesList.count else { return false }
      return messagesList[indexPath.section].sender.senderId == messagesList[indexPath.section + 1].sender.senderId
    }
}

extension IdentityApplicationViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
    }
}


//MESSAGE HEADER


class MessageDateReusableView: MessageReusableView {
    lazy var backView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 30 / 2
        view.clipsToBounds = true
        view.addSubview(label)
        view.backgroundColor = .init(hex: "40000000")
        label.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(5)
        }
        return view
    }()
    
    lazy var label: UILabel = {
       let label = UILabel()
        label.textColor = .grayColor?.withAlphaComponent(0.6)
        label.textAlignment = .center
        label.numberOfLines = 1
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.clipsToBounds = true
        return label
    }()

    override init (frame : CGRect) {
        super.init(frame : frame)
        self.addSubview(backView)
        backView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.height.equalTo(30)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
