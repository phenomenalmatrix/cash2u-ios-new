//
//  CUButtonInputBar.swift
//  Cash2U
//
//  Created talgar osmonov on 18/10/22.
//


import UIKit
import InputBarAccessoryView

enum InputBarType: Int {
    case empty, start, inn, city, frontPasport, backPasport, selfie, sendApplication, ready, videoCall
}

enum ButtonInputBarType: Int {
    case start = 0
    case takePhoto = 1
    case fromGallery = 2
    case send = 3
    case sendApplication = 4
    case ready = 5
}


protocol CUButtonInputBarDelegate: AnyObject {
    func onButtonPressed(type: ButtonInputBarType, inputType: InputBarType)
    func innData(inn: String, an: String)
    func cityData(city: String, street: String, house: String)
}

final class CUButtonInputBar: InputBarAccessoryView {
    
    private var inputType: InputBarType = .empty
    
    weak var myDelegate: CUButtonInputBarDelegate? = nil
    
    private var isDisableTouch = false {
        didSet {
            middleContentView?.alpha = isDisableTouch ? 0.5 : 1
            middleContentView?.isUserInteractionEnabled = !isDisableTouch
        }
    }
    
    private lazy var untouchView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .red
        return view
    }()
    
    private lazy var startButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Начать")
        view.setHeight(ScreenHelper.mainButtonHeight)
        view.tag = ButtonInputBarType.start.rawValue
        view.addTarget(self, action: #selector(onButtonPressed(_:)), for: .touchUpInside)
        return view
    }()
    
    private lazy var readyButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Ок! Я готов")
        view.setHeight(ScreenHelper.mainButtonHeight)
        view.tag = ButtonInputBarType.ready.rawValue
        view.addTarget(self, action: #selector(onButtonPressed(_:)), for: .touchUpInside)
        return view
    }()
    
    private lazy var sendApplicationButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Отправить заявку")
        view.setHeight(ScreenHelper.mainButtonHeight)
        view.tag = ButtonInputBarType.sendApplication.rawValue
        view.addTarget(self, action: #selector(onButtonPressed(_:)), for: .touchUpInside)
        return view
    }()
    
    private lazy var takePhotoButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Фото лицевой стороны паспорта", titleColor: .mainTextColor, backgroundColor: .clear, borderWidth: 1, borderColor: .grayColor)
        view.setHeight(ScreenHelper.mainButtonHeight)
        view.tag = ButtonInputBarType.takePhoto.rawValue
        view.addTarget(self, action: #selector(onButtonPressed(_:)), for: .touchUpInside)
        return view
    }()
    
    private lazy var fromGalleryButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Выбрать из галереи", titleColor: .mainTextColor, backgroundColor: .clear, borderWidth: 1, borderColor: .grayColor)
        view.setHeight(ScreenHelper.mainButtonHeight)
        view.tag = ButtonInputBarType.fromGallery.rawValue
        view.addTarget(self, action: #selector(onButtonPressed(_:)), for: .touchUpInside)
        return view
    }()
    
    private lazy var INNFiled: TextFieldWithPadding = {
        let view = TextFieldWithPadding(insets: .init(top: 10, left: 16, bottom: 10, right: 16))
        view.translatesAutoresizingMaskIntoConstraints = false
        view.placeholder = "Введите 14-и значное число"
        view.backgroundColor = .thirdaryColor
        view.keyboardType = .numberPad
        view.setHeight(64)
        view.cornerRadius = 18
        return view
    }()
    
    private lazy var ANFiled: TextFieldWithPadding = {
        let view = TextFieldWithPadding(insets: .init(top: 10, left: 16, bottom: 10, right: 16))
        view.translatesAutoresizingMaskIntoConstraints = false
        view.placeholder = "AN / ID0000000"
        view.backgroundColor = .thirdaryColor
        view.keyboardType = .numberPad
        view.setHeight(64)
        view.cornerRadius = 18
        return view
    }()
    
    private lazy var CityFiled: TextFieldWithPadding = {
        let view = TextFieldWithPadding(insets: .init(top: 10, left: 16, bottom: 10, right: 16))
        view.translatesAutoresizingMaskIntoConstraints = false
        view.placeholder = "Город/село"
        view.backgroundColor = .thirdaryColor
        view.setHeight(64)
        view.cornerRadius = 18
        return view
    }()
    
    private lazy var StreetFiled: TextFieldWithPadding = {
        let view = TextFieldWithPadding(insets: .init(top: 10, left: 16, bottom: 10, right: 16))
        view.translatesAutoresizingMaskIntoConstraints = false
        view.placeholder = "Улица"
        view.backgroundColor = .thirdaryColor
        view.setHeight(64)
        view.cornerRadius = 18
        return view
    }()
    
    private lazy var HouseFiled: TextFieldWithPadding = {
        let view = TextFieldWithPadding(insets: .init(top: 10, left: 16, bottom: 10, right: 16))
        view.translatesAutoresizingMaskIntoConstraints = false
        view.placeholder = "Дом/офис/строение"
        view.keyboardType = .numberPad
        view.backgroundColor = .thirdaryColor
        view.setHeight(64)
        view.cornerRadius = 18
        return view
    }()
    
    //MARK: - STACKS
    
    private lazy var cityButtonStackView: UIStackView = {
        let view = UIStackView(arrangedSubviews: [creatEmptyView(height: 6), CityFiled, StreetFiled, HouseFiled, creatEmptyView(height: 16), createSendButton()])
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .vertical
        view.spacing = 5
        view.alignment = .fill
        view.distribution = .fill
        return view
    }()
    
    private lazy var innButtonStackView: UIStackView = {
        let view = UIStackView(arrangedSubviews: [creatEmptyView(height: 6), INNFiled, ANFiled, creatEmptyView(height: 16), createSendButton()])
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .vertical
        view.spacing = 5
        view.alignment = .fill
        view.distribution = .fill
        return view
    }()
    
    private lazy var startButtonStackView: UIStackView = {
        let view = UIStackView(arrangedSubviews: [creatEmptyView(height: 6), startButton])
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .vertical
        view.spacing = 5
        view.alignment = .fill
        view.distribution = .fill
        return view
    }()
    
    private lazy var readyButtonStackView: UIStackView = {
        let view = UIStackView(arrangedSubviews: [creatEmptyView(height: 6), readyButton])
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .vertical
        view.spacing = 5
        view.alignment = .fill
        view.distribution = .fill
        return view
    }()
    
    private lazy var sendApplicationButtonStackView: UIStackView = {
        let view = UIStackView(arrangedSubviews: [creatEmptyView(height: 6), sendApplicationButton])
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .vertical
        view.spacing = 5
        view.alignment = .fill
        view.distribution = .fill
        return view
    }()
    
    private lazy var emptyStackView: UIStackView = {
        let view = UIStackView(arrangedSubviews: [creatEmptyView(height: 6), creatEmptyView(height: ScreenHelper.mainButtonHeight)])
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .vertical
        view.spacing = 5
        view.alignment = .fill
        view.distribution = .fill
        return view
    }()
    
    private lazy var photoStackView: UIStackView = {
        let view = UIStackView(arrangedSubviews: [creatEmptyView(height: 6), takePhotoButton, fromGalleryButton])
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .vertical
        view.spacing = 5
        view.alignment = .fill
        view.distribution = .fill
        return view
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func onButtonPressed(_ sender: UIControl) {
        switch inputType {
        case .inn:
            if let innText = INNFiled.text, let anText = ANFiled.text {
                myDelegate?.innData(inn: innText, an: anText)
            }
        case .city:
            if let cityText = CityFiled.text, let streetText = StreetFiled.text, let houseText = HouseFiled.text {
                myDelegate?.cityData(city: cityText, street: streetText, house: houseText)
            }
        default:
            break
        }
        myDelegate?.onButtonPressed(type: ButtonInputBarType.init(rawValue: sender.tag)!, inputType: inputType)
        
        changeTouchDisabling(true)
        resignAllResponders()
    }

    func configure() {
        backgroundColor = .mainColor
        setStackViewItems([], forStack: .right, animated: false)
        setRightStackViewWidthConstant(to: 0, animated: false)
        setMiddleContentView(emptyStackView, animated: false)
    }
    
    func changeInputType(type: InputBarType) {
        self.inputType = type
        switch type {
        case .start:
            setMiddleContentView(startButtonStackView, animated: false)
        case .inn:
            setMiddleContentView(innButtonStackView, animated: false)
        case .city:
            setMiddleContentView(cityButtonStackView, animated: false)
        case .frontPasport:
            takePhotoButton.changeText(text: "Фото лицевой стороны паспорта")
            setMiddleContentView(photoStackView, animated: false)
        case .backPasport:
            takePhotoButton.changeText(text: "Фото обратной стороны паспорта")
            setMiddleContentView(photoStackView, animated: false)
        case .selfie:
            takePhotoButton.changeText(text: "Сделать селфи с паспортом")
            setMiddleContentView(photoStackView, animated: false)
        case .videoCall:
            break
        case .empty:
            setMiddleContentView(emptyStackView, animated: false)
        case .ready:
            setMiddleContentView(readyButtonStackView, animated: false)
        case .sendApplication:
            setMiddleContentView(sendApplicationButtonStackView, animated: false)
        }
        changeTouchDisabling(false)
    }
    
    func changeTouchDisabling(_ isDisable: Bool) {
        isDisableTouch = isDisable
    }
}

private extension CUButtonInputBar {
    func clearTopSubviews() {
        topStackView.removeAllArrangedSubviews()
    }
    
    func resignAllResponders() {
        INNFiled.resignFirstResponder()
        ANFiled.resignFirstResponder()
        CityFiled.resignFirstResponder()
        StreetFiled.resignFirstResponder()
        HouseFiled.resignFirstResponder()
    }
    
    func creatEmptyView(height: CGFloat) -> UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setHeight(height)
        
        return view
    }
    
    func createSendButton() -> CUMainButton {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Отправить", titleColor: .mainTextColor, backgroundColor: .clear, borderWidth: 1, borderColor: .grayColor)
        view.setHeight(ScreenHelper.mainButtonHeight)
        view.tag = ButtonInputBarType.send.rawValue
        view.addTarget(self, action: #selector(onButtonPressed(_:)), for: .touchUpInside)
        return view
    }
}
