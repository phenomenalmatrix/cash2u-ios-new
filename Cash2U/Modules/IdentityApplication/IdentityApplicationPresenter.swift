//
//  IdentityApplicationPresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 20/9/22.
//  
//

import Foundation

final class IdentityApplicationPresenter {
    
    weak var view: IdentityApplicationViewProtocol?
    var interactor: IdentityApplicationInteractorProtocol?
    var router: IdentityApplicationRouterProtocol?
    
}

extension IdentityApplicationPresenter: IdentityApplicationViewToPresenterProtocol {
}

extension IdentityApplicationPresenter: IdentityApplicationInteractorToPresenterProtocol {
}
