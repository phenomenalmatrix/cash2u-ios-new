//
//  IdentityApplicationProtocols.swift
//  Cash2U
//
//  Created by talgar osmonov on 20/9/22.
//  
//

import Foundation

protocol IdentityApplicationViewProtocol: AnyObject {
}

protocol IdentityApplicationViewToPresenterProtocol: AnyObject {
}

protocol IdentityApplicationInteractorProtocol: AnyObject {
}

protocol IdentityApplicationInteractorToPresenterProtocol: AnyObject {
}

protocol IdentityApplicationRouterProtocol: AnyObject {
}
