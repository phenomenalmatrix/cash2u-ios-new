//
//  IdentityApplicationModuleBuilder.swift
//  Cash2U
//
//  Created by talgar osmonov on 20/9/22.
//  
//

import UIKit

final class IdentityApplicationModuleConfigurator {
    /// Собрать модуль
    class func build() -> UIViewController {
        let view = IdentityApplicationViewController()
        let presenter = IdentityApplicationPresenter()
        let interactor = IdentityApplicationInteractor()
        let router = IdentityApplicationRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
