//
//  IdentityApplicationRouter.swift
//  Cash2U
//
//  Created by talgar osmonov on 20/9/22.
//  
//

import UIKit

final class IdentityApplicationRouter {
    weak var viewController: UIViewController?
}

extension IdentityApplicationRouter: IdentityApplicationRouterProtocol {
}
