//
//  PhotoResultViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 20/10/22.
//  
//

import UIKit

protocol PhotoResultViewControllerDelegate: AnyObject {
    func retakePhoto()
    func sendPhoto(image: UIImage)
}

final class PhotoResultViewController: CUViewController {
    
    weak var delegate: PhotoResultViewControllerDelegate? = nil
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    lazy var photo: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        view.cornerRadius = 10
        return view
    }()
    
    private lazy var sendPhotoButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Отправить")
        view.addTarget(self, action: #selector(onSendPhotoPressed), for: .touchUpInside)
        return view
    }()
    
    private lazy var retakePhotoButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Сделать фото заново", titleColor: .grayColor, backgroundColor: .clear, borderWidth: 1, borderColor: .grayColor)
        view.addTarget(self, action: #selector(onRetakePhotoPressed), for: .touchUpInside)
        return view
    }()
    
    override func loadView() {
        super.loadView()
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    //MARK: - UIACtions
    
    @objc private func onRetakePhotoPressed() {
        delegate?.retakePhoto()
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc private func onSendPhotoPressed() {
        if let image = photo.image {
            delegate?.sendPhoto(image: image)
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension PhotoResultViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        
        view.addSubview(photo)
        view.addSubview(sendPhotoButton)
        view.addSubview(retakePhotoButton)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        photo.snp.makeConstraints { make in
            make.centerY.equalToSuperview().offset(-20)
            make.centerX.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(20)
            make.height.equalToSuperview().dividedBy(2.8)
        }
        
        sendPhotoButton.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(20)
            make.height.equalTo(ScreenHelper.mainButtonHeight)
            make.bottom.equalTo(view.safeArea.bottom).offset(-16)
        }
        
        retakePhotoButton.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(20)
            make.height.equalTo(ScreenHelper.mainButtonHeight - 10)
            make.bottom.equalTo(sendPhotoButton.snp.top).offset(-10)
        }
    }
}
