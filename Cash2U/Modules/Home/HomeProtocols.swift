//
//  HomeProtocols.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/5/22.
//  
//

import Foundation

protocol HomeViewProtocol: AnyObject {
}

protocol HomeViewToPresenterProtocol: AnyObject {
}

protocol HomeInteractorProtocol: AnyObject {
}

protocol HomeInteractorToPresenterProtocol: AnyObject {
}

protocol HomeRouterProtocol: AnyObject {
}
