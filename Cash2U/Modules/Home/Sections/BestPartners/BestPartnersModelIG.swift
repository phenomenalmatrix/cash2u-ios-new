//
//  BestPartnersModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 4/8/22.
//

import IGListKit

final class BestPartnersModelIG: ListDiffable {
    
    let id = "BestPartners"
    let name: String
    let isLoading: Bool
    let items: [PartnersItemModelIG]
    
    init(name: String, isLoading: Bool, items: [PartnersItemModelIG]) {
        self.name = name
        self.isLoading = isLoading
        self.items = items
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? BestPartnersModelIG else { return false }
        return id == object.id && name == object.name && isLoading == object.isLoading && items == object.items
    }
}
