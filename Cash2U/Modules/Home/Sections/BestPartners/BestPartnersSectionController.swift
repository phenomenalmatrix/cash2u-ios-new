//
//  BestPartnersSectiomController.swift
//  Cash2U
//
//  Created by talgar osmonov on 4/8/22.
//

import UIKit
import IGListKit

protocol PartnerSelectionDelegate: AnyObject {
    func didSelectPartner(partnerId: Int)
}

class BestPartnersSectionController: ListSectionController, ListSupplementaryViewSource {
    
    weak var delegate: (SectionControllerHeaderDelegate & PartnerSelectionDelegate)? = nil
    
    private var item: BestPartnersModelIG?
    private var isHeaderButtonHidden: Bool = true

    required init(isHeaderButtonHidden: Bool = true) {
        self.isHeaderButtonHidden = isHeaderButtonHidden
        super.init()
        self.inset = UIEdgeInsets(top: 20, left: 0, bottom: 32, right: 0)
        supplementaryViewSource = self
    }
    
    override func numberOfItems() -> Int {
        return 1
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width), height: 140)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUBestPartnersView = collectionContext!.dequeueReusableCell(of: CUBestPartnersView.self, for: self, at: index) as? CUBestPartnersView else {
            return UICollectionViewCell()
        }
        
        if let item = item {
            cell.delegate = self
            cell.setup(isLoading: item.isLoading, items: item.items)
        }
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? BestPartnersModelIG
    }
    
    override func didSelectItem(at index: Int) {
    }
}

extension BestPartnersSectionController: PartnerSelectionDelegate {
    func didSelectPartner(partnerId: Int) {
        delegate?.didSelectPartner(partnerId: partnerId)
    }
}

// MARK: - SuplementaryViews

extension BestPartnersSectionController {
    func supportedElementKinds() -> [String] {
        return [UICollectionView.elementKindSectionHeader]
    }

    func viewForSupplementaryElement(ofKind elementKind: String, at index: Int) -> UICollectionReusableView {
        switch elementKind {
        case UICollectionView.elementKindSectionHeader:
            return userHeaderView(atIndex: index)
        default:
            return UICollectionReusableView()
        }
    }
    
    func sizeForSupplementaryView(ofKind elementKind: String, at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: context.containerSize.width - 40, height: 40)
    }
    
    // private funcs
    
    private func userHeaderView(atIndex index: Int) -> UICollectionReusableView {
        guard let headerView = collectionContext?.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, for: self, class: CUSectionHeaderView.self, at: index) as? CUSectionHeaderView else {
            return UICollectionReusableView()
        }
        headerView.delegate = self
        if let item = item {
            headerView.setup(titleText: item.name, isRightButtonHidden: isHeaderButtonHidden, isLoading: item.isLoading )
        }
        return headerView
    }
}

extension BestPartnersSectionController: CUSectionHeaderViewDelegate {
    func viewAllTapped() {
        delegate?.viewAllTapped(type: .BestPartners)
    }
}
