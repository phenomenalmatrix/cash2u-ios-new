//
//  GuestPartnerModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 7/8/22.
//

import IGListKit

final class GuestPartnerModelIG: ListDiffable {
    
    let id = "GuestPartner"
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? GuestPartnerModelIG else { return false }
        return id == object.id
    }
}
