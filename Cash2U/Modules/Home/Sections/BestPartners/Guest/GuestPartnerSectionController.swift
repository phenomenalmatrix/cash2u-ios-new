//
//  GuestPartnerSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 7/8/22.
//

import UIKit
import IGListKit

protocol GuestPartnerSectionControllerDelegate: AnyObject {
    func didSelect()
}

class GuestPartnerSectionController: ListSectionController {
    
    weak var delegate: GuestPartnerSectionControllerDelegate? = nil
    
    private var item: GuestPartnerModelIG?

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 0, left: 0, bottom: 32, right: 0)
    }
    
    override func numberOfItems() -> Int {
        return 1
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width) - 40, height: 190)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUGuestPartnerView = collectionContext!.dequeueReusableCell(of: CUGuestPartnerView.self, for: self, at: index) as? CUGuestPartnerView else {
            return UICollectionViewCell()
        }
        
        if let item = item {
            
        }
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? GuestPartnerModelIG
    }
    
    override func didSelectItem(at index: Int) {
        delegate?.didSelect()
    }
}
