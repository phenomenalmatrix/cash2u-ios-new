//
//  VersionSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 4/8/22.
//

import UIKit
import IGListKit

class VersionSectionController: ListSectionController {
    
    private var item: VersionModelIG?

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 0, left: 0, bottom: 70, right: 0)
    }
    
    override func numberOfItems() -> Int {
        return 1
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width), height: 80)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUVersionView = collectionContext!.dequeueReusableCell(of: CUVersionView.self, for: self, at: index) as? CUVersionView else {
            return UICollectionViewCell()
        }
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? VersionModelIG
    }
    
    override func didSelectItem(at index: Int) {
//        delegate?.selectSectionController(self)
//        if let cell = self.collectionContext?.cellForItem(at: index, sectionController: self) as? DZBigStickerCell {
//            if item?.isAnimated == true {
//                cell.animatedLogoImageView.startAnimating()
//            } else {}
//        }
    }
}
