//
//  VersionModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 4/8/22.
//

import IGListKit

final class VersionModelIG: ListDiffable {
    
    let id = "Version"
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? VersionModelIG else { return false }
        return id == object.id
    }
}
