//
//  WalletModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 4/8/22.
//

import IGListKit

final class CurrencyModelIG: ListDiffable {
    
    let id = "Currency"
    let type: Int
    
    init(type: Int) {
        self.type = type
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? CurrencyModelIG else { return false }
        return id == object.id && type == object.type
    }
}
