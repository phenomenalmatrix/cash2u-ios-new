//
//  WalletSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 3/8/22.
//

import UIKit
import IGListKit

class CurrencySectionController: ListSectionController {
    
    private var item: CurrencyModelIG?

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 0, left: 0, bottom: 32, right: 0)
    }
    
    override func numberOfItems() -> Int {
        return 1
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width) - 40, height: 205)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUCurrencyOperationsView = collectionContext!.dequeueReusableCell(of: CUCurrencyOperationsView.self, for: self, at: index) as? CUCurrencyOperationsView else {
            return UICollectionViewCell()
        }
        
        if let item = item {
            cell.setup(isArrowHidden: item.type == 0 ? true : false)
        }
        
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? CurrencyModelIG
    }
    
    override func didSelectItem(at index: Int) {
//        delegate?.selectSectionController(self)
//        if let cell = self.collectionContext?.cellForItem(at: index, sectionController: self) as? DZBigStickerCell {
//            if item?.isAnimated == true {
//                cell.animatedLogoImageView.startAnimating()
//            } else {}
//        }
    }
}
