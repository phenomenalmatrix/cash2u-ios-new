//
//  RemotePaymentSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 13/10/22.
//

import UIKit
import IGListKit

protocol RemotePaymentSectionControllerDelegate: AnyObject {
    func onButtonTapped(type: Int)
}

class RemotePaymentSectionController: ListSectionController {
    
    weak var delegate: RemotePaymentSectionControllerDelegate? = nil
    
    private var item: RemotePaymentModelIG?

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 0, left: 0, bottom: 32, right: 0)
        minimumLineSpacing = 20
    }
    
    override func numberOfItems() -> Int {
        return 2
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        
        if index == 0 {
            return CGSize(width: (context.containerSize.width) - 40, height: 80)
        } else {
            return CGSize(width: (context.containerSize.width) - 40, height: 250)
        }
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        
        if index == 0 {
            
            guard let cell: CURemotePaymentTopView = collectionContext!.dequeueReusableCell(of: CURemotePaymentTopView.self, for: self, at: index) as? CURemotePaymentTopView else {
                return UICollectionViewCell()
            }
            return cell
            
        } else {
            
            guard let cell: CURemotePaymentView = collectionContext!.dequeueReusableCell(of: CURemotePaymentView.self, for: self, at: index) as? CURemotePaymentView else {
                return UICollectionViewCell()
            }
            
            if let item = item {
                cell.setup()
                cell.delegate = self
            }
            return cell
            
        }
    }

    override func didUpdate(to object: Any) {
        item = object as? RemotePaymentModelIG
    }
    
}

extension RemotePaymentSectionController: CURemotePaymentViewDelegate {
    func onButtonTapped(type: Int) {
        delegate?.onButtonTapped(type: type)
    }
}
