//
//  RemotePaymentModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 13/10/22.
//

import IGListKit

final class RemotePaymentModelIG: ListDiffable {
    
    let id = "RemotePaymentModelIG"
    let name: String
    
    init(name: String) {
        self.name = name
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? RemotePaymentModelIG else { return false }
        return id == object.id && name == object.name
    }
}
