//
//  ButtonSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 7/8/22.
//

import UIKit
import IGListKit

extension ButtonSectionControllerDelegate {
    func onButtonTapped(action: String = "") {
        return onButtonTapped(action: action)
    }
}

protocol ButtonSectionControllerDelegate: AnyObject {
    func onButtonTapped(action: String)
}

class ButtonSectionController: ListSectionController {
    
    weak var delegate: ButtonSectionControllerDelegate? = nil
    
    private var item: ButtonSectionModelIG?

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 0, left: 0, bottom: 16, right: 0)
    }
    
    override func numberOfItems() -> Int {
        return 1
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width) - 40, height: 64)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUButtonSectionView = collectionContext!.dequeueReusableCell(of: CUButtonSectionView.self, for: self, at: index) as? CUButtonSectionView else {
            return UICollectionViewCell()
        }
        
        cell.delegate = self
        
        if let item = item {
            cell.setup(title: item.title)
        }
        
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? ButtonSectionModelIG
    }
}

extension ButtonSectionController: CUButtonSectionViewDelegate {
    func buttonTapped() {
        if let item = item {
            delegate?.onButtonTapped(action: item.action)
        }
    }
}
