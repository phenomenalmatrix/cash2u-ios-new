//
//  ButtonSectionModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 7/8/22.
//

import IGListKit

final class ButtonSectionModelIG: ListDiffable {
    
    let id = "Button"
    let title: String
    var action: String = ""
    
    init(title: String, action: String = "") {
        self.title = title
        self.action = action
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? ButtonSectionModelIG else { return false }
        return id == object.id
    }
}
