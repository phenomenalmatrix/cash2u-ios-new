//
//  BonusCardsSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 4/8/22.
//

import UIKit
import IGListKit

class BonusCardsSectionController: ListSectionController, ListSupplementaryViewSource {
    
    weak var delegate: SectionControllerHeaderDelegate? = nil
    
    private var item: BonusCardsModelIG?

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 20, left: 0, bottom: 32, right: 0)
        supplementaryViewSource = self
    }
    
    override func numberOfItems() -> Int {
        return 1
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width), height: 130)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUBonusCardsView = collectionContext!.dequeueReusableCell(of: CUBonusCardsView.self, for: self, at: index) as? CUBonusCardsView else {
            return UICollectionViewCell()
        }
        
        if let item = item {
            cell.delegate = self
            cell.setup(isLoading: item.isLoading )
        }
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? BonusCardsModelIG
    }
    
    override func didSelectItem(at index: Int) {
    }
}

extension BonusCardsSectionController: CUBonusCardsViewDelegate {
    func didSelect() {
        let vc = BonusModuleConfigurator.build()
        vc.hidesBottomBarWhenPushed = true
        self.viewController?.navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: - SuplementaryViews

extension BonusCardsSectionController {
    func supportedElementKinds() -> [String] {
        return [UICollectionView.elementKindSectionHeader]
    }

    func viewForSupplementaryElement(ofKind elementKind: String, at index: Int) -> UICollectionReusableView {
        switch elementKind {
        case UICollectionView.elementKindSectionHeader:
            return userHeaderView(atIndex: index)
        default:
            return UICollectionReusableView()
        }
    }
    
    func sizeForSupplementaryView(ofKind elementKind: String, at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: context.containerSize.width - 40, height: 40)
    }
    
    // private funcs
    
    private func userHeaderView(atIndex index: Int) -> UICollectionReusableView {
        guard let headerView = collectionContext?.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, for: self, class: CUSectionHeaderView.self, at: index) as? CUSectionHeaderView else {
            return UICollectionReusableView()
        }
        headerView.delegate = self
        
        headerView.setup(titleText: "Бонусные карты", isLoading: item?.isLoading ?? false)
        return headerView
    }
}

extension BonusCardsSectionController: CUSectionHeaderViewDelegate {
    func viewAllTapped() {
        delegate?.viewAllTapped(type: .BonusCards)
    }
}
