//
//  SupportSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 4/8/22.
//

import UIKit
import IGListKit

class SupportSectionController: ListSectionController {
    
    private var item: SupportModelIG?

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 35, left: 0, bottom: 10, right: 0)
    }
    
    override func numberOfItems() -> Int {
        return 1
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width) - 40, height: ((context.containerSize.width) / 2.4) + 35)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUSupportView = collectionContext!.dequeueReusableCell(of: CUSupportView.self, for: self, at: index) as? CUSupportView else {
            return UICollectionViewCell()
        }
        cell.setup(title: "Центр поддержки", subtitle: "Вы найдете ответы на все \nинтересующие вас вопросы", image: "support_robot_dog", supportBackground: true)
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? SupportModelIG
    }
    
    override func didSelectItem(at index: Int) {
        print(index)
    }
}
