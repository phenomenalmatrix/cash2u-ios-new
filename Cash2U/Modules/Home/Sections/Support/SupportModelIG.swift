//
//  SupportModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 4/8/22.
//

import Foundation
import IGListKit

final class SupportModelIG: ListDiffable {
    
    let id = "Support"
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? SupportModelIG else { return false }
        return id == object.id
    }
}
