//
//  CitySectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 5/8/22.
//

import UIKit
import IGListKit

protocol GreetingsSectionControllerDelegate: AnyObject {
    func settingsTapped()
}

class GreetingsSectionController: ListSectionController {
    
    weak var delegate: GreetingsSectionControllerDelegate? = nil
    
    private var item: GreetingsSectionModelIG?

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 20, left: 0, bottom: 32, right: 0)
    }
    
    override func numberOfItems() -> Int {
        return 1
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width), height: 50)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUGreetingsView = collectionContext!.dequeueReusableCell(of: CUGreetingsView.self, for: self, at: index) as? CUGreetingsView else {
            return UICollectionViewCell()
        }
        
        if let item = item {
            cell.setup(title: item.name, type: item.type)
        }
        
        cell.delegate = self
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? GreetingsSectionModelIG
    }
    
}

extension GreetingsSectionController: CUGreetingsViewDelegate {
    func settingsTapped() {
        delegate?.settingsTapped()
    }
}

