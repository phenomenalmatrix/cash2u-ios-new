//
//  CityModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 5/8/22.
//

import IGListKit

final class GreetingsSectionModelIG: ListDiffable {
    
    let id = "Greetings"
    let name: String
    let type: Int
    
    init(name: String, type: Int) {
        self.name = name
        self.type = type
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? GreetingsSectionModelIG else { return false }
        return id == object.id && name == object.name
    }
}
