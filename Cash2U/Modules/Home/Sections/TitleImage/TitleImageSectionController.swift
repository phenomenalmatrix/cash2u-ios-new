//
//  TitleImageSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 7/8/22.
//

import UIKit
import IGListKit


protocol TitleImageSectionControllerDelegate: AnyObject {
    func didSelect(type: TitleImageModelType)
}

class TitleImageSectionController: ListSectionController {
    
    weak var delegate: TitleImageSectionControllerDelegate? = nil
    
    private var item: TitleImageModelIG?

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 0, left: 0, bottom: 32, right: 0)
    }
    
    override func numberOfItems() -> Int {
        return 1
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width) - 40, height: ((context.containerSize.width) / 2.1))
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUSupportView = collectionContext!.dequeueReusableCell(of: CUSupportView.self, for: self, at: index) as? CUSupportView else {
            return UICollectionViewCell()
        }
        
        if let item = item {
            cell.setup(title: item.title, subtitle: item.subtitle, image: item.image, backgroundColor: item.backgroundColor)
        }
        
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? TitleImageModelIG
    }
    
    override func didSelectItem(at index: Int) {
        if let item {
            delegate?.didSelect(type: item.type)
        }
    }
}
