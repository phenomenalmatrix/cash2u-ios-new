//
//  TitleImageModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 7/8/22.
//

import Foundation
import IGListKit

enum TitleImageModelType: String {
    case wallet = "Wallet"
    case partners = "Partners"
    case installment = "Installment"
}

final class TitleImageModelIG: ListDiffable {
    
    let type: TitleImageModelType
    let title: String
    let subtitle: String
    let image: String
    let backgroundColor: UIColor?
    
    init(type: TitleImageModelType, title: String, subtitle: String, image: String, backgroundColor: UIColor? = .mainColor) {
        self.type = type
        self.title = title
        self.subtitle = subtitle
        self.image = image
        self.backgroundColor = backgroundColor
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return type.rawValue as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? TitleImageModelIG else { return false }
        return type == object.type && title == object.title && subtitle == object.subtitle
    }
}
