//
//  LogoSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 8/8/22.
//

import UIKit
import IGListKit

class LogoSectionController: ListSectionController {
    
    private var item: LogoSectionModelIG?

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 20, left: 0, bottom: 32, right: 0)
    }
    
    override func numberOfItems() -> Int {
        return 1
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width) - 40, height: 32)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CULogoSectionView = collectionContext!.dequeueReusableCell(of: CULogoSectionView.self, for: self, at: index) as? CULogoSectionView else {
            return UICollectionViewCell()
        }
        
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? LogoSectionModelIG
    }
}
