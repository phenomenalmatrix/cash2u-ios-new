//
//  LogoSectionModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 8/8/22.
//

import IGListKit

final class LogoSectionModelIG: ListDiffable {
    
    let id = "Logo"
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? LogoSectionModelIG else { return false }
        return id == object.id
    }
}
