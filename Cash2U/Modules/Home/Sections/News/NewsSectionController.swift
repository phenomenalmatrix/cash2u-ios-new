//
//  NewsSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 4/8/22.
//

import UIKit
import IGListKit

enum HomeSectionType: String {
    case BestPartners = "BestPartners"
    case BonusCards = "BonusCards"
    case News = "News"
}

protocol SectionControllerHeaderDelegate: AnyObject {
    func viewAllTapped(type: HomeSectionType)
}

protocol SectionControllerDelegate: AnyObject {
    func selectSection(_ sectionController: ListSectionController)
}


class NewsSectionController: ListSectionController, ListSupplementaryViewSource {
    
    weak var delegate: SectionControllerHeaderDelegate? = nil
    
    private var item: NewsModelIG?

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 20, left: 0, bottom: 32, right: 0)
        supplementaryViewSource = self
    }
    
    override func numberOfItems() -> Int {
        return 1
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width), height: ((context.containerSize.width) / 2.3) + 115)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUNewsView = collectionContext!.dequeueReusableCell(of: CUNewsView.self, for: self, at: index) as? CUNewsView else {
            return UICollectionViewCell()
        }
        
        if let item = item {
            
        }
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? NewsModelIG
    }
    
    override func didSelectItem(at index: Int) {
//        delegate?.selectSectionController(self)
//        if let cell = self.collectionContext?.cellForItem(at: index, sectionController: self) as? DZBigStickerCell {
//            if item?.isAnimated == true {
//                cell.animatedLogoImageView.startAnimating()
//            } else {}
//        }
    }
}

// MARK: - SuplementaryViews

extension NewsSectionController {
    func supportedElementKinds() -> [String] {
        return [UICollectionView.elementKindSectionHeader]
    }

    func viewForSupplementaryElement(ofKind elementKind: String, at index: Int) -> UICollectionReusableView {
        switch elementKind {
        case UICollectionView.elementKindSectionHeader:
            return userHeaderView(atIndex: index)
        default:
            return UICollectionReusableView()
        }
    }
    
    func sizeForSupplementaryView(ofKind elementKind: String, at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: context.containerSize.width - 40, height: 40)
    }
    
    // private funcs
    
    private func userHeaderView(atIndex index: Int) -> UICollectionReusableView {
        guard let headerView = collectionContext?.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, for: self, class: CUSectionHeaderView.self, at: index) as? CUSectionHeaderView else {
            return UICollectionReusableView()
        }
        headerView.delegate = self
        headerView.setup(titleText: "Новости")
        return headerView
    }
}

extension NewsSectionController: CUSectionHeaderViewDelegate {
    func viewAllTapped() {
        delegate?.viewAllTapped(type: .News)
    }
}
