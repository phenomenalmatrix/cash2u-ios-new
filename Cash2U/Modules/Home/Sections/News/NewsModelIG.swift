//
//  NewsModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 4/8/22.
//

import Foundation
import IGListKit

final class NewsModelIG: ListDiffable {
    
    let id = "News"
    let name: String
    
    init(name: String) {
        self.name = name
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? NewsModelIG else { return false }
        return id == object.id && name == object.name
    }
}
