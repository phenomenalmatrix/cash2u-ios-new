//
//  HomeInteractor.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/5/22.
//  
//

import Foundation

final class HomeInteractor {
    weak var presenter: HomeInteractorToPresenterProtocol?
}

extension HomeInteractor: HomeInteractorProtocol {
}
