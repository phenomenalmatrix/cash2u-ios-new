//
//  MainTabBar.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/5/22.
//

import Foundation
import UIKit
import AppAuth

final class MainTabBar: UITabBarController {
    
    private var authState: OIDAuthState?
    
    var isAfterAuthorization = false {
        didSet {
            if !isAfterAuthorization {
                if UserDefaultsService.shared.isSecured {
                    DispatchQueue.main.async {
                        let vc = EnterPinCodeModuleBuilder.build()
                        vc.modalPresentationStyle = .overFullScreen
                        vc.modalTransitionStyle = .crossDissolve
                        self.navigationController?.present(vc, animated: false, completion: nil)
                    }
                }
            }
        }
    }
    
    private lazy var home = HomeModuleConfigurator.build()
    private lazy var wallet = WalletModuleBuilder.build()
    private lazy var installment = InstallmentModuleBuilder.build()
    private lazy var partners = PartnersModuleConfigurator.build()
    private lazy var menu = MenuModuleBuilder.build()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .mainColor
        configureViewControllers()
        
        //        let authorizationEndpoint = URL(string: "\(ProjectConfig.baseAuthUrl)/connect/token")!
        //        let tokenEndpoint = URL(string: "\(ProjectConfig.baseAuthUrl)/connect/token")!
        //        let configuration = OIDServiceConfiguration(authorizationEndpoint: authorizationEndpoint,
        //                                                    tokenEndpoint: tokenEndpoint)
        
        //        let request = OIDTokenRequest(
        //            configuration: configuration,
        //            grantType: OIDGrantTypeRefreshToken,
        //            authorizationCode: nil,
        //            redirectURL: nil,
        //            clientID: "ropc",
        //            clientSecret: "secret",
        //            scope: nil,
        //            refreshToken: KeychainService.shared.refreshToken,
        //            codeVerifier: nil,
        //            additionalParameters: nil)
        //
        //        OIDAuthorizationService.perform(request) { response, error in
        //            print(response?.refreshToken)
        //            KeychainService.shared.saveToken(token: UserTokenResponseModel(accessToken: response?.accessToken ?? "", refreshToken: response?.refreshToken ?? ""))
        //        }
        
        
        //        let request = OIDTokenRequest(
        //            configuration: configuration,
        //            grantType: OIDGrantTypePassword,
        //            authorizationCode: nil,
        //            redirectURL: nil,
        //            clientID: "ropc",
        //            clientSecret: "secret",
        //            scope: nil,
        //            refreshToken: nil,
        //            codeVerifier: nil,
        //            additionalParameters: ["username":"996777777777", "password":"666666"])
        //
        //        OIDAuthorizationService.perform(request) { response, error in
        //            KeychainService.shared.saveToken(token: UserTokenResponseModel(accessToken: response?.accessToken ?? "", refreshToken: response?.refreshToken ?? ""))
        //
        //        }
        
        //        let aa = OIDAuthorizationRequest(
        //            configuration: configuration,
        //            clientId: "ropc",
        //            clientSecret: "secret",
        //            scope: nil,
        //            redirectURL: nil,
        //            responseType: "password",
        //            state: nil,
        //            nonce: nil,
        //            codeVerifier: nil,
        //            codeChallenge: nil,
        //            codeChallengeMethod: "POST",
        //            additionalParameters: ["username":"996777777777", "password":"666666"])
        
        //        let authorizationEndpoint = URL(string: "\(ProjectConfig.baseAuthUrl)/connect/token")!
        //        let tokenEndpoint = URL(string: "\(ProjectConfig.baseAuthUrl)/connect/token")!
        //        let configuration = OIDServiceConfiguration(authorizationEndpoint: authorizationEndpoint,
        //                                                    tokenEndpoint: tokenEndpoint)
        //
        
        //        let authorizationEndpoint = URL(string: "\(ProjectConfig.baseAuthUrl)/passwordless/v1/start")!
        //        let tokenEndpoint = URL(string: "\(ProjectConfig.baseAuthUrl)/passwordless/v1/start")!
        //        let configuration = OIDServiceConfiguration(authorizationEndpoint: authorizationEndpoint,
        //                                                    tokenEndpoint: tokenEndpoint)
        //
        //        let request = OIDAuthorizationRequest(
        //            configuration: configuration,
        //            clientId: "ropc",
        //            clientSecret: "secret",
        //            scope: nil,
        //            redirectURL: nil,
        //            responseType: OIDResponseTypeCode,
        //            state: nil,
        //            nonce: nil,
        //            codeVerifier: nil,
        //            codeChallenge: nil,
        //            codeChallengeMethod: nil,
        //            additionalParameters: ["phone_number":"9976777777777", "is_public_offer_read":"true", "application_signature":""])
        //
        //        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //
        //        appDelegate.currentAuthorizationFlow =
        //            OIDAuthState.authState(byPresenting: request, presenting: self) { authState, error in
        //          if let authState = authState {
        ////            self.setAuthState(authState)
        //            print("Got authorization tokens. Access token: " +
        //                  "\(authState.lastTokenResponse?.accessToken ?? "nil")")
        //          } else {
        //            print("Authorization error: \(error?.localizedDescription ?? "Unknown error")")
        ////            self.setAuthState(nil)
        //          }
        //        }
        
        
        
        //        print(self.authState?.lastTokenResponse?.accessToken)
        
    }
    
    private func configureViewControllers() {
        
        
        let home = templateNavigationController(titleName: "Главная", image: UIImage(named: "home_tabbar")?.withTintColor(.grayColor ?? .gray, renderingMode: .alwaysOriginal),
                                                selectedImage: UIImage(named: "home_tabbar")?.withTintColor(.white, renderingMode: .alwaysOriginal),
                                                rootViewController: home)
        
        let wallet = templateNavigationController(titleName: "Кошелек", image: UIImage(named: "wallet_tabbar")?.withTintColor(.grayColor ?? .gray, renderingMode: .alwaysOriginal),
                                                   selectedImage: UIImage(named: "wallet_tabbar")?.withTintColor(.white, renderingMode: .alwaysOriginal),
                                                   rootViewController: wallet)
        
        let installment = templateNavigationController(titleName: "Частями", image: UIImage(named: "installment_tabbar")?.withTintColor(.grayColor ?? .gray, renderingMode: .alwaysOriginal),
                                                   selectedImage: UIImage(named: "installment_tabbar")?.withTintColor(.white, renderingMode: .alwaysOriginal),
                                                   rootViewController: installment)
        
        let partners = templateNavigationController(titleName: "Партнеры", image: UIImage(named: "partners_tabbar")?.withTintColor(.grayColor ?? .gray, renderingMode: .alwaysOriginal),
                                                    selectedImage: UIImage(named: "partners_tabbar")?.withTintColor(.white, renderingMode: .alwaysOriginal),
                                                    rootViewController: partners)
        
        let menu = templateNavigationController(titleName: "Меню", image: UIImage(named: "menu_tabbar")?.withTintColor(.grayColor ?? .gray, renderingMode: .alwaysOriginal),
                                                   selectedImage: UIImage(named: "menu_tabbar")?.withTintColor(.white, renderingMode: .alwaysOriginal),
                                                   rootViewController: menu)
        
        self.viewControllers = [home, wallet, installment, partners, menu]
        self.selectedIndex = 0
        self.tabBar.isTranslucent = false
        self.tabBar.clipsToBounds = true
        self.tabBar.tintColor = .mainTextColor
        self.tabBar.unselectedItemTintColor = .grayColor
        
        let tabBarAppearance: UITabBarAppearance = UITabBarAppearance()
        tabBarAppearance.configureWithDefaultBackground()
        tabBarAppearance.backgroundColor = .mainColor
        UITabBar.appearance().standardAppearance = tabBarAppearance
        
        if #available(iOS 15.0, *) {
            UITabBar.appearance().scrollEdgeAppearance = tabBarAppearance
        }
    }
    
    private func templateNavigationController(titleName: String, image: UIImage?,
                                              selectedImage: UIImage?,
                                              rootViewController: UIViewController) -> UINavigationController {
        let nav = UINavigationController(rootViewController: rootViewController)
        nav.navigationBar.standardAppearance.backgroundColor = .mainColor
        nav.tabBarItem.image = image
        nav.tabBarItem.selectedImage = selectedImage
        nav.tabBarItem.title = titleName
        return nav
    }
}
