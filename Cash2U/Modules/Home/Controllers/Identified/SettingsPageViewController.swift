//
//  SettingsPageViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 10/8/22.
//  
//

import UIKit

final class SettingsPageViewController: CUViewController {
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    var items = [UserDefaultsService.shared.homeCellTypes, UserDefaultsService.shared.homeRemoveCellTypes]
    
    private lazy var table: CUTableView = {
        let view = CUTableView(frame: .zero, style: .insetGrouped)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.dataSource = self
        view.delegate = self
        view.separatorStyle = .none
        view.rowHeight = 70
        view.isEditing = true
        view.register(CUSettingsPageCell.self)
        return view
    }()
    
    override func loadView() {
        super.loadView()
        navigationItem.title = "Настрока главной страницы"
        
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func onActionTapped(indexPath: IndexPath) {
        let item = items[indexPath.section][indexPath.row]
        if indexPath.section == 0 {
            items[0].remove(at: indexPath.row)
            items[1].append(item)
        } else {
            items[1].remove(at: indexPath.row)
            items[0].append(item)
        }
        table.reloadData()
        UserDefaultsService.shared.homeCellTypes = items[0]
        UserDefaultsService.shared.homeRemoveCellTypes = items[1]
    }
}

extension SettingsPageViewController: UITableViewDataSource, UITableViewDragDelegate, UITableViewDelegate, CUSettingsPageCellDelegate {
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }

    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = CUTitleWithArrow()
        view.setup(titleText: section == 0 ? "Используемые элементы" : "Еще элементы", isShowArrow: false)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        64
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(cellType: CUSettingsPageCell.self, for: indexPath)
        let row = items[indexPath.section][indexPath.row]
        cell.indexPath = indexPath
        cell.delegate = self
        
        if indexPath.section == 0 {
            cell.setup(title: row.description, image: "x_icon")
        } else {
            cell.setup(title: row.description, image: "add_icon")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        let dragItem = UIDragItem(itemProvider: NSItemProvider())
            dragItem.localObject = items[indexPath.row]
            return [ dragItem ]
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let sourceSection = sourceIndexPath.section
        let sourceRow = sourceIndexPath.row
        let destinationSection = destinationIndexPath.section
        let destinationRow = destinationIndexPath.row
        if sourceSection == destinationSection {
            let sourceValue = items[sourceSection][sourceRow]
            items[sourceSection][sourceRow] = items[destinationSection][destinationRow]
            items[destinationSection][destinationRow] = sourceValue
        } else {
            let sourceValue = items[sourceSection].remove(at: sourceRow)
            items[destinationSection].insert(sourceValue, at: destinationRow)
        }
        tableView.reloadData()
        UserDefaultsService.shared.homeCellTypes = items[0]
        UserDefaultsService.shared.homeRemoveCellTypes = items[1]
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}

extension SettingsPageViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        view.addSubview(table)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        table.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
