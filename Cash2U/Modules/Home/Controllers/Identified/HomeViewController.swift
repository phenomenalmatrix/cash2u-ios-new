//
//  HomeViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/5/22.
//  
//

import UIKit
import IGListKit
import SwiftyUserDefaults

enum HomeCellType: String {
    case City = "City"
    case Currency = "Курс валют"
    case BestPartners = "Лучшие партнеры"
    case OffersAndPromotions = "Предложения и акции"
    case BonusCards = "Бонусные карты"
    case News = "Новости  "
    case Support = "Support"
    case Version = "Version"
}

final class HomeViewController: CUBellViewController {
    
    var presenter: HomeViewToPresenterProtocol?
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private lazy var qrButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Оплатить QR", titleSize: 16, cornerRadius: 45 / 2)
        view.addTarget(self, action: #selector(onOpenQR), for: .touchUpInside)
        return view
    }()
    
    private lazy var adapter: ListAdapter = {
        let adapter = ListAdapter(updater: ListAdapterUpdater(), viewController: self)
        adapter.collectionView = mainCollection
        adapter.dataSource = self
        return adapter
    }()
    
    private var currencyModel = CurrencyModelIG(type: 0)
    private var bestPartnersModel = BestPartnersModelIG(name: "Лучшие партнеры", isLoading: true, items: [])
    private var offersAndPromotionsModel = OffersAndPromotionsModelIG(name: "Offers", isLoading: true)
    private var bonusCardsModel = BonusCardsModelIG(name: "Bonus", isLoading: true)
    private var newsModel = NewsModelIG(name: "News")
    
    private func createItemsData(arr: [String]) -> [ListDiffable] {
        var aa = [
        ] as [ListDiffable]
        aa.append(GreetingsSectionModelIG(name: "Привет, Бакытбек!", type: 1))
        for i in arr {
            if i == HomeCellType.Currency.rawValue {
                aa.append(self.currencyModel)
            } else if i == HomeCellType.BestPartners.rawValue {
                aa.append(self.bestPartnersModel)
            } else if i == HomeCellType.OffersAndPromotions.rawValue {
                aa.append(self.offersAndPromotionsModel)
            } else if i == HomeCellType.BonusCards.rawValue {
                aa.append(self.bonusCardsModel)
            } else if i == HomeCellType.News.rawValue {
                aa.append(self.newsModel)
            }
        }
        aa.append(SupportModelIG())
        aa.append(VersionModelIG())
        return aa
    }
    
    private lazy var itemsData: [ListDiffable] = createItemsData(arr: UserDefaultsService.shared.homeCellTypes)
    
    private lazy var mainCollection: CUCollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        let mainCollection = CUCollectionView(
            frame: CGRect.zero,
            collectionViewLayout: flowLayout
        )
        mainCollection.translatesAutoresizingMaskIntoConstraints = false
        mainCollection.alwaysBounceVertical = true
        return mainCollection
    }()
    
    private lazy var selectedCity: CUSelectedCityView = {
        let view = CUSelectedCityView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.cornerRadius = 44 / 2
        view.setup(title: "Бишкек")
        view.setDimensions(height: 44, width: 200)
        view.addTarget(self, action: #selector(onSelectedCityTapped), for: .touchUpInside)
        return view
    }()
    
    private var settinsListener: DefaultsDisposable?
    
    override func loadView() {
        super.loadView()
        setNavBar()
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    func setNavBar() {
        let cityItem = UIBarButtonItem.init(customView: selectedCity)
        navigationItem.leftBarButtonItems = [cityItem]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        adapter.performUpdates(animated: true, completion: nil)
        
        settinsListener = Defaults.observe(\.homeCellTypes) { update in
            
            if let arr = update.newValue {
                self.itemsData = self.createItemsData(arr: arr)
            }
            
            self.adapter.performUpdates(animated: true, completion: nil)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.itemsData.insert(RemotePaymentModelIG(name: "ASDASDdd"), at: 1)
            self.adapter.performUpdates(animated: true)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            guard let index = self.itemsData.firstIndex(where: {$0.diffIdentifier().description == "BestPartners"}) else {return}
            self.bestPartnersModel = BestPartnersModelIG(name: "Лучшие партнеры", isLoading: false, items: [])
            self.itemsData[index] = self.bestPartnersModel
            self.adapter.performUpdates(animated: true)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 7) {
            guard let index = self.itemsData.firstIndex(where: {$0.diffIdentifier().description == "OffersAndPromotions"}) else {return}
            self.offersAndPromotionsModel = OffersAndPromotionsModelIG(name: "Offers", isLoading: false)
            self.itemsData[index] = self.offersAndPromotionsModel
            self.adapter.performUpdates(animated: true)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 9) {
            guard let index = self.itemsData.firstIndex(where: {$0.diffIdentifier().description == "BonusCards"}) else {return}
            self.bonusCardsModel = BonusCardsModelIG(name: "Bonus", isLoading: false)
            self.itemsData[index] = self.bonusCardsModel
            self.adapter.performUpdates(animated: true)
        }
    }
    
    deinit {
        settinsListener?.dispose()
    }
    
    // MARK: - UIActions
    
    @objc private func onSelectedCityTapped() {
        print("City")
    }
}

// MARK: - ListAdapterDataSource

extension HomeViewController: ListAdapterDataSource {
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        itemsData
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        if object is CurrencyModelIG {
            let section = CurrencySectionController()
            return section
        } else if object is RemotePaymentModelIG {
            let section = RemotePaymentSectionController()
            section.delegate = self
            return section
        } else if object is BestPartnersModelIG {
            let section = BestPartnersSectionController(isHeaderButtonHidden: false)
            section.delegate = self
            return section
        } else if object is OffersAndPromotionsModelIG {
            return OffersAndPromotionsSectionController()
        } else if object is BonusCardsModelIG {
            let section = BonusCardsSectionController()
            section.delegate = self
            return section
        } else if object is NewsModelIG {
            let section = NewsSectionController()
            section.delegate = self
            return section
        } else if object is SupportModelIG {
            return SupportSectionController()
        } else if object is VersionModelIG {
            return VersionSectionController()
        } else if object is GreetingsSectionModelIG {
            let section = GreetingsSectionController()
            section.delegate = self
            return section
        } else {
            return ListSectionController()
        }
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }
}

extension HomeViewController: HomeViewProtocol {
}

extension HomeViewController: RemotePaymentSectionControllerDelegate {
    func onButtonTapped(type: Int) {
        if type == 0 {
            
        } else {
            
        }
    }
}

extension HomeViewController: GreetingsSectionControllerDelegate {
    func settingsTapped() {
        let vc = SettingsPageViewController()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension HomeViewController: PartnerSelectionDelegate {
    func didSelectPartner(partnerId: Int) {
        let vc = PartnerPageModuleBuilder.build(partnerId: partnerId)
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension HomeViewController: SectionControllerHeaderDelegate {
    
    func viewAllTapped(type: HomeSectionType) {
        switch type {
        case .BestPartners:
            print("best")
        case .BonusCards:
            print("cards")
        case .News:
            let vc = NewsCenterModuleConfigurator.build()
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension HomeViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        
        view.addSubview(mainCollection)
        view.addSubview(qrButton)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        mainCollection.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        qrButton.snp.makeConstraints { make in
            make.bottom.equalTo(view.safeArea.bottom).offset(-12)
            make.centerX.equalToSuperview()
            make.width.equalTo(ScreenHelper.getWidth / 2.5)
            make.height.equalTo(45)
        }
    }
}
