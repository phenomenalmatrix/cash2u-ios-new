//
//  RegisteredHomeViewControllerViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 5/8/22.
//  
//

import UIKit
import IGListKit

final class ParentRegisteredHomeViewController: CUViewController {
    
    var isAfterAuthorization = false
    
    override func loadView() {
        super.loadView()
        
        view.backgroundColor = .mainColor
        
        let vc = RegisteredHomeViewController()
        vc.isAfterAuthorization = self.isAfterAuthorization
        let nav = UINavigationController(rootViewController: vc)
        nav.navigationBar.standardAppearance.backgroundColor = .mainColor
        addChild(nav)
        view.addSubview(nav.view)
        nav.didMove(toParent: self)
        
    }
}

final class RegisteredHomeViewController: CUViewController {
    
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    var isAfterAuthorization = false {
        didSet {
            if !isAfterAuthorization {
                if UserDefaultsService.shared.isSecured {
                    DispatchQueue.main.async {
                        let vc = EnterPinCodeModuleBuilder.build()
                        vc.modalPresentationStyle = .overFullScreen
                        vc.modalTransitionStyle = .crossDissolve
                        self.navigationController?.present(vc, animated: false, completion: nil)
                    }
                }
            }
        }
    }
    
    private lazy var adapter: ListAdapter = {
        let adapter = ListAdapter(updater: ListAdapterUpdater(), viewController: self)
        adapter.collectionView = mainCollection
        adapter.dataSource = self
        return adapter
    }()
    
    private func createItemsData() -> [ListDiffable] {
        var aa = [
        ] as [ListDiffable]
        aa.append(GreetingsSectionModelIG(name: "Привет!", type: 0))
        aa.append(CurrencyModelIG(type: 0))
        aa.append(TitleImageModelIG(type: .wallet, title: "Кошелек", subtitle: "Мультивалютные счета, \nпереводы, обмен валют, \nоплата услуг и многое другое", image: "wallet", backgroundColor: .brownColor))
        aa.append(GuestPartnerModelIG())
        aa.append(TitleImageModelIG(type: .installment, title: "Оплата частями", subtitle: "Удобная оплата частями \nбез комиссий и переплат", image: "somWithLights", backgroundColor: .secondBlueColor))
        aa.append(NewsModelIG(name: "ASDdd"))
        aa.append(SupportModelIG())
        aa.append(VersionModelIG())
        aa.append(ButtonSectionModelIG(title: "Подтвердить личность"))
        return aa
    }
    
    private lazy var itemsData: [ListDiffable] = createItemsData()
    
    private lazy var mainCollection: CUCollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        let mainCollection = CUCollectionView(
            frame: CGRect.zero,
            collectionViewLayout: flowLayout
        )
        mainCollection.translatesAutoresizingMaskIntoConstraints = false
        mainCollection.alwaysBounceVertical = true
        return mainCollection
    }()
    
    private lazy var selectedCity: CUSelectedCityView = {
        let view = CUSelectedCityView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.cornerRadius = 44 / 2
        view.setup(title: "Бишкек")
        view.setDimensions(height: 44, width: 200)
        view.addTarget(self, action: #selector(onSelectedCityTapped), for: .touchUpInside)
        return view
    }()
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = .mainColor
        setNavBar()
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    func setNavBar() {
        let cityItem = UIBarButtonItem.init(customView: selectedCity)
        navigationItem.leftBarButtonItems = [cityItem]
        navigationItem.rightBarButtonItem = .init(image: UIImage(systemName: "bell"), style: .plain, target: self, action: #selector(onBellTapped))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        adapter.performUpdates(animated: true, completion: nil)
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//            let vc = IdentityApplicationModuleConfigurator.build()
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
    }
    
    // MARK: - UIActions
    
    @objc private func onBellTapped() {
        print("Bell")
    }
    
    @objc private func onSelectedCityTapped() {
        print("City")
    }
}

// MARK: - ListAdapterDataSource

extension RegisteredHomeViewController: ListAdapterDataSource {
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        itemsData
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        if object is CurrencyModelIG {
            let section = CurrencySectionController()
            return section
        } else if object is NewsModelIG {
            let section = NewsSectionController()
            section.delegate = self
            return section
        } else if object is SupportModelIG {
            return SupportSectionController()
        } else if object is GreetingsSectionModelIG {
            return GreetingsSectionController()
        } else if object is VersionModelIG {
            return VersionSectionController()
        } else if object is TitleImageModelIG {
            let section = TitleImageSectionController()
            section.delegate = self
            return section
        } else if object is GuestPartnerModelIG {
            let section = GuestPartnerSectionController()
            section.delegate = self       
            return section
        } else if object is ButtonSectionModelIG {
            let section = ButtonSectionController()
            section.delegate = self
            return section
        }  else {
            return ListSectionController()
        }
    }
     
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }
}

extension RegisteredHomeViewController: GreetingsSectionControllerDelegate {
    
    func settingsTapped() {
    }
}

extension RegisteredHomeViewController: GuestPartnerSectionControllerDelegate {
    func didSelect() {
        let vc = PartnersModuleConfigurator.buildGuest()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension RegisteredHomeViewController: TitleImageSectionControllerDelegate {
    func didSelect(type: TitleImageModelType) {
        switch type {
        case .wallet:
            break
        case .partners:
            break
        case .installment:
            let vc = InstallmentModuleBuilder.build(isGuest: true)
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension RegisteredHomeViewController: ButtonSectionControllerDelegate {
    
    func onButtonTapped(action: String) {
//        KeychainService.shared.deleteAll()
//        UserDefaultsService.shared.removeAll()
//        self .dismiss(animated: true) {
//            let loginResponse = ["becomeFirstResponder": true]
//            NotificationCenter.default
//                .post(name: .phoneBecomeFirstResponder, object: nil, userInfo: loginResponse)
//        }
        let vc = IdentityApplicationModuleConfigurator.build()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension RegisteredHomeViewController: SectionControllerHeaderDelegate {
    
    func viewAllTapped(type: HomeSectionType) {
        switch type {
        case .News:
            let vc = NewsCenterModuleConfigurator.build()
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
    }
}

extension RegisteredHomeViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        
        view.addSubview(mainCollection)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        mainCollection.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
