//
//  HomePresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/5/22.
//  
//

import Foundation

final class HomePresenter {
    
    weak var view: HomeViewProtocol?
    var interactor: HomeInteractorProtocol?
    var router: HomeRouterProtocol?
    
}

extension HomePresenter: HomeViewToPresenterProtocol {
}

extension HomePresenter: HomeInteractorToPresenterProtocol {
}
