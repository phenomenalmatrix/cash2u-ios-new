//
//  HomeModuleBuilder.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/5/22.
//  
//

import UIKit

final class HomeModuleConfigurator {
    /// Собрать модуль
    class func build() -> UIViewController {
        let view = HomeViewController()
        let presenter = HomePresenter()
        let interactor = HomeInteractor()
        let router = HomeRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
    
    class func buildRegistered(isAfterAuthorization: Bool) -> UIViewController {
        let view = ParentRegisteredHomeViewController()
        view.isAfterAuthorization = isAfterAuthorization
        return view
    }
    
    class func buildGuest() -> UIViewController {
        let view = ParentGuestHomeViewController()
        return view
    }
}
