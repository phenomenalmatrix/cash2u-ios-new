//
//  SearchPresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 23/5/22.
//  
//

import Foundation

final class SearchPresenter {
    
    weak var view: SearchViewProtocol?
    var interactor: SearchInteractorProtocol?
    var router: SearchRouterProtocol?
    
}

extension SearchPresenter: SearchViewToPresenterProtocol {
}

extension SearchPresenter: SearchInteractorToPresenterProtocol {
}
