//
//  SearchProtocols.swift
//  Cash2U
//
//  Created by talgar osmonov on 23/5/22.
//  
//

import Foundation

protocol SearchViewProtocol: AnyObject {
}

protocol SearchViewToPresenterProtocol: AnyObject {
}

protocol SearchInteractorProtocol: AnyObject {
}

protocol SearchInteractorToPresenterProtocol: AnyObject {
}

protocol SearchRouterProtocol: AnyObject {
}
