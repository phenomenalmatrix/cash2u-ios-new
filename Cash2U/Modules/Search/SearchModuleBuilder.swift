//
//  SearchModuleBuilder.swift
//  Cash2U
//
//  Created by talgar osmonov on 23/5/22.
//  
//

import UIKit

final class SearchModuleConfigurator {
    /// Собрать модуль
    class func build() -> UIViewController {
        let view = SearchViewController()
        let presenter = SearchPresenter()
        let interactor = SearchInteractor()
        let router = SearchRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
