//
//  SearchInteractor.swift
//  Cash2U
//
//  Created by talgar osmonov on 23/5/22.
//  
//

import Foundation

final class SearchInteractor {
    weak var presenter: SearchInteractorToPresenterProtocol?
}

extension SearchInteractor: SearchInteractorProtocol {
}
