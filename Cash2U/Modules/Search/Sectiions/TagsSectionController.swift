//
//  TagsSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 8/9/22.
//

import UIKit
import IGListKit
import Atributika

class TagsSectionController: ListSectionController, ListSupplementaryViewSource {
    
    weak var delegate: SectionControllerHeaderDelegate? = nil
    
    private var item: TagsModelIG?
    private var isHeaderButtonHidden: Bool = true

    required init(isHeaderButtonHidden: Bool = true) {
        self.isHeaderButtonHidden = isHeaderButtonHidden
        super.init()
        self.inset = UIEdgeInsets(top: 20, left: 20, bottom: 32, right: 20)
        supplementaryViewSource = self
        minimumLineSpacing = 5
        minimumInteritemSpacing = 5
    }
    
    override func numberOfItems() -> Int {
        return item?.tags.count ?? 0
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext, let item = item else {
            return .zero
        }
        
        let tag = item.tags[index]
        
        var attribitedTitle: NSAttributedString {
            let style = Style.mainTextStyle(
                size: 18,
                fontType: .regular,
                color: .mainTextColor,
                alignment: .left
            )
            return tag.styleAll(style).attributedString
        }

        return CGSize(width: attribitedTitle.getWidth(withConstrainedHeight: context.containerSize.height) + 20, height: 35)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUTagsCell = collectionContext!.dequeueReusableCell(of: CUTagsCell.self, for: self, at: index) as? CUTagsCell else {
            return UICollectionViewCell()
        }
        
        if let item = item {
            
        }
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? TagsModelIG
    }
    
    override func didSelectItem(at index: Int) {
//        delegate?.selectSectionController(self)
//        if let cell = self.collectionContext?.cellForItem(at: index, sectionController: self) as? DZBigStickerCell {
//            if item?.isAnimated == true {
//                cell.animatedLogoImageView.startAnimating()
//            } else {}
//        }
    }
}

// MARK: - SuplementaryViews

extension TagsSectionController {
    func supportedElementKinds() -> [String] {
        return [UICollectionView.elementKindSectionHeader]
    }

    func viewForSupplementaryElement(ofKind elementKind: String, at index: Int) -> UICollectionReusableView {
        switch elementKind {
        case UICollectionView.elementKindSectionHeader:
            return userHeaderView(atIndex: index)
        default:
            return UICollectionReusableView()
        }
    }
    
    func sizeForSupplementaryView(ofKind elementKind: String, at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: context.containerSize.width - 40, height: 40)
    }
    
    // private funcs
    
    private func userHeaderView(atIndex index: Int) -> UICollectionReusableView {
        guard let headerView = collectionContext?.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, for: self, class: CUSectionHeaderView.self, at: index) as? CUSectionHeaderView else {
            return UICollectionReusableView()
        }
        headerView.delegate = self
        if let item = item {
            headerView.setup(titleText: item.name, isRightButtonHidden: isHeaderButtonHidden)
        }
        return headerView
    }
}

extension TagsSectionController: CUSectionHeaderViewDelegate {
    func viewAllTapped() {
        delegate?.viewAllTapped(type: .BestPartners)
    }
}
