//
//  TagsModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 8/9/22.
//

import Foundation
import IGListKit

final class TagsModelIG: ListDiffable {
    
    let id = "Tags"
    let name: String
    let tags: [String]
    
    init(name: String, tags: [String]) {
        self.name = name
        self.tags = tags
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? TagsModelIG else { return false }
        return id == object.id && name == object.name && tags == object.tags
    }
}
