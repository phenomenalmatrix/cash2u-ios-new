//
//  SearchViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 23/5/22.
//  
//

import UIKit
import IGListKit
import TTGTags

class TableVC: UIViewController, ListAdapterDataSource, UICollectionViewDelegate {
    
    lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self)
    }()
    let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    var filterString = ""
    
    lazy var words: [String] = {
        // swiftlint:disable:next
        let str = "Humblebrag skateboard tacos viral small batch blue bottle, schlitz fingerstache etsy squid. Listicle tote bag helvetica XOXO literally, meggings cardigan kickstarter roof party deep v selvage scenester venmo truffaut. You probably haven't heard of them fanny pack austin next level 3 wolf moon. Everyday carry offal brunch 8-bit, keytar banjo pinterest leggings hashtag wolf raw denim butcher. Single-origin coffee try-hard echo park neutra, cornhole banh mi meh austin readymade tacos taxidermy pug tattooed. Cold-pressed +1 ethical, four loko cardigan meh forage YOLO health goth sriracha kale chips. Mumblecore cardigan humblebrag, lo-fi typewriter truffaut leggings health goth."
        var unique = Set<String>()
        var words = [String]()
        let range = str.startIndex ..< str.endIndex
        str.enumerateSubstrings(in: range, options: .byWords) { (substring, _, _, _) in
            guard let substring = substring else { return }
            if !unique.contains(substring) {
                unique.insert(substring)
                words.append(substring)
            }
        }
        return words
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .mainColor
        collectionView.delegate = self
        adapter.collectionView = collectionView
        adapter.dataSource = self
        
        view.addSubview(collectionView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView.frame = view.bounds
    }
    
    // MARK: ListAdapterDataSource

    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        guard filterString != "" else { return words.map { $0 as ListDiffable } }
        return words.filter { $0.lowercased().contains(filterString.lowercased()) }.map { $0 as ListDiffable }
    }

    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        return LabelSectionController()
    }

    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }
    
    func searchText(text: String){
        filterString = text
        adapter.performUpdates(animated: true, completion: nil)
    }

}

final class SearchViewController: CUViewController{
    
    private lazy var scroll: UIScrollView = {
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.alwaysBounceVertical = true
        view.showsHorizontalScrollIndicator = false
        view.showsVerticalScrollIndicator = false
        view.delaysContentTouches = false
        view.backgroundColor = .mainColor
        return view
    }()
    
    private let vc = TableVC()
    
    private lazy var container = UIView()
    
    private lazy var searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: vc)
        searchController.searchResultsUpdater = self
        searchController.delegate = self
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation =  true
        searchController.hidesNavigationBarDuringPresentation = true
        searchController.searchBar.placeholder = "Я ищу..."
        searchController.searchBar.setImage(UIImage(named: "search_icon"), for: .search, state: .normal)
        searchController.searchBar.searchTextPositionAdjustment = .init(horizontal: 8, vertical: 0)
        searchController.searchBar.setPositionAdjustment(UIOffset(horizontal: (14), vertical: 0), for: .search)
        return searchController
    }()
    
    private lazy var topTagList: TTGTextTagCollectionView = {
        let view = TTGTextTagCollectionView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.manualCalculateHeight = true
        return view
    }()
    
    private lazy var topLineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .grayColor
        view.alpha = 0.3
        return view
    }()
    
    private lazy var recentSearchLabel: CUTitleWithArrow = {
        let view = CUTitleWithArrow()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(titleText: "Недавний поиск", isShowArrow: false)
        return view
    }()
    
    private lazy var recentTagList: TTGTextTagCollectionView = {
        let view = TTGTextTagCollectionView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.manualCalculateHeight = true
        return view
    }()
    
    private lazy var oftenSearchedLabel: CUTitleWithArrow = {
        let view = CUTitleWithArrow()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(titleText: "Часто ищут", isShowArrow: false)
        return view
    }()
    
    private lazy var oftenSearchedTagList: TTGTextTagCollectionView = {
        let view = TTGTextTagCollectionView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.manualCalculateHeight = true
        return view
    }()
    
    private lazy var popularCategoryLabel: CUTitleWithArrow = {
        let view = CUTitleWithArrow()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(titleText: "Популярные категории", isShowArrow: false)
        return view
    }()
    
    private lazy var categoryCollectionView: CUCollectionView = {
        let flowLayout = SnappingCollectionViewLayout()
        flowLayout.scrollDirection = .horizontal
        
        let view = CUCollectionView(frame: .zero, collectionViewLayout: flowLayout)
        view.contentInset = .init(top: 0, left: 20, bottom: 0, right: 20)
        view.delegate = self
        view.dataSource = self
        view.decelerationRate = .fast
        view.translatesAutoresizingMaskIntoConstraints = false
        view.register(CUCategoryView.self)
        return view
    }()
    
    private lazy var ttags: TTGTextTagCollectionView = {
        let view = TTGTextTagCollectionView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.manualCalculateHeight = true
        return view
    }()
    
    var presenter: SearchViewToPresenterProtocol?
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    override func loadView() {
        super.loadView()
        title = "Поиск"
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
        navigationItem.searchController = searchController
        definesPresentationContext = true
        self.searchController.searchBar.searchTextField.font = .systemFont(ofSize: 14)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.navigationItem.hidesSearchBarWhenScrolling = true
        }
        topTags()
        recentTags()
        oftenTags()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.hidesSearchBarWhenScrolling = false
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        navigationItem.hidesSearchBarWhenScrolling = true
//    }
    
    // MARK: - Helpers
    
    private func topTags() {
        let strings = ["Избранные", "0/0/3", "0/0/6"]
        
        var borderColor: UIColor = .systemGreen
        
        for text in strings {
            
            if text == "Избранные" {
                borderColor = .systemGreen
            } else if text == "0/0/3" {
                borderColor = .mainBlueColor!
            } else {
                borderColor = .systemPurple
            }
            
            let content = TTGTextTagStringContent.init(text: text)
            content.textColor = .grayColor!
            content.textFont = .systemFont(ofSize: 18, weight: .medium)
            
            let normalStyle = TTGTextTagStyle.init()
            normalStyle.backgroundColor = .clear
            normalStyle.borderWidth = 1
            normalStyle.borderColor = borderColor
            normalStyle.cornerRadius = 40
            normalStyle.extraSpace = CGSize.init(width: 34, height: 16)
            
            let selectedStyle = TTGTextTagStyle.init()
            selectedStyle.borderColor = borderColor
            selectedStyle.backgroundColor = .clear
            selectedStyle.cornerRadius = 40
            selectedStyle.extraSpace = CGSize.init(width: 34, height: 16)
            
            let tag = TTGTextTag.init()
            tag.content = content
            tag.style = normalStyle
            tag.selectedStyle = selectedStyle
            
            topTagList.addTag(tag)
        }
        
        topTagList.reload()
    }
    
    private func recentTags() {
        let strings = ["AutoLayout", "dynamically", "calculates", "the", "size", "and", "position",
                       "of", "all"]
        
        for text in strings {
            let content = TTGTextTagStringContent.init(text: text)
            content.textColor = .grayColor!
            content.textFont = .systemFont(ofSize: 18, weight: .medium)
            
            let normalStyle = TTGTextTagStyle.init()
            normalStyle.backgroundColor = .clear
            normalStyle.borderWidth = 1
            normalStyle.borderColor = .grayColor!
            normalStyle.cornerRadius = 40
            normalStyle.extraSpace = CGSize.init(width: 34, height: 16)
            
            let selectedStyle = TTGTextTagStyle.init()
            selectedStyle.borderColor = .grayColor!
            selectedStyle.backgroundColor = .clear
            selectedStyle.cornerRadius = 40
            selectedStyle.extraSpace = CGSize.init(width: 34, height: 16)
            
            let tag = TTGTextTag.init()
            tag.content = content
            tag.style = normalStyle
            tag.selectedStyle = selectedStyle
            
            recentTagList.addTag(tag)
        }
        
        recentTagList.reload()
    }
    
    private func oftenTags() {
        let strings = ["AutoLayout", "dynamically", "calculates", "the", "size", "and", "position",
                       "of", "all", "the", "views", "in", "your"]
        
        for text in strings {
            let content = TTGTextTagStringContent.init(text: text)
            content.textColor = .grayColor!
            content.textFont = .systemFont(ofSize: 18, weight: .medium)
            
            let normalStyle = TTGTextTagStyle.init()
            normalStyle.backgroundColor = .clear
            normalStyle.borderWidth = 1
            normalStyle.borderColor = .grayColor!
            normalStyle.cornerRadius =  40
            normalStyle.extraSpace = CGSize.init(width: 34, height: 16)
            
            let selectedStyle = TTGTextTagStyle.init()
            selectedStyle.borderColor = .grayColor!
            selectedStyle.backgroundColor = .clear
            selectedStyle.cornerRadius = 40
            selectedStyle.extraSpace = CGSize.init(width: 34, height: 16)
            
            let tag = TTGTextTag.init()
            tag.content = content
            tag.style = normalStyle
            tag.selectedStyle = selectedStyle
            
            oftenSearchedTagList.addTag(tag)
        }
        
        oftenSearchedTagList.reload()
    }
    
}

extension SearchViewController: SearchViewProtocol {
}

extension SearchViewController: UISearchResultsUpdating, UISearchControllerDelegate, UISearchBarDelegate {
    func updateSearchResults(for searchController: UISearchController) {
        if searchController.searchBar.text != nil {
            print("AAAA: \(searchController.searchBar.text)")
            vc.searchText(text: searchController.searchBar.text ?? "")
        }
    }
}

extension SearchViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = CategoryDetailModuleConfigurator.build()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(cellType: CUCategoryView.self, for: indexPath)
//        cell.setup(model: CategoryModelItemIG(id: <#T##Int#>, name: <#T##String#>, logo: <#T##String#>, partnerCount: <#T##Int#>, backgroundColor: <#T##String#>))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemPerRow: CGFloat = 3
        let paddingWidth = 20 * (itemPerRow + 1) - 20
        let awailableWidth = collectionView.frame.width - paddingWidth
        let widthPerItem = awailableWidth / itemPerRow
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}


extension SearchViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        view.addSubview(scroll)
        
        scroll.addSubview(container)
        container.addSubview(topTagList)
        container.addSubview(topLineView)
        container.addSubview(recentSearchLabel)
        container.addSubview(recentTagList)
        container.addSubview(oftenSearchedLabel)
        container.addSubview(oftenSearchedTagList)
        container.addSubview(popularCategoryLabel)
        container.addSubview(categoryCollectionView)
    }
    
    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        scroll.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
        container.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.width.equalToSuperview()
        }
        
        topTagList.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.top.equalToSuperview()
        }
        
        topLineView.snp.makeConstraints { make in
            make.top.equalTo(topTagList.snp.bottom).offset(16)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.height.equalTo(1)
        }
        
        recentSearchLabel.snp.makeConstraints { make in
            make.top.equalTo(topLineView.snp.bottom).offset(24)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }

        recentTagList.snp.makeConstraints { make in
            make.top.equalTo(recentSearchLabel.snp.bottom).offset(16)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }

        oftenSearchedLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.top.equalTo(recentTagList.snp.bottom).offset(32)
        }

        oftenSearchedTagList.snp.makeConstraints { make in
            make.top.equalTo(oftenSearchedLabel.snp.bottom).offset(16)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }

        popularCategoryLabel.snp.makeConstraints { make in
            make.top.equalTo(oftenSearchedTagList.snp.bottom).offset(32)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }

        categoryCollectionView.snp.makeConstraints { make in
            make.top.equalTo(popularCategoryLabel.snp.bottom).offset(16)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo((ScreenHelper.getWidth - 50) / 3 + 10)
            make.bottom.equalToSuperview()
        }
        
    }
}
