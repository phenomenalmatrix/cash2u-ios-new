//
//  CategoryDetailModuleBuilder.swift
//  Cash2U
//
//  Created by talgar osmonov on 25/8/22.
//  
//

import UIKit

final class CategoryDetailModuleConfigurator {
    /// Собрать модуль
    class func build() -> UIViewController {
        let view = CategoryDetailViewController()
        let presenter = CategoryDetailPresenter()
        let interactor = CategoryDetailInteractor()
        let router = CategoryDetailRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
