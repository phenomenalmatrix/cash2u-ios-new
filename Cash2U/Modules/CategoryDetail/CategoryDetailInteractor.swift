//
//  CategoryDetailInteractor.swift
//  Cash2U
//
//  Created by talgar osmonov on 25/8/22.
//  
//

import Foundation

final class CategoryDetailInteractor {
    weak var presenter: CategoryDetailInteractorToPresenterProtocol?
}

extension CategoryDetailInteractor: CategoryDetailInteractorProtocol {
}
