//
//  CategoryDetailProtocols.swift
//  Cash2U
//
//  Created by talgar osmonov on 25/8/22.
//  
//

import Foundation

protocol CategoryDetailViewProtocol: AnyObject {
}

protocol CategoryDetailViewToPresenterProtocol: AnyObject {
}

protocol CategoryDetailInteractorProtocol: AnyObject {
}

protocol CategoryDetailInteractorToPresenterProtocol: AnyObject {
}

protocol CategoryDetailRouterProtocol: AnyObject {
}
