//
//  CategoryDetailViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 25/8/22.
//  
//

import UIKit
import IGListKit

final class CategoryDetailViewController: CUViewController {
    
    var presenter: CategoryDetailViewToPresenterProtocol?
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private lazy var adapter: ListAdapter = {
        let adapter = ListAdapter(updater: ListAdapterUpdater(), viewController: self)
        adapter.collectionView = mainCollection
        adapter.dataSource = self
        return adapter
    }()
    
    private func createItemsData() -> [ListDiffable] {
        var aa = [
        ] as [ListDiffable]
        aa.append(CategoryTopSectionModelIG(name: "Top"))
        aa.append(CategorySelectionModelIG(name: "Selection", items: ["asdf", "fasdfas", "asdfasdf", "asdf"]))
        aa.append(SearchModelIG(name: "Search2"))
        aa.append(BestPartnersModelIG(name: "Рекомендуем", isLoading: false, items: []))
        aa.append(TagsModelIG(name: "Партнеры", tags: [""]))
        aa.append(PartnersModelIG(name: "Partners"))
        aa.append(SupportModelIG())
        return aa
    }
    
    private lazy var itemsData: [ListDiffable] = createItemsData()
    
    private lazy var mainCollection: CUCollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        let mainCollection = CUCollectionView(
            frame: CGRect.zero,
            collectionViewLayout: flowLayout
        )
        mainCollection.translatesAutoresizingMaskIntoConstraints = false
        mainCollection.alwaysBounceVertical = true
        return mainCollection
    }()
    
    override func loadView() {
        super.loadView()
        self.navigationItem.title = "Sport"
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adapter.performUpdates(animated: true, completion: nil)
    }
}

extension CategoryDetailViewController: CategoryDetailViewProtocol {
}

extension CategoryDetailViewController: SearchSectionControllerDelegate {
    func sectionTapped() {
        let vc = SearchModuleConfigurator.build()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func filterButtonTapped() {
        print("Filter")
    }
}

extension CategoryDetailViewController: PartnersSectionControllerDelegate {
    func didSelect() {
        let vc = PartnerPageModuleBuilder.build(partnerId: 0)
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension CategoryDetailViewController: MallsSectionControllerDelegate {
    func didSelectMall() {
        let vc = MallsDetailModuleConfigurator.build()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension CategoryDetailViewController: CategorySectionControllerDelegate {
    func didSelectCategory() {
        let vc = CategoryDetailModuleConfigurator.build()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension CategoryDetailViewController: PartnerSelectionDelegate {
    func didSelectPartner(partnerId: Int) {
        let vc = PartnerPageModuleBuilder.build(partnerId: partnerId)
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: - ListAdapterDataSource

extension CategoryDetailViewController: ListAdapterDataSource {
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        itemsData
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        if object is CurrencyModelIG {
            let section = CurrencySectionController()
            return section
        } else if object is CategoryTopSectionModelIG {
            let section = CategoryTopSectionController()
//            section.delegate = self
            return section
        } else if object is CategorySelectionModelIG {
            let section = CategorySelectionSectionController()
//            section.delegate = self
            return section
        } else if object is PartnersModelIG {
            let section = PartnersSectionController()
            section.delegate = self
            return section
        } else if object is BestPartnersModelIG {  
            let section = RecomendationSectionController()
            section.delegate = self
            return section
        } else if object is TagsModelIG {
            let section = HorizontalTagsSectionController()
//            section.delegate = self
            return section
        } else if object is LocationModelIG {
            let section = LocationSectionController()
//            section.delegate = self
            return section
        } else if object is SearchModelIG {
            let section = SearchSectionController()
            section.delegate = self
            return section
        } else if object is NewPartnersModelIG {
            let section = NewPartnersSectionController()
//            section.delegate = self
            return section
        } else if object is MallsModelIG {
            let section = MallsSectionController(isHeaderButtonHidden: false)
            section.delegate = self
            return section
        } else if object is DiscountModelIG {
            let section = DiscountSectionController()
//            section.delegate = self
            return section
        } else if object is SupportModelIG {
            return SupportSectionController()
        } else if object is VersionModelIG {
            return VersionSectionController()
        } else if object is GreetingsSectionModelIG {
            let section = GreetingsSectionController()
//            section.delegate = self
            return section
        } else if object is BazaarModelIG {
            let section = BazaarsSectionController(isHeaderButtonHidden: false)
//            section.delegate = self
            return section
        } else if object is CategoryModelIG {
            let section = CategorySectionController()
            section.delegate = self
            return section
        } else {
            return ListSectionController()
        }
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }
}

extension CategoryDetailViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        view.addSubview(mainCollection)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        mainCollection.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
    }
}
