//
//  CategoryDetailPresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 25/8/22.
//  
//

import Foundation

final class CategoryDetailPresenter {
    
    weak var view: CategoryDetailViewProtocol?
    var interactor: CategoryDetailInteractorProtocol?
    var router: CategoryDetailRouterProtocol?
    
}

extension CategoryDetailPresenter: CategoryDetailViewToPresenterProtocol {
}

extension CategoryDetailPresenter: CategoryDetailInteractorToPresenterProtocol {
}
