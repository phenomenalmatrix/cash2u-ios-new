//
//  CategoryTopSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 25/8/22.
//

import UIKit
import IGListKit

class CategoryTopSectionController: ListSectionController {
    
    weak var delegate: SectionControllerHeaderDelegate? = nil
    
    private var item: CategoryTopSectionModelIG?

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        minimumLineSpacing = 10
        minimumInteritemSpacing = 20
    }
    
    override func numberOfItems() -> Int {
        return 1
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width) - 40, height: 100)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUCategoryTopView = collectionContext!.dequeueReusableCell(of: CUCategoryTopView.self, for: self, at: index) as? CUCategoryTopView else {
            return UICollectionViewCell()
        }
        
        cell.setup()
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? CategoryTopSectionModelIG
    }
}
