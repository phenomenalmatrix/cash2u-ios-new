//
//  CategorySelectionSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 26/8/22.
//

import UIKit
import IGListKit

protocol CategorySelectionSectionControllerDelegate: AnyObject {
    func didSelectCategorySelection()
}

class CategorySelectionSectionController: ListSectionController {
    
    weak var sectionDelegate: SectionControllerHeaderDelegate? = nil
    weak var delegate: CategorySelectionSectionControllerDelegate? = nil
    
    private var item: CategorySelectionModelIG?
    
    private var isExpanded = false

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
    }
    
    override func numberOfItems() -> Int {
        return isExpanded ? 1 + (item?.items.count ?? 0) : 1
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width) - 40, height: 50)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUCategorySelectionCell = collectionContext!.dequeueReusableCell(of: CUCategorySelectionCell.self, for: self, at: index) as? CUCategorySelectionCell else {
            return UICollectionViewCell()
        }
        
        if let item = item {
            
            if index == 0 {
                cell.setup(text: "Подкатегории", isFirst: true, isLast: isExpanded ? false : true, isExpanded: isExpanded)
            } else if index == 1 {
                cell.setup(text: "Аксессуары для спорта", isExpanded: false)
            } else if index == item.items.count {
                cell.setup(text: "Аксессуары для спорта", isLast: true, isExpanded: false)
            } else {
                cell.setup(text: "Аксессуары для спорта", isExpanded: false)
            }
            
        }
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? CategorySelectionModelIG
    }
    
    override func didSelectItem(at index: Int) {
        if index == 0 {
            collectionContext!.performBatch(animated: true) { batchContext in
                self.isExpanded.toggle()
                batchContext.reload(self)
            } completion: { _ in
                
            }
        } else {
            delegate?.didSelectCategorySelection()
        }
    }
}
