//
//  CategorySelectionModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 26/8/22.
//

import UIKit
import IGListKit

final class CategorySelectionModelIG: ListDiffable {
    
    let id = "CategorySelection"
    let name: String
    let items: [String]
    
    init(name: String, items: [String]) {
        self.name = name
        self.items = items
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? CategorySelectionModelIG else { return false }
        return id == object.id && name == object.name && items == object.items
    }
}

