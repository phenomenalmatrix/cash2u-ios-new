//
//  PartnersModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 25/8/22.
//

import UIKit
import IGListKit

final class PartnersModelIG: ListDiffable {
    
    let id = "Partners"
    let name: String
    
    init(name: String) {
        self.name = name
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? PartnersModelIG else { return false }
        return id == object.id && name == object.name
    }
}
