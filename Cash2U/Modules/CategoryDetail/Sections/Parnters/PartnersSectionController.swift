//
//  PartnersSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 25/8/22.
//

import UIKit
import IGListKit

protocol PartnersSectionControllerDelegate: AnyObject {
    func didSelect()
}

class PartnersSectionController: ListSectionController {
    
    weak var delegate: PartnersSectionControllerDelegate? = nil
    
    private var item: CategoryModelIG?

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 0, left: 20, bottom: 32, right: 20)
        minimumLineSpacing = 10
        minimumInteritemSpacing = 20
    }
    
    override func numberOfItems() -> Int {
        return 6
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width / 2) - 30, height: (context.containerSize.width / 2) + 95)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUPartnersSectionCell = collectionContext!.dequeueReusableCell(of: CUPartnersSectionCell.self, for: self, at: index) as? CUPartnersSectionCell else {
            return UICollectionViewCell()
        }
        
        cell.setup()
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? CategoryModelIG
    }
    
    override func didSelectItem(at index: Int) {
        delegate?.didSelect()
    }
}
