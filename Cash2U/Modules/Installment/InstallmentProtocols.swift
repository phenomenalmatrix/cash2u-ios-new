//
//  InstallmentProtocols.swift
//  Cash2U
//
//  Created by talgar osmonov on 15/8/22.
//  
//

import Foundation

protocol InstallmentViewProtocol: AnyObject {
}

protocol InstallmentViewToPresenterProtocol: AnyObject {
    func onNavigateToInstallment(type: InstallmentType)
}

protocol InstallmentInteractorProtocol: AnyObject {
}

protocol InstallmentInteractorToPresenterProtocol: AnyObject {
}

protocol InstallmentRouterProtocol: AnyObject {
    func navigateToInstallment(type: InstallmentType)
    func navigateToGuestInstallment(type: InstallmentType)
}
