//
//  InstallmentRouter.swift
//  Cash2U
//
//  Created by talgar osmonov on 15/8/22.
//  
//

import UIKit

final class InstallmentRouter {
    weak var viewController: UIViewController?
}

extension InstallmentRouter: InstallmentRouterProtocol {
    func navigateToInstallment(type: InstallmentType) {
        switch type {
        case .nol3:
            let vc = Nol3InstallmentViewController()
            vc.hidesBottomBarWhenPushed = true
            self.viewController?.navigationController?.pushViewController(vc, animated: true)
        case .nol6:
            let vc = Nol3InstallmentViewController()
            vc.hidesBottomBarWhenPushed = true
            self.viewController?.navigationController?.pushViewController(vc, animated: true)
        case .fuel:
            let vc = Nol3InstallmentViewController()
            vc.hidesBottomBarWhenPushed = true
            self.viewController?.navigationController?.pushViewController(vc, animated: true)
        case .product:
            let vc = Nol3InstallmentViewController()
            vc.hidesBottomBarWhenPushed = true
            self.viewController?.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func navigateToGuestInstallment(type: InstallmentType) {
        let vc = InstallmentOnboardViewController()
        vc.installmentType = type
        vc.hidesBottomBarWhenPushed = true
        self.viewController?.navigationController?.pushViewController(vc, animated: true)
    }
}
