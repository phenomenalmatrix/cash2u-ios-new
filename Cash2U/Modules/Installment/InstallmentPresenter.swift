//
//  InstallmentPresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 15/8/22.
//  
//

import Foundation

final class InstallmentPresenter {
    
    weak var view: InstallmentViewProtocol?
    var interactor: InstallmentInteractorProtocol?
    var router: InstallmentRouterProtocol?
    var isGuest = false
    
}

extension InstallmentPresenter: InstallmentViewToPresenterProtocol {
    func onNavigateToInstallment(type: InstallmentType) {
        if isGuest {
            router?.navigateToGuestInstallment(type: type)
        } else {
            router?.navigateToInstallment(type: type)
        }
    }
}

extension InstallmentPresenter: InstallmentInteractorToPresenterProtocol {
}
