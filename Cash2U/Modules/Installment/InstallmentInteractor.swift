//
//  InstallmentInteractor.swift
//  Cash2U
//
//  Created by talgar osmonov on 15/8/22.
//  
//

import Foundation

final class InstallmentInteractor {
    weak var presenter: InstallmentInteractorToPresenterProtocol?
}

extension InstallmentInteractor: InstallmentInteractorProtocol {
}
