//
//  InstallmentModuleBuilder.swift
//  Cash2U
//
//  Created by talgar osmonov on 15/8/22.
//  
//

import UIKit

final class InstallmentModuleBuilder {
    /// Собрать модуль
    class func build(isGuest: Bool = false) -> UIViewController {
        let view = InstallmentViewController()
        let presenter = InstallmentPresenter()
        let interactor = InstallmentInteractor()
        let router = InstallmentRouter()

        view.presenter = presenter
        view.isGuest = isGuest
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        presenter.isGuest = isGuest
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
