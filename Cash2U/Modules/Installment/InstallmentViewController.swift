//
//  InstallmentViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 15/8/22.
//  
//

import UIKit
import IGListKit

final class InstallmentViewController: CUBellViewController {
    
    var presenter: InstallmentViewToPresenterProtocol?
    
    var isGuest = false
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private func createItemsData() -> [ListDiffable] {
        var aa = [
        ] as [ListDiffable]
        aa.append(InstallmentModelIG(type: .nol3))
        aa.append(InstallmentModelIG(type: .nol6))
        aa.append(InstallmentModelIG(type: .fuel))
        aa.append(InstallmentModelIG(type: .product))
        aa.append(isGuest ? SupportModelIG() : VersionModelIG())
        return aa
    }
    
    private lazy var itemsData: [ListDiffable] = createItemsData()
    
    private lazy var adapter: ListAdapter = {
        let view = ListAdapter(updater: ListAdapterUpdater(), viewController: self)
        view.dataSource = self
        view.collectionView = mainCollectionView
        return view
    }()
    
    private lazy var navLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Оплата частями", size: 26, fontType: .semibold, isSizeToFit: true)
        return view
    }()
    
    private lazy var mainCollectionView: CUCollectionView = {
        let layout = UICollectionViewFlowLayout()
        let view = CUCollectionView(frame: .zero, collectionViewLayout: layout)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.alwaysBounceVertical = true
        return view
    }()
    
    private lazy var qrButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Оплатить QR", titleSize: 16, cornerRadius: 45 / 2)
        view.addTarget(self, action: #selector(onOpenQR), for: .touchUpInside)
        view.isHidden = isGuest
        return view
    }()
    
    override func loadView() {
        super.loadView()
        setNavBar()
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        adapter.performUpdates(animated: true, completion: nil)
    }
    
    // MARK: - UIActions
    
    // MARK: - Helpers
    
    func setNavBar() {
        if isGuest {
            title = "Оплата частями"
        } else {
            let cityItem = UIBarButtonItem.init(customView: navLabel)
            navigationItem.leftBarButtonItems = [cityItem]
        }
    }
}

extension InstallmentViewController: InstallmentViewProtocol {
}

extension InstallmentViewController: InstallmentSectionControllerDelegate {
    func didSelect(type: InstallmentType) {
        presenter?.onNavigateToInstallment(type: type)
    }
}

// MARK: - ListAdapterDataSource

extension InstallmentViewController: ListAdapterDataSource {
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        itemsData
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        if object is InstallmentModelIG {
            let section = InstallmentSectionController(isGuest: isGuest)
            section.delegate = self
            return section
        } else if object is BestPartnersModelIG {
            let section = BestPartnersSectionController(isHeaderButtonHidden: false)
//            section.delegate = self
            return section
        } else if object is OffersAndPromotionsModelIG {
            return OffersAndPromotionsSectionController()
        } else if object is BonusCardsModelIG {
            let section = BonusCardsSectionController()
//            section.delegate = self
            return section
        } else if object is NewsModelIG {
            let section = NewsSectionController()
//            section.delegate = self
            return section
        } else if object is SupportModelIG {
            return SupportSectionController()
        } else if object is VersionModelIG {
            return VersionSectionController()
        } else if object is GreetingsSectionModelIG {
            let section = GreetingsSectionController()
//            section.delegate = self
            return section
        } else {
            return ListSectionController()
        }
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }
}

extension InstallmentViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        
        view.addSubview(mainCollectionView)
        view.addSubview(qrButton)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        mainCollectionView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        qrButton.snp.makeConstraints { make in
            make.bottom.equalTo(view.safeArea.bottom).offset(-12)
            make.centerX.equalToSuperview()
            make.width.equalTo(ScreenHelper.getWidth / 2.5)
            make.height.equalTo(45)
        }
    }
}
