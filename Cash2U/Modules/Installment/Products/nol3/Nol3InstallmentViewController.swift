//
//  Nol3InstallmentViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 30/9/22.
//  
//

import UIKit

protocol PagerAwareProtocol: AnyObject {
    var pageDelegate: BottomPageDelegate? {get set}
    var currentViewController: UIViewController? {get}
    var pagerTabHeight: CGFloat? {get}
}

protocol BottomPageDelegate: AnyObject {
    func tp_pageViewController(_ currentViewController: UIViewController?, didSelectPageAt index: Int)
}

protocol TPDataSource: AnyObject {
    func headerViewController() -> UIViewController
    func bottomViewController() -> UIViewController & PagerAwareProtocol
    func minHeaderHeight() -> CGFloat // stop scrolling headerView at this point
    func headerHeight() -> CGFloat
}

protocol TPProgressDelegate: AnyObject {
    func tp_scrollView(_ scrollView: UIScrollView, didUpdate progress: CGFloat)
    func tp_scrollViewDidLoad(_ scrollView: UIScrollView)
}

final class Nol3InstallmentViewController: CUViewController, UIScrollViewDelegate {
    
    private var headerVC: Nol3InstallmentHeaderViewController?
    private var bottomVC: Nol3InstallmentBottomViewController?
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private lazy var navView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var backArrow: CUIconButton = {
        let configuration = UIImage.SymbolConfiguration(pointSize: 50, weight: .regular, scale: .default)
        let view = CUIconButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(image: UIImage(named: "WhiteArrowLeft"), backgroundColor: .clear, insets: 7)
        view.addTarget(self, action: #selector(onBackPressed), for: .touchUpInside)
        return view
    }()
    
    private lazy var rightButton: CUIconButton = {
        let configuration = UIImage.SymbolConfiguration(pointSize: 50, weight: .regular, scale: .default)
        let view = CUIconButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(image: UIImage(named: "info_icon"), backgroundColor: .clear, insets: 2)
        return view
    }()
    
    private lazy var navTitleLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(size: 18, fontType: .semibold, alignment: .center)
        return view
    }()
    
    override func loadView() {
        super.loadView()
        navTitleLabel.changeText(title: "0/0/3")
        self.tp_configure(with: self, delegate: self)
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.interactivePopGestureRecognizer?.delegate = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    // MARK: - UIActions
    
    @objc private func onBackPressed() {
        self.navigationController?.popViewController(animated: true)
    }
}

extension Nol3InstallmentViewController: TPDataSource, TPProgressDelegate {
    func headerHeight() -> CGFloat {
        return 445
    }
    
    func headerViewController() -> UIViewController {
        headerVC = Nol3InstallmentHeaderViewController()
        return headerVC!
    }
    
    func bottomViewController() -> UIViewController & PagerAwareProtocol {
        bottomVC = Nol3InstallmentBottomViewController()
        return bottomVC!
    }
    
    func minHeaderHeight() -> CGFloat {
        return (topInset + 145)
    }
    
    func tp_scrollView(_ scrollView: UIScrollView, didUpdate progress: CGFloat) {
        headerVC?.update(with: progress, minHeaderHeight: minHeaderHeight())
    }
    
    func tp_scrollViewDidLoad(_ scrollView: UIScrollView) {
        print("Scroll DidLoad")
    }
}

extension Nol3InstallmentViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        
        view.addSubview(navView)
        navView.addSubview(backArrow)
        navView.addSubview(navTitleLabel)
        navView.addSubview(rightButton)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        navView.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(44)
        }
        
        backArrow.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(-12)
            make.centerY.equalToSuperview()
            make.size.equalTo(CGSize(width: 54, height: 32))
        }
        
        navTitleLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(60)
        }
        
        rightButton.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-10)
            make.centerY.equalToSuperview()
            make.size.equalTo(32)
        }
    }
}
