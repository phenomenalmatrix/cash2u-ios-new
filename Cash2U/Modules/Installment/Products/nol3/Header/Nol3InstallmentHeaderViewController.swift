//
//  Nol3InstallmentHeaderViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 3/10/22.
//  
//

import UIKit

final class Nol3InstallmentHeaderViewController: CUViewController {
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private var lastProgress: CGFloat = .zero
    private var lastMinHeaderHeight: CGFloat = .zero
    
    private lazy var animator: UIViewPropertyAnimator = {
        let view = UIViewPropertyAnimator(duration: 0.5, curve: .linear)
        return view
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    private lazy var blueView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .init(hex: "#131075")
        view.cornerRadius = 24
        view.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        return view
    }()
    
    private lazy var topBlueView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .init(hex: "#131075")
        view.cornerRadius = 24
        view.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        view.alpha = 0
        view.isUserInteractionEnabled = false
        return view
    }()
    
    private lazy var image: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "frunze_test")
        return view
    }()
    
    private lazy var availableForBuyTitleLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Доступно для покупок:", size: 16, fontType: .semibold)
        return view
    }()
    
    private lazy var availableForBuyAmountLabel: CUDoubleAmountLabelView = {
        let view = CUDoubleAmountLabelView()
        view.setup(
            summ: 29030.65,
            colorForInteger: .mainTextColor!,
            colorForTheRest: .grayColor!,
            fontForInteger: UIFont.systemFont(ofSize: 40, weight: .bold),
            fontForTheRemainder: UIFont.systemFont(ofSize: 40, weight: .bold),
            currencySymbol: "c",
            currencySymbolFont: UIFont.systemFont(ofSize: 40, weight: .bold),
            currencySymbolColor: .grayColor!
        )
        return view
    }()
    
    private lazy var usedTitleLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Использовано:", size: 14, fontType: .semibold, titleColor: .grayColor)
        return view
    }()
    
    private lazy var usedAmountLabel: CUDoubleAmountLabelView = {
        let view = CUDoubleAmountLabelView()
        view.setup(
            summ: 12.0,
            colorForInteger: .mainTextColor!,
            colorForTheRest: .grayColor!,
            fontForInteger: UIFont.systemFont(ofSize: 22, weight: .semibold),
            fontForTheRemainder: UIFont.systemFont(ofSize: 22, weight: .semibold),
            currencySymbol: "c",
            currencySymbolFont: UIFont.systemFont(ofSize: 22, weight: .semibold),
            currencySymbolColor: .grayColor!
        )
        return view
    }()
    
    private lazy var approvedLimitTitleLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Одобренный лимит:", size: 14, fontType: .semibold, titleColor: .grayColor)
        return view
    }()
    
    
    private lazy var approvedLimitAmountLabel: CUDoubleAmountLabelView = {
        let view = CUDoubleAmountLabelView()
        view.setup(
            summ: 30000.00,
            colorForInteger: .mainTextColor!,
            colorForTheRest: .grayColor!,
            fontForInteger: UIFont.systemFont(ofSize: 22, weight: .semibold),
            fontForTheRemainder: UIFont.systemFont(ofSize: 22, weight: .semibold),
            currencySymbol: "c",
            currencySymbolFont: UIFont.systemFont(ofSize: 22, weight: .semibold),
            currencySymbolColor: .grayColor!
        )
        return view
    }()
    
    private lazy var upgradeLimitButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Повысить лимит", titleSize: 14, fontType: .semibold, titleColor: .mainBlueColor, backgroundColor: .white, cornerRadius: 35 / 2)
        view.addTarget(self, action: #selector(onUpgradeLimitTapped), for: .touchUpInside)
        return view
    }()
    
    private lazy var tabsView: CUPartnerPageButtonView = {
        let view = CUPartnerPageButtonView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(type: 1, tabsHeight: 70)
        view.delegate = self
        return view
    }()
    
    override func loadView() {
        super.loadView()
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        topBlueView.createGradientBlur()
        blueView.createGradientBlur()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(appWillEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appDidEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addAnimation()
        update(with: lastProgress, minHeaderHeight: lastMinHeaderHeight)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        resetAnimator()
    }
    
    @objc private func appWillEnterForeground() {
        addAnimation()
        update(with: lastProgress, minHeaderHeight: lastMinHeaderHeight)
    }

    @objc private func appDidEnterBackground() {
        resetAnimator()
    }
    
    private func addAnimation() {
        animator.addAnimations {
            self.topBlueView.alpha = 1
        }
        animator.stopAnimation(true)
    }
    
    private func resetAnimator() {
        if animator.state == .active {
            animator.stopAnimation(false)
        }
        if animator.state == .stopped {
            animator.finishAnimation(at: .current)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5 ) {
            self.topBlueView.alpha = 0
        }
    }
    
    func update(with progress: CGFloat, minHeaderHeight: CGFloat) {
        lastProgress = progress
        lastMinHeaderHeight = minHeaderHeight
        
        animator.fractionComplete = progress
    }
    
    @objc private func onUpgradeLimitTapped() {
        print("Limit")
    }
}

extension Nol3InstallmentHeaderViewController: CUPartnerPageButtonViewDelegate {
    func didSelect(type: CUPartnerPageButtonType) {
        let installmentPageSelected = ["installmentPageSelected": type.rawValue]
        NotificationCenter.default
            .post(name: .installmentPageSelected, object: nil, userInfo: installmentPageSelected)
    }
}

extension Nol3InstallmentHeaderViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        view.addSubview(blueView)
        view.addSubview(containerView)
        containerView.addSubview(availableForBuyTitleLabel)
        containerView.addSubview(availableForBuyAmountLabel)
        
        containerView.addSubviews([approvedLimitAmountLabel, approvedLimitTitleLabel])
        containerView.addSubviews([usedAmountLabel, usedTitleLabel])
        
        containerView.addSubview(upgradeLimitButton)
        
        view.addSubview(topBlueView)
        
        view.addSubview(tabsView)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        blueView.snp.makeConstraints { make in
            make.leading.top.trailing.equalToSuperview()
            make.bottom.equalToSuperview().offset(-90)
        }
        
        containerView.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top).offset(50)
            make.leading.trailing.bottom.equalTo(blueView)
        }
        
        availableForBuyTitleLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(20)
            make.top.equalToSuperview().offset(16)
        }
        
        availableForBuyAmountLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(20)
            make.top.equalTo(availableForBuyTitleLabel.snp.bottom).offset(5)
        }
        
        approvedLimitAmountLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.bottom.equalTo(blueView.snp.bottom).offset(-20)
        }
        
        approvedLimitTitleLabel.snp.makeConstraints { make in
            make.bottom.equalTo(approvedLimitAmountLabel.snp.top).offset(-5)
            make.leading.equalToSuperview().offset(20)
        }
        
        usedAmountLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.bottom.equalTo(approvedLimitTitleLabel.snp.top).offset(-20)
        }
        
        usedTitleLabel.snp.makeConstraints { make in
            make.bottom.equalTo(usedAmountLabel.snp.top).offset(-5)
            make.leading.equalToSuperview().offset(20)
        }
        
        upgradeLimitButton.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-20)
            make.bottom.equalTo(blueView.snp.bottom).offset(-20)
            make.height.equalTo(35)
            make.width.equalTo(upgradeLimitButton.getTitleWidth(withConstrainedHeight: 35) + 25)
        }
        
        topBlueView.snp.makeConstraints { make in
            make.edges.equalTo(blueView)
        }
        
        tabsView.snp.makeConstraints { make in
            make.top.equalTo(blueView.snp.bottom).offset(10)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.height.equalTo(70)
        }
    }
}










final class Nol3Installment1ViewController: CUViewController {
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .red
    }
}

final class Nol3Installment2ViewController: CUViewController {
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .blue
    }
}

final class Nol3Installment3ViewController: CUViewController {
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .systemPink
    }
}
