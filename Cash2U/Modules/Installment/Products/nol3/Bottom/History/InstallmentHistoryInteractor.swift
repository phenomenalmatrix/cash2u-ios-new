//
//  InstallmentHistoryInteractor.swift
//  Cash2U
//
//  Created by talgar osmonov on 4/10/22.
//  
//

import Foundation

final class InstallmentHistoryInteractor {
    weak var presenter: InstallmentHistoryInteractorToPresenterProtocol?
}

extension InstallmentHistoryInteractor: InstallmentHistoryInteractorProtocol {
}
