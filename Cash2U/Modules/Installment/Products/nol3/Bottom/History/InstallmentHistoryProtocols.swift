//
//  InstallmentHistoryProtocols.swift
//  Cash2U
//
//  Created by talgar osmonov on 4/10/22.
//  
//

import Foundation

protocol InstallmentHistoryViewProtocol: AnyObject {
}

protocol InstallmentHistoryViewToPresenterProtocol: AnyObject {
}

protocol InstallmentHistoryInteractorProtocol: AnyObject {
}

protocol InstallmentHistoryInteractorToPresenterProtocol: AnyObject {
}

protocol InstallmentHistoryRouterProtocol: AnyObject {
}
