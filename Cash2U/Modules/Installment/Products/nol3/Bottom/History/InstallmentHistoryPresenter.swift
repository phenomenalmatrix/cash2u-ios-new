//
//  InstallmentHistoryPresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 4/10/22.
//  
//

import Foundation

final class InstallmentHistoryPresenter {
    
    weak var view: InstallmentHistoryViewProtocol?
    var interactor: InstallmentHistoryInteractorProtocol?
    var router: InstallmentHistoryRouterProtocol?
    
}

extension InstallmentHistoryPresenter: InstallmentHistoryViewToPresenterProtocol {
}

extension InstallmentHistoryPresenter: InstallmentHistoryInteractorToPresenterProtocol {
}
