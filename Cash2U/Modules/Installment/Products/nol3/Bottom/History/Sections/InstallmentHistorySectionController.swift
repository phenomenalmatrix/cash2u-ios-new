//
//  InstallmentHistorySectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 4/10/22.
//

import UIKit
import IGListKit

class InstallmentHistorySectionController: ListSectionController, ListSupplementaryViewSource {
    
    private var item: InstallmentHistoryModelIG?

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 20, left: 0, bottom: 32, right: 0)
        minimumLineSpacing = 12
        supplementaryViewSource = self
    }
    
    override func numberOfItems() -> Int {
        return 10
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width) - 40 , height: 90)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUInstallmentHistoryCell = collectionContext!.dequeueReusableCell(of: CUInstallmentHistoryCell.self, for: self, at: index) as? CUInstallmentHistoryCell else {
            return UICollectionViewCell()
        }
        
        if let item = item {
            cell.setup()
        }
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? InstallmentHistoryModelIG
    }
    
    override func didSelectItem(at index: Int) {
//        delegate?.selectSectionController(self)
//        if let cell = self.collectionContext?.cellForItem(at: index, sectionController: self) as? DZBigStickerCell {
//            if item?.isAnimated == true {
//                cell.animatedLogoImageView.startAnimating()
//            } else {}
//        }
    }
}

// MARK: - SuplementaryViews

extension InstallmentHistorySectionController  {
    func supportedElementKinds() -> [String] {
        return [UICollectionView.elementKindSectionHeader]
    }

    func viewForSupplementaryElement(ofKind elementKind: String, at index: Int) -> UICollectionReusableView {
        switch elementKind {
        case UICollectionView.elementKindSectionHeader:
            return userHeaderView(atIndex: index)
        default:
            return UICollectionReusableView()
        }
    }
    
    func sizeForSupplementaryView(ofKind elementKind: String, at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: context.containerSize.width - 40, height: 40)
    }
    
    // private funcs
    
    private func userHeaderView(atIndex index: Int) -> UICollectionReusableView {
        guard let headerView = collectionContext?.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, for: self, class: CUSectionHeaderView.self, at: index) as? CUSectionHeaderView else {
            return UICollectionReusableView()
        }
        headerView.setup(titleText: "Вчера")
        return headerView
    }
}
