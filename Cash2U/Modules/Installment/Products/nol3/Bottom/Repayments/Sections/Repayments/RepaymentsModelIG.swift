//
//  RepaymentsModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 4/10/22.
//

import UIKit
import IGListKit

final class RepaymentsModelIG: ListDiffable {
    
    let id = "RepaymentsModelIG"
    let name: String
    let isLoading: Bool
    
    init(name: String, isLoading: Bool) {
        self.name = name
        self.isLoading = isLoading
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? RepaymentsModelIG else { return false }
        return id == object.id && name == object.name && isLoading == object.isLoading
    }
}
