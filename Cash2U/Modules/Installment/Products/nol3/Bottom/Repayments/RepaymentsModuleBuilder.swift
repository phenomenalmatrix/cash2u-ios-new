//
//  RepaymentsModuleBuilder.swift
//  Cash2U
//
//  Created by talgar osmonov on 4/10/22.
//  
//

import UIKit

final class RepaymentsModuleConfigurator {
    /// Собрать модуль
    class func build() -> UIViewController {
        let view = RepaymentsViewController()
        let presenter = RepaymentsPresenter()
        let interactor = RepaymentsInteractor()
        let router = RepaymentsRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
