//
//  RepaymentsPresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 4/10/22.
//  
//

import Foundation

final class RepaymentsPresenter {
    
    weak var view: RepaymentsViewProtocol?
    var interactor: RepaymentsInteractorProtocol?
    var router: RepaymentsRouterProtocol?
    
}

extension RepaymentsPresenter: RepaymentsViewToPresenterProtocol {
}

extension RepaymentsPresenter: RepaymentsInteractorToPresenterProtocol {
}
