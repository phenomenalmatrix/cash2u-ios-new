//
//  RepaymentsProtocols.swift
//  Cash2U
//
//  Created by talgar osmonov on 4/10/22.
//  
//

import Foundation

protocol RepaymentsViewProtocol: AnyObject {
}

protocol RepaymentsViewToPresenterProtocol: AnyObject {
}

protocol RepaymentsInteractorProtocol: AnyObject {
}

protocol RepaymentsInteractorToPresenterProtocol: AnyObject {
}

protocol RepaymentsRouterProtocol: AnyObject {
}
