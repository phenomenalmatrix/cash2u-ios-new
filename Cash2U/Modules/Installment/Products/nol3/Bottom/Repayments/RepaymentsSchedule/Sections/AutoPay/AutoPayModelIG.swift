//
//  AutoPayModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 6/10/22.
//

import UIKit
import IGListKit

final class AutoPayModelIG: ListDiffable {
    
    let id = "AutoPayModelIG"
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? AutoPayModelIG else { return false }
        return id == object.id
    }
}
