//
//  RepaymentsScheduleProtocols.swift
//  Cash2U
//
//  Created by talgar osmonov on 5/10/22.
//  
//

import Foundation

protocol RepaymentsScheduleViewProtocol: AnyObject {
}

protocol RepaymentsScheduleViewToPresenterProtocol: AnyObject {
}

protocol RepaymentsScheduleInteractorProtocol: AnyObject {
}

protocol RepaymentsScheduleInteractorToPresenterProtocol: AnyObject {
}

protocol RepaymentsScheduleRouterProtocol: AnyObject {
}
