//
//  RepaymentsScheduleBlankSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 6/10/22.
//

import UIKit
import IGListKit

class RepaymentsScheduleBlankSectionController: ListSectionController, ListSupplementaryViewSource {
    
    private var item: RepaymentsScheduleBlankModelIG?
    
    required init(isHeaderHidden: Bool = false) {
        super.init()
        self.inset = UIEdgeInsets(top: 0, left: 0, bottom: 16, right: 0)
        if !isHeaderHidden {
            supplementaryViewSource = self
        }
    }
    
    override func numberOfItems() -> Int {
        return 5
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        
        if index == 2 {
            return CGSize(width: (context.containerSize.width) - 40 , height: 80)
        } else if index == 0 {
            return CGSize(width: (context.containerSize.width) - 40 , height: 45)
        } else {
            return CGSize(width: (context.containerSize.width) - 40 , height: 45)
        }
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        
        if index == 2 {
            guard let cell: CURepaymentScheduleShopCell = collectionContext!.dequeueReusableCell(of: CURepaymentScheduleShopCell.self, for: self, at: index) as? CURepaymentScheduleShopCell else {
                return UICollectionViewCell()
            }
            
            if let item = item {
                cell.setup(text: item.shopName, image: item.shopImage)
            }
            
            return cell
        } else {
            guard let cell: CUBlankView = collectionContext!.dequeueReusableCell(of: CUBlankView.self, for: self, at: index) as? CUBlankView else {
                return UICollectionViewCell()
            }
            
            if let item = item {
                if index == 0 {
                    cell.setup(placeholder: "До 5-го мая:", text: item.payBeforeAmount, placeholderSize: 20, textSize: 22, placeholderColor: .mainTextColor, isBottomLineHidden: true)
                } else if index == 1 {
                    cell.setup(placeholder: "Уже внесено:", text: item.contributedAmount, placeholderColor: .mainTextColor)
                } else if index == 3 {
                    cell.setup(placeholder: "Дата покупки:", text: item.purchaseDate, placeholderColor: .mainTextColor)
                } else if index == 4 {
                    cell.setup(placeholder: "Сумма покупки:", text: item.purchaseAmount, placeholderColor: .mainTextColor, textColor: .systemGreen, isBottomLineHidden: true)
                }
            }
            
            return cell
        }
    }

    override func didUpdate(to object: Any) {
        item = object as? RepaymentsScheduleBlankModelIG
    }
    
    override func didSelectItem(at index: Int) {
        
    }
}

// MARK: - SuplementaryViews

extension RepaymentsScheduleBlankSectionController {
    func supportedElementKinds() -> [String] {
        return [UICollectionView.elementKindSectionHeader]
    }

    func viewForSupplementaryElement(ofKind elementKind: String, at index: Int) -> UICollectionReusableView {
        switch elementKind {
        case UICollectionView.elementKindSectionHeader:
            return userHeaderView(atIndex: index)
        default:
            return UICollectionReusableView()
        }
    }
    
    func sizeForSupplementaryView(ofKind elementKind: String, at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: context.containerSize.width - 40, height: 65)
    }
    
    // private funcs
    
    private func userHeaderView(atIndex index: Int) -> UICollectionReusableView {
        guard let headerView = collectionContext?.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, for: self, class: CUSectionTitleHeaderView.self, at: index) as? CUSectionTitleHeaderView else {
            return UICollectionReusableView()
        }
        headerView.setup(leftTitleText: "Май 2022")
        return headerView
    }
}
