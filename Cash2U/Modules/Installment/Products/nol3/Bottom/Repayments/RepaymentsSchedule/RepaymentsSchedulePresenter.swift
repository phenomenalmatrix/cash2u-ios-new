//
//  RepaymentsSchedulePresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 5/10/22.
//  
//

import Foundation

final class RepaymentsSchedulePresenter {
    
    weak var view: RepaymentsScheduleViewProtocol?
    var interactor: RepaymentsScheduleInteractorProtocol?
    var router: RepaymentsScheduleRouterProtocol?
    
}

extension RepaymentsSchedulePresenter: RepaymentsScheduleViewToPresenterProtocol {
}

extension RepaymentsSchedulePresenter: RepaymentsScheduleInteractorToPresenterProtocol {
}
