//
//  AutoPaySectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 6/10/22.
//

import UIKit
import IGListKit

class AutoPaySectionController: ListSectionController {
    
    private var item: AutoPayModelIG?

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 0, left: 0, bottom: 25, right: 0)
    }
    
    override func numberOfItems() -> Int {
        return 1
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width) , height: 60)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUSwitchWithTitleView = collectionContext!.dequeueReusableCell(of: CUSwitchWithTitleView.self, for: self, at: index) as? CUSwitchWithTitleView else {
            return UICollectionViewCell()
        }
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? AutoPayModelIG
    }
    
    override func didSelectItem(at index: Int) {
    }
}
