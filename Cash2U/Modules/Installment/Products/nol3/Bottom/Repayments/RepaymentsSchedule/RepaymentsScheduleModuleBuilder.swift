//
//  RepaymentsScheduleModuleBuilder.swift
//  Cash2U
//
//  Created by talgar osmonov on 5/10/22.
//  
//

import UIKit

final class RepaymentsScheduleModuleConfigurator {
    /// Собрать модуль
    class func build() -> UIViewController {
        let view = RepaymentsScheduleViewController()
        let presenter = RepaymentsSchedulePresenter()
        let interactor = RepaymentsScheduleInteractor()
        let router = RepaymentsScheduleRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
