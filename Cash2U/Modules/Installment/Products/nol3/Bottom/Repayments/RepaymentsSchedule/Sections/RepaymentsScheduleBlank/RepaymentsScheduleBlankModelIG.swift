//
//  RepaymentsScheduleBlankModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 6/10/22.
//

import UIKit
import IGListKit

final class RepaymentsScheduleBlankModelIG: ListDiffable {
    
    let id = "RepaymentsScheduleBlankModelIG"
    let payBeforeAmount: String
    let contributedAmount: String
    let shopName: String
    let shopImage: String
    let purchaseDate: String
    let purchaseAmount: String
    let isLoading: Bool
    
    init(
        payBeforeAmount: String,
        contributedAmount: String,
        shopName: String,
        shopImage: String,
        purchaseDate: String,
        purchaseAmount: String,
        isLoading: Bool
    ) {
        self.contributedAmount = contributedAmount
        self.shopImage = shopImage
        self.payBeforeAmount = payBeforeAmount
        self.purchaseDate = purchaseDate
        self.shopName = shopName
        self.purchaseAmount = purchaseAmount
        self.isLoading = isLoading
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? RepaymentsScheduleBlankModelIG else { return false }
        return id == object.id && purchaseDate == object.purchaseDate && shopName == object.shopName && purchaseAmount == object.purchaseAmount && isLoading == object.isLoading
    }
}
