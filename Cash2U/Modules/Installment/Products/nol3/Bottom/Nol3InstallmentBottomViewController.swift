//
//  Nol3InstallmentFooterViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 4/10/22.
//

import UIKit

final class Nol3InstallmentBottomViewController: UIPageViewController, PagerAwareProtocol {
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    weak var pageDelegate: BottomPageDelegate?

    var currentViewController: UIViewController? {
        return viewControllers?.first
    }

    var pagerTabHeight: CGFloat? {
        return 60
    }
    
    private lazy var vcs: [UIViewController] = {
        var vcList: [UIViewController] = []
        vcList.append(PurchasesModuleConfigurator.build())
        vcList.append(RepaymentsModuleConfigurator.build())
        vcList.append(InstallmentHistoryModuleConfigurator.build())
        return vcList
    }()
    
    override init(transitionStyle style: UIPageViewController.TransitionStyle, navigationOrientation: UIPageViewController.NavigationOrientation, options: [UIPageViewController.OptionsKey : Any]? = nil) {
        super.init(transitionStyle: .scroll, navigationOrientation: navigationOrientation)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = .mainColor
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
        setViewControllers([vcs[0]], direction: .forward, animated: false, completion: nil)
        let viewController = vcs[0]
        let index = vcs.firstIndex(of: viewController)
        pageDelegate?.tp_pageViewController(viewController, didSelectPageAt: index!)
        for view in self.view.subviews {
            if view is UIScrollView {
                (view as? UIScrollView)!.delaysContentTouches = false
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(
            self,selector:#selector(installmentPageSelectedListener(_:)),
            name: .installmentPageSelected,
            object: nil)
        
    }
    
    deinit {
        NotificationCenter().removeObserver(self)
    }
    
    // MARK: - UIActions
    
    @objc private func installmentPageSelectedListener(_ notification: Notification) {
        if let installmentPageSelected = CUPartnerPageButtonType(rawValue: notification.userInfo?["installmentPageSelected"] as! String) {
            switch installmentPageSelected {
            case .purchases:
                setViewControllers([vcs[0]], direction: .forward, animated: false, completion: nil)
                let viewController = vcs[0]
                let index = vcs.firstIndex(of: viewController)
                pageDelegate?.tp_pageViewController(viewController, didSelectPageAt: index!)
            case .repayment:
                setViewControllers([vcs[1]], direction: .forward, animated: false, completion: nil)
                let viewController = vcs[1]
                let index = vcs.firstIndex(of: viewController)
                pageDelegate?.tp_pageViewController(viewController, didSelectPageAt: index!)
            case .history:
                setViewControllers([vcs[2]], direction: .forward, animated: false, completion: nil)
                let viewController = vcs[2]
                let index = vcs.firstIndex(of: viewController)
                pageDelegate?.tp_pageViewController(viewController, didSelectPageAt: index!)
            default:
                break
            }
        }
        
    }
}

extension Nol3InstallmentBottomViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
    }
}
