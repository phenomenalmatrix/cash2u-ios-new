//
//  PurchasesInteractor.swift
//  Cash2U
//
//  Created by talgar osmonov on 4/10/22.
//  
//

import Foundation

final class PurchasesInteractor {
    weak var presenter: PurchasesInteractorToPresenterProtocol?
}

extension PurchasesInteractor: PurchasesInteractorProtocol {
}
