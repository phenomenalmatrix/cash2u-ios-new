//
//  PurchasesModuleBuilder.swift
//  Cash2U
//
//  Created by talgar osmonov on 4/10/22.
//  
//

import UIKit

final class PurchasesModuleConfigurator {
    /// Собрать модуль
    class func build() -> UIViewController {
        let view = PurchasesViewController()
        let presenter = PurchasesPresenter()
        let interactor = PurchasesInteractor()
        let router = PurchasesRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
