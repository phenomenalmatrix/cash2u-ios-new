//
//  PurchasesDetailModuleBuilder.swift
//  Cash2U
//
//  Created by talgar osmonov on 5/10/22.
//  
//

import UIKit

final class PurchasesDetailModuleConfigurator {
    /// Собрать модуль
    class func build() -> UIViewController {
        let view = PurchasesDetailViewController()
        let presenter = PurchasesDetailPresenter()
        let interactor = PurchasesDetailInteractor()
        let router = PurchasesDetailRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
