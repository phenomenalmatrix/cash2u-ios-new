//
//  PurchasesDetailViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 5/10/22.
//  
//

import UIKit
import IGListKit

final class PurchasesDetailViewController: CUViewController {
    
    var presenter: PurchasesDetailViewToPresenterProtocol?
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private lazy var adapter: ListAdapter = {
        let adapter = ListAdapter(updater: ListAdapterUpdater(), viewController: self)
        adapter.collectionView = mainCollection
        adapter.dataSource = self
        return adapter
    }()
    
    private func createItemsData(type: CUPartnerPageButtonType) -> [ListDiffable] {
        var aa = [
        ] as [ListDiffable]
        aa.append(BlankModelIG(purchaseDate: "05 апреля 2022", shopName: "Nike shop KG", purchaseAmount: "2 200 с", isLoading: false))
        aa.append(PaymentScheduleModelIG(purchaseDate: "05 апреля 2022", shopName: "Nike shop KG", purchaseAmount: "2 200 с", isLoading: false))
        aa.append(ButtonSectionModelIG(title: "Оставьте отзыв", action: "feedback"))
        return aa
    }
    
    private lazy var itemsData: [ListDiffable] = createItemsData(type: .info)
    
    private lazy var mainCollection: CUCollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        let mainCollection = CUCollectionView(
            frame: CGRect.zero,
            collectionViewLayout: flowLayout
        )
        mainCollection.translatesAutoresizingMaskIntoConstraints = false
        mainCollection.alwaysBounceVertical = true
        return mainCollection
    }()
    
    override func loadView() {
        super.loadView()
        title = "Детали покупки"
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adapter.performUpdates(animated: true, completion: nil)
    }
}

extension PurchasesDetailViewController: PurchasesDetailViewProtocol {
}

extension PurchasesDetailViewController: PartnerPageButtonSectionControllerDelegate {
    func didSelect(type: CUPartnerPageButtonType) {
        itemsData = createItemsData(type: type)
        adapter.performUpdates(animated: true)
    }
}

extension PurchasesDetailViewController: ButtonSectionControllerDelegate {
    func onButtonTapped(action: String) {
        switch action {
        case "feedback":
            let vc = LeaveFeedbackViewController()
            self.navigationController?.presentPanModal(vc)
        default:
            break
        }
    }
}

extension PurchasesDetailViewController: PaymentScheduleSectionControllerDelegate {
    func leaveFeedbackButtonTapped() {
        let vc = LeaveFeedbackViewController()
        self.navigationController?.presentPanModal(vc)
    }
}

// MARK: - ListAdapterDataSource

extension PurchasesDetailViewController: ListAdapterDataSource {
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        itemsData
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        if object is CurrencyModelIG {
            let section = CurrencySectionController()
            return section
        } else if object is BlankModelIG {
            let section = BlankSectionController()
//            section.delegate = self
            return section
        } else if object is PaymentScheduleModelIG {
            let section = PaymentScheduleSectionController(isHeaderHidden: false)
            section.delegate = self
            return section
        } else if object is GoodsModelIG {
            let section = GoodsSectionController()
//            section.delegate = self
            return section
        } else if object is ReviewsModelIG {
            let section = ReviewsSectionController()
//            section.delegate = self
            return section
        } else if object is ButtonSectionModelIG {
            let section = ButtonSectionController()
            section.delegate = self
            return section
        } else if object is GalleryModelIG {
            let section = GallerySectionController()
//            section.delegate = self
            return section
        } else if object is DescriptionModelIG {
            let section = DescriptionSectionController()
//            section.delegate = self
            return section
        } else if object is InstallmentAvailableModelIG {
            let section = InstallmentAvailableSectionController()
//            section.delegate = self
            return section
        } else if object is PartnerPageButtonModelIG {
            let section = PartnerPageButtonSectionController()
            section.delegate = self
            return section
        } else if object is PartnerPageTopModelIG {
            let section = PartnerPageTopSectionController()
//            section.delegate = self
            return section
        } else if object is CategoryTopSectionModelIG {
            let section = CategoryTopSectionController()
//            section.delegate = self
            return section
        } else if object is CategorySelectionModelIG {
            let section = CategorySelectionSectionController()
//            section.delegate = self
            return section
        } else if object is PartnersModelIG {
            let section = PartnersSectionController()
//            section.delegate = self
            return section
        } else if object is BestPartnersModelIG {
            let section = RecomendationSectionController()
//            section.delegate = self
            return section
        } else if object is TagsModelIG {
            let section = HorizontalTagsSectionController()
//            section.delegate = self
            return section
        } else if object is LocationModelIG {
            let section = LocationSectionController()
//            section.delegate = self
            return section
        } else if object is SearchModelIG {
            let section = SearchSectionController()
//            section.delegate = self
            return section
        } else if object is NewPartnersModelIG {
            let section = NewPartnersSectionController()
//            section.delegate = self
            return section
        } else if object is MallsModelIG {
            let section = MallsSectionController(isHeaderButtonHidden: false)
//            section.delegate = self
            return section
        } else if object is DiscountModelIG {
            let section = DiscountSectionController(isHeaderHidden: false)
//            section.delegate = self
            return section
        } else if object is SupportModelIG {
            return SupportSectionController()
        } else if object is VersionModelIG {
            return VersionSectionController()
        } else if object is GreetingsSectionModelIG {
            let section = GreetingsSectionController()
//            section.delegate = self
            return section
        } else if object is BazaarModelIG {
            let section = BazaarsSectionController(isHeaderButtonHidden: false)
//            section.delegate = self
            return section
        } else if object is CategoryModelIG {
            let section = CategorySectionController()
//            section.delegate = self
            return section
        } else {
            return ListSectionController()
        }
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }
}

extension PurchasesDetailViewController  {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        view.addSubview(mainCollection)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        mainCollection.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
