//
//  PurchasesDetailPresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 5/10/22.
//  
//

import Foundation

final class PurchasesDetailPresenter {
    
    weak var view: PurchasesDetailViewProtocol?
    var interactor: PurchasesDetailInteractorProtocol?
    var router: PurchasesDetailRouterProtocol?
    
}

extension PurchasesDetailPresenter: PurchasesDetailViewToPresenterProtocol {
}

extension PurchasesDetailPresenter: PurchasesDetailInteractorToPresenterProtocol {
}
