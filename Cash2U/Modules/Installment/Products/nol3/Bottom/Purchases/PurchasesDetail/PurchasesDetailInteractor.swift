//
//  PurchasesDetailInteractor.swift
//  Cash2U
//
//  Created by talgar osmonov on 5/10/22.
//  
//

import Foundation

final class PurchasesDetailInteractor {
    weak var presenter: PurchasesDetailInteractorToPresenterProtocol?
}

extension PurchasesDetailInteractor: PurchasesDetailInteractorProtocol {
}
