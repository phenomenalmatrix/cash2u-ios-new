//
//  PaymentScheduleSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 5/10/22.
//

import UIKit
import IGListKit

protocol PaymentScheduleSectionControllerDelegate: AnyObject {
    func leaveFeedbackButtonTapped()
}

class PaymentScheduleSectionController: ListSectionController, ListSupplementaryViewSource {
    
    private var item: PaymentScheduleModelIG?
    weak var delegate: PaymentScheduleSectionControllerDelegate? = nil
    
    required init(isHeaderHidden: Bool = true) {
        super.init()
        self.inset = UIEdgeInsets(top: isHeaderHidden ? 0 : 10, left: 0, bottom: 32, right: 0)
        minimumLineSpacing = 12
        if !isHeaderHidden {
            supplementaryViewSource = self
        }
    }
    
    override func numberOfItems() -> Int {
        return 3
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        
//        if index == 3 {
//            return CGSize(width: (context.containerSize.width) - 40 , height: 50)
//        } else {
//            return CGSize(width: (context.containerSize.width) - 40 , height: 100)
//        }
        
        return CGSize(width: (context.containerSize.width) - 40 , height: 100)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        
//        if index == 3 {
//            guard let cell: CUCellButton = collectionContext!.dequeueReusableCell(of: CUCellButton.self, for: self, at: index) as? CUCellButton else {
//                return UICollectionViewCell()
//            }
//
//            cell.setup(title: "Оставьте отзыв", titleColor: .grayColor, backgroundColor: .clear, borderWidth: 1, borderColor: .grayColor, cornerRadius: 50 / 2)
//
//            return cell
//        } else {
//            guard let cell: CUPaymentScheduleCell = collectionContext!.dequeueReusableCell(of: CUPaymentScheduleCell.self, for: self, at: index) as? CUPaymentScheduleCell else {
//                return UICollectionViewCell()
//            }
//
//            if let item = item {
//                cell.setup()
//            }
//            return cell
//        }
        
        guard let cell: CUPaymentScheduleCell = collectionContext!.dequeueReusableCell(of: CUPaymentScheduleCell.self, for: self, at: index) as? CUPaymentScheduleCell else {
            return UICollectionViewCell()
        }
        
        if let item = item {
            cell.setup()
        }
        
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? PaymentScheduleModelIG
    }
    
    override func didSelectItem(at index: Int) {
//        if index == 3 {
//            delegate?.leaveFeedbackButtonTapped()
//        }
    }
}

// MARK: - SuplementaryViews

extension PaymentScheduleSectionController {
    func supportedElementKinds() -> [String] {
        return [UICollectionView.elementKindSectionHeader]
    }

    func viewForSupplementaryElement(ofKind elementKind: String, at index: Int) -> UICollectionReusableView {
        switch elementKind {
        case UICollectionView.elementKindSectionHeader:
            return userHeaderView(atIndex: index)
        default:
            return UICollectionReusableView()
        }
    }
    
    func sizeForSupplementaryView(ofKind elementKind: String, at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: context.containerSize.width - 40, height: 40)
    }
    
    // private funcs
    
    private func userHeaderView(atIndex index: Int) -> UICollectionReusableView {
        guard let headerView = collectionContext?.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, for: self, class: CUSectionHeaderView.self, at: index) as? CUSectionHeaderView else {
            return UICollectionReusableView()
        }
        headerView.setup(titleText: "График оплаты", isRightButtonHidden: true)
        return headerView
    }
}
