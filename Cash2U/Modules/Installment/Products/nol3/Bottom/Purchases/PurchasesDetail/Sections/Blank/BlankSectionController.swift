//
//  BlankSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 5/10/22.
//

import UIKit
import IGListKit
import Atributika

class BlankSectionController: ListSectionController {
    
    private var item: BlankModelIG?

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 10, left: 0, bottom: 32, right: 0)
    }
    
    override func numberOfItems() -> Int {
        return 3
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        
        guard let item = item else {
            return .zero
        }
        
        if index == 1 {
            var attribitedTitle: NSAttributedString {
                let style = Style.mainTextStyle(
                    size: 16,
                    fontType: .medium,
                    alignment: .right
                )
                return item.shopName.styleAll(style).attributedString
            }
            
            return CGSize(width: (context.containerSize.width) - 40, height: attribitedTitle.getHeight(withConstrainedWidth: (context.containerSize.width) - 80) + 45)
        } else {
            return CGSize(width: (context.containerSize.width) - 40 , height: 45)
        }
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUBlankView = collectionContext!.dequeueReusableCell(of: CUBlankView.self, for: self, at: index) as? CUBlankView else {
            return UICollectionViewCell()
        }
        
        if let item = item {
            if index == 0 {
                cell.setup(placeholder: "Дата покупки:", text: item.purchaseDate)
            } else if index == 1 {
                cell.setup(placeholder: "Магазин:", text: item.shopName)
            } else if index == 2 {
                cell.setup(placeholder: "Сумма покупки:", text: item.purchaseAmount )
            }
        }
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? BlankModelIG
    }
    
    override func didSelectItem(at index: Int) {
//        delegate?.selectSectionController(self)
//        if let cell = self.collectionContext?.cellForItem(at: index, sectionController: self) as? DZBigStickerCell {
//            if item?.isAnimated == true {
//                cell.animatedLogoImageView.startAnimating()
//            } else {}
//        }
    }
}
