//
//  PaymentScheduleModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 5/10/22.
//

import UIKit
import IGListKit

final class PaymentScheduleModelIG: ListDiffable {
    
    let id = "PaymentScheduleModelIG"
    let purchaseDate: String
    let shopName: String
    let purchaseAmount: String
    let isLoading: Bool
    
    init(purchaseDate: String, shopName: String, purchaseAmount: String, isLoading: Bool) {
        self.purchaseDate = purchaseDate
        self.shopName = shopName
        self.purchaseAmount = purchaseAmount
        self.isLoading = isLoading
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? PaymentScheduleModelIG else { return false }
        return id == object.id && purchaseDate == object.purchaseDate && shopName == object.shopName && purchaseAmount == object.purchaseAmount && isLoading == object.isLoading
    }
}
