//
//  PurchasesModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 4/10/22.
//

import UIKit
import IGListKit

final class PurchasesModelIG: ListDiffable {
    
    let id = "PurchasesModelIG"
    let name: String
    let isLoading: Bool
    
    init(name: String, isLoading: Bool) {
        self.name = name
        self.isLoading = isLoading
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? PurchasesModelIG else { return false }
        return id == object.id && name == object.name && isLoading == object.isLoading
    }
}
