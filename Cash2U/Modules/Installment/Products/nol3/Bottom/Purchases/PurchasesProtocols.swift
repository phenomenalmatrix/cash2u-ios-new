//
//  PurchasesProtocols.swift
//  Cash2U
//
//  Created by talgar osmonov on 4/10/22.
//  
//

import Foundation

protocol PurchasesViewProtocol: AnyObject {
}

protocol PurchasesViewToPresenterProtocol: AnyObject {
}

protocol PurchasesInteractorProtocol: AnyObject {
}

protocol PurchasesInteractorToPresenterProtocol: AnyObject {
}

protocol PurchasesRouterProtocol: AnyObject {
}
