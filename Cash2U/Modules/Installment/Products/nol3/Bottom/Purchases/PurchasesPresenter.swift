//
//  PurchasesPresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 4/10/22.
//  
//

import Foundation

final class PurchasesPresenter {
    
    weak var view: PurchasesViewProtocol?
    var interactor: PurchasesInteractorProtocol?
    var router: PurchasesRouterProtocol?
    
}

extension PurchasesPresenter: PurchasesViewToPresenterProtocol {
}

extension PurchasesPresenter: PurchasesInteractorToPresenterProtocol {
}
