//
//  InstallmentModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 10/10/22.
//

import UIKit
import IGListKit

enum InstallmentType: String {
    case nol3 = "nol3"
    case nol6 = "nol6"
    case fuel = "fuel"
    case product = "product"
}

final class InstallmentModelIG: ListDiffable {
    
    let id = UUID().uuidString
    let type: InstallmentType
    
    init(type: InstallmentType) {
        self.type = type
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? InstallmentModelIG else { return false }
        return id == object.id && type == object.type
    }
}
