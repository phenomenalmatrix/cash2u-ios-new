//
//  InstallmentSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 10/10/22.
//

import UIKit
import IGListKit

protocol InstallmentSectionControllerDelegate: AnyObject {
    func didSelect(type: InstallmentType)
}

class InstallmentSectionController: ListSectionController {
    
    weak var delegate: InstallmentSectionControllerDelegate? = nil
    
    private var item: InstallmentModelIG?
    private var isGuest = false
    
    required init(isGuest: Bool = false) {
        self.isGuest = isGuest
        super.init()
        self.inset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
    }
    
    override func numberOfItems() -> Int {
        return 1
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width) - 40, height: ((context.containerSize.width) / 2.1))
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUInstallmentCell = collectionContext!.dequeueReusableCell(of: CUInstallmentCell.self, for: self, at: index) as? CUInstallmentCell else {
            return UICollectionViewCell()
        }
        
        if let item = item {
            cell.setup(type: item.type, isGuest: isGuest)
        }
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? InstallmentModelIG
    }
    
    override func didSelectItem(at index: Int) {
        if let item {
            delegate?.didSelect(type: item.type)
        }
    }
}
