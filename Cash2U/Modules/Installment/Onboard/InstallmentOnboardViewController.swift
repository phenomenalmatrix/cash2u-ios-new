//
//  InstallmentOnboardViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 12/10/22.
//  
//

import UIKit
import AVFoundation

struct InstallmentOnboardModel {
    let type: InstallmentType
    let name: String
    let text: String
    let description: String
    let subdescription: String?
    let video: String
}

final class InstallmentOnboardViewController: CUViewController {
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    var installmentType: InstallmentType = .nol3
    
    private var data = [
        InstallmentOnboardModel(type: .nol3, name: "0/0/3", text: "Оплата частями",
                        description: "Удобный способ оплаты покупок частями в магазинах наших партнеров",
                        subdescription: "🔥 Без первичного взноса\n🤝 Без скрытых комиссий\n😎 Без процентов\n🚀 Без переплат",
                        video: "instraction"),
        InstallmentOnboardModel(type: .nol6, name: "0/0/6", text: "Мобильный кошелек",
                        description: "Мультивалютные счета, переводы, обмен валют, оплата услуг и многое другое",
                        subdescription: "💸 Мультивалютные счета \n🔃 Конвертация валют \n🌍 Переводы заграницу и внутри страны \n🏧 Вывод средств",
                        video: "instraction"),
        InstallmentOnboardModel(type: .fuel, name: "Топливная карта", text: "Партнеры cash2u",
                        description: "Оплачивайте покупки или услуги у наших партнеров с помощью cash2u",
                        subdescription: nil,
                        video: "instraction"),
        InstallmentOnboardModel(type: .product, name: "Продуктовая карта", text: "Оплата частями",
                        description: "Удобный способ оплаты покупок частями в магазинах наших партнеров",
                        subdescription: "🔥 Без первичного взноса\n🤝 Без скрытых комиссий\n😎 Без процентов\n🚀 Без переплат",
                        video: "instraction")
    ]
    
    var player: AVQueuePlayer?
    var playerLooper: AVPlayerLooper?
    
    private lazy var playerLayer: AVPlayerLayer = {
        let view = AVPlayerLayer()
        view.videoGravity = .resizeAspectFill
        view.masksToBounds = true
        return view
    }()
    
    private lazy var playerContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.cornerRadius = 18
        return view
    }()
    
    private lazy var titleLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(size: 28, numberOfLines: 2, fontType: .bold, isSizeToFit: true)
        return view
    }()
    
    private lazy var descriptionLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(size: 20, numberOfLines: 3, fontType: .medium, isSizeToFit: true)
        return view
    }()
    
    private lazy var subdescriptionLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(size: 22, numberOfLines: 0, fontType: .medium, lineHeight: 40, isSizeToFit: true)
        return view
    }()
    
    private lazy var connectButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Подключить услугу")
        return view
    }()
    
    override func loadView() {
        super.loadView()
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let index = data.firstIndex(where: {$0.type == installmentType}) {
            let item = data[index]
            title = item.name
            setup(model: item, isMuted: true)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        playerLayer.frame = CGRect(origin: .zero, size: .init(width: playerContainer.frame.width, height: playerContainer.frame.height))
    }
    
    // MARK: - Helpers
    
    private func setup(model: InstallmentOnboardModel, isMuted: Bool = false) {
        DispatchQueue.main.async {
            
            self.titleLabel.changeText(title: model.text)
            self.descriptionLabel.changeText(title: model.description)
            self.subdescriptionLabel.changeText(title: model.subdescription ?? "")
            
            guard let path = Bundle.main.path(forResource: model.video, ofType:"MP4") else {return}
            self.player = AVQueuePlayer(url: URL(fileURLWithPath: path))
            self.player?.volume = isMuted ? 0 : 1
            self.playerLooper = AVPlayerLooper(player: self.player!, templateItem: self.player!.currentItem!)
            self.playerLayer.player = self.player
            
            self.player?.play()
        }
    }
}

extension InstallmentOnboardViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        
        view.addSubview(playerContainer)
        playerContainer.layer.addSublayer(playerLayer)
        view.addSubview(subdescriptionLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(titleLabel)
        view.addSubview(connectButton)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        playerContainer.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top).offset(20)
            make.leading.trailing.equalToSuperview().inset(20)
            make.bottom.equalTo(view.safeArea.bottom).offset(-100)
        }
        
        subdescriptionLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(40)
            make.bottom.equalTo(view.safeArea.bottom).offset(-120)
        }
        
        descriptionLabel.snp.makeConstraints { make in
            make.bottom.equalTo(subdescriptionLabel.snp.top).offset(-10)
            make.leading.trailing.equalToSuperview().inset(40)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.bottom.equalTo(descriptionLabel.snp.top).offset(-20)
            make.leading.trailing.equalToSuperview().inset(40)
        }
        
        connectButton.snp.makeConstraints { make in
            make.bottom.equalTo(view.safeArea.bottom).offset(-16)
            make.leading.trailing.equalToSuperview().inset(20)
            make.height.equalTo(ScreenHelper.mainButtonHeight)
        }
    }
}
