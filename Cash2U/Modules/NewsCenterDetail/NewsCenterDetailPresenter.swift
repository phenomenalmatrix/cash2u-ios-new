//
//  NewsCenterDetailPresenter.swift
//  Cash2U
//
//  Created by Oroz on 20/9/22.
//  
//

import Foundation

final class NewsCenterDetailPresenter {
    
    weak var view: NewsCenterDetailViewProtocol?
    var interactor: NewsCenterDetailInteractorProtocol?
    var router: NewsCenterDetailRouterProtocol?
    var model: NewsCenterlModelIG?
}

extension NewsCenterDetailPresenter: NewsCenterDetailViewToPresenterProtocol {
}

extension NewsCenterDetailPresenter: NewsCenterDetailInteractorToPresenterProtocol {
}
