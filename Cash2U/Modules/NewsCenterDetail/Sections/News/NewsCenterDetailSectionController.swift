//
//  NewsCenterDetailSectionController.swift
//  Cash2U
//
//  Created by Oroz on 22/9/22.
//

import Foundation
import IGListKit
import Atributika

class NewsCenterDetailSectionController: ListSectionController {
    
    private var item: NewsCenterlModelIG?
    
    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 10, left: 0, bottom: 20, right: 0)
        minimumLineSpacing = 30
    } 
    
    override func numberOfItems() -> Int {
        return 3
    }

    override func sizeForItem(at index: Int) -> CGSize {
        
        guard let item = item else {
            return .zero
        }

        var attribitedTitle: NSAttributedString {
            let style = Style.mainTextStyle(
                size: 17,
                fontType: .regular,
                color: .mainTextColor,
                alignment: .left
            )
            return item.description?.styleAll(style).attributedString ?? NSAttributedString(string: "")
        }
        
            guard let context = collectionContext else {
                return .zero
            }
            
        if index == 0{
            return CGSize(width: (context.containerSize.width) - 40, height: ((context.containerSize.width) / 2.3) + 115)
        } else if index == 1 {
            return CGSize(width: (context.containerSize.width) - 40, height: attribitedTitle.getHeight(withConstrainedWidth: (context.containerSize.width) - 40) + 50)
        } else {
            return CGSize(width: (context.containerSize.width) - 40, height: 80)
        }
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        if index == 0 {
            guard let cell: CUNewsCell = collectionContext!.dequeueReusableCell(of: CUNewsCell.self, for: self, at: index) as? CUNewsCell else {
                return UICollectionViewCell()
            }
            cell.setup()
            return cell
        } else if index == 1 {
            guard let cell: CUNewsCenterDetailTextCell = collectionContext!.dequeueReusableCell(of: CUNewsCenterDetailTextCell.self, for: self, at: index) as? CUNewsCenterDetailTextCell else {
                return UICollectionViewCell()
            }
            cell.setup(title: item?.description ?? "")
            return cell
        } else {
            guard let cell: CUNewsCenterDetailShareCell = collectionContext!.dequeueReusableCell(of: CUNewsCenterDetailShareCell.self, for: self, at: index) as? CUNewsCenterDetailShareCell else {
                return UICollectionViewCell()
            }
            cell.setup()
            return cell
        }
    }

    override func didUpdate(to object: Any) {
        item = object as? NewsCenterlModelIG
    }
    
}

class NewsCenterlDetailOtherNewsSectionController: ListSectionController, ListSupplementaryViewSource {
    
    private var item: NewsCenterlDetailOtherNewsModelIG?
    
    weak var delegate: SectionControllerDelegate? = nil

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 30, left: 0, bottom: 0, right: 0)
        minimumLineSpacing = 30
        self.supplementaryViewSource = self
    }
    
    override func numberOfItems() -> Int {
        return item?.alsoRead.count ?? 0
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width) - 40, height: ((context.containerSize.width) / 2.3) + 115)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUNewsCell = collectionContext!.dequeueReusableCell(of: CUNewsCell.self, for: self, at: index) as? CUNewsCell else {
            return UICollectionViewCell()
        }
        
        cell.setup()
        return cell
    }
    
    override func didSelectItem(at index: Int) {
        delegate?.selectSection(self)
    }

    override func didUpdate(to object: Any) {
        item = object as? NewsCenterlDetailOtherNewsModelIG
    }
    
}

// MARK: - SuplementaryViews

extension NewsCenterlDetailOtherNewsSectionController {
    func supportedElementKinds() -> [String] {
        return [UICollectionView.elementKindSectionHeader]
    }

    func viewForSupplementaryElement(ofKind elementKind: String, at index: Int) -> UICollectionReusableView {
        switch elementKind {
        case UICollectionView.elementKindSectionHeader:
            return userHeaderView(atIndex: index)
        default:
            return UICollectionReusableView()
        }
    }
    
    func sizeForSupplementaryView(ofKind elementKind: String, at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: context.containerSize.width - 40, height: 40)
    }
    
    // private funcs
    
    private func userHeaderView(atIndex index: Int) -> UICollectionReusableView {
        guard let headerView = collectionContext?.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, for: self, class: CUSectionHeaderView.self, at: index) as? CUSectionHeaderView else {
            return UICollectionReusableView()
        }
        headerView.setup(titleText: "Читайте также", isRightButtonHidden: true)
        return headerView
    }
}
