//
//  NewsCenterlDetailOtherNewsModelIG.swift
//  Cash2U
//
//  Created by Oroz on 27/9/22.
//

import Foundation
import IGListKit


final class NewsCenterlDetailOtherNewsModelIG: ListDiffable {
    
    let id: String
    let alsoRead: [NewsCenterlModelIG]
    
    init (id: String, alsoRead: [NewsCenterlModelIG]){
        self.id = id
        self.alsoRead = alsoRead
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? NewsCenterlDetailOtherNewsModelIG else { return false }
        return id == object.id && alsoRead.last?.title == object.alsoRead.last?.title
    }
    
}
