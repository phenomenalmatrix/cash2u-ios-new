//
//  NewsCenterDetailProtocols.swift
//  Cash2U
//
//  Created by Oroz on 20/9/22.
//  
//

import Foundation

protocol NewsCenterDetailViewProtocol: AnyObject {
    
}

protocol NewsCenterDetailViewToPresenterProtocol: AnyObject {
    
}

protocol NewsCenterDetailInteractorProtocol: AnyObject {
    
}

protocol NewsCenterDetailInteractorToPresenterProtocol: AnyObject {
    
}

protocol NewsCenterDetailRouterProtocol: AnyObject {
    
}
