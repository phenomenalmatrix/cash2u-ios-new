//
//  NewsCenterDetailModuleBuilder.swift
//  Cash2U
//
//  Created by Oroz on 20/9/22.
//  
//

import UIKit

final class NewsCenterDetailModuleConfigurator {
    /// Собрать модуль
    class func build(model: NewsCenterlModelIG) -> UIViewController {
        let view = NewsCenterDetailViewController()
        let presenter = NewsCenterDetailPresenter() 
        let interactor = NewsCenterDetailInteractor()
        let router = NewsCenterDetailRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        presenter.model = model
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
