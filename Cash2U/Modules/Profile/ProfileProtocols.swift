//
//  ProfileProtocols.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/5/22.
//  
//

import Foundation

protocol ProfileViewProtocol: AnyObject {
}

protocol ProfileViewToPresenterProtocol: AnyObject {
}

protocol ProfileInteractorProtocol: AnyObject {
}

protocol ProfileInteractorToPresenterProtocol: AnyObject {
}

protocol ProfileRouterProtocol: AnyObject {
}
