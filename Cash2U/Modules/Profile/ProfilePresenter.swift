//
//  ProfilePresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/5/22.
//  
//

import Foundation

final class ProfilePresenter {
    
    weak var view: ProfileViewProtocol?
    var interactor: ProfileInteractorProtocol?
    var router: ProfileRouterProtocol?
    
}

extension ProfilePresenter: ProfileViewToPresenterProtocol {
}

extension ProfilePresenter: ProfileInteractorToPresenterProtocol {
}
