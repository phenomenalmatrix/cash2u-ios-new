//
//  ProfileViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/5/22.
//  
//

import UIKit
import SwiftyUserDefaults

final class ProfileViewController: CUViewController {
    
    var presenter: ProfileViewToPresenterProtocol?
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = .orange
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension ProfileViewController: ProfileViewProtocol {
}

extension ProfileViewController {
    
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
    }
}
