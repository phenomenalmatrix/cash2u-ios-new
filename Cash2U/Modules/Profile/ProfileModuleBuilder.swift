//
//  ProfileModuleBuilder.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/5/22.
//  
//

import UIKit

final class ProfileModuleConfigurator {
    /// Собрать модуль
    class func build() -> UIViewController {
        let view = ProfileViewController()
        let presenter = ProfilePresenter()
        let interactor = ProfileInteractor()
        let router = ProfileRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
