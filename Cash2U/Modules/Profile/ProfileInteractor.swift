//
//  ProfileInteractor.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/5/22.
//  
//

import Foundation

final class ProfileInteractor {
    weak var presenter: ProfileInteractorToPresenterProtocol?
}

extension ProfileInteractor: ProfileInteractorProtocol {
}
