//
//  NewsCenterInteractor.swift
//  Cash2U
//
//  Created by Oroz on 13/9/22.
//  
//

import Foundation

final class NewsCenterInteractor {
    weak var presenter: NewsCenterInteractorToPresenterProtocol?
}

extension NewsCenterInteractor: NewsCenterInteractorProtocol {
}
