//
//  NewsCenterModuleBuilder.swift
//  Cash2U
//
//  Created by Oroz on 13/9/22.
//  
//

import UIKit

final class NewsCenterModuleConfigurator {
    /// Собрать модуль
    class func build() -> UIViewController {
        let view = NewsCenterViewController()
        let presenter = NewsCenterPresenter()
        let interactor = NewsCenterInteractor()
        let router = NewsCenterRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
