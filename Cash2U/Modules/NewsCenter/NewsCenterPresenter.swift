//
//  NewsCenterPresenter.swift
//  Cash2U
//
//  Created by Oroz on 13/9/22.
//  
//

import Foundation

final class NewsCenterPresenter {
    
    weak var view: NewsCenterViewProtocol?
    var interactor: NewsCenterInteractorProtocol?
    var router: NewsCenterRouterProtocol?
    
}

extension NewsCenterPresenter: NewsCenterViewToPresenterProtocol {
}

extension NewsCenterPresenter: NewsCenterInteractorToPresenterProtocol {
}
