//
//  NewsCenterViewController.swift
//  Cash2U
//
//  Created by Oroz on 13/9/22.
//  
//

import UIKit
import IGListKit

final class NewsCenterViewController: CUViewController {
    
    var presenter: NewsCenterViewToPresenterProtocol?
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private lazy var itemsData: [ListDiffable] = createItemsData()
    
    private lazy var adapter: ListAdapter = {
        let adapter = ListAdapter(updater: ListAdapterUpdater(), viewController: self)
        adapter.collectionView = newCollection
        adapter.dataSource = self
        return adapter
    }()
    
    private lazy var newCollection: CUCollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        let mainCollection = CUCollectionView(
            frame: CGRect.zero,
            collectionViewLayout: flowLayout
        )
        mainCollection.translatesAutoresizingMaskIntoConstraints = false
        mainCollection.alwaysBounceVertical = true
        return mainCollection
    }()
    
    private func createItemsData() -> [ListDiffable] {
        var aa = [
        ] as [ListDiffable]
        aa.append(NewsCenterlModelIG(id: "0", typeTitle: "asd", title: "sad", type: "asd", cretedAt: Date(), icon: "", img: "", description: "What Snowflake could do betterAs an investor, I expect Snowflake to show amazing profitability and record-breaking revenue numbers. As an Engineer, if Snowflake continues on the current path of ignoring performance, I expect them to lose share to the open-source community or some other competitor, eventually walking down the path of Oracle and Teradata. Here are a few things I think they can do to stay relevant in five years. Disclose Hardware SpecSnowflake charges you based on your consumption. You’re not buying any specific hardware and instead pay as you go using virtual warehouse credits that have no hardware definitions. For folks in technical or engineering backgrounds, this is a red flag. Whether your query runs on a machine with SSD or hard drive, low or high RAM, slow or fast CPU, high or low network bandwidth makes a measurable impact on performance. Snowflake is very secretive about their hardware and when I interacted with a Sales team during migration from Redshift, I could never get any SLAs on their query performance nor information on the hardware specs. This is distinctly different from Redshift, Firebolt, and Databricks which are very transparent and provide more flexibility in customizing your performance through hardware.Lack of transparency can also lead to bad incentives where Snowflake could revert to less optimal machinery due to internal pressures to improve its margins.Not adopting benchmarkSeveral months ago, DataBricks published a study highlighting that they outperform Snowflake on the TPC-DS benchmark, to which Snowflake posted a rebuttal. Snowflake's statement on benchmarks was very clear:In the same way that we had clarity about many things we wanted to do, we also had conviction about what we didn’t want to do. One such thing was engaging in benchmarking wars and making competitive performance claims divorced from real-world experiences. This practice is simply inconsistent with our core value of putting customers first.This is a very shortsighted statement and contradicts why data warehouses became so popular and what customers care about. Data warehouses became popular precisely because they delivered on performance promises, where you don’t have to wait for days or weeks for reporting to be done."))
        aa.append(NewsCenterlModelIG(id: "1", typeTitle: "asad", title: "sad", type: "asd", cretedAt: Date(), icon: "", img: "", description: "What Snowflake could do betterAs an investor, I expect Snowflake to show amazing profitability and record-breaking revenue numbers. As an Engineer, if Snowflake continues on the current path of ignoring performance, I expect them to lose share to the open-source community or some other competitor, eventually walking down the path of Oracle and Teradata. Here are a few things I think they can do to stay relevant in five years. Disclose Hardware SpecSnowflake charges you based on your consumption. You’re not buying any specific hardware and instead pay as you go using virtual warehouse credits that have no hardware definitions. For folks in technical or engineering backgrounds, this is a red flag. Whether your query runs on a machine with SSD or hard drive, low or high RAM, slow or fast CPU, high or low network bandwidth makes a measurable impact on performance. Snowflake is very secretive about their hardware and when I interacted with a Sales team during migration from Redshift, I could never get any SLAs on their query performance nor information on the hardware specs. This is distinctly different from Redshift, Firebolt, and Databricks which are very transparent and provide more flexibility in customizing your performance through hardware.Lack of transparency can also lead to bad incentives where Snowflake could revert to less optimal machinery due to internal pressures to improve its margins.Not adopting benchmarkSeveral months ago, DataBricks published a study highlighting that they outperform Snowflake on the TPC-DS benchmark, to which Snowflake posted a rebuttal. Snowflake's statement on benchmarks was very clear:In the same way that we had clarity about many things we wanted to do, we also had conviction about what we didn’t want to do. One such thing was engaging in benchmarking wars and making competitive performance claims divorced from real-world experiences. This practice is simply inconsistent with our core value of putting customers first.This is a very shortsighted statement and contradicts why data warehouses became so popular and what customers care about. Data warehouses became popular precisely because they delivered on performance promises, where you don’t have to wait for days or weeks for reporting to be done."))
        aa.append(NewsCenterlModelIG(id: "2", typeTitle: "sad", title: "sad", type: "sad", cretedAt: Date(), icon: "", img: "", description: "What Snowflake could do betterAs an investor, I expect Snowflake to show amazing profitability and record-breaking revenue numbers. As an Engineer, if Snowflake continues on the current path of ignoring performance, I expect them to lose share to the open-source community or some other competitor, eventually walking down the path of Oracle and Teradata. Here are a few things I think they can do to stay relevant in five years. Disclose Hardware SpecSnowflake charges you based on your consumption. You’re not buying any specific hardware and instead pay as you go using virtual warehouse credits that have no hardware definitions. For folks in technical or engineering backgrounds, this is a red flag. Whether your query runs on a machine with SSD or hard drive, low or high RAM, slow or fast CPU, high or low network bandwidth makes a measurable impact on performance. Snowflake is very secretive about their hardware and when I interacted with a Sales team during migration from Redshift, I could never get any SLAs on their query performance nor information on the hardware specs. This is distinctly different from Redshift, Firebolt, and Databricks which are very transparent and provide more flexibility in customizing your performance through hardware.Lack of transparency can also lead to bad incentives where Snowflake could revert to less optimal machinery due to internal pressures to improve its margins.Not adopting benchmarkSeveral months ago, DataBricks published a study highlighting that they outperform Snowflake on the TPC-DS benchmark, to which Snowflake posted a rebuttal. Snowflake's statement on benchmarks was very clear:In the same way that we had clarity about many things we wanted to do, we also had conviction about what we didn’t want to do. One such thing was engaging in benchmarking wars and making competitive performance claims divorced from real-world experiences. This practice is simply inconsistent with our core value of putting customers first.This is a very shortsighted statement and contradicts why data warehouses became so popular and what customers care about. Data warehouses became popular precisely because they delivered on performance promises, where you don’t have to wait for days or weeks for reporting to be done."))
        aa.append(NewsCenterlModelIG(id: "3", typeTitle: "sad", title: "sad", type: "sad", cretedAt: Date(), icon: "", img: "", description: "What Snowflake could do betterAs an investor, I expect Snowflake to show amazing profitability and record-breaking revenue numbers. As an Engineer, if Snowflake continues on the current path of ignoring performance, I expect them to lose share to the open-source community or some other competitor, eventually walking down the path of Oracle and Teradata. Here are a few things I think they can do to stay relevant in five years. Disclose Hardware SpecSnowflake charges you based on your consumption. You’re not buying any specific hardware and instead pay as you go using virtual warehouse credits that have no hardware definitions. For folks in technical or engineering backgrounds, this is a red flag. Whether your query runs on a machine with SSD or hard drive, low or high RAM, slow or fast CPU, high or low network bandwidth makes a measurable impact on performance. Snowflake is very secretive about their hardware and when I interacted with a Sales team during migration from Redshift, I could never get any SLAs on their query performance nor information on the hardware specs. This is distinctly different from Redshift, Firebolt, and Databricks which are very transparent and provide more flexibility in customizing your performance through hardware.Lack of transparency can also lead to bad incentives where Snowflake could revert to less optimal machinery due to internal pressures to improve its margins.Not adopting benchmarkSeveral months ago, DataBricks published a study highlighting that they outperform Snowflake on the TPC-DS benchmark, to which Snowflake posted a rebuttal. Snowflake's statement on benchmarks was very clear:In the same way that we had clarity about many things we wanted to do, we also had conviction about what we didn’t want to do. One such thing was engaging in benchmarking wars and making competitive performance claims divorced from real-world experiences. This practice is simply inconsistent with our core value of putting customers first.This is a very shortsighted statement and contradicts why data warehouses became so popular and what customers care about. Data warehouses became popular precisely because they delivered on performance promises, where you don’t have to wait for days or weeks for reporting to be done."))
        aa.append(NewsCenterlModelIG(id: "4", typeTitle: "sad", title: "sad", type: "fasd", cretedAt: Date(), icon: "", img: "", description: "What Snowflake could do betterAs an investor, I expect Snowflake to show amazing profitability and record-breaking revenue numbers. As an Engineer, if Snowflake continues on the current path of ignoring performance, I expect them to lose share to the open-source community or some other competitor, eventually walking down the path of Oracle and Teradata. Here are a few things I think they can do to stay relevant in five years. Disclose Hardware SpecSnowflake charges you based on your consumption. You’re not buying any specific hardware and instead pay as you go using virtual warehouse credits that have no hardware definitions. For folks in technical or engineering backgrounds, this is a red flag. Whether your query runs on a machine with SSD or hard drive, low or high RAM, slow or fast CPU, high or low network bandwidth makes a measurable impact on performance. Snowflake is very secretive about their hardware and when I interacted with a Sales team during migration from Redshift, I could never get any SLAs on their query performance nor information on the hardware specs. This is distinctly different from Redshift, Firebolt, and Databricks which are very transparent and provide more flexibility in customizing your performance through hardware.Lack of transparency can also lead to bad incentives where Snowflake could revert to less optimal machinery due to internal pressures to improve its margins.Not adopting benchmarkSeveral months ago, DataBricks published a study highlighting that they outperform Snowflake on the TPC-DS benchmark, to which Snowflake posted a rebuttal. Snowflake's statement on benchmarks was very clear:In the same way that we had clarity about many things we wanted to do, we also had conviction about what we didn’t want to do. One such thing was engaging in benchmarking wars and making competitive performance claims divorced from real-world experiences. This practice is simply inconsistent with our core value of putting customers first.This is a very shortsighted statement and contradicts why data warehouses became so popular and what customers care about. Data warehouses became popular precisely because they delivered on performance promises, where you don’t have to wait for days or weeks for reporting to be done."))
        return aa
    }
    
    override func loadView() {
        super.loadView()
        self.title = "Новости"
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adapter.performUpdates(animated: true)
    }
}

extension NewsCenterViewController: NewsCenterViewProtocol {
}

extension NewsCenterViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        view.addSubview(newCollection)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        newCollection.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}

extension NewsCenterViewController: SectionControllerDelegate {
    func selectSection(_ sectionController: ListSectionController) {
        let section = adapter.section(for: sectionController)
        guard let object = adapter.object(atSection: section) as? NewsCenterlModelIG else {return}
        navigationController?.pushViewController(NewsCenterDetailModuleConfigurator.build(model: object), animated: true)
    }
}

extension NewsCenterViewController: ListAdapterDataSource {
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return itemsData
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        if object is NewsCenterlModelIG {
            let section = NewsCenterSectionController()
            section.delegate = self
            return section
        } else {
            return NewsCenterSectionController()
        }
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }
}

