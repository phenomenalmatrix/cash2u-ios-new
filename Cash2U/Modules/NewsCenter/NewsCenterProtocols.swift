//
//  NewsCenterProtocols.swift
//  Cash2U
//
//  Created by Oroz on 13/9/22.
//  
//

import Foundation

protocol NewsCenterViewProtocol: AnyObject {
}

protocol NewsCenterViewToPresenterProtocol: AnyObject {
}

protocol NewsCenterInteractorProtocol: AnyObject {
}

protocol NewsCenterInteractorToPresenterProtocol: AnyObject {
}

protocol NewsCenterRouterProtocol: AnyObject {
}
