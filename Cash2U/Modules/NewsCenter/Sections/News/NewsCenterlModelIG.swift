//
//  NewsCenterlModelIG.swift
//  Cash2U
//
//  Created by Oroz on 13/9/22.
//

import Foundation
import IGListKit

final class NewsCenterlModelIG: ListDiffable {
    
    let id: String
    let typeTitle: String
    let title: String
    let type: String?
    let cretedAt: Date?
    let icon: String?
    let img: String?
    let description: String?
    
    init (id: String, typeTitle: String, title: String, type: String?, cretedAt: Date?, icon: String?, img: String?, description: String?){
        self.id = id
        self.typeTitle = typeTitle
        self.title = title
        self.type = type
        self.cretedAt = cretedAt
        self.icon = icon
        self.img = img
        self.description = description
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? NewsCenterlModelIG else { return false }
        return id == object.id && title == object.title
    }
    
}
