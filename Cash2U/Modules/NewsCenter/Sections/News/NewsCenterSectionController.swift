//
//  NewsCenterSectionController.swift
//  Cash2U
//
//  Created by Oroz on 13/9/22.
//

import Foundation
import IGListKit

class NewsCenterSectionController: ListSectionController {
    
    private var item: NewsCenterlModelIG?
    
    weak var delegate: SectionControllerDelegate? = nil

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 10, left: 0, bottom: 20, right: 0)
    }
    
    override func numberOfItems() -> Int {
        return 1
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width) - 40, height: ((context.containerSize.width) / 2.3) + 115)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUNewsCell = collectionContext!.dequeueReusableCell(of: CUNewsCell.self, for: self, at: index) as? CUNewsCell else {
            return UICollectionViewCell()
        }
        
        cell.setup()
        return cell
    }
    
    override func didSelectItem(at index: Int) {
        delegate?.selectSection(self)
    }

    override func didUpdate(to object: Any) {
        item = object as? NewsCenterlModelIG
    }
    
}
