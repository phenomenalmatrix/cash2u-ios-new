//
//  CreatePinCodeModuleBuilder.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/7/22.
//  
//

import UIKit

final class CreatePinCodeModuleBuilder {
    /// Собрать модуль
    class func build() -> UIViewController {
        let view = CreatePinCodeViewController()
        let presenter = CreatePinCodePresenter()
        let interactor = CreatePinCodeInteractor()
        let router = CreatePinCodeRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
