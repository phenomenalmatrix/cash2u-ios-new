//
//  CreatePinCodePresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/7/22.
//  
//

import Foundation

final class CreatePinCodePresenter {
    
    weak var view: CreatePinCodeViewProtocol?
    var interactor: CreatePinCodeInteractorProtocol?
    var router: CreatePinCodeRouterProtocol?
    
}

extension CreatePinCodePresenter: CreatePinCodeViewToPresenterProtocol {
    func onNavigateToRepeatPinCode(pinCode: String) {
        router?.navigateToRepeatPinCode(pinCode: pinCode)
    }
    
    func onNavigateToHome() {
        router?.navigateToHome()
    }
}

extension CreatePinCodePresenter: CreatePinCodeInteractorToPresenterProtocol {
}
