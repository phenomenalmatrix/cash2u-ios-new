//
//  CreatePinCodeProtocols.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/7/22.
//  
//

import Foundation

protocol CreatePinCodeViewProtocol: AnyObject {
}

protocol CreatePinCodeViewToPresenterProtocol: AnyObject {
    func onNavigateToRepeatPinCode(pinCode: String)
    func onNavigateToHome()
}

protocol CreatePinCodeInteractorProtocol: AnyObject {
}

protocol CreatePinCodeInteractorToPresenterProtocol: AnyObject {
}

protocol CreatePinCodeRouterProtocol: AnyObject {
    func navigateToRepeatPinCode(pinCode: String)
    func navigateToHome()
}
