//
//  CreatePinCodeInteractor.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/7/22.
//  
//

import Foundation

final class CreatePinCodeInteractor {
    weak var presenter: CreatePinCodeInteractorToPresenterProtocol?
}

extension CreatePinCodeInteractor: CreatePinCodeInteractorProtocol {
}
