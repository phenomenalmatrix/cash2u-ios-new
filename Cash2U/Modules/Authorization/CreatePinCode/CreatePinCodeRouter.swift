//
//  CreatePinCodeRouter.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/7/22.
//  
//

import UIKit

final class CreatePinCodeRouter {
    weak var viewController: UIViewController?
    
    func navigateToMainTabBar() {
        UserDefaultsService.shared.isSecured = false
        UserDefaultsService.shared.hasFaceAndTouchPermission = false
        let vc = JWTDecoderService.shared.getUserViewController()
        self.viewController?.navigationController?.present(vc, animated: true) {
            self.viewController?.navigationController?.popToRootViewController(animated: false)
        }
    }
    
    func navigateToReferralCode() {
        let vc = ReferalCodeModuleBuilder.build()
        self.viewController?.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension CreatePinCodeRouter: CreatePinCodeRouterProtocol {
    func navigateToRepeatPinCode(pinCode: String) {
        self.viewController?.navigationController?.pushViewController(RepeatPinCodeModuleBuilder.build(pinCode: pinCode), animated: true)
    }
    
    func navigateToHome() {
        SwiftEntryKitService.shared.hasReferralCode {
            self.navigateToReferralCode()
            SwiftEntryKitService.shared.dissmiss()
        } declineClick: {
            self.navigateToMainTabBar()
            SwiftEntryKitService.shared.dissmiss()
        }
    }
}
