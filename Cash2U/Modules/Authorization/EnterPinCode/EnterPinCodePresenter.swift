//
//  EnterPinCodePresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/7/22.
//  
//

import Foundation

final class EnterPinCodePresenter {
    
    weak var view: EnterPinCodeViewProtocol?
    var interactor: EnterPinCodeInteractorProtocol?
    var router: EnterPinCodeRouterProtocol?
    
}

extension EnterPinCodePresenter: EnterPinCodeViewToPresenterProtocol {
}

extension EnterPinCodePresenter: EnterPinCodeInteractorToPresenterProtocol {
}
