//
//  EnterPinCodeInteractor.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/7/22.
//  
//

import Foundation

final class EnterPinCodeInteractor {
    weak var presenter: EnterPinCodeInteractorToPresenterProtocol?
}

extension EnterPinCodeInteractor: EnterPinCodeInteractorProtocol {
}
