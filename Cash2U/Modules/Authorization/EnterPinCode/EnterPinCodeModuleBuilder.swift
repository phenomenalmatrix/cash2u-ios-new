//
//  EnterPinCodeModuleBuilder.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/7/22.
//  
//

import UIKit

final class EnterPinCodeModuleBuilder {
    /// Собрать модуль
    class func build() -> UIViewController {
        let view = EnterPinCodeViewController()
        let presenter = EnterPinCodePresenter()
        let interactor = EnterPinCodeInteractor()
        let router = EnterPinCodeRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
