//
//  EnterPinCodeViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/7/22.
//  
//

import UIKit
import SnapKit
import LocalAuthentication

final class EnterPinCodeViewController: CUViewController {
    
    var presenter: EnterPinCodeViewToPresenterProtocol?
    
    private var imageHeightConstraint: Constraint?
    private var nextButtonBottomConstraint: Constraint?
    private var otpHeightConstraint: Constraint?
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private lazy var imagePinCode: UIImageView = {
        let view = UIImageView(image: UIImage(named: "ValidPin"))
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private lazy var titleView: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Введите PIN-код", size: 28, numberOfLines: 1, fontType: .bold, titleColor: .mainTextColor, isSizeToFit:  true)
        return view
    }()
    
    private lazy var subTitleView: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Войдите в свой аккаунт с помощью \nPIN-кода", size: 17, numberOfLines: 2, fontType: .medium, isSizeToFit: true)
        return view
    }()
    
//    private lazy var titleView = UILabelView("Введите PIN-код", font: .systemFont(ofSize: 28, weight: .bold))
//    private lazy var subTitleView = UILabelView("Войдите в свой аккаунт с помощью \nPIN-кода", font: .systemFont(ofSize: 17))
    
    private lazy var otpView: CUOTPView = {
        let view = CUOTPView()
        view.otpView.isSecureTextEntry = true
        view.otpView.dismissOnLastEntry = false
        view.otpView.isCustomKeyboard = true
        view.otpView.isFaceIdHidden = UserDefaultsService.shared.hasFaceAndTouchPermission ? false : true
        view.setup(count: 4, placeholder: "0000")
        view.delegate = self
        view.faceIDdelegate = self
        return view
    }()
    
    private lazy var forgotPinCodeButton: CUMainButton = {
        let view = CUMainButton()
        view.setup(title: "Забыли PIN-код?", titleColor: .grayColor, backgroundColor: .clear)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    override func loadView() {
        super.loadView()
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        faceAndTouchID()
    }
    
    // MARK: - Helpers
    
    private func faceAndTouchID() {
        if UserDefaultsService.shared.hasFaceAndTouchPermission {
            let context = LAContext()
            var error: NSError? = nil
            
            if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
                context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "ID") { [weak self] success, error in
                    DispatchQueue.main.async {
                        guard success, error == nil else {
                            self?.otpView.otpView.becomeFirstResponder()
                            return
                        }
                        self?.dismiss(animated: true, completion: nil)
                    }
                }
            } else {
                self.otpView.otpView.becomeFirstResponder()
            }
        } else {
            self.otpView.otpView.becomeFirstResponder()
        }
    }
    
    private func showError(title: String) {
        hideError()
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.1, initialSpringVelocity: 0, options: [.curveEaseOut]) {
            self.otpView.setError(title: title)
            self.otpHeightConstraint?.update(offset: 100)
            self.view.layoutIfNeeded()
        } completion: { _ in
            
        }
    }
    
    private func hideError() {
        UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0, options: [.curveEaseOut]) {
            self.otpView.unsetError()
            self.otpHeightConstraint?.update(offset: 80)
            self.view.layoutIfNeeded()
        }
    }
    
    override func showKeyBoard(height: CGFloat) {
        UIView.animate(withDuration: 0.5) {
            self.imageHeightConstraint?.update(offset: (ScreenHelper.getWidth / 8))
            if !ScreenHelper.isSmallScreen {
                self.nextButtonBottomConstraint?.update(inset: ScreenHelper.isEyebrowsScreen ? (height - 16) : (height + 16))
            }
            self.view.layoutIfNeeded()
        }
    }
    
    override func hideKeyBoard(height: CGFloat) {
        UIView.animate(withDuration: 0.5) {
            self.imageHeightConstraint?.update(offset: (ScreenHelper.getWidth / 2.2))
            if !ScreenHelper.isSmallScreen {
                self.nextButtonBottomConstraint?.update(inset: 16)
            }
            self.view.layoutIfNeeded()
        }
    }
}

extension EnterPinCodeViewController: CUOTPViewDelegate, FaceIDDelegate {
    
    func faceIdTapped() {
        faceAndTouchID()
    }
    
    func dpOTPViewAddText(_ text: String, position: Int) {
        hideError()
        if otpView.otpView.validate() && position == 3 {
            if let text = otpView.otpView.text {
                if text == KeychainService.shared.pinCode {
                    self.dismiss(animated: true, completion: nil)
                } else {
                    otpView.otpView.text = ""
                    otpView.otpView.becomeFirstResponder()
                    showError(title: "PIN-код введен неверно")
                }
            }
        }
    }
    
    func dpOTPViewRemoveText(_ text: String, position: Int) {
        hideError()
    }
    
    func dpOTPViewChangePositionAt(_ position: Int) {
        
    }
    
    func dpOTPViewBecomeFirstResponder() {
        
    }
    
    func dpOTPViewResignFirstResponder() {
        
    }
}

extension EnterPinCodeViewController: EnterPinCodeViewProtocol {
}

extension EnterPinCodeViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        
        view.addSubview(imagePinCode)
        view.addSubview(titleView)
        view.addSubview(subTitleView)
        view.addSubview(otpView)
        view.addSubview(forgotPinCodeButton)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        imagePinCode.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top).offset(31)
            make.leading.equalToSuperview().offset(22)
            make.trailing.equalToSuperview()
            imageHeightConstraint = make.height.equalTo(ScreenHelper.getWidth / 2.2).constraint
        }
        
        titleView.snp.makeConstraints { make in
            make.top.equalTo(imagePinCode.snp.bottom).offset(26)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        subTitleView.snp.makeConstraints { make in
            make.top.equalTo(titleView.snp.bottom).offset(8)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        otpView.snp.makeConstraints { make in
            make.top.equalTo(subTitleView.snp.bottom).offset(6)
            make.leading.equalToSuperview().offset(70)
            make.trailing.equalToSuperview().offset(-70)
            otpHeightConstraint = make.height.equalTo(80).constraint
        }
        
        forgotPinCodeButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(80)
            make.trailing.equalToSuperview().offset(-80)
            nextButtonBottomConstraint = make.bottom.equalTo(view.safeArea.bottom).offset(-16).constraint
            make.height.equalTo(45)
        }
    }
}
