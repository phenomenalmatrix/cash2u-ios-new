//
//  EnterPinCodeProtocols.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/7/22.
//  
//

import Foundation

protocol EnterPinCodeViewProtocol: AnyObject {
}

protocol EnterPinCodeViewToPresenterProtocol: AnyObject {
}

protocol EnterPinCodeInteractorProtocol: AnyObject {
}

protocol EnterPinCodeInteractorToPresenterProtocol: AnyObject {
}

protocol EnterPinCodeRouterProtocol: AnyObject {
}
