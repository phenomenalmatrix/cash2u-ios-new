//
//  EnableFaceOrTouchIDViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 29/7/22.
//  
//

import UIKit
import LocalAuthentication

final class EnableFaceOrTouchIDViewController: CUViewController {
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private lazy var image: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: ScreenHelper.noFaceID ? "touch_id" : "face_id")
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 2
        view.font = .systemFont(ofSize: 22, weight: .bold)
        view.text = ScreenHelper.noFaceID ? "Используйте Touch-ID для быстрого входа в приложение" : "Используйте Face-ID для быстрого входа в приложение"
        return view
    }()
    
    private lazy var switchLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 1
        view.font = .systemFont(ofSize: 20, weight: .regular)
        view.text = ScreenHelper.noFaceID ? "Использовать Touch-ID" : "Использовать Face-ID"
        return view
    }()
    
    private lazy var switchToggle: UISwitch = {
        let view = UISwitch()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isOn = UserDefaultsService.shared.hasFaceAndTouchPermission
        view.addTarget(self, action: #selector(onSwitch(_:)), for: .valueChanged)
        return view
    }()
    
    private lazy var topLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .mainTextColor
        view.alpha = 0.8
        return view
    }()
    
    private lazy var bottomLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .mainTextColor
        view.alpha = 0.8
        return view
    }()
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = .secondaryColor
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - @objc
    
    @objc private func onSwitch(_ sender: UISwitch) {
        if sender.isOn {
            let context = LAContext()
            var error: NSError? = nil
            
            if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
                context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "ID") { [weak self] success, error in
                    DispatchQueue.main.async {
                        guard success, error == nil else {
                            UserDefaultsService.shared.hasFaceAndTouchPermission = false
                            sender.isOn = false
                            return
                        }
                        UserDefaultsService.shared.hasFaceAndTouchPermission = true
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                            self?.dismiss(animated: true, completion: nil)
                        }
                    }
                }
            } else {
                UserDefaultsService.shared.hasFaceAndTouchPermission = false
                sender.isOn = false
            }
        }
    }
}

extension EnableFaceOrTouchIDViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        view.addSubviews([image, titleLabel, switchLabel, switchToggle, topLine, bottomLine])
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        image.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(70)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(100)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(image.snp.bottom).offset(60)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        switchLabel.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(50)
            make.leading.equalToSuperview().offset(20)
        }
        
        switchToggle.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-20)
            make.centerY.equalTo(switchLabel)
        }
        
        topLine.snp.makeConstraints { make in
            make.top.equalTo(switchLabel.snp.top).offset(-15)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(0.5)
        }
        
        bottomLine.snp.makeConstraints { make in
            make.top.equalTo(switchLabel.snp.bottom).offset(15)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(0.5)
        }
    }
}

// MARK: - PanModalPresentable

extension EnableFaceOrTouchIDViewController: PanModalPresentable {
    
    func panModalDidDismiss() {
        didDismissDelegate?.didDismiss()
    }
    
    var panScrollable: UIScrollView? {
        return nil
    }
    
    var longFormHeight: PanModalHeight {
        .contentHeight(420)
    }
    
    var shortFormHeight: PanModalHeight {
        .contentHeight(420)
    }
}
