//
//  RepeatPinCodePresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/7/22.
//  
//

import Foundation

final class RepeatPinCodePresenter {
    
    weak var view: RepeatPinCodeViewProtocol?
    var interactor: RepeatPinCodeInteractorProtocol?
    var router: RepeatPinCodeRouterProtocol?
    var pincCode = ""
}

extension RepeatPinCodePresenter: RepeatPinCodeViewToPresenterProtocol {
    func onConfirmPinCode(pinCode: String) {
        if pinCode == self.pincCode {
            SwiftEntryKitService.shared.pinSaveSuccess { [weak self] in
                guard let strong = self else {return}
                KeychainService.shared.pinCode = pinCode
                UserDefaultsService.shared.isSecured = true
                strong.router?.goNextScreen()
                SwiftEntryKitService.shared.dissmiss()
            }
        } else {
            view?.pinCodeError(text: "PIN-код не совпадает")
        }
    }
}

extension RepeatPinCodePresenter: RepeatPinCodeInteractorToPresenterProtocol {
}
