//
//  RepeatPinCodeViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/7/22.
//  
//

import UIKit
import SnapKit

final class RepeatPinCodeViewController: CUViewController {
    
    var presenter: RepeatPinCodeViewToPresenterProtocol?
    
    private var imageHeightConstraint: Constraint?
    private var nextButtonBottomConstraint: Constraint?
    private var otpHeightConstraint: Constraint?
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private lazy var imagePinCode: UIImageView = {
        let view = UIImageView(image: UIImage(named: "Lock"))
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private lazy var titleView: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Повторите PIN-код", size: 28, numberOfLines: 1, fontType: .bold, titleColor: .mainTextColor, isSizeToFit:  true)
        return view
    }()
    
    private lazy var subTitleView: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Защитите свой аккаунт с помощью\nPIN-кода", size: 17, numberOfLines: 2, fontType: .medium, isSizeToFit: true)
        return view
    }()
    
    private lazy var otpView: CUOTPView = {
        let view = CUOTPView()
        view.otpView.mainButtonText = "Подтвердить"
        view.setup(count: 4, placeholder: "0000")
        view.delegate = self
        view.mainButtondelegate = self
        view.otpView.isCustomKeyboard = true
        view.otpView.dismissOnLastEntry = false
        view.otpView.isMainButtonHidden = false
        return view
    }()
    
    
    private lazy var confirmButton: CUMainButton = {
        let view = CUMainButton()
        view.setup(title: "Подтвердить", backgroundColor: .mainButtonColor)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(onConfirmButtonTapped), for: .touchUpInside)
        return view
    }()
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = .mainColor
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) { [weak self] in
            self?.otpView.otpView.becomeFirstResponder()
        }
    }
    
    // MARK: - UIActions
    
    @objc private func onConfirmButtonTapped() {
        hideKeyboard { [weak self] in
            guard let strong = self else {return}
            if strong.otpView.otpView.validate() {
                if let text = strong.otpView.otpView.text {
                    strong.presenter?.onConfirmPinCode(pinCode: text)
                }
            } else {
                strong.showError(title: "Введите PIN-код")
            }
        }
    }
    
    
    // MARK: - Helpers
    
    private func showError(title: String) {
        hideError()
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.1, initialSpringVelocity: 0, options: [.curveEaseOut]) {
            self.otpView.setError(title: title)
            self.otpHeightConstraint?.update(offset: 100)
            self.view.layoutIfNeeded()
        } completion: { _ in
            
        }
    }
    
    private func hideError() {
        UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0, options: [.curveEaseOut]) {
            self.otpView.unsetError()
            self.otpHeightConstraint?.update(offset: 80)
            self.view.layoutIfNeeded()
        }
    }
    
    override func showKeyBoard(height: CGFloat) {
        UIView.animate(withDuration: 0.5) {
            self.imageHeightConstraint?.update(offset: (ScreenHelper.getWidth / 8))
            self.confirmButton.alpha = 0
            self.view.layoutIfNeeded()
        }
    }
    
    override func hideKeyBoard(height: CGFloat) {
        UIView.animate(withDuration: 0.5) {
            self.imageHeightConstraint?.update(offset: (ScreenHelper.getWidth / 2.2))
            self.confirmButton.alpha = 1
            self.view.layoutIfNeeded()
        }
    }
}

extension RepeatPinCodeViewController: CUOTPViewDelegate, MainButtonDelegate {
    func mainButtonTapped() {
        hideKeyboard { [weak self] in
            guard let strong = self else {return}
            if strong.otpView.otpView.validate() {
                if let text = strong.otpView.otpView.text {
                    strong.presenter?.onConfirmPinCode(pinCode: text)
                }
            } else {
                strong.showError(title: "Введите PIN-код")
            }
        }
    }
    
    func dpOTPViewAddText(_ text: String, position: Int) {
        hideError()
    }
    
    func dpOTPViewRemoveText(_ text: String, position: Int) {
        hideError()
    }
    
    func dpOTPViewChangePositionAt(_ position: Int) {
        
    }
    
    func dpOTPViewBecomeFirstResponder() {
        
    }
    
    func dpOTPViewResignFirstResponder() {
        
    }
}

extension RepeatPinCodeViewController: RepeatPinCodeViewProtocol {
    func pinCodeError(text: String) {
        otpView.otpView.text = ""
        otpView.otpView.becomeFirstResponder()
        showError(title: text)
    }
    
}

extension RepeatPinCodeViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        
        view.addSubview(imagePinCode)
        view.addSubview(titleView)
        view.addSubview(subTitleView)
        view.addSubview(otpView)
        view.addSubview(confirmButton)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        imagePinCode.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top)
            make.leading.trailing.equalToSuperview()
            imageHeightConstraint = make.height.equalTo(ScreenHelper.getWidth / 2.2).constraint
        }
        
        titleView.snp.makeConstraints { make in
            make.top.equalTo(imagePinCode.snp.bottom).offset(16)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        subTitleView.snp.makeConstraints { make in
            make.top.equalTo(titleView.snp.bottom).offset(8)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        otpView.snp.makeConstraints { make in
            make.top.equalTo(subTitleView.snp.bottom).offset(6)
            make.leading.equalToSuperview().offset(70)
            make.trailing.equalToSuperview().offset(-70)
            otpHeightConstraint = make.height.equalTo(80).constraint
        }
        
        confirmButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            nextButtonBottomConstraint = make.bottom.equalTo(view.safeArea.bottom).offset(-16).constraint
            make.height.equalTo(ScreenHelper.mainButtonHeight)
        }
    }
}
