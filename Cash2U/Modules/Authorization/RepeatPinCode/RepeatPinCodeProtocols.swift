//
//  RepeatPinCodeProtocols.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/7/22.
//  
//

import Foundation

protocol RepeatPinCodeViewProtocol: AnyObject {
    func pinCodeError(text: String)
}

protocol RepeatPinCodeViewToPresenterProtocol: AnyObject {
    func onConfirmPinCode(pinCode: String)
}

protocol RepeatPinCodeInteractorProtocol: AnyObject {
}

protocol RepeatPinCodeInteractorToPresenterProtocol: AnyObject {
}

protocol RepeatPinCodeRouterProtocol: AnyObject {
    func goNextScreen()
}
