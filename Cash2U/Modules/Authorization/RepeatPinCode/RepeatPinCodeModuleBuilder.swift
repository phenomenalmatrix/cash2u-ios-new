//
//  RepeatPinCodeModuleBuilder.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/7/22.
//  
//

import UIKit

final class RepeatPinCodeModuleBuilder {
    /// Собрать модуль
    class func build(pinCode: String) -> UIViewController {
        let view = RepeatPinCodeViewController()
        let presenter = RepeatPinCodePresenter()
        presenter.pincCode = pinCode
        
        let interactor = RepeatPinCodeInteractor()
        let router = RepeatPinCodeRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
