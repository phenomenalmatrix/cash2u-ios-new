//
//  RepeatPinCodeRouter.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/7/22.
//  
//

import UIKit

final class RepeatPinCodeRouter {
    
    weak var viewController: UIViewController?
}

extension RepeatPinCodeRouter: RepeatPinCodeRouterProtocol {
    func navigateToMainTabBar() {
        let vc = JWTDecoderService.shared.getUserViewController()
        self.viewController?.navigationController?.present(vc, animated: true) {
            self.viewController?.navigationController?.popToRootViewController(animated: false)
        }
    }
    
    func navigateToReferralCode() {
        let vc = ReferalCodeModuleBuilder.build()
        self.viewController?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goNextScreen() {
        let vc = EnableFaceOrTouchIDViewController()
        vc.didDismissDelegate = self
        self.viewController?.navigationController?.presentPanModal(vc)
    }
}

extension RepeatPinCodeRouter: CUViewControllerDelegate {
    
    func didDismiss(action: String) {
        SwiftEntryKitService.shared.hasReferralCode {
            self.navigateToReferralCode()
            SwiftEntryKitService.shared.dissmiss()
        } declineClick: {
            self.navigateToMainTabBar()
            SwiftEntryKitService.shared.dissmiss()
        }
    }
}
