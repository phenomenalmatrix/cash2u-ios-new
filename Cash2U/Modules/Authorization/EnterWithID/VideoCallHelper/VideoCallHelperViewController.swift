//
//  VideoCallHelperViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 6/9/22.
//  
//

import UIKit

final class VideoCallHelperViewController: CUViewController {
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private var dismissAction = ""
    
    private lazy var navTitle: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Дальше будет видеозвонок \nс нашим специалистом", size: 28, numberOfLines: 2, fontType: .bold, isSizeToFit: true)
        return view
    }()
    
    private lazy var subtitle: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Это необходимо для подтверждения вашей личности", size: 20, numberOfLines: 2, fontType: .medium, isSizeToFit: true)
        return view
    }()
    
    private lazy var helperLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Пожалуйста не закрывайте и не сворачивайте приложение во время ожидания вызова \n\nОжидание займёт не более 2 минут!", size: 17, numberOfLines: 0, fontType: .medium, titleColor: .mainDarkColor)
        return view
    }()
    
    private lazy var yellowView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.cornerRadius = 18
        view.backgroundColor = .systemYellow
        return view
    }()
    
    private lazy var girlImage: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "happy_girl")
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private lazy var continueButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Продолжить")
        view.addTarget(self, action: #selector(onContinueTapped), for: .touchUpInside)
        return view
    }()
    
    override func loadView() {
        super.loadView()
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - UIActions
    
    @objc private func onContinueTapped() {
        dismissAction = "make_call"
        dismiss(animated: true)
    }
}


extension VideoCallHelperViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        
        view.addSubview(navTitle)
        view.addSubview(subtitle)
        view.addSubview(continueButton)
        view.addSubview(girlImage)
        view.addSubview(yellowView)
        view.addSubview(helperLabel)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        navTitle.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(42)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        subtitle.snp.makeConstraints { make in
            make.top.equalTo(navTitle.snp.bottom).offset(20)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            
        }
        
        girlImage.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(120)
            make.leading.equalToSuperview().offset(120)
            make.trailing.equalToSuperview().offset(-20)
            make.bottom.equalTo(view.safeArea.bottom).offset(-116)
        }
        
        yellowView.snp.makeConstraints { make in
            make.bottom.equalTo(girlImage.snp.bottom)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.height.equalTo(helperLabel.getHeight(withConstrainedWidth: ScreenHelper.getWidth - 40) + 100)
        }
        
        helperLabel.snp.makeConstraints { make in
            make.top.bottom.equalTo(yellowView)
            make.leading.trailing.equalTo(yellowView).inset(30)
            
        }
        
        continueButton.snp.makeConstraints { make in
            make.bottom.equalTo(view.safeArea.bottom).offset(-16)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.height.equalTo(ScreenHelper.mainButtonHeight)
            
        }
    }
}

// MARK: - PanModalPresentable

extension VideoCallHelperViewController: PanModalPresentable {
    
    func panModalDidDismiss() {
        didDismissDelegate?.didDismiss(action: dismissAction)
    }
    var panScrollable: UIScrollView? {
        return nil
    }
    
    var longFormHeight: PanModalHeight {
        .maxHeight
    }
    
    var shortFormHeight: PanModalHeight {
        .maxHeight
    }
}
