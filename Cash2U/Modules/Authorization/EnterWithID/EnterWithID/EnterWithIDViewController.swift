//
//  EnterWithIDViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 8/9/22.
//  
//

import UIKit

final class EnterWithIDViewController: CUViewController {
    
    var presenter: EnterWithIDViewToPresenterProtocol?
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private lazy var helperLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "<b>После проверки ваших паспортных данных будет осуществлен видеозвонок от оператора cash2u</b> \nто действие необходимо для того, чтобы избежать входа недоброжелателей в вашу учетную запись", size: 17, numberOfLines: 0, fontType: .medium, titleColor: .darkGray, isSizeToFit: true)
        return view
    }()
    
    private lazy var enterLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Вход ", size: 38, numberOfLines: 1, fontType: .bold, titleColor: .mainTextColor)
        return view
    }()
    
    private lazy var subenterLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Войдите в приложение с помощью своих паспортных данных:", size: 17, numberOfLines: 2)
        return view
    }()
    
    private lazy var yellowView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.cornerRadius = 18
        view.backgroundColor = .systemYellow
        return view
    }()
    
    private lazy var nextButton: CUMainButton = {
        let view = CUMainButton()
        view.setup(title: "Далее", backgroundColor: .mainButtonColor)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        view.addTarget(self, action: #selector(onNextButtonTapped), for: .touchUpInside)
        return view
    }()
    
    private lazy var termsOfUseView = UITermsOfUseView()
    
    private lazy var checkBox: CUCheckBox = {
        let view = CUCheckBox()
        return view
    }()
    
    private lazy var idNumberView: CUIDNumberTextField = {
        let view = CUIDNumberTextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var innNumberView: CUINNNumberTextField = {
        let view = CUINNNumberTextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    override func loadView() {
        super.loadView()
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
        
        termsOfUseView.descriptionSubTitle.onClickListener { view in
            self.hideKeyboard()
            let vc = TermsOfUseViewController()
            vc.checkBoxIsSelected = self.checkBox.checkmarkIsSelected
            self.navigationController?.presentPanModal(vc)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(
            self,selector:#selector(checkBoxListener(_:)),
            name: .termsOfUseCheckBox,
            object: nil)
    }
    
    
    deinit {
        NotificationCenter().removeObserver(self)
    }
    
    // MARK: - UIAction
    
    @objc private func checkBoxListener(_ notification: Notification) {
        let isSelected = notification.userInfo?["isSelected"] as? Bool
        checkBox.checkmarkIsSelected = isSelected ?? false
    }
    
    @objc private func onNextButtonTapped() {
//        if checkBox.checkmarkIsSelected &&  phoneNumberIsFull {
//            let number = numberEdit.numberField.text
//            if let number = number {
//                presenter?.onSendNumber(number: number)
//            }
//        } else if !checkBox.checkmarkIsSelected {
//            SwiftEntryKitService.shared.showWarningView(title: "Пожалуйта примите условия\n политики обработки данных\n и конфиденциальности!", image: "WarningIcon")
//        } else if !phoneNumberIsFull {
//            SwiftEntryKitService.shared.showWarningView(title: "Пожалуйта заполните\n номер телефона!", image: "WarningIcon")
//        }
        
        let vc = VideoCallHelperViewController()
        vc.didDismissDelegate = self
        self.presentPanModal(vc)
    }
}

extension EnterWithIDViewController: EnterWithIDViewProtocol {
}

extension EnterWithIDViewController: CUViewControllerDelegate {
    func didDismiss(action: String) {
        if action == "make_call" {
            let vc = WaitVideoCallModuleConfigurator.build()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension EnterWithIDViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        
        view.addSubview(yellowView)
        view.addSubview(helperLabel)
        view.addSubview(enterLabel)
        view.addSubview(subenterLabel)
        view.addSubview(idNumberView)
        view.addSubview(innNumberView)
        view.addSubview(nextButton)
        view.addSubview(checkBox)
        view.addSubview(termsOfUseView)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        yellowView.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.height.equalTo((ScreenHelper.getWidth / 2.2) - 12)
        }
        
        helperLabel.snp.makeConstraints { make in
            make.top.bottom.equalTo(yellowView).inset(10)
            make.leading.trailing.equalTo(yellowView).inset(30)
        }
        
        enterLabel.snp.makeConstraints { make in
            make.top.equalTo(yellowView.snp.bottom).offset(20)
            make.leading.trailing.equalToSuperview().inset(20)
        }
        
        subenterLabel.snp.makeConstraints { make in
            make.top.equalTo(enterLabel.snp.bottom).offset(8)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        idNumberView.snp.makeConstraints { make in
            make.top.equalTo(subenterLabel.snp.bottom).offset(22)
            make.leading.trailing.equalToSuperview().inset(20)
            make.height.equalTo(72)
        }
        
        innNumberView.snp.makeConstraints { make in
            make.top.equalTo(idNumberView.snp.bottom).offset(10)
            make.leading.trailing.equalToSuperview().inset(20)
            make.height.equalTo(72)
        }
        
        nextButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.bottom.equalTo(view.safeArea.bottom).offset(-16)
            make.height.equalTo(ScreenHelper.mainButtonHeight)
        }
        
        checkBox.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.bottom.equalTo(nextButton.snp.top).offset(-16)
            make.size.equalTo(40)
        }
        
        termsOfUseView.snp.makeConstraints { make in
            make.centerY.equalTo(checkBox).offset(1)
            make.leading.equalTo(checkBox.snp.trailing).offset(5)
            make.trailing.equalToSuperview().offset(-20)
            make.height.equalTo(50)
        }
    }
}
