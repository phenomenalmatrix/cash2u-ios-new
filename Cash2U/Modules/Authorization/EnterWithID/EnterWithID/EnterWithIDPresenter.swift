//
//  EnterWithIDPresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 8/9/22.
//  
//

import Foundation

final class EnterWithIDPresenter {
    
    weak var view: EnterWithIDViewProtocol?
    var interactor: EnterWithIDInteractorProtocol?
    var router: EnterWithIDRouterProtocol?
    
}

extension EnterWithIDPresenter: EnterWithIDViewToPresenterProtocol {
}

extension EnterWithIDPresenter: EnterWithIDInteractorToPresenterProtocol {
}
