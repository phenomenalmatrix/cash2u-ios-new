//
//  EnterWithIDModuleBuilder.swift
//  Cash2U
//
//  Created by talgar osmonov on 8/9/22.
//  
//

import UIKit

final class EnterWithIDModuleConfigurator {
    /// Собрать модуль
    class func build() -> UIViewController {
        let view = EnterWithIDViewController()
        let presenter = EnterWithIDPresenter()
        let interactor = EnterWithIDInteractor()
        let router = EnterWithIDRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
