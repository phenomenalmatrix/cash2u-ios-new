//
//  EnterWithIDInteractor.swift
//  Cash2U
//
//  Created by talgar osmonov on 8/9/22.
//  
//

import Foundation

final class EnterWithIDInteractor {
    weak var presenter: EnterWithIDInteractorToPresenterProtocol?
}

extension EnterWithIDInteractor: EnterWithIDInteractorProtocol {
}
