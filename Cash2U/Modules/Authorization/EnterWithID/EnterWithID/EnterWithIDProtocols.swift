//
//  EnterWithIDProtocols.swift
//  Cash2U
//
//  Created by talgar osmonov on 8/9/22.
//  
//

import Foundation

protocol EnterWithIDViewProtocol: AnyObject {
}

protocol EnterWithIDViewToPresenterProtocol: AnyObject {
}

protocol EnterWithIDInteractorProtocol: AnyObject {
}

protocol EnterWithIDInteractorToPresenterProtocol: AnyObject {
}

protocol EnterWithIDRouterProtocol: AnyObject {
}
