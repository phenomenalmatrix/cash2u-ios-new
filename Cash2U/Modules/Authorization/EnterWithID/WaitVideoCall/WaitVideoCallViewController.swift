//
//  WaitVideoCallViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 6/9/22.
//  
//

import UIKit
import SnapKit

final class WaitVideoCallViewController: CUViewController {
    
    var presenter: WaitVideoCallViewToPresenterProtocol?
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private var recallHeightConstraint: Constraint?
    private var blueViewHeightConstraint: Constraint?
    
    private var timer: Timer?
    private var timeStamp: Double = 0
    
    private lazy var topTitle: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Ожидание видеозвонка", size: 32, numberOfLines: 1, fontType: .bold, isSizeToFit: true)
        return view
    }()
    
    private lazy var timeDescription: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "До звонка со специалистом осталось:", size: 22, numberOfLines: 1, fontType: .bold, alignment: .center, isSizeToFit: true)
        return view
    }()
    
    private lazy var time: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "00:00", size: 75, numberOfLines: 1, fontType: .bold, alignment: .center, isSizeToFit: true)
        return view
    }()
    
    private lazy var blueView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.cornerRadius = 18
        view.backgroundColor = .mainBlueColor
        return view
    }()
    
    private lazy var lobbyImage: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "lobby_image")
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private lazy var recallButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Запросить повторный звонок")
        view.addTarget(self, action: #selector(onRecallTapped), for: .touchUpInside)
        return view
    }()
    
    override func loadView() {
        super.loadView()
        title = "Вход через паспорт"
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter?.didLoad()
    }
    
    deinit {
        CallSocket.shared.stopConnection()
    }
    
    // MARK: - UIActions
    
    @objc private func onRecallTapped() {
        
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0, options: [.curveEaseOut]) {
            self.recallHeightConstraint?.update(offset: 0)
            self.blueViewHeightConstraint?.update(offset: 180)
            self.view.layoutIfNeeded()
        } completion: { _ in }
        
        presenter?.didLoad()
    }
}

extension WaitVideoCallViewController: WaitVideoCallViewProtocol {
    func timeIsCounting(time: String) {
        self.time.changeText(title: time)
    }
    
    func timeIsOver() {
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0, options: [.curveEaseOut]) {
            self.recallHeightConstraint?.update(offset: 64)
            self.blueViewHeightConstraint?.update(offset: 0)
            self.view.layoutIfNeeded()
        }
    }
}

extension WaitVideoCallViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        
        view.addSubview(topTitle)
        view.addSubview(blueView)
        blueView.addSubview(timeDescription)
        blueView.addSubview(time)
        view.addSubview(recallButton)
        view.addSubview(lobbyImage)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        topTitle.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top).offset(38)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        blueView.snp.makeConstraints { make in
            make.top.equalTo(topTitle.snp.bottom).offset(20)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            blueViewHeightConstraint = make.height.equalTo(180).constraint
        }
        
        timeDescription.snp.makeConstraints { make in
            make.top.equalTo(blueView.snp.top).offset(35)
            make.leading.equalToSuperview().offset(40)
            make.trailing.equalToSuperview().offset(-40)
        }
        
        time.snp.makeConstraints { make in
            make.top.equalTo(timeDescription.snp.bottom).offset(5)
            make.leading.trailing.equalToSuperview().inset(40)
        }
        
        recallButton.snp.makeConstraints { make in
            make.bottom.equalTo(view.safeArea.bottom).offset(-16)
            make.leading.trailing.equalToSuperview().inset(20)
            recallHeightConstraint = make.height.equalTo(0).constraint
        }
        
        lobbyImage.snp.makeConstraints { make in
            
            make.top.equalTo(blueView.snp.bottom).offset(0)
            make.leading.equalToSuperview().offset(35)
            make.trailing.equalToSuperview().offset(0)
            make.height.equalTo(ScreenHelper.getWidth)
        }
    }
}
