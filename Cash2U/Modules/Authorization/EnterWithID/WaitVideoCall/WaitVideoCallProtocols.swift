//
//  WaitVideoCallProtocols.swift
//  Cash2U
//
//  Created by talgar osmonov on 7/9/22.
//  
//

import Foundation
import Moya

protocol WaitVideoCallViewProtocol: AnyObject {
    func timeIsCounting(time: String)
    func timeIsOver()
}

protocol WaitVideoCallViewToPresenterProtocol: AnyObject {
    func didLoad()
}

protocol WaitVideoCallInteractorProtocol: AnyObject {
    func didLoad()
}

protocol WaitVideoCallInteractorToPresenterProtocol: AnyObject {
    func socketIsConnected(isConnected: Bool)
    func onRequestIdentificationError(error: MoyaError)
    func onRequestIdentificationSuccess()
}

protocol WaitVideoCallRouterProtocol: AnyObject {
}
