//
//  WaitVideoCallRouter.swift
//  Cash2U
//
//  Created by talgar osmonov on 7/9/22.
//  
//

import UIKit

final class WaitVideoCallRouter {
    weak var viewController: UIViewController?
}

extension WaitVideoCallRouter: WaitVideoCallRouterProtocol {
}
