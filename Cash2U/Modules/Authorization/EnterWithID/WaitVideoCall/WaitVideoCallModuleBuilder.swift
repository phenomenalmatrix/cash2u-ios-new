//
//  WaitVideoCallModuleBuilder.swift
//  Cash2U
//
//  Created by talgar osmonov on 7/9/22.
//  
//

import UIKit

final class WaitVideoCallModuleConfigurator {
    /// Собрать модуль
    class func build() -> UIViewController {
        let view = WaitVideoCallViewController()
        let presenter = WaitVideoCallPresenter()
        let interactor = WaitVideoCallInteractor()
        let router = WaitVideoCallRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
