//
//  WaitVideoCallPresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 7/9/22.
//  
//

import Foundation
import Moya

final class WaitVideoCallPresenter {
    
    weak var view: WaitVideoCallViewProtocol?
    var interactor: WaitVideoCallInteractorProtocol?
    var router: WaitVideoCallRouterProtocol?
    
    private var timer: Timer?
    private var timeStamp: Double = 0
    
    deinit {
        timer?.invalidate()
    }
    
    // MARK: - Targets
    
    @objc private func counter() {
        if Date().currentTimeMillis() > timeStamp {
            view?.timeIsOver()
            timer?.invalidate()
        } else {
            let date = Date(timeIntervalSince1970: timeStamp - Date().currentTimeMillis())
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "mm:ss"
            dateFormatter.timeZone = .current
            view?.timeIsCounting(time: dateFormatter.string(from: date))
        }
    }
    
}

extension WaitVideoCallPresenter: WaitVideoCallViewToPresenterProtocol {
    func didLoad() {
        interactor?.didLoad()
    }
}

extension WaitVideoCallPresenter: WaitVideoCallInteractorToPresenterProtocol {
    func onRequestIdentificationSuccess() {
        
    }
    
    func socketIsConnected(isConnected: Bool) {
        if isConnected {
            self.timeStamp = UserDefaultsService.shared.callOtpTimestamp
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.counter), userInfo: nil, repeats: true)
        } else {
            timer?.invalidate()
        }
    }
    
    func onRequestIdentificationError(error: MoyaError) {
        
    }
}
