//
//  WaitVideoCallInteractor.swift
//  Cash2U
//
//  Created by talgar osmonov on 7/9/22.
//  
//

import Foundation
import SwiftSignalRClient

final class WaitVideoCallInteractor {
    weak var presenter: WaitVideoCallInteractorToPresenterProtocol?
    private let authService = AuthService.shared
    private let callSocket = CallSocket.shared
    
    init() {
        callSocket.delegate = self
    }
}

extension WaitVideoCallInteractor: WaitVideoCallInteractorProtocol {
    func didLoad() {
        authService.requestVideoIdentification { [weak self] result in
            guard let strong = self else {return}
            switch result {
            case .success(_):
                UserDefaultsService.shared.callOtpTimestamp = Date().currentTimeMillis() + 121
                strong.callSocket.startConnection()
                strong.presenter?.onRequestIdentificationSuccess()
            case .failure(let error):
                strong.presenter?.onRequestIdentificationError(error: error)
            }
        }
    }
}

extension WaitVideoCallInteractor: SocketDelegate {
    func connectionDidOpen(hubConnection: HubConnection) {
        presenter?.socketIsConnected(isConnected: true)
    }
    
    func connectionDidFailToOpen(error: Error) {
        presenter?.socketIsConnected(isConnected: false)
    }
    
    func connectionDidClose(error: Error?) {
        presenter?.socketIsConnected(isConnected: false)
    }
}
