//
//  InputPhoneNumberInteractor.swift
//  Cash2U
//
//  Created by talgar osmonov on 18/7/22.
//  
//

import Foundation

final class InputPhoneNumberInteractor {
    weak var presenter: InputPhoneNumberInteractorToPresenterProtocol?
    
    let authService = AuthService.shared
}

extension InputPhoneNumberInteractor: InputPhoneNumberInteractorProtocol {
    func sendNumber(number: String) {
        let replaced = "996\(number.replacingOccurrences(of: " ", with: ""))"
        authService.sendNumber(number: replaced) { [weak self] result in
            guard let strong = self else {return}
            switch result {
            case .success(_):
                UserDefaultsService.shared.phoneNumber = replaced
                UserDefaultsService.shared.otpTimestamp = Date().currentTimeMillis() + 61
                strong.presenter?.onSendSuccess()
            case .failure(let error):
                if let error = try? JSONDecoder().decode(InputPhoneNumberErrorModel.self, from: error.response?.data ?? Data()) {
                    if error.errors.phoneNumber.first == "UserIsLockedOut" {
                        if let date = error.errors.lockoutEnd?.first {
                            strong.presenter?.onUserLocked(date: Date.getStringToDate(stringDate: date))
                        }
                    } else if error.errors.phoneNumber.first == "TimeLimitExceed" {
                        strong.presenter?.onSendLimitExpired()
                    }
                }
            }
        }
    }
}
