//
//  TermsOfUseViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/7/22.
//  
//

import UIKit

final class TermsOfUseViewCell: UITableViewCell {
    
    lazy var label: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 2
        return view
    }()
    
    private lazy var backView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.cornerRadius = 17
        view.clipsToBounds = true
        view.backgroundColor = .darkBlueColor
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = .clear
        self.cornerRadius = 17
        self.clipsToBounds = true
        
        addSubview(backView)
        backView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalToSuperview().offset(8)
            make.bottom.equalToSuperview().offset(-8)
        }
        addSubview(label)
        label.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().offset(32)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

final class TermsOfUseViewController: CUViewController {
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    var checkBoxIsSelected = false
    
    private let lists = ["Пользовательское \nсоглашение", "Соглашение \nна обработку данных", "Политика \nконфиденциальности"]
    
    private lazy var titleView = UILabelView("Пользовательское\nсоглашение", font: .systemFont(ofSize: 28, weight: .bold))
    private lazy var subTitleView = UILabelView("Чтобы пройти дальше, вам нужно ознакомиться и принять все соглашения:", font: .systemFont(ofSize: 15))
    
    private lazy var listTermsOfUse: UITableView = {
        let view = UITableView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.rowHeight = 80
        view.delegate = self
        view.dataSource = self
        view.backgroundColor = .secondaryColor
        view.isScrollEnabled = false
        view.showsVerticalScrollIndicator = false
        view.separatorStyle = .none
        return view
    }()
    
    private lazy var checkBox: CUCheckBox = {
        let view = CUCheckBox()
        view.delegate = self
        return view
    }()

    private lazy var descriptionTermsOfUse = UILabelView("Я ознакомился с публичной офертой и всеми документами и принимаю все условия", font: .systemFont(ofSize: 15, weight: .semibold))
    
    private lazy var nextButton: CUMainButton = {
        let view = CUMainButton()
        view.setup(title: "Далее", backgroundColor: .mainButtonColor)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(onConfirmButtonTapped), for: .touchUpInside)
        return view
    }()
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = .secondaryColor
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
        
        if checkBoxIsSelected {
            checkBox.checkmarkIsSelected = true
        } else {
            checkBox.checkmarkIsSelected = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    // MARK: - UIActions
    
    @objc private func onConfirmButtonTapped() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension TermsOfUseViewController: CUCheckBoxDelegate {
    func isSelected(_ isSelected: Bool) {
        let loginResponse = ["isSelected": isSelected]
        NotificationCenter.default
            .post(name: .termsOfUseCheckBox, object: nil, userInfo: loginResponse)
    }
}

extension TermsOfUseViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = TermsOfUseViewCell()
        cell.label.text = lists[indexPath.row]
        cell.selectionStyle = .none
        cell.accessoryType = .disclosureIndicator
        return cell
    }
}

extension TermsOfUseViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        view.addSubview(titleView)
        view.addSubview(subTitleView)
        view.addSubview(listTermsOfUse)
        view.addSubview(checkBox)
        view.addSubview(descriptionTermsOfUse)
        view.addSubview(nextButton)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        titleView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(32)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        subTitleView.snp.makeConstraints { make in
            make.top.equalTo(titleView.snp.bottom).offset(18)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        listTermsOfUse.snp.makeConstraints { make in
            make.top.equalTo(subTitleView.snp.bottom).offset(20)
            make.height.equalTo(240)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        checkBox.snp.makeConstraints { make in
            make.top.equalTo(listTermsOfUse.snp.bottom).offset(22)
            make.width.height.equalTo(40)
            make.leading.equalToSuperview().offset(21)
        }
        
        descriptionTermsOfUse.snp.makeConstraints { make in
            make.centerY.equalTo(checkBox)
            make.leading.equalTo(checkBox.snp.trailing).offset(5)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        nextButton.snp.makeConstraints { make in
            make.top.equalTo(descriptionTermsOfUse.snp.bottom).offset(32)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.height.equalTo(ScreenHelper.mainButtonHeight)
        }
    }
}

// MARK: - PanModalPresentable

extension TermsOfUseViewController: PanModalPresentable {
    func panModalDidDismiss() {
        didDismissDelegate?.didDismiss()
    }
    var panScrollable: UIScrollView? {
        return nil
    }
    
    var longFormHeight: PanModalHeight {
        .contentHeight(600)
    }
    
    var shortFormHeight: PanModalHeight {
        .contentHeight(600)
    }
}
