//
//  InputPhoneNumberViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 18/7/22.
//  
//

import UIKit
import SnapKit
import Atributika
import TTTAttributedLabel

final class InputPhoneNumberViewController: CUViewController {
    
    var presenter: InputPhoneNumberViewToPresenterProtocol?
    
    var isAfterAuthorization = false
    var isAfterOnboarding = false
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private var imageHeightConstraint: Constraint?
    private var nextButtonBottomConstraint: Constraint?
    private var errorTitleHeightConstraint: Constraint?
    
    private var phoneNumberIsFull = false
    
    private var firstDidLoad = 0
    
    private lazy var nextButton: CUMainButton = {
        let view = CUMainButton()
        view.setup(title: "Далее", backgroundColor: .mainButtonColor)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        view.addTarget(self, action: #selector(onNextButtonTapped), for: .touchUpInside)
        return view
    }()
    
    private lazy var enterWithIdButton: CUMainButton = {
        let view = CUMainButton()
        view.setup(title: "У меня нет доступа \nк старому номеру", titleColor: .grayColor, backgroundColor: .clear, borderWidth: 1, borderColor: .grayColor)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(onEnterWithIdTapped), for: .touchUpInside)
        view.isHidden = true 
        return view
    }()
    
    private lazy var numberEdit: CUNumberTextField = {
        let view = CUNumberTextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.delegate = self
        return view
    }()
    
    private lazy var termsOfUseView = UITermsOfUseView()
    
    private lazy var checkBox: CUCheckBox = {
        let view = CUCheckBox()
        view.delegate = self
        return view
    }()
    
    private lazy var titleView: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Вход ", size: 38, numberOfLines: 1, fontType: .bold, titleColor: .mainTextColor, isSizeToFit:  true)
        return view
    }()
    
    private lazy var subTitleView: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Войдите в приложение с помощью своего номера телефона:", size: 17, numberOfLines: 2, fontType: .medium, isSizeToFit: true)
        return view
    }()
    
    private lazy var cristalView: UIImageView = {
        let view = UIImageView(image: UIImage(named: "Crystal"))
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private lazy var errorTitleLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(size: 17, numberOfLines: 2, fontType: .bold, titleColor: .redColor, alignment: .center)
        return view
    }()
    
    override func loadView() {
        super.loadView()
        
        let cityItem = UIBarButtonItem.init(customView: UIView())
        navigationItem.leftBarButtonItems = [cityItem]
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Пропустить", style: .plain, target: self, action: #selector(onSkipTapped))
        
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
        
        termsOfUseView.descriptionSubTitle.onClickListener { view in
            self.hideKeyboard()
            let vc = TermsOfUseViewController()
            vc.checkBoxIsSelected = self.checkBox.checkmarkIsSelected
            self.navigationController?.presentPanModal(vc)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !isAfterOnboarding {
            let vc = JWTDecoderService.shared.getUserViewController(isAfterAuthorization: isAfterAuthorization)
            self.navigationController?.present(vc, animated: false, completion: nil)
        }
        
        NotificationCenter.default.addObserver(
            self,selector:#selector(checkBoxListener(_:)),
            name: .termsOfUseCheckBox,
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,selector:#selector(firstResponderListener(_:)),
            name: .phoneBecomeFirstResponder,
            object: nil)
    }
    
    deinit {
        NotificationCenter().removeObserver(self)
    }
    
    // MARK: - UIActions
    
    @objc private func checkBoxListener(_ notification: Notification) {
        let isSelected = notification.userInfo?["isSelected"] as? Bool
        checkBox.checkmarkIsSelected = isSelected ?? false
    }
    
    @objc private func firstResponderListener(_ notification: Notification) {
        let isSelected = notification.userInfo?["becomeFirstResponder"] as? Bool
        if isSelected == true {
            self.numberEdit.numberField.becomeFirstResponder()
        }
    }
    
    @objc private func onSkipTapped() {
        hideKeyboard { [weak self] in
            guard let self else {return}
            let vc = HomeModuleConfigurator.buildGuest()
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            self.navigationController?.present(vc, animated: true, completion: nil)
        }
    }
    
    @objc private func onNextButtonTapped() {
        hideKeyboard { [weak self] in
            guard let self else {return}
            if self.checkBox.checkmarkIsSelected && self.phoneNumberIsFull {
                let number = self.numberEdit.numberField.text
                if let number = number {
                    self.presenter?.onSendNumber(number: number)
                }
            } else if !self.checkBox.checkmarkIsSelected {
                SwiftEntryKitService.shared.showWarningView(title: "Пожалуйcта примите условия\n политики обработки данных\n и конфиденциальности!", image: "WarningIcon")
            } else if !self.phoneNumberIsFull {
                SwiftEntryKitService.shared.showWarningView(title: "Пожалуйcта заполните\n номер телефона!", image: "WarningIcon")
            }
        }
    }
    
    @objc private func onEnterWithIdTapped() {
        let vc = EnterWithIDModuleConfigurator.build()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - Helpers
    
    private func showError(title: String) {
        errorTitleLabel.changeText(title: title)
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.1, initialSpringVelocity: 0, options: [.curveEaseOut]) {
            self.errorTitleHeightConstraint?.update(offset: 20)
            self.view.layoutIfNeeded()
        } completion: { _ in
            
        }
    }
    
    private func hideError() {
        errorTitleLabel.changeText(title: "")
        UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0, options: [.curveEaseOut]) {
            self.errorTitleHeightConstraint?.update(offset: 0)
            self.view.layoutIfNeeded()
        }
    }
    
    override func showKeyBoard(height: CGFloat) {
        UIView.animate(withDuration: 0.5) {
            self.enterWithIdButton.alpha = 0
            self.imageHeightConstraint?.update(offset: (ScreenHelper.getWidth / 8))
            if !ScreenHelper.isSmallScreen {
                self.nextButtonBottomConstraint?.update(inset: ScreenHelper.isEyebrowsScreen ? (height - 16) : (height + 16))
            }
            self.view.layoutIfNeeded()
        }
    }
    
    override func hideKeyBoard(height: CGFloat) {
        UIView.animate(withDuration: 0.5) {
            self.enterWithIdButton.alpha = 1
            self.imageHeightConstraint?.update(offset: (ScreenHelper.getWidth / 2.2))
            if !ScreenHelper.isSmallScreen {
                self.nextButtonBottomConstraint?.update(inset: 16)
            }
            self.view.layoutIfNeeded()
        }
    }
}

extension InputPhoneNumberViewController: CUViewControllerDelegate {
    func didDismiss(action: String) {
        
    }
}

extension InputPhoneNumberViewController: CUCheckBoxDelegate {
    func isSelected(_ isSelected: Bool) {
        
    }
}

extension InputPhoneNumberViewController: CUNumberTextFieldDelegate {
    func isPhoneNumberFull(isFull: Bool) {
        self.phoneNumberIsFull = isFull
        hideError()
    }
}

extension InputPhoneNumberViewController: InputPhoneNumberViewProtocol {
    func userLocked() {
        showError(title: "nomer zablochen na 5 min")
    }
    
    func sendLimitExpired() {
        showError(title: "Лимит отправки номера истек")
    }
    
}

extension InputPhoneNumberViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        
        view.addSubview(cristalView)
        view.addSubview(titleView)
        view.addSubview(subTitleView)
        view.addSubview(numberEdit)
        view.addSubview(errorTitleLabel)
        view.addSubview(checkBox)
        view.addSubview(termsOfUseView)
        view.addSubview(nextButton)
        view.addSubview(enterWithIdButton)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        cristalView.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top)
            make.leading.trailing.equalToSuperview()
            imageHeightConstraint = make.height.equalTo(ScreenHelper.getWidth / 2.2).constraint
        }
        
        titleView.snp.makeConstraints { make in
            make.top.equalTo(cristalView.snp.bottom).offset(8)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        subTitleView.snp.makeConstraints { make in
            make.top.equalTo(titleView.snp.bottom).offset(8)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        numberEdit.snp.makeConstraints { make in
            make.top.equalTo(subTitleView.snp.bottom).offset(22)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        errorTitleLabel.snp.makeConstraints { make in
            make.top.equalTo(numberEdit.snp.bottom).offset(8)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            errorTitleHeightConstraint = make.height.equalTo(0).constraint
        }
        
        checkBox.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.top.equalTo(errorTitleLabel.snp.bottom).offset(2)
            make.size.equalTo(40)
        }
        
        termsOfUseView.snp.makeConstraints { make in
            make.centerY.equalTo(checkBox).offset(1)
            make.leading.equalTo(checkBox.snp.trailing).offset(5)
            make.trailing.equalToSuperview().offset(-20)
            make.height.equalTo(50)
        }
        
        nextButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            nextButtonBottomConstraint = make.bottom.equalTo(view.safeArea.bottom).offset(-16).constraint
            make.height.equalTo(ScreenHelper.mainButtonHeight)
        }
        
        enterWithIdButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.bottom.equalTo(nextButton.snp.top).offset(-10)
            make.height.equalTo(ScreenHelper.mainButtonHeight)
        }
    }
}
