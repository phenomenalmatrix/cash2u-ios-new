//
//  InputPhoneNumberPresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 18/7/22.
//  
//

import Foundation

final class InputPhoneNumberPresenter {
    
    weak var view: InputPhoneNumberViewProtocol?
    var interactor: InputPhoneNumberInteractorProtocol?
    var router: InputPhoneNumberRouterProtocol?
    
    private var number = ""
}

extension InputPhoneNumberPresenter: InputPhoneNumberViewToPresenterProtocol {
    func onSendNumber(number: String) {
        self.number = number
        SwiftEntryKitService.shared.showActivityIndicator(true)
        interactor?.sendNumber(number: number)
    }
    
}

extension InputPhoneNumberPresenter: InputPhoneNumberInteractorToPresenterProtocol {
    func onUserLocked(date: Date) {
        SwiftEntryKitService.shared.showActivityIndicator(false)
        view?.userLocked()
    }
    
    func onSendSuccess() {
        SwiftEntryKitService.shared.showActivityIndicator(false)
        router?.navigateToSmsScreen(number: number)
    }
    
    func onSendLimitExpired() {
        SwiftEntryKitService.shared.showActivityIndicator(false)
        view?.sendLimitExpired()
    }
}
