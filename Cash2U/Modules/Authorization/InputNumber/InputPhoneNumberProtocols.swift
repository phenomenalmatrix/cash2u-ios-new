//
//  InputPhoneNumberProtocols.swift
//  Cash2U
//
//  Created by talgar osmonov on 18/7/22.
//  
//

import Foundation

protocol InputPhoneNumberViewProtocol: AnyObject {
    func sendLimitExpired()
    func userLocked()
}

protocol InputPhoneNumberViewToPresenterProtocol: AnyObject {
    func onSendNumber(number: String)
}

protocol InputPhoneNumberInteractorProtocol: AnyObject {
    func sendNumber(number: String)
}

protocol InputPhoneNumberInteractorToPresenterProtocol: AnyObject {
    func onSendSuccess()
    func onSendLimitExpired()
    func onUserLocked(date: Date)
}

protocol InputPhoneNumberRouterProtocol: AnyObject {
    func navigateToSmsScreen(number: String)
}
