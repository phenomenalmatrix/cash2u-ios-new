//
//  InputPhoneNumberModuleBuilder.swift
//  Cash2U
//
//  Created by talgar osmonov on 18/7/22.
//  
//

import UIKit

final class InputPhoneNumberModuleBuilder {
    /// Собрать модуль
    class func build(isAfterAuthorization: Bool = true, isAfterOnboarding: Bool = false) -> UIViewController {
        let view = InputPhoneNumberViewController()
        let presenter = InputPhoneNumberPresenter()
        let interactor = InputPhoneNumberInteractor()
        let router = InputPhoneNumberRouter()

        view.isAfterAuthorization = isAfterAuthorization
        view.isAfterOnboarding = isAfterOnboarding
        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
