//
//  InputPhoneNumberRouter.swift
//  Cash2U
//
//  Created by talgar osmonov on 18/7/22.
//  
//

import UIKit

final class InputPhoneNumberRouter {
    weak var viewController: UIViewController?
}

extension InputPhoneNumberRouter: InputPhoneNumberRouterProtocol {
    func navigateToSmsScreen(number: String) {
        let vc = InputSmsCodeModuleBuilder.build() as! InputSmsCodeViewController
        vc.phoneNumber = number
        self.viewController?.navigationController?.pushViewController(vc, animated: true)
    }
}
