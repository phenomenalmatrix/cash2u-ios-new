//
//  InputSmsCodeInteractor.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/7/22.
//  
//

import Foundation

final class InputSmsCodeInteractor {
    
    let authService = AuthService.shared
    
    weak var presenter: InputSmsCodeInteractorToPresenterProtocol?
    
    private var sendedOtpCount = 0
}

extension InputSmsCodeInteractor: InputSmsCodeInteractorProtocol {
    func resendNumber(number: String) {
        authService.sendNumber(number: number) { [weak self] result in
            guard let strong = self else {return}
            switch result {
            case .success(_):
                UserDefaultsService.shared.otpTimestamp = Date().currentTimeMillis() + 61
                strong.presenter?.onResendSuccess()
            case .failure(let error):
                if let error = try? JSONDecoder().decode(InputPhoneNumberErrorModel.self, from: error.response?.data ?? Data()) {
                    if error.errors.phoneNumber.first == "UserIsLockedOut" {
                        
                    } else if error.errors.phoneNumber.first == "TimeLimitExceed" {
                        strong.presenter?.onResendLimitExpired()
                    }
                }
            }
        }
    }
    
    func sendOtp(number: String, otp: String) {
        authService.sendOtp(number: number, otp: otp) { [weak self] result in
            guard let strong = self else {return}
            switch result {
            case .success(let data):
                if let data = data {
                    let data = try? JSONDecoder().decode(UserTokenResponseModel.self, from: data.data)
                    KeychainService.shared.saveToken(token: data)
                    strong.presenter?.onOtpSuccess()
                }
            case .failure(let error):
                if let error = try? JSONDecoder().decode(InputSmsErrorModel.self, from: error.response?.data ?? Data()) {
                    if error.errorDescription == "InvalidLoginAttempt" {
                        strong.presenter?.onIncorrectOtp()
                    } else if error.errorDescription == "UserIsLockedOut" {
                        if let date = error.lockoutEnd {
                            strong.presenter?.onIncorrectOtp(time: Date.getStringToDate(stringDate: date).currentTimeMillis())
                            strong.presenter?.onWaitForTime()
                        }
                    }
                }
            }
        }
    }
}
