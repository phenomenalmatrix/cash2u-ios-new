//
//  InputSmsCodeProtocols.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/7/22.
//  
//

import Foundation

protocol InputSmsCodeViewProtocol: AnyObject {
    func incorrectOtp(time: String)
    func incorrectOtp(text: String)
    func waitForTime(text: String)
}

protocol InputSmsCodeViewToPresenterProtocol: AnyObject {
    func onResendNumber(number: String)
    func onSendOtp(otp: String?)
    func onViewDidLoad()
}

protocol InputSmsCodeInteractorProtocol: AnyObject {
    func sendOtp(number: String, otp: String)
    func resendNumber(number: String)
}

protocol InputSmsCodeInteractorToPresenterProtocol: AnyObject {
    func onOtpSuccess()
    func onIncorrectOtp(time: Double)
    func onIncorrectOtp()
    func onWaitForTime()
    func onResendLimitExpired()
    func onResendSuccess()
}

protocol InputSmsCodeRouterProtocol: AnyObject {
    func showEnableSecure()
}
