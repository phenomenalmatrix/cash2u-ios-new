//
//  InputSmsCodePresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/7/22.
//  
//

import Foundation

final class InputSmsCodePresenter {
    
    weak var view: InputSmsCodeViewProtocol?
    var interactor: InputSmsCodeInteractorProtocol?
    var router: InputSmsCodeRouterProtocol?
    
    private var timer = Timer()
    private var timeStamp: Double = 0
    
    private var timerCount = Timer()
    private var timeStampCount: Double = 0
    
    // MARK: - TARGETS
    
    @objc func counter() {
        
        if Date().currentTimeMillis() > timeStamp {
            timer.invalidate()
            view?.incorrectOtp(time: "otp")
        } else {
            let date = Date(timeIntervalSince1970: timeStamp - Date().currentTimeMillis())
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "mm:ss"
            dateFormatter.timeZone = .current
            view?.incorrectOtp(time: "Повторно отправить код" + " \(dateFormatter.string(from: date))")
        }
    }
    
    @objc private func counterCount() {
        timer.invalidate()
        
        if Date().currentTimeMillis() > timeStampCount {
            timerCount.invalidate()
            view?.incorrectOtp(time: "waitOtp")
        } else {
            let date = Date(timeIntervalSince1970: timeStampCount - Date().currentTimeMillis())
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "mm:ss"
            dateFormatter.timeZone = .current
            view?.incorrectOtp(time: "Повторно отправить код" + " \(dateFormatter.string(from: date))")
        }
    }
    
}

extension InputSmsCodePresenter: InputSmsCodeViewToPresenterProtocol {
    func onResendNumber(number: String) {
        SwiftEntryKitService.shared.showActivityIndicator(true)
        interactor?.resendNumber(number: UserDefaultsService.shared.phoneNumber)
    }
    
    func onViewDidLoad() {
        
        if UserDefaultsService.shared.otpTimestamp > Date().currentTimeMillis() {
            timeStamp = UserDefaultsService.shared.otpTimestamp
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(counter), userInfo: nil, repeats: true)
        }
    }
    
    func onSendOtp(otp: String?) {
        if let otp = otp {
            SwiftEntryKitService.shared.showActivityIndicator(true)
            interactor?.sendOtp(number: UserDefaultsService.shared.phoneNumber, otp: otp)
        }
    }
}

extension InputSmsCodePresenter: InputSmsCodeInteractorToPresenterProtocol {
    func onResendLimitExpired() {
        SwiftEntryKitService.shared.showActivityIndicator(false)
        view?.incorrectOtp(text: "Лимит отправки номера истек")
    }
    
    func onResendSuccess() {
        SwiftEntryKitService.shared.showActivityIndicator(false)
        onViewDidLoad()
    }
    
    func onOtpSuccess() {
        SwiftEntryKitService.shared.registrationSuccess { [weak self] in
            guard let strong = self else {return}
            SwiftEntryKitService.shared.showActivityIndicator(false)
            strong.router?.showEnableSecure()
            SwiftEntryKitService.shared.dissmiss()
        }
    }
    
    func onIncorrectOtp(time: Double) {
        SwiftEntryKitService.shared.showActivityIndicator(false)
        self.timeStampCount = time
        timerCount = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(counterCount), userInfo: nil, repeats: true)
    }
    
    func onIncorrectOtp() {
        SwiftEntryKitService.shared.showActivityIndicator(false)
        view?.incorrectOtp(text: "Смс-код введен неверно")
    }
    
    func onWaitForTime() {
        SwiftEntryKitService.shared.showActivityIndicator(false)
        view?.waitForTime(text: "Ждите конца таймера")
    }
    
}
