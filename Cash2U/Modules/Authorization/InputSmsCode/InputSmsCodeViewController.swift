//
//  InputSmsCodeViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/7/22.
//  
//

import UIKit
import SnapKit

final class InputSmsCodeViewController: CUViewController {
    
    var presenter: InputSmsCodeViewToPresenterProtocol?
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    var phoneNumber = ""
    
    private var isResendActive = false
    
    private var imageHeightConstraint: Constraint?
    private var nextButtonBottomConstraint: Constraint?
    private var otpHeightConstraint: Constraint?
    
    private lazy var backButton = UIBackButton()
    
    private lazy var imageMessage: UIImageView = {
        let view = UIImageView(image: UIImage(named: "Message"))
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private lazy var titleView: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Введите смс-код", size: 38, numberOfLines: 1, fontType: .bold, titleColor: .mainTextColor, isSizeToFit:  true)
        return view
    }()
    
    private lazy var subTitleView: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Мы отправили код сюда:\n+996 \(phoneNumber)", size: 17, numberOfLines: 2, fontType: .medium, isSizeToFit: true)
        return view
    }()
    
    private lazy var timerLabel = UILabelView("Повторно отправить код 00:00", font: .systemFont(ofSize: 15, weight: .bold), textColor: .init(named: "NumberGrayColorText")!)
    
    private lazy var otpView: CUOTPView = {
        let view = CUOTPView()
        view.delegate = self
        return view
    }()
    
    private lazy var confirmButton: CUMainButton = {
        let view = CUMainButton()
        view.setup(title: "Подтвердить", backgroundColor: .mainButtonColor)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(onConfirmButtonTapped), for: .touchUpInside)
        return view
    }()
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = .mainColor
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded() 
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.onViewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) { [weak self] in
            self?.otpView.otpView.becomeFirstResponder()
        }
    }
    
    // MARK: - UIActions
    
    @objc private func onConfirmButtonTapped() {
        hideKeyboard { [weak self] in
            guard let self else {return}
            if self.isResendActive {
                self.otpView.otpView.text = ""
                self.presenter?.onResendNumber(number: UserDefaultsService.shared.phoneNumber)
                self.isResendActive = false
                self.confirmButton.setup(title: "Подтвердить", backgroundColor: .mainButtonColor)
            } else {
                if self.otpView.otpView.validate() {
                    self.presenter?.onSendOtp(otp: self.otpView.otpView.text)
                } else {
                    self.showError(title: "Введите cмс-код")
                }
            }
        }
    }
    
    
    // MARK: - Helpers
    
    private func showError(title: String) {
        hideError()
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.1, initialSpringVelocity: 0, options: [.curveEaseOut]) {
            self.otpView.setError(title: title)
            self.otpHeightConstraint?.update(offset: 100)
            self.view.layoutIfNeeded()
        } completion: { _ in
            
        }
    }
    
    private func hideError() {
        UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0, options: [.curveEaseOut]) {
            self.otpView.unsetError()
            self.otpHeightConstraint?.update(offset: 80)
            self.view.layoutIfNeeded()
        }
    }
    
    override func showKeyBoard(height: CGFloat) {
        UIView.animate(withDuration: 0.5) {
            self.imageHeightConstraint?.update(offset: (ScreenHelper.getWidth / 8))
            if !ScreenHelper.isSmallScreen {
                self.nextButtonBottomConstraint?.update(inset: ScreenHelper.isEyebrowsScreen ? (height - 16) : (height + 16))
            }
            self.view.layoutIfNeeded()
        }
    }
    
    override func hideKeyBoard(height: CGFloat) {
        UIView.animate(withDuration: 0.5) {
            self.imageHeightConstraint?.update(offset: (ScreenHelper.getWidth / 2.2))
            if !ScreenHelper.isSmallScreen {
                self.nextButtonBottomConstraint?.update(inset: 16)
            }
            self.view.layoutIfNeeded()
        }
    }
}

extension InputSmsCodeViewController: InputSmsCodeViewProtocol {
    
    func incorrectOtp(time: String) {
        if time == "otp" {
            self.isResendActive = true
            timerLabel.isHidden = true
            confirmButton.setup(title: "Отправить код повторно", backgroundColor: .mainButtonColor)
        } else if time == "waitOtp" {
            self.isResendActive = true
            timerLabel.isHidden = true
            confirmButton.setup(title: "Отправить код повторно", backgroundColor: .mainButtonColor)
        } else {
            timerLabel.isHidden = false
            timerLabel.text = time
        }
    }
    
    func incorrectOtp(text: String) {
        otpView.otpView.text = ""
        otpView.otpView.becomeFirstResponder()
        showError(title: text)
    }
    
    func waitForTime(text: String) {
        otpView.otpView.text = ""
        showError(title: text)
    }
    
}

extension InputSmsCodeViewController: CUOTPViewDelegate {
    func dpOTPViewAddText(_ text: String, position: Int) {
        hideError()
    }
    
    func dpOTPViewRemoveText(_ text: String, position: Int) {
        hideError()
    }
    
    func dpOTPViewChangePositionAt(_ position: Int) {
        
    }
    
    func dpOTPViewBecomeFirstResponder() {
        
    }
    
    func dpOTPViewResignFirstResponder() {
        
    }
    
    
}

extension InputSmsCodeViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        
        view.addSubview(imageMessage)
        view.addSubview(titleView)
        view.addSubview(subTitleView)
        view.addSubview(otpView)
        view.addSubview(timerLabel)
        view.addSubview(confirmButton)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        imageMessage.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top)
            make.leading.trailing.equalToSuperview()
            imageHeightConstraint = make.height.equalTo(ScreenHelper.getWidth / 2.2).constraint
        }
        
        titleView.snp.makeConstraints { make in
            make.top.equalTo(imageMessage.snp.bottom).offset(8)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        subTitleView.snp.makeConstraints { make in
            make.top.equalTo(titleView.snp.bottom).offset(8)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        otpView.snp.makeConstraints { make in
            make.top.equalTo(subTitleView.snp.bottom).offset(2)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            otpHeightConstraint = make.height.equalTo(80).constraint
        }
        
        timerLabel.snp.makeConstraints { make in
            make.top.equalTo(otpView.snp.bottom).offset(8)
            make.centerX.equalToSuperview()
        }
        
        confirmButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            nextButtonBottomConstraint = make.bottom.equalTo(view.safeArea.bottom).offset(-16).constraint
            make.height.equalTo(ScreenHelper.mainButtonHeight)
        }
    }
}
