//
//  InputSmsCodeRouter.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/7/22.
//  
//

import UIKit

final class InputSmsCodeRouter {
    weak var viewController: UIViewController?
}

extension InputSmsCodeRouter: InputSmsCodeRouterProtocol {
    
    func navigateToMainTabBar() {
        UserDefaultsService.shared.isSecured = false
        let vc = JWTDecoderService.shared.getUserViewController()
        self.viewController?.navigationController?.present(vc, animated: true) {
            self.viewController?.navigationController?.popToRootViewController(animated: false)
        }
    }
    
    func navigateToReferralCode() {
        let vc = ReferalCodeModuleBuilder.build()
        self.viewController?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showEnableSecure() {
        let vc = EnableSecureViewController()
        vc.didDismissDelegate = self
        self.viewController?.presentPanModal(vc)
    }
}

extension InputSmsCodeRouter: CUViewControllerDelegate {
    func didDismiss(action: String) {
        if action == "send" {
            self.viewController?.navigationController?.pushViewController(CreatePinCodeModuleBuilder.build(), animated: true)
        } else {
            SwiftEntryKitService.shared.hasReferralCode {
                self.navigateToReferralCode()
                SwiftEntryKitService.shared.dissmiss()
            } declineClick: {
                self.navigateToMainTabBar()
                SwiftEntryKitService.shared.dissmiss()
            }
        }
    }
}
