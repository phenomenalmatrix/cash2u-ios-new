//
//  InputSmsCodeModuleBuilder.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/7/22.
//  
//

import UIKit

final class InputSmsCodeModuleBuilder {
    /// Собрать модуль
    class func build() -> UIViewController {
        let view = InputSmsCodeViewController()
        let presenter = InputSmsCodePresenter()
        let interactor = InputSmsCodeInteractor()
        let router = InputSmsCodeRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
