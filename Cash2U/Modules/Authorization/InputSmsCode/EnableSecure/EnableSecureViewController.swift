//
//  EnableSecureViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 3/8/22.
//  
//

import UIKit

final class EnableSecureViewController: CUViewController {
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private var secureAction = "later"
    
    private lazy var titleView = UILabelView("Защитите свой \nаккаунт", font: .systemFont(ofSize: 28, weight: .bold))
    
    private lazy var image: UIImageView = {
        let view = UIImageView(image: UIImage(named: "ValidPin"))
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private lazy var protectButton: CUMainButton = {
        let view = CUMainButton()
        view.setup(title: "Защитить", backgroundColor: .mainButtonColor)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(onSendButtonTapped), for: .touchUpInside)
        return view
    }()
    
    private lazy var laterButton: CUMainButton = {
        let view = CUMainButton()
        view.setup(title: "Не сейчас", backgroundColor: .secondaryColor, borderWidth: 1, borderColor: .grayColor)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(onLaterButtonTapped), for: .touchUpInside)
        return view
    }()
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = .secondaryColor
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @objc private func onSendButtonTapped() {
        self.secureAction = "send"
        self.dismiss(animated: true,completion: nil)
    }
    
    @objc private func onLaterButtonTapped() {
        self.secureAction = "later"
        self.dismiss(animated: true,completion: nil)
    }
}

extension EnableSecureViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        view.addSubviews([titleView, image, laterButton, protectButton])
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        titleView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(32)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        image.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(titleView.snp.bottom).offset(20)
            make.height.equalTo(170)
        }
        
        laterButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.top.equalTo(image.snp.bottom).offset(32)
            make.height.equalTo(ScreenHelper.mainButtonHeight)
        }
        
        protectButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.top.equalTo(laterButton.snp.bottom).offset(10)
            make.height.equalTo(ScreenHelper.mainButtonHeight)
        }
    }
}

// MARK: - PanModalPresentable

extension EnableSecureViewController: PanModalPresentable {
    func panModalDidDismiss() {
        didDismissDelegate?.didDismiss(action: secureAction)
    }
    
    var panScrollable: UIScrollView? {
        return nil
    }
    
    var longFormHeight: PanModalHeight {
        .contentHeight(480)
    }
    
    var shortFormHeight: PanModalHeight {
        .contentHeight(480)
    }
}
