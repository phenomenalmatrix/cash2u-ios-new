//
//  ReferalCodeProtocols.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/7/22.
//  
//

import Foundation

protocol ReferalCodeViewProtocol: AnyObject {
    func incorrectCode()
}

protocol ReferalCodeViewToPresenterProtocol: AnyObject {
    func validateRefCode(code: String)
}

protocol ReferalCodeInteractorProtocol: AnyObject {
}

protocol ReferalCodeInteractorToPresenterProtocol: AnyObject {
}

protocol ReferalCodeRouterProtocol: AnyObject {
    func navigateToHome()
}
