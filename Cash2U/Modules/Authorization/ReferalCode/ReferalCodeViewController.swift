//
//  ReferalCodeViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/7/22.
//  
//

import UIKit
import SnapKit
import SwiftQRScanner

final class ReferalCodeViewController: CUViewController {
    
    var presenter: ReferalCodeViewToPresenterProtocol?
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private var nextButtonBottomConstraint: Constraint?
    private var otpHeightConstraint: Constraint?
    private var errorTitleHeightConstraint: Constraint?
    private var imageHeightConstraint: Constraint?
    
    private lazy var confirmButton: CUMainButton = {
        let view = CUMainButton()
        view.setup(title: "Подтвердить", backgroundColor: .mainButtonColor)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(onConfirmButtonTapped), for: .touchUpInside)
        return view
    }()
    
    private lazy var errorTitleLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "ASDASDASADSASD SADASDASDAS", size: 17, numberOfLines: 2, fontType: .bold, titleColor: .redColor, alignment: .center)
        return view
    }()
    
    private lazy var warningIcon: UIImageView = {
        let view = UIImageView(image: UIImage(named: "WarningIcon"))
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var scanQrButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Или отсканируйте QR", backgroundColor: .clear)
        view.addTarget(self, action: #selector(onScanTapped), for: .touchUpInside)
        return view
    }()
    
    private lazy var titleView = UILabelView("Реферальный\nкод друга", font: .systemFont(ofSize: 38, weight: .bold))
    
    private lazy var otpView: CUOTPView = {
        let view = CUOTPView()
        view.otpView.keyboardType = .default
        view.setup(count: 5, placeholder: "aaaaa")
        view.delegate = self
        view.returnButtondelegate = self
        view.otpView.keyboardType = UIKeyboardType.alphabet
        view.otpView.isLowerCased = true
        return view
    }()
    
    override func loadView() {
        super.loadView()
        
        let cityItem = UIBarButtonItem.init(customView: UIView())
        navigationItem.leftBarButtonItems = [cityItem]
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Пропустить", style: .plain, target: self, action: #selector(onSkipTapped))
        
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) { [weak self] in
            self?.otpView.otpView.becomeFirstResponder()
        }
    }
    
    // MARK: - UIActions
    
    @objc private func onSkipTapped() {
        let vc = JWTDecoderService.shared.getUserViewController()
        self.navigationController?.present(vc, animated: true) {
            self.navigationController?.popToRootViewController(animated: false)
        }
    }
    
    @objc private func onConfirmButtonTapped() {
        hideKeyboard { [weak self] in
            guard let self else {return}
            if let text = self.otpView.otpView.text {
                if !text.isEmpty {
                    self.presenter?.validateRefCode(code: text)
                } else {
                    self.showError(title: "Введите код")
                }
            }
        }
    }
    
    @objc private func onScanTapped() {
//        let scanner = QRCodeScannerController(qrScannerConfiguration: QRScannerConfiguration(title: "", hint: "Сканирование QR", uploadFromPhotosTitle: "", invalidQRCodeAlertTitle: "Что-то пошло не так", invalidQRCodeAlertActionTitle: "Продолжить", cameraImage: UIImage(systemName: "camera.fill"), flashOnImage: UIImage(systemName: "flashlight.off.fill"), length: 10, color: .grayColor ?? .white, radius: 1, thickness: 10, readQRFromPhotos: false, cancelButtonTitle: "Закрыть", cancelButtonTintColor: .red))
////        scanner.delegate = self
//        scanner.view.backgroundColor = .black
//        self.present(scanner, animated: true, completion: nil)
    }
    
    // MARK: - Helpers
    
    private func hideQR(isHidden: Bool) {
        UIView.animate(withDuration: 0.2, delay: 0) { [weak self] in
            guard let self else {return}
            self.scanQrButton.alpha = isHidden ? 0 : 1
        }
    }
    
    private func showError(title: String) {
        hideError()
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [.curveEaseOut]) {
            self.errorTitleLabel.alpha = 1
            self.warningIcon.alpha = 1
            self.errorTitleLabel.changeText(title: title)
            self.errorTitleHeightConstraint?.update(offset: 40)
            self.imageHeightConstraint?.update(offset: 48)
            self.view.layoutIfNeeded()
        } completion: { _ in
            
        }
    }
    
    private func hideError() {
        UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0, options: [.curveEaseOut]) {
            self.errorTitleLabel.alpha = 0
            self.warningIcon.alpha = 0
            self.errorTitleHeightConstraint?.update(offset: 0)
            self.imageHeightConstraint?.update(offset: 0)
            self.view.layoutIfNeeded()
        }
    }
    
    override func showKeyBoard(height: CGFloat) {
        UIView.animate(withDuration: 0.5) {
            if !ScreenHelper.isSmallScreen {
                self.nextButtonBottomConstraint?.update(inset: ScreenHelper.isEyebrowsScreen ? (height - 16) : (height + 16))
            }
            self.view.layoutIfNeeded()
        }
    }
    
    override func hideKeyBoard(height: CGFloat) {
        UIView.animate(withDuration: 0.5) {
            if !ScreenHelper.isSmallScreen {
                self.nextButtonBottomConstraint?.update(inset: 16)
            }
            self.view.layoutIfNeeded()
        }
    }
}

extension ReferalCodeViewController: ReferalCodeViewProtocol {
    
    func incorrectCode() {
        self.otpView.otpView.text = ""
        self.showError(title: "Вы ввели недействительный код")
        self.confirmButton.setup(title: "Попробовать еще раз", backgroundColor: .mainButtonColor)
    }
    
}

extension ReferalCodeViewController: CUOTPViewDelegate, ReturnButtonDelegate {
    
    func returnButtonTapped() {
        if let text = otpView.otpView.text {
            if !text.isEmpty {
                presenter?.validateRefCode(code: text)
            } else {
                hideQR(isHidden: !text.isEmpty)
                showError(title: "Введите код")
            }
        }
        hideKeyboard()
    }
    
    func dpOTPViewAddText(_ text: String, position: Int) {
        hideQR(isHidden: !text.isEmpty)
        if text.isContainsLettersOnly {
            hideError()
        } else if text.isContainsNumberOnly {
            showError(title: "Нельзя вводить цифры")
        } else if !text.isContainsNumberOnly && !text.isContainsLettersOnly {
            showError(title: "Нельзя вводить символы")
        } else {
            showError(title: "Можно вводить \nтолько в нижнем реестре")
        }
    }
    
    func dpOTPViewRemoveText(_ text: String, position: Int) {
        hideQR(isHidden: !text.isEmpty)
        hideError()
    }
    
    func dpOTPViewChangePositionAt(_ position: Int) {
        if let text = otpView.otpView.text {
            hideQR(isHidden: !text.isEmpty)
        }
    }
    
    func dpOTPViewBecomeFirstResponder() {
        if let text = otpView.otpView.text {
            hideQR(isHidden: !text.isEmpty)
        }
        hideError()
    }
    
    func dpOTPViewResignFirstResponder() {
        if let text = otpView.otpView.text {
            hideQR(isHidden: !text.isEmpty)
        }
    }
    
    
}

extension ReferalCodeViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        
        view.addSubview(titleView)
        view.addSubview(otpView)
        view.addSubview(warningIcon)
        view.addSubview(errorTitleLabel)
        view.addSubview(scanQrButton)
        view.addSubview(confirmButton)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        titleView.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top).offset(35)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        otpView.snp.makeConstraints { make in
            make.top.equalTo(titleView.snp.bottom).offset(14)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            otpHeightConstraint = make.height.equalTo(80).constraint
        }
        
        warningIcon.snp.makeConstraints { make in
            make.top.equalTo(otpView.snp.bottom).offset(10)
            make.width.equalTo(48)
            imageHeightConstraint = make.height.equalTo(0).constraint
            make.centerX.equalToSuperview()
        }

        errorTitleLabel.snp.makeConstraints { make in
            make.top.equalTo(warningIcon.snp.bottom).offset(0)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            errorTitleHeightConstraint = make.height.equalTo(0).constraint
            
        }
        
        scanQrButton.snp.makeConstraints { make in
            make.top.equalTo(errorTitleLabel.snp.bottom).offset(20)
            make.width.equalTo(scanQrButton.getTitleWidth(withConstrainedHeight: 44) + 20)
            make.centerX.equalToSuperview()
            make.height.equalTo(44)
        }
        
        confirmButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            nextButtonBottomConstraint = make.bottom.equalTo(view.safeArea.bottom).offset(-16).constraint
            make.height.equalTo(ScreenHelper.mainButtonHeight)
        }
    }
}
