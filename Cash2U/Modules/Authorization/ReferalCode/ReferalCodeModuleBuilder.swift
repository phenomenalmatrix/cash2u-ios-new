//
//  ReferalCodeModuleBuilder.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/7/22.
//  
//

import UIKit

final class ReferalCodeModuleBuilder {
    /// Собрать модуль
    class func build() -> UIViewController {
        let view = ReferalCodeViewController()
        let presenter = ReferalCodePresenter()
        let interactor = ReferalCodeInteractor()
        let router = ReferalCodeRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
