//
//  ReferalCodeRouter.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/7/22.
//  
//

import UIKit

final class ReferalCodeRouter {
    weak var viewController: UIViewController?
}

extension ReferalCodeRouter: ReferalCodeRouterProtocol {
    func navigateToHome() {
        SwiftEntryKitService.shared.referralCodeSuccess {
            
            let vc = JWTDecoderService.shared.getUserViewController()
            
            self.viewController?.navigationController?.present(vc, animated: true) {
                self.viewController?.navigationController?.popToRootViewController(animated: false)
            }
            
            SwiftEntryKitService.shared.dissmiss()
        }
    }
}
