//
//  ReferalCodePresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/7/22.
//  
//

import Foundation

final class ReferalCodePresenter {
    
    weak var view: ReferalCodeViewProtocol?
    var interactor: ReferalCodeInteractorProtocol?
    var router: ReferalCodeRouterProtocol?
    
}

extension ReferalCodePresenter: ReferalCodeViewToPresenterProtocol {
    func validateRefCode(code: String) {
        if code == "aaaaa"{
            router?.navigateToHome()
        } else {
            view?.incorrectCode()
        }
    }
}

extension ReferalCodePresenter: ReferalCodeInteractorToPresenterProtocol {
    
}
