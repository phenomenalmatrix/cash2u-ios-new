//
//  RepaymentModuleBuilder.swift
//  Cash2U
//
//  Created by talgar osmonov on 6/10/22.
//  
//

import UIKit

final class RepaymentModuleConfigurator {
    /// Собрать модуль
    class func build() -> UIViewController {
        let view = RepaymentViewController()
        let presenter = RepaymentPresenter()
        let interactor = RepaymentInteractor()
        let router = RepaymentRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
