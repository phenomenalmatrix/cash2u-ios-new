//
//  RepaymentInteractor.swift
//  Cash2U
//
//  Created by talgar osmonov on 6/10/22.
//  
//

import Foundation

final class RepaymentInteractor {
    weak var presenter: RepaymentInteractorToPresenterProtocol?
}

extension RepaymentInteractor: RepaymentInteractorProtocol {
}
