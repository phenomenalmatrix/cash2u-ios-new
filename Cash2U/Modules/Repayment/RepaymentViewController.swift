//
//  RepaymentViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 6/10/22.
//  
//

import UIKit

class TextFieldWithPadding: UITextField {
    
    var textPadding = UIEdgeInsets(
        top: 10,
        left: 20,
        bottom: 10,
        right: 20
    )
    
    init(insets: UIEdgeInsets = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)) {
        textPadding = insets
        super.init(frame: .zero)
        self.clearButtonMode = .whileEditing
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.textRect(forBounds: bounds)
        return rect.inset(by: textPadding)
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.editingRect(forBounds: bounds)
        return rect.inset(by: textPadding)
    }
}

final class RepaymentViewController: CUViewController {
    
    var presenter: RepaymentViewToPresenterProtocol?
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private lazy var titleLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Погашение оплаты\nчастями", size: 28, numberOfLines: 2, fontType: .bold)
        return view
    }()
    
    private lazy var blueView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .init(hex: "#131075")
        view.cornerRadius = 18
        return view
    }()
    
    private lazy var leftIcon: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .mainBlueColor
        view.setImage(UIImage(named: "som_logo"), for: .normal)
        view.isUserInteractionEnabled = false
        view.cornerRadius = 12
        return view
    }()
    
    private lazy var blueTitle: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "cash2u\nсом", size: 18, fontType: .bold)
        return view
    }()
    
    private lazy var balanceTitle: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Ваш баланс:", size: 17, fontType: .medium)
        return view
    }()
    
    private lazy var balanceAmountTitle: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "100 500,00 c", size: 22, fontType: .bold)
        return view
    }()
    
    private lazy var amountTextField: TextFieldWithPadding = {
        let view = TextFieldWithPadding()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.cornerRadius = 18
        view.attributedPlaceholder = NSAttributedString(string: "Введите сумму", attributes: [NSAttributedString.Key.foregroundColor: UIColor.grayColor!])
        view.layer.borderColor = .init(gray: 1, alpha: 0.5)
        view.layer.borderWidth = 0.3
        view.backgroundColor = .thirdaryColor
        view.keyboardType = .numberPad
        return view
    }()
    
    private lazy var totalLable: CUBlankView = {
        let view = CUBlankView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(placeholder: "Итого:", text: "12000,00 c", placeholderSize: 16, textSize: 17, textColor: .mainTextColor, isBottomLineHidden: true, isTopLineHidden: false)
        return view
    }()
    
    private lazy var continueButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Далее")
        view.addTarget(self, action: #selector(onContinueButtonTapped), for: .touchUpInside)
        return view
    }()
    
    override func loadView() {
        super.loadView()
        title = "Погашение"
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - UIActions
    
    @objc private func onContinueButtonTapped() {
        let vc = PaymentDetailsModuleConfigurator.build()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension RepaymentViewController: RepaymentViewProtocol {
}

extension RepaymentViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        
        view.addSubview(titleLabel)
        view.addSubviews([blueView, leftIcon, blueTitle, balanceTitle, balanceAmountTitle, amountTextField, totalLable, continueButton])
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top).offset(20)
            make.leading.trailing.equalToSuperview().inset(20)
        }
        
        blueView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(20)
            make.leading.trailing.equalToSuperview().inset(20)
            make.height.equalTo(155)
        }
        
       
        
        leftIcon.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(35)
            make.leading.equalToSuperview().offset(35)
            make.size.equalTo(60)
        }
        
        blueTitle.snp.makeConstraints { make in
            make.leading.equalTo(leftIcon.snp.trailing).offset(15)
            make.centerY.equalTo(leftIcon)
        }
        
        balanceTitle.snp.makeConstraints { make in
            make.top.equalTo(leftIcon.snp.bottom).offset(10)
            make.leading.equalToSuperview().offset(35)
        }
        
        balanceAmountTitle.snp.makeConstraints { make in
            make.top.equalTo(balanceTitle.snp.bottom).offset(8)
            make.leading.equalToSuperview().offset(35)
        }
        
        amountTextField.snp.makeConstraints { make in
            make.top.equalTo(blueView.snp.bottom).offset(20)
            make.leading.trailing.equalToSuperview().inset(20)
            make.height.equalTo(70)
        }
        
        totalLable.snp.makeConstraints { make in
            make.top.equalTo(amountTextField.snp.bottom).offset(40)
            make.leading.trailing.equalToSuperview().inset(20)
            make.height.equalTo(60)
        }
        
        continueButton.snp.makeConstraints { make in
            make.bottom.equalTo(view.safeArea.bottom).offset(-16)
            make.leading.trailing.equalToSuperview().inset(20)
            make.height.equalTo(ScreenHelper.mainButtonHeight)
        }
    }
}
