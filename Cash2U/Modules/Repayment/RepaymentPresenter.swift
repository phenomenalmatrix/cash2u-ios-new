//
//  RepaymentPresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 6/10/22.
//  
//

import Foundation

final class RepaymentPresenter {
    
    weak var view: RepaymentViewProtocol?
    var interactor: RepaymentInteractorProtocol?
    var router: RepaymentRouterProtocol?
    
}

extension RepaymentPresenter: RepaymentViewToPresenterProtocol {
}

extension RepaymentPresenter: RepaymentInteractorToPresenterProtocol {
}
