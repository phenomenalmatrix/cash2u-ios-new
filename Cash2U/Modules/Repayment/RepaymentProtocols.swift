//
//  RepaymentProtocols.swift
//  Cash2U
//
//  Created by talgar osmonov on 6/10/22.
//  
//

import Foundation

protocol RepaymentViewProtocol: AnyObject {
}

protocol RepaymentViewToPresenterProtocol: AnyObject {
}

protocol RepaymentInteractorProtocol: AnyObject {
}

protocol RepaymentInteractorToPresenterProtocol: AnyObject {
}

protocol RepaymentRouterProtocol: AnyObject {
}
