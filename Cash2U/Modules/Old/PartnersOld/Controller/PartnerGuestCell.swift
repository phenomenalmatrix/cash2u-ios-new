//
//  PartnerGuestCell.swift
//  cash2u
//
//  Created by Eldar Akkozov on 13/4/22.
//

import Foundation
import SnapKit
import UIKit

class PartnerGuestCell: BaseCollectionCell {
    
    private lazy var toolbar = UIPartnersToolbar()
    
    private lazy var scroll = UIScrollView()
    private lazy var container = UIView()
        
    private lazy var searchView = UISearchView()
    private lazy var filterButton = UIFilterButton()
    
    private lazy var partnerMallCollectionView = UIPartnersMallCollectionView()
    private lazy var catigoriesCollectionView = UICatigoriesCollectionView()

    
    override func setupSubViews() {
        addSubview(toolbar)
        toolbar.snp.makeConstraints { make in
            make.left.right.top.equalToSuperview()
            make.height.equalTo(50)
        }
        
        addSubview(scroll)
        scroll.snp.makeConstraints { make in
            make.top.equalTo(toolbar.snp.bottom)
            make.left.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
        scroll.addSubview(container)
        container.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.width.equalTo(self)
        }
        
        container.addSubview(searchView)
        searchView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(16)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-88)
        }

        container.addSubview(filterButton)
        filterButton.snp.makeConstraints { make in
            filterButton.backgroundColor = .blue
            make.top.equalToSuperview().offset(16)
            make.left.equalTo(searchView.snp.right).offset(8)
            make.right.equalToSuperview().offset(-20)
        }
        
        container.addSubview(partnerMallCollectionView)
        partnerMallCollectionView.snp.makeConstraints { make in
            make.top.equalTo(filterButton.snp.bottom).offset(29)
            make.height.equalTo(self).dividedBy(3)
            make.left.right.equalToSuperview()
        }
        
        container.addSubview(catigoriesCollectionView)
        catigoriesCollectionView.snp.makeConstraints { make in
            make.top.equalTo(partnerMallCollectionView.snp.bottom).offset(31)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.bottom.equalToSuperview()
        }
    }
}
