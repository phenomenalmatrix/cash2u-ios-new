//
//  UIPromotionCollectionView.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation
import UIKit

class UIPromotionCollectionView: BaseView{
    
    private lazy var promotionsCollection: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        
        let view = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        view.showsHorizontalScrollIndicator = false
        view.showsVerticalScrollIndicator = false
        view.dataSource = self
        view.delegate = self
        view.contentInset = .init(top: 0, left: 18, bottom: 0, right: 18)
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        view.register(PromotionsCell.self, forCellWithReuseIdentifier: "PromotionsCell")
        return view
    }()
    
    override func setupView() {
        
    }
    
    override func setupSubViews() {
        addSubview(promotionsCollection)
        promotionsCollection.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
        }
    }
}

extension UIPromotionCollectionView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PromotionsCell", for: indexPath) as! PromotionsCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 337, height: 210)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }

}

class PromotionsCell: BaseCollectionCell {
    
    private lazy var title: UILabel = {
       let view = UILabel()
        view.textColor = .darkGray
        view.text = "Zara"
        return view
    }()
    
    private lazy var subTitle: UILabel = {
        let view = UILabel()
        view.textColor = .white
        view.numberOfLines = 0
        view.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        view.text = "Скидка 80% на все коллекции 2021"
        return view
    }()
    
    private lazy var text: UILabel = {
        let view = UILabel()
        view.textColor = .white
        view.numberOfLines = 0
        view.text = "Налетай, торопись! Мирный житель, поспеши пока можешь!"
        return view
    }()
    
    private lazy var icon: UIImageView = {
        let view = UIImageView()
        view.layer.masksToBounds = true
        view.image = UIImage(named: "MegaPay")
        view.layer.cornerRadius = 100
        return view
    }()
    
    override func setupCell() {
        backgroundColor = .black
        layer.cornerRadius = 24
        subTitle.textAlignment = .left
        text.textAlignment = .left
    }
    
    override func setupSubViews() {
        addSubview(title)
        title.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(18)
            make.top.equalToSuperview().offset(20)
        }
        
        addSubview(subTitle)
        subTitle.snp.makeConstraints { make in
            make.top.equalTo(title.snp.bottom).offset(5)
            make.left.equalTo(title.snp.left)
            
        }
        
        addSubview(icon)
        icon.layer.cornerRadius = 44 / 2
        icon.snp.makeConstraints { make in
            make.width.height.equalTo(44)
            make.right.equalToSuperview().offset(-18)
            make.top.equalTo(title.snp.top)
            make.left.equalTo(subTitle.snp.right).offset(15)
        }
        
        addSubview(text)
        text.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-20)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview()
        }
    }
    
//    func fill(model: PromotionsModel){
//
//    }
}
