//
//  UIMessageView.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation
import SnapKit

class UIMessageView: BaseView {
    
    let screenWidth = UIScreen.main.bounds.size.width
    let screenHeight = UIScreen.main.bounds.size.height
    
    private lazy var promotionsCollection: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumLineSpacing = 0
        flowLayout.scrollDirection = .vertical
        
        let view = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        view.showsHorizontalScrollIndicator = false
        view.showsVerticalScrollIndicator = false
        view.isPagingEnabled = true
        view.dataSource = self
        view.delegate = self
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        view.register(MessageCell.self, forCellWithReuseIdentifier: "MessageCell")
        return view
    }()
    
    private lazy var arrowDown: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "ArrowDown")
        return view
    }()
    
    private lazy var customPageControll: UICustomPageController = {
        let view = UICustomPageController(pageSize: 10)
        return view
    }()
    
    override func setupView() {
        backgroundColor = UIColor(red: 255/255, green: 219/255, blue: 54/255, alpha: 1)
        layer.cornerRadius = 13
        clipsToBounds = true
    }
    
    override func setupSubViews() {
        snp.makeConstraints { make in
            make.height.equalTo(65)
        }
        
        addSubview(promotionsCollection)
        promotionsCollection.snp.makeConstraints { make in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.centerY.equalToSuperview()
            make.height.equalTo(64)
        }
        
        addSubview(customPageControll)
        customPageControll.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(10)
            make.top.bottom.equalToSuperview().inset(10)
        }
        
        addSubview(arrowDown)
        arrowDown.snp.makeConstraints { make in
            make.right.equalToSuperview().offset(-15)
            make.centerY.equalToSuperview()
        }
        
        
    }
    
}

extension UIMessageView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: 335, height: 64)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MessageCell", for: indexPath) as! MessageCell
            return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        customPageControll.currentPage(indexPath.item)

    }

}

class MessageCell: BaseCollectionCell {
    
    private lazy var honkIcon: UIImageView = {
       let view = UIImageView()
        view.image = UIImage(named: "Honk")
        return view
    }()
    
    private lazy var title: UILabel = {
        let view = UILabel()
        view.text = "Вам необходимо"
        view.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        view.textColor = .black
        return view
    }()
    
    private lazy var subTitle: UILabel = {
       let view = UILabel()
        view.text = "Подтвердить личность"
        view.font = UIFont.systemFont(ofSize: 15)
        view.textColor = .gray
        view.numberOfLines = 0
        return view
    }()
    
    override func setupCell() {
        backgroundColor = UIColor(red: 255/255, green: 219/255, blue: 54/255, alpha: 1)
        clipsToBounds = true
    }
    
    override func setupSubViews() {
        addSubview(honkIcon)
        honkIcon.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(30)
            make.centerY.equalToSuperview()
        }
        
        addSubview(title)
        title.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(13)
            make.left.equalTo(honkIcon.snp.right).offset(17)
        }
        
        addSubview(subTitle)
        subTitle.snp.makeConstraints { make in
            make.top.equalTo(title.snp.bottom).offset(4)
            make.left.equalTo(honkIcon.snp.right).offset(17)
            make.right.equalToSuperview().offset(-53)
            make.bottom.equalToSuperview().offset(-12)
        }
    }
    
}
