//
//  UIMallCollectionView.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation
import UIKit

class UIMallCollectionView: BaseView {
    
    private lazy var mallCollection: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        
        let view = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        view.showsHorizontalScrollIndicator = false
        view.showsVerticalScrollIndicator = false
        view.dataSource = self
        view.contentInset = .init(top: 0, left: 18, bottom: 0, right: 18)
        view.delegate = self
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        view.register(MallCell.self, forCellWithReuseIdentifier: "MallCell")
        return view
    }()
    
    private lazy var title: UILabel = {
       let view = UILabel()
        view.text = "Торговые центры"
        view.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        view.textColor = .black
        return view
    }()
    
    private lazy var rightArrow: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "ArrowRight")
        return view
    }()
    
    override func setupView() {
        
    }
    
    override func setupSubViews() {        
        addSubview(title)
        title.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
        }
        
        addSubview(rightArrow)
        rightArrow.snp.makeConstraints { make in
            make.right.equalToSuperview().offset(-20)
            make.top.equalToSuperview()
        }
        
        addSubview(mallCollection)
        mallCollection.snp.makeConstraints { make in
            make.top.equalTo(rightArrow.snp.bottom).offset(20)
            make.bottom.equalToSuperview()
            make.right.equalToSuperview()
            make.left.equalToSuperview()
        }
    }
    
}

extension UIMallCollectionView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MallCell", for: indexPath) as! MallCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 160, height: 189)
    }
    
}

class MallCell: BaseCollectionCell {
    
    private lazy var backgroundImg: UIImageView = {
       let view = UIImageView()
        view.layer.backgroundColor = UIColor.black.cgColor
        view.layer.opacity = 0.9
        view.layer.masksToBounds = true
        view.image = UIImage(named: "MegaPay")
        return view
    }()
    
    private lazy var title: UILabel = {
        let view = UILabel()
        view.text = "ТЦ Бишкек Парк"
        view.font = UIFont.systemFont(ofSize: 22, weight: .bold)
        view.textColor = .white
        view.numberOfLines = 0
        return view
    }()
    
    private lazy var subTitle: UILabel = {
        let view = UILabel()
        view.text = "12 партнеров"
        view.font = UIFont.systemFont(ofSize: 15)
        view.textColor = .white
        return view
    }()
    
    private lazy var fadeBackground: UIView = {
       let view = UIView()
        view.backgroundColor = .black
        view.layer.opacity = 0.5
        return view
    }()
    
    override func setupSubViews() {
        addSubview(backgroundImg)
        backgroundImg.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.right.left.equalToSuperview()
        }
        
        addSubview(fadeBackground)
        fadeBackground.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.right.left.equalToSuperview()
        }
        
        addSubview(subTitle)
        subTitle.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-16)
            make.left.equalToSuperview().offset(8)
        }
        
        addSubview(title)
        title.snp.makeConstraints { make in
            make.bottom.equalTo(subTitle.snp.top).offset(-10)
            make.left.equalTo(subTitle.snp.left)
            make.right.equalToSuperview().offset(-16)
        }
    }
    
    override func setupCell() {
        layer.cornerRadius = 18
        clipsToBounds = true
    }
    
//    func fill(model: MallsModel){
//
//    }
}
