//
//  UITitleWithDescriptionView.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation
import UIKit

class UITitleWithDescriptionView: BaseView {
        
    private lazy var title: UILabel = {
       let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        view.textColor = .black
        view.textAlignment = .left
        return view
    }()
    
    private lazy var subTitle: UILabel = {
       let view = UILabel()
        view.numberOfLines = 0
        view.font = UIFont.systemFont(ofSize: 15)
        view.textColor = .black
        view.textAlignment = .left
        return view
    }()
    
    override func setupSubViews() {
        addSubview(title)
        title.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
        }
        
        addSubview(subTitle)
        subTitle.snp.makeConstraints { make in
            make.top.equalTo(title.snp.bottom).offset(10)
            make.left.equalToSuperview()
            make.right.equalTo(title.snp.right)
            make.bottom.equalToSuperview()
        }
    }
    
    init(title: String, subTitle: String) {
        super.init(frame: .zero)
        self.title.text = title
        self.subTitle.text = subTitle
    }
    
    init(title: String) {
        super.init(frame: .zero)

        self.title.text = title
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
