//
//  UIGuidePanel.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation
import SnapKit

class UIGuidePanel: BaseView{
    
    private lazy var backgroundCircleImg: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "BackgroundCircle")
        return view
    }()
    
    private lazy var logoImg: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "Cash2uLightLogo")
        return view
    }()
    
    private lazy var cardImg: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "MiniCards")
        return view
    }()
    
    private lazy var button: UIBlueButton = {
        let view = UIBlueButton(title: "Как это работает?")
        view.layer.cornerRadius = 14
        view.backgroundColor = .blue
        return view
    }()
    
    private lazy var title: UILabel = {
        let view = UILabel()
        view.text = "самый удобный способ оплачивать покупки частями"
        view.numberOfLines = 0
        view.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        view.textColor = .white
        return view
    }()
    
    private var onClick: () -> Void = { }

    func onClickListener(onClick: @escaping () -> Void) {
        self.onClick = onClick
    }
    
    override func setupView() {
        backgroundColor = .init(hex: "#3530FA")
        layer.cornerRadius = 20
        clipsToBounds = true
        
        button.onClickListener { UIView in
            self.onClick()
        }

    }
    
    override func setupSubViews() {
        
        snp.makeConstraints { make in
            make.height.equalTo(520)
        }
        
        addSubview(logoImg)
        logoImg.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(24)
            make.left.equalToSuperview().offset(16)
        }
        
        addSubview(title)
        title.snp.makeConstraints { make in
            title.textAlignment = .left
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-55)
            make.top.equalTo(logoImg.snp.bottom).offset(10)
        }
        
        addSubview(backgroundCircleImg)
        backgroundCircleImg.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.left.right.equalToSuperview()
        }

        addSubview(cardImg)
        cardImg.snp.makeConstraints { make in
            make.centerY.equalTo(backgroundCircleImg.snp.top).offset(20)
            make.centerX.equalToSuperview()
        }

        addSubview(button)
        button.snp.makeConstraints { make in
            make.height.equalTo(65)
            make.bottom.equalToSuperview().offset(-20)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
        }
        
    }
    
    
}
