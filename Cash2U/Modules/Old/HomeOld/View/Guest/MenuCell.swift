//
//  MenuCell.swift
//  cash2u
//
//  Created by jojo on 14/4/22.
//

import Foundation
import UIKit

class MenuCell: BaseTableCell {
    
    private lazy var icon: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "WalletIcon")
        return view
    }()
    
    private lazy var title = UILabelView("Юридическая информация", font: UIFont.systemFont(ofSize: 17, weight: .medium), textColor: .mainTextColor ?? .white)
    
    private lazy var line: UIView = {
        let view = UIView()
        view.backgroundColor = .init(named: "LineBackgroundColor")
        return view
    }()
    
    
    override func setupCell() {
        backgroundColor = .clear
    }
    
    override func setupSubViews() {
        addSubview(icon)
        icon.snp.makeConstraints { make in
            make.height.equalTo(42)
            make.left.equalToSuperview().offset(20)
            make.centerY.equalToSuperview()
        }
        
        addSubview(title)
        title.snp.makeConstraints { make in
            make.left.equalTo(icon.snp.right).offset(12)
            make.top.bottom.equalToSuperview()
        }
        
        addSubview(line)
        line.snp.makeConstraints { make in
            make.height.equalTo(0.5)
            make.bottom.equalToSuperview()
            make.right.equalToSuperview().offset(-16)
            make.left.equalTo(title.snp.left)
        }
    }
}
