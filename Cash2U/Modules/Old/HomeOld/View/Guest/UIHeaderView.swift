//
//  UIHeaderView.swift
//  cash2u
//
//  Created by jojo on 14/4/22.
//

import Foundation
import SnapKit

class UIHeaderView: BaseView {
    
    private lazy var title = UILabelView("Вы вошли как \nгость", font: UIFont.systemFont(ofSize: 24, weight: .bold), textColor: .white)
    private lazy var arrowIcon = UIImageView(image: UIImage(named: "WhiteArrowRight"))
    private lazy var button = UIButtonView("Зарегистрироваться", UIFont.systemFont(ofSize: 15, weight: .bold), 30, UIColor.init(named: "HeaderButtonColor")!, .black)
    
    override func setupSubViews() {
        layer.cornerRadius = 28
        layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        backgroundColor = UIColor.init(named: "HeaderBackgroundColor")
    }
    
    override func setupView() {
        
        addSubview(title)
        title.snp.makeConstraints { make in
            make.top.equalTo(safeArea.top).offset(16)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-36)
        }
        
        addSubview(arrowIcon)
        arrowIcon.snp.makeConstraints { make in
            make.centerY.equalTo(title)
            make.right.equalToSuperview().offset(-23)
        }

        
        addSubview(button)
        button.snp.makeConstraints { make in
            make.top.equalTo(title.snp.bottom).offset(12)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.bottom.equalToSuperview().offset(-20)
        }
    }
    
}
