//
//  UIFAQView.swift
//  cash2u
//
//  Created by Eldar Akkozov on 14/4/22.
//

import Foundation
import UIKit
import SnapKit

class UIFAQView: BaseView {
    
    private lazy var title: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        view.textColor = .black
        view.text = "Частые вопросы"
        view.textAlignment = .left
        return view
    }()
    
    private lazy var container = UIStackView()
    
    override func setupSubViews() {
        container.axis = .vertical
        
        addSubview(title)
        title.snp.makeConstraints { make in
            make.top.left.right.equalToSuperview()
        }
        
        addSubview(container)
        container.snp.makeConstraints { make in
            make.top.equalTo(title.snp.bottom).offset(24)
            make.left.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
        container.addArrangedSubview(UIFAQItem())
        container.addArrangedSubview(UIFAQItem())
        container.addArrangedSubview(UIFAQItem().last())

    }
    
    override func setupView() {
        container.backgroundColor = .init(named: "FAQColor")
        container.layer.cornerRadius = 28
    }
}

private class UIFAQItem: BaseView {
     
    private lazy var title = UILabelView("Почему мне не одобрили рассрочку cash2u?", font: .systemFont(ofSize: 17, weight: .medium), textColor: .init(hex: "#21222B"))
    private lazy var message = UILabelView("Возможно несколько причин:\n- У вас есть просроченная задолженность по кредитам (КИБ)\n- Вам менее 23 лет или более 60 лет.\n- Вы ранее уже зарегистрировались в приложении cash2u\n- У Вас есть непогашенная налоговая задолженность\n- Ваши текущие доходы не позволяют покрывать обязательства по кредиту\n- Ваше место проживания за пределами города Бишкек", font: .systemFont(ofSize: 14), textColor: .init(hex: "#21222B"))

    private lazy var separatorView = UIView()
    
    private lazy var imageIcon = UIImageView(image: UIImage(named: "FAQOpen"))
    
    override func setupSubViews() {
        separatorView.backgroundColor = .init(hex: "#DBE6F0")
        
        addSubview(title)
        title.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(13)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset((16 + 24 + 16) * -1)
            make.bottom.equalToSuperview().offset(-13)
        }
        
        addSubview(imageIcon)
        imageIcon.snp.makeConstraints { make in
            make.right.equalToSuperview().offset(-16)
            make.height.width.equalTo(24)
            make.centerY.equalTo(title)
        }
        
        addSubview(separatorView)
        separatorView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
            make.height.equalTo(2)
        }
        
        addSubview(message)
        
        message.isHidden = true
    }
    
    func last() -> UIFAQItem {
        separatorView.isHidden = true
        
        return self
    }
    
    private var isOpen = false
    
    override func setupView() {
        onClickListener { [self] view in
            if isOpen {
                isOpen = false
                
//                message.showHidden(hidden: true) { [self] in
//                    title.snp.remakeConstraints { make in
//                        make.top.equalToSuperview().offset(13)
//                        make.left.equalToSuperview().offset(16)
//                        make.right.equalToSuperview().offset(-16)
//                        make.bottom.equalToSuperview().offset(-13)
//                    }
//                    
//                    message.snp.removeConstraints()
//                }
                
//                imageIcon.showHidden(hidden: true) { [self] in
//                    imageIcon.image = UIImage(named: "FAQOpen")
//
//                    imageIcon.showHidden(hidden: false)
//                }
            } else {
                isOpen = true
                
//                imageIcon.showHidden(hidden: true) { [self] in
//                    imageIcon.image = UIImage(named: "FAQClose")
//
//                    imageIcon.showHidden(hidden: false)
//                }
                
                title.snp.remakeConstraints { make in
                    make.top.equalToSuperview().offset(13)
                    make.left.equalToSuperview().offset(16)
                    make.right.equalToSuperview().offset(-16)
                }
                
                message.showHidden(hidden: false)
                
                message.snp.remakeConstraints { make in
                    make.top.equalTo(title.snp.bottom).offset(13)
                    make.left.equalToSuperview().offset(16)
                    make.right.equalToSuperview().offset(-16)
                    make.bottom.equalToSuperview().offset(-13)
                }
            }
        }
    }
}
