//
//  UIguideRefPanel.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation
import SnapKit

class UIGuideRefPanel: BaseView {
    
    private let title: UILabel = {
       let view = UILabel()
        view.textColor = .white
        view.text = "Реферальные бонусы"
        view.numberOfLines = 0
        view.font = UIFont.systemFont(ofSize: 34, weight: .bold)
        return view
    }()
    
    private let cards: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "RefCards")
        return view
    }()
    
    private let manWithGirl: UIImageView = {
       let view = UIImageView()
        view.image = UIImage(named: "ManWithGirl")
        return view
    }()
    
    private let button: UIBlueButton = {
       let view = UIBlueButton(title: "Поделиться приложением")
        return view
    }()
    
    override func setupView() {
        backgroundColor = .init(named: "RefPanelBackground")
        layer.cornerRadius = 28
        clipsToBounds = true
    }
    
    override func setupSubViews() {
        snp.makeConstraints { make in
            make.height.equalTo(571)
        }
        addSubview(title)
        title.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(24)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-27)
        }
        
        addSubview(manWithGirl)
        manWithGirl.snp.makeConstraints { make in
            make.top.equalTo(title.snp.bottom).offset(15)
            make.centerX.equalToSuperview().offset(128)
        }
        
        addSubview(cards)
        cards.snp.makeConstraints { make in
            make.top.equalTo(title.snp.bottom).offset(38)
            make.centerX.equalTo(manWithGirl.snp.left).offset(-15)
        }
        
        addSubview(button)
        button.snp.makeConstraints { make in
            make.height.equalTo(64)
            make.top.equalTo(cards.snp.bottom).offset(6)
            make.left.equalToSuperview().offset(27)
            make.right.equalToSuperview().offset(-27)
            make.bottom.equalToSuperview().offset(-20)
        }
    }
    
}
