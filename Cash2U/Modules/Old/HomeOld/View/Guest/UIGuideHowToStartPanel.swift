//
//  UIguideHowToStartPanel.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation
import SnapKit

class UIGuideHowToStartPanel: BaseView {
    
    private let title: UILabel = {
       let view = UILabel()
        view.textColor = .white
        view.text = "Как начать?"
        view.numberOfLines = 0
        view.font = UIFont.systemFont(ofSize: 34, weight: .bold)
        return view
    }()
    
    private let subTitle: UILabel = {
        let view = UILabel()
        view.textColor = .white
        view.numberOfLines = 0
        view.text = "Оформление займет всего 15-20 минут"
        view.font = UIFont.systemFont(ofSize: 20)
        return view
    }()
    
    private let cards: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "MiniCardsHowToStart")
        return view
    }()
    
    private let man: UIImageView = {
       let view = UIImageView()
        view.image = UIImage(named: "ManWithPhone")
        return view
    }()
    
    private let button: UIBlueButton = {
       let view = UIBlueButton(title: "Оформить заявку")
        return view
    }()
    
    private var onClick: () -> Void = { }

    func onClickListener(onClick: @escaping () -> Void) {
        self.onClick = onClick
    }
    
    
    override func setupView() {
        backgroundColor = .init(named: "HowToStartPanelBackground")
        layer.cornerRadius = 28
        clipsToBounds = true
        
        button.onClickListener { UIView in
            self.onClick()
        }
    }
    
    override func setupSubViews() {
        addSubview(title)
        title.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(24)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-110)
        }
        
        addSubview(subTitle)
        subTitle.snp.makeConstraints { make in
            make.top.equalTo(title.snp.bottom).offset(10)
            make.left.equalTo(title.snp.left)
            make.right.equalTo(title.snp.right)
        }
        
        addSubview(man)
        man.snp.makeConstraints { make in
            make.top.equalTo(subTitle.snp.bottom).offset(12)
            make.centerX.equalToSuperview().offset(130)
        }
        
        addSubview(cards)
        cards.snp.makeConstraints { make in
            make.top.equalTo(subTitle.snp.bottom).offset(35)
            make.centerX.equalTo(man.snp.left).offset(-20)
        }
        
        addSubview(button)
        button.snp.makeConstraints { make in
            make.height.equalTo(64)
            make.top.equalTo(cards.snp.bottom).offset(17)
            make.left.equalToSuperview().offset(27)
            make.right.equalToSuperview().offset(-27)
            make.bottom.equalToSuperview().offset(-24)
        }
        
    }
    
}
