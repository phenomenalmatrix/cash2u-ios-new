//
//  UIPartnersMallCollectionView.swift
//  cash2u
//
//  Created by Eldar Akkozov on 14/4/22.
//

import Foundation
import UIKit

class UIPartnersMallCollectionView: BaseView {
    
    private lazy var mallsCollectionView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        
        let view = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        view.contentInset = .init(top: 0, left: 20, bottom: 0, right: 20)
        view.showsHorizontalScrollIndicator = false
        view.showsVerticalScrollIndicator = false
        view.delegate = self
        view.dataSource = self
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        view.register(PartnersMallCell.self, forCellWithReuseIdentifier: "PartnersMallCell")
        return view
    }()
    
    private lazy var title: UILabel = {
       let view = UILabel()
        view.text = "Торговые центры и базары"
        view.font = UIFont.systemFont(ofSize: 22, weight: .bold)
        view.textColor = .black
        return view
    }()
    
    private lazy var rightArrow: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "ArrowRight")
        return view
    }()
    
    override func setupView() {
        
    }
    
    override func setupSubViews() {
    
        addSubview(title)
        title.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.left.equalToSuperview().offset(19)
        }
        
        addSubview(rightArrow)
        rightArrow.snp.makeConstraints { make in
            make.right.equalToSuperview().offset(20)
        }
        
        addSubview(mallsCollectionView)
        mallsCollectionView.snp.makeConstraints { make in
            make.top.equalTo(title.snp.bottom).offset(18)
            make.bottom.equalToSuperview()
            make.right.left.equalToSuperview()
        }
    }
    
}

extension UIPartnersMallCollectionView: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return 5
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PartnersMallCell", for: indexPath) as! PartnersMallCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 337, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
}
