//
//  PartnersCategoryCell.swift
//  cash2u
//
//  Created by Eldar Akkozov on 14/4/22.
//

import Foundation
import SnapKit

class PartnersCategoryCell: BaseCollectionCell {
    
    private lazy var title = UILabelView("Новые партнеры", font: UIFont.systemFont(ofSize: 18, weight: .bold), textColor: .white)
    
    private lazy var subTitle = UILabelView("12 парнеров", font: UIFont.systemFont(ofSize: 15, weight: .regular), textColor: .white)
    
    private lazy var blurView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.layer.opacity = 3
        return view
    }()

    private lazy var backgoundImage = UIImageView()
    
    override func setupCell() {
        clipsToBounds = true
        cornerRadius = 18
    }
    
    override func setupSubViews() {
        addSubview(backgoundImage)
        backgoundImage.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.left.right.equalToSuperview()
        }
        
        addSubview(blurView)
        blurView.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.right.left.equalToSuperview()
        }
        
        addSubview(subTitle)
        subTitle.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-16)
            make.left.equalToSuperview().offset(8)
        }

        addSubview(title)
        title.snp.makeConstraints { make in
            make.bottom.equalTo(subTitle.snp.top).offset(-12)
            make.left.equalToSuperview().offset(8)
            make.right.equalToSuperview().offset(-33)
        }
    }
}
