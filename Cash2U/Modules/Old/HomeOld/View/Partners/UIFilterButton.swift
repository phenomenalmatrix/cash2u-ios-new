//
//  UIFilterButton.swift
//  cash2u
//
//  Created by Eldar Akkozov on 14/4/22.
//

import Foundation
import UIKit

class UIFilterButton: BaseView{
    
    private lazy var filterIcon: UIImageView = {
        let view = UIImageView()
        view.image = UIImage.init(named: "FilterIcon")
        return view
    }()
    
    override func setupView() {
        backgroundColor = .init(named: "FilterButtonBackground")
        cornerRadius = 18
    
    }
    
    override func setupSubViews() {
        snp.makeConstraints { make in
            make.height.equalTo(56)
        }
        
        addSubview(filterIcon)
        filterIcon.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.centerX.equalToSuperview()
        }
    }
    
}
