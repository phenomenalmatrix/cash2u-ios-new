//
//  PartnersMallCell.swift
//  cash2u
//
//  Created by Eldar Akkozov on 14/4/22.
//

import Foundation
import UIKit

class PartnersMallCell: BaseCollectionCell {
    
    private lazy var backgoundImage: UIImageView = {
        let view = UIImageView()
        return view
    }()
    
    private lazy var title: UILabelView = {
        let view = UILabelView(
            "ТЦ Дордой плаза",
            font: UIFont.systemFont(ofSize: 22, weight: .bold),
            textColor: .white
        )
        return view
    }()
    
    private lazy var subTitle: UILabelView = {
        let view = UILabelView(
            "35 партнеров",
            font: UIFont.systemFont(ofSize: 15, weight: .regular),
            textColor: .white
        )
        return view
    }()
    
    private lazy var blurView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.layer.opacity = 6
        return view
    }()
    
    override func setupCell() {
        clipsToBounds = true
        cornerRadius = 24
        backgroundColor = .black
    }
    
    override func setupSubViews() {
        
        addSubview(backgoundImage)
        backgoundImage.snp.makeConstraints { make in
            make.bottom.top.equalToSuperview()
            make.left.right.equalToSuperview()
        }
        
        addSubview(blurView)
        blurView.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.right.left.equalToSuperview()
        }
        
        blurView.addSubview(subTitle)
        subTitle.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-16)
            make.left.equalToSuperview().offset(8)
        }
        
        blurView.addSubview(title)
        title.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(8)
            make.bottom.equalTo(subTitle.snp.top).offset(-10)
        }
    }    
}
