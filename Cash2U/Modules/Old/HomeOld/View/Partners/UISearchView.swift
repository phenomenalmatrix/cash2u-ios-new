//
//  UISearchView.swift
//  cash2u
//
//  Created by Eldar Akkozov on 14/4/22.
//

import Foundation
import SnapKit
import UIKit

class UISearchView: BaseView {
    
    private lazy var loupeIcon: UIImageView = {
        let view = UIImageView()
        view.image = UIImage.init(named: "Loupe")
        return view
    }()
    
    private lazy var label: UILabelView = {
        let view = UILabelView("Поиск", font: UIFont.systemFont(ofSize: 15), textColor: UIColor.init(named: "SearchViewTextColor") ?? UIColor())
        return view
    }()
    
    override func setupView() {
        cornerRadius = 18
        backgroundColor = .init(named: "SearchViewBackground")
    }
    
    override func setupSubViews() {
        snp.makeConstraints { make in
            make.height.equalTo(56)
        }
        
        addSubview(loupeIcon)
        loupeIcon.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(22)
        }
        
        addSubview(label)
        label.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalTo(loupeIcon.snp.right).offset(18)
        }
    }
}
