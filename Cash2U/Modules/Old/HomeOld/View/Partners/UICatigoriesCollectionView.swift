//
//  UICatigoriesCollectionView.swift
//  cash2u
//
//  Created by Eldar Akkozov on 14/4/22.
//

import Foundation
import SnapKit

class UICatigoriesCollectionView: BaseView {
    
    private lazy var categoryCollectionView: UICollectionView = {
        let flowlayout = UICollectionViewFlowLayout()
        flowlayout.scrollDirection = .vertical
        
        let view = UICollectionView(frame: .zero, collectionViewLayout: flowlayout)
        view.isScrollEnabled = false
        view.delegate = self
        view.dataSource = self
        view.showsHorizontalScrollIndicator = false
        view.showsVerticalScrollIndicator = false
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        view.register(PartnersCategoryCell.self, forCellWithReuseIdentifier: "PartnersCategoryCell")
        return view
    }()
    
    private lazy var title: UILabel = {
       let view = UILabel()
        view.text = "Категории"
        view.font = UIFont.systemFont(ofSize: 22, weight: .bold)
        view.textColor = .black
        return view
    }()
    
    override func setupView() {
        print(getHeightToContent())
    }
    
    override func setupSubViews() {
        
        snp.makeConstraints { make in
            make.height.equalTo(
                getHeightToContent()
            )
        }
        
        addSubview(title)
        title.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
        }
        
        addSubview(categoryCollectionView)
        categoryCollectionView.snp.makeConstraints { make in
            make.top.equalTo(title.snp.bottom).offset(11)
            make.left.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
    }
    
    private func getHeightToContent() -> Int {
        let itemsCount = categoryCollectionView.numberOfItems(inSection: 0)
        if itemsCount % 2 == 0 {
            return ((itemsCount / 2) * (177 + 11)) + 22 + 11
        } else {
            return (((itemsCount / 2) + 1) * (177 + 11)) + 22 + 11
        }
    }
    
}

extension UICatigoriesCollectionView: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCell(withReuseIdentifier: "PartnersCategoryCell", for: indexPath) as! PartnersCategoryCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width / 2) - 5, height: 177)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 11
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
         return 0
    }
    
}
