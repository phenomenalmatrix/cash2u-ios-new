//
//  StatusView.swift
//  cash2u
//
//  Created by Eldar Akkozov on 20/4/22.
//

import Foundation
import UIKit
import Moya

class StatusView: BaseView {
    
    private lazy var date = BaseView()
    private lazy var datea = BaseView()

    override func setupSubViews() {
        date.backgroundColor = .init(hex: "#D4FAD3")
        date.layer.cornerRadius = 20
        
        datea.backgroundColor = .init(hex: "#3530FA")
        datea.layer.cornerRadius = 20
        
        addSubview(datea)
        datea.snp.makeConstraints { make in
            make.top.left.right.equalToSuperview()
            make.height.equalTo(186)
        }
        
        addSubview(date)
        date.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(48)
            make.left.right.bottom.equalToSuperview()
            make.height.equalTo(186)
        }
    }
    
    private lazy var isOpen = true
    
    override func setupView() {
        datea.onClickListener { [self] _ in
            if isOpen {
                isOpen = false
                
                DispatchQueue.main.async { [self] in
                    UIView.animate(withDuration: 0.6, animations: {
                        date.frame.origin.y += (146 / 2)

                    }, completion: {(_ completed: Bool) -> Void in

                    })
                }
            } else {
                isOpen = true
                
                DispatchQueue.main.async { [self] in
                    UIView.animate(withDuration: 0.6, animations: {
                        date.frame.origin.y -= (146 / 2)
                        
                        
                    }, completion: {(_ completed: Bool) -> Void in


                    })
                }
            }
        }
    }
}
