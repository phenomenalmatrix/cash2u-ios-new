//
//  ProfileGuestCell.swift
//  cash2u
//
//  Created by Eldar Akkozov on 13/4/22.
//

import Foundation
import UIKit

class ProfileGuestCell: BaseCollectionCell {
    
    private lazy var headerView = UIHeaderView()
    
    private lazy var menuTableView = UIMenuTableView()
    
    override func setupSubViews() {
        addSubview(headerView)
        headerView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.left.right.equalToSuperview()
        }
        
        addSubview(menuTableView)
        menuTableView.snp.makeConstraints { make in
            make.top.equalTo(headerView.snp.bottom)
            make.right.left.equalToSuperview()
            make.bottom.equalToSuperview()
        }
    }
}
