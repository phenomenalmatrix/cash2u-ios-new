//
//  GuestController.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation
import SnapKit
import UIKit

class GuestController: BaseModelController<GuestViewModel> {
    
    private lazy var tabsView = UITabsView()
    
    private lazy var tabsBackView: UIView = {
        let view = UIView()
        view.backgroundColor = .init(hex: "#FBFBFB")
        return view
    }()
    
    private lazy var windowTopView = UIView()
    
    private lazy var viewPager: UIViewPager = {
        let view = UIViewPager()
        
        view.collection.register(HomeGuestCell.self, forCellWithReuseIdentifier: "HomeGuestCell")
        view.collection.register(PartnerGuestCell.self, forCellWithReuseIdentifier: "PartnerGuestCell")
        view.collection.register(ProfileGuestCell.self, forCellWithReuseIdentifier: "ProfileGuestCell")

        view.setViewHolders({ collection, indexPath in
            switch indexPath.row {
            case 0:
                return collection.dequeueReusableCell(withReuseIdentifier: "HomeGuestCell", for: indexPath)
            case 1:
                return collection.dequeueReusableCell(withReuseIdentifier: "PartnerGuestCell", for: indexPath)
            case 2:
                return collection.dequeueReusableCell(withReuseIdentifier: "ProfileGuestCell", for: indexPath)
            default:
                return UICollectionViewCell()
            }
        }, countCell: 3)
        
        return view
    }()
    
    override func setupUI() {
        windowTopView.isHidden = true
        windowTopView.backgroundColor = .init(hex: "#3530FA")

        tabsView.onTabClickListener { index in
            self.viewPager.scrollToPage(index: index)
            
            if index == 2 {
                self.windowTopView.showHidden(hidden: false, withDuration: 0.2)
            } else {
                self.windowTopView.isHidden = true
            }
        }
    }
    
    override func setupConstraint() {
        view.addSubview(tabsView)
        tabsView.snp.makeConstraints { make in
            make.left.right.equalToSuperview()
            make.bottom.equalTo(view.safeArea.bottom)
        }
        
        view.addSubview(tabsBackView)
        tabsBackView.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.left.right.equalToSuperview()
            make.top.equalTo(view.safeArea.bottom)
        }
        
        view.addSubview(viewPager)
        viewPager.snp.makeConstraints { make in
            make.left.right.equalToSuperview()
            make.top.equalTo(view.safeArea.top)
            make.bottom.equalTo(tabsView.snp.top)
        }
        
        view.addSubview(windowTopView)
        windowTopView.snp.makeConstraints { make in
            make.top.left.right.equalToSuperview()
            make.bottom.equalTo(view.safeArea.top)
        }
    }
}

extension GuestController: GuestDelegate{
    
    func showProgress() {
        
    }
    
    func hideProgress() {
        
    }
    
    func showAlert(_ title: String, _ message: String) {
        
    }
}
