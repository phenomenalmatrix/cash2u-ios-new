//
//  UILabelInPanel.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation
import UIKit
import SnapKit

class UILabelInPanel: BaseView {
    
    private var text = ""
    
    private lazy var label: UILabel = {
        let view = UILabel()
        view.text = text
        view.numberOfLines = 0
        view.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        return view
    }()
    
    override func setupView() {
        label.textAlignment = .center
        backgroundColor = .white
        layer.cornerRadius = 24
    }
    
    override func setupSubViews() {
        snp.makeConstraints { make in
            make.height.equalTo(104)
        }
        
        addSubview(label)
        label.snp.makeConstraints { make in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
        }
    }
    
    init(title: String) {
        super.init(frame: .zero)
        text = title
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
