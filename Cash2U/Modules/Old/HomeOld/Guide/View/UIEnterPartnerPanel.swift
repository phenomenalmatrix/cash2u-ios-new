//
//  UIEnterPartnerPanel.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation
import UIKit
import SnapKit

class UIEnterPartnerPanel: BaseView{
    
    private lazy var background: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "EnterPartnersCardBackground")
        return view
    }()
    
    private lazy var label: UILabel = {
        let view = UILabel()
        view.text = "3. Заходите к партнерам и совершайте покуки "
        view.font = UIFont.systemFont(ofSize: 28, weight: .bold)
        view.textColor = .white
        view.numberOfLines = 0
        return view
    }()
    
    private lazy var firstDescription: UILabel = {
        let view = UILabel()
        view.text = "Рассрочку можно оформить сейчас, а использовать потом. Погашение начнется только после первой покупки в одном из магазинов парнеров"
        view.font = UIFont.systemFont(ofSize: 17, weight: .medium)
        view.numberOfLines = 0
        view.textColor = .white
        return view
    }()
    
    override func setupView() {
        cornerRadius = 24
        clipsToBounds = true
    }
    
    override func setupSubViews() {
        snp.makeConstraints { make in
            make.height.equalTo(691)
        }
        
        addSubview(background)
        background.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.left.right.equalToSuperview()
        }
        
        addSubview(label)
        label.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(24)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-117)
        }
        
        addSubview(firstDescription)
        firstDescription.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-22)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-19)
            
        }
    }
}
