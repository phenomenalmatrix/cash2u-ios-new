//
//  UIIdentificatePanel.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation
import SnapKit
import UIKit

class UIIdentificatePanel: BaseView {
    
    private lazy var background: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "IdentificalCardBackground")
        return view
    }()
    
    private lazy var cards: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "IdentificalMiniCards")
        return view
    }()
    
    private lazy var label: UILabel = {
        let view = UILabel()
        view.text = "1. Подвердить личность онлайн"
        view.font = UIFont.systemFont(ofSize: 28, weight: .bold)
        view.textColor = .white
        view.numberOfLines = 0
        return view
    }()
    
    override func setupView() {
        cornerRadius = 24
        clipsToBounds = true
    }
    
    override func setupSubViews() {
        snp.makeConstraints { make in
            make.height.equalTo(544)
        }
        
        addSubview(background)
        background.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.left.right.equalToSuperview()
        }
        
        addSubview(label)
        label.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(24)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-78)
        }
        
        addSubview(cards)
        cards.snp.makeConstraints { make in
            make.top.equalTo(label.snp.bottom).offset(32)
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-23)
        }
    }
}
