//
//  UIFillOutPanel.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation
import UIKit
import SnapKit

class UIFillOutPanel: BaseView {
    
    private lazy var title: UITitleWithDescriptionView = {
        let view = UITitleWithDescriptionView(title: "2. Заполните заявку", subTitle: "Туда входит:")
        return view
    }()
    
    private lazy var cards: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "FillOutMiniCards")
        return view
    }()
    
    override func setupView() {
        layer.cornerRadius = 28
        backgroundColor = .white
    }
    
    override func setupSubViews() {
        
        snp.makeConstraints { make in
            make.height.equalTo(500)
        }
        
        addSubview(title)
        title.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(24)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-27)
        }
        
        addSubview(cards)
        cards.snp.makeConstraints { make in
            make.top.equalTo(title.snp.bottom).offset(35)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.bottom.equalToSuperview().offset(-5)
        }
    }
}
