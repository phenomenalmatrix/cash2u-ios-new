//
//  GuideViewController.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation
import SnapKit

class GuideViewController: BaseController{
    
    private lazy var guideTitleView: UITitleWithDescriptionView = {
       let view = UITitleWithDescriptionView(
        title: "Как начать использовать?",
        subTitle: "Cash2u предоставляет беспроцентную рассрочку на 3 месяца для всех граждан КР."
       )
        return view
    }()
    
    private lazy var scroll: UIScrollView = {
        let view = UIScrollView()
        return view
    }()
    
    private lazy var  container: UIView = {
        let view = UIView()
        view.backgroundColor = .init(named: "GuideBackgroundColor")
        return view
    }()
    
    private lazy var identificationPanel: UIIdentificatePanel = {
        let view = UIIdentificatePanel()
        return view
    }()
    
    private lazy var labelPanel: UILabelInPanel = {
        let view = UILabelInPanel(title: "Среднее время \nзаполнения 15 минут")
        return view
    }()
    
    private lazy var fiilOutPanel: UIFillOutPanel = {
        let view = UIFillOutPanel()
        return view
    }()
    
    private lazy var enterPaner = UIEnterPartnerPanel()
    
    override func setupUI() {
        view.backgroundColor = .init(named: "GuideBackgroundColor")
    }
    
    override func setupConstraint() {
        view.addSubview(scroll)
        scroll.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.left.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
        scroll.addSubview(container)
        container.snp.makeConstraints { make in
            make.width.equalTo(view)
            make.top.bottom.equalToSuperview()
            make.left.right.equalToSuperview()
        }
        
        container.addSubview(guideTitleView)
        guideTitleView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(46)
            make.left.equalToSuperview().offset(18)
            make.right.equalToSuperview().offset(-35)
        }
        
        container.addSubview(labelPanel)
        labelPanel.snp.makeConstraints { make in
            make.top.equalTo(guideTitleView.snp.bottom).offset(50)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
        }
        
        container.addSubview(identificationPanel)
        identificationPanel.snp.makeConstraints { make in
            make.top.equalTo(labelPanel.snp.bottom).offset(30)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
        }
        
        container.addSubview(fiilOutPanel)
        fiilOutPanel.snp.makeConstraints { make in
            make.top.equalTo(identificationPanel.snp.bottom).offset(30)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            
        }
        
        container.addSubview(enterPaner)
        enterPaner.snp.makeConstraints { make in
            make.top.equalTo(fiilOutPanel.snp.bottom).offset(30)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.bottom.equalToSuperview()
        }
    }
}
