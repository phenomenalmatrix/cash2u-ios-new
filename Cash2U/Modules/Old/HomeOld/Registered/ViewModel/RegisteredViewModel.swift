//
//  RegisteredViewModel.swift
//  cash2u
//
//  Created by Eldar Akkozov on 18/4/22.
//

import Foundation

protocol RegisteredDelegate: AnyObject {
    
}

class RegisteredViewModel: BaseViewModel<RegisteredDelegate> {
    
}
