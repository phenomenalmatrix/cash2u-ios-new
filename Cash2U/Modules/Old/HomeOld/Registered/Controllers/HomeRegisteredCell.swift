//
//  HomeRegisteredCel.swift
//  cash2u
//
//  Created by Eldar Akkozov on 20/4/22.
//

import Foundation
import UIKit

class HomeRegisteredCell: BaseCollectionCell {

    private lazy var toolbar = UIHomeToolbar()
    
    private lazy var scrollView = UIScrollView()
    private lazy var containerView = UIView()
    
    private lazy var messageView = UIMessageView()
    
    private lazy var statusView = StatusView()
    
    private lazy var offersAndPromotions = UITitleWithDescriptionView(title: "Предложения и акции", subTitle: "Горячие предложения и акции от проверенных партнёров cash2u")
    private lazy var promotionCollectionView = UIPromotionCollectionView()
    
    private lazy var whereCanYouUse = UITitleWithDescriptionView(title: "Наши партнеры", subTitle: "С помощью баланса можно оплачивать в рассрочку товары и услуги у наших партнёров:")
    
    private lazy var mallCollectionView = UIMallCollectionView()
    private lazy var categoryCollectionView = UICategoryCollectionView()

    
    private lazy var faqView = UIFAQView()
    private lazy var qButtom = UIBlueButton(title: "😎 Задайте вопрос")
    
    override func setupCell() {
        
    }
    
    override func setupSubViews() {
        addSubview(toolbar)
        toolbar.snp.makeConstraints { make in
            make.left.right.top.equalToSuperview()
            make.height.equalTo(50)
        }
        
        addSubview(scrollView)
        scrollView.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.right.left.equalToSuperview()
            make.top.equalTo(toolbar.snp.bottom)
        }
        
        scrollView.addSubview(containerView)
        containerView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
            make.width.equalTo(self)
        }
        
        containerView.addSubview(messageView)
        messageView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(6)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(65)
        }
        
        containerView.addSubview(statusView)
        statusView.snp.makeConstraints { make in
            make.top.equalTo(messageView.snp.bottom).offset(24)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
        }

        containerView.addSubview(offersAndPromotions)
        offersAndPromotions.snp.makeConstraints { make in
            make.top.equalTo(statusView.snp.bottom).offset(24)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
        }
        
        containerView.addSubview(promotionCollectionView)
        promotionCollectionView.snp.makeConstraints { make in
            make.left.right.equalToSuperview()
            make.height.equalTo(210)
            make.top.equalTo(offersAndPromotions.snp.bottom).offset(20)
        }
        
        containerView.addSubview(whereCanYouUse)
        whereCanYouUse.snp.makeConstraints { make in
            make.top.equalTo(promotionCollectionView.snp.bottom).offset(40)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
        }
        
        containerView.addSubview(mallCollectionView)
        mallCollectionView.snp.makeConstraints { make in
            make.left.right.equalToSuperview()
            make.top.equalTo(whereCanYouUse.snp.bottom).offset(31)
            make.height.equalTo(236)
        }
        
        containerView.addSubview(categoryCollectionView)
        categoryCollectionView.snp.makeConstraints { make in
            make.left.right.equalToSuperview()
            make.top.equalTo(mallCollectionView.snp.bottom).offset(31)
            make.height.equalTo(224)
        }
        
        containerView.addSubview(faqView)
        faqView.snp.makeConstraints { make in
            make.top.equalTo(categoryCollectionView.snp.bottom).offset(45)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
        }
        
        containerView.addSubview(qButtom)
        qButtom.snp.makeConstraints { make in
            make.height.equalTo(56)
            make.top.equalTo(faqView.snp.bottom).offset(20)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.bottom.equalToSuperview().offset(-20)
        }
    }
    
}
