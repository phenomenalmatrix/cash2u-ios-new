//
//  OnBoardCell.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation
import UIKit
import SnapKit

protocol OnBoardCellDelegate: AnyObject {
    func clickEnd()
}

class OnBoardCell: BaseCollectionCell {
    
    private lazy var titleCell: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        view.sizeToFit()
        view.textColor = .mainTextColor
        view.numberOfLines = 0
        return view
    }()
    
    private lazy var subTitleCell: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 15)
        view.textAlignment = .left
        view.textColor = .mainTextColor
        view.numberOfLines = 0
        return view
    }()
    
    private lazy var imageCell: UIImageView = {
       let view = UIImageView()
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private lazy var potImageCell: UIImageView = {
       let view = UIImageView()
        view.image = UIImage(named: "Pot")
        return view
    }()
    
    private lazy var titleContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .onBoardCardColor
        view.layer.cornerRadius = 25
        return view
    }()
    
    private var enterBtn = UIBlueButton(title: "Войти")
    
    private lazy var enterButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Войти")
        view.addTarget(self, action: #selector(onEnterTapped), for: .touchUpInside)
        return view
    }()
    
    weak var delegate: OnBoardCellDelegate?
    
    @objc private func onEnterTapped() {
        self.delegate?.clickEnd()
    }
    
    override func setupCell() {
        backgroundColor = .mainColor
        titleCell.textAlignment = .center
    }
    
    override func setupSubViews() {
        addSubview(titleContainer)
        titleContainer.snp.makeConstraints { make in
            make.height.equalToSuperview().dividedBy(2.72)
            make.left.right.equalToSuperview().inset(20)
            make.bottom.equalToSuperview().offset(-20)
        }

        addSubview(imageCell)
        imageCell.snp.makeConstraints { make in
            make.left.right.equalToSuperview().inset(20)
            make.height.equalToSuperview().dividedBy(2.13)
            make.bottom.equalTo(titleContainer.snp.top)
        }
        
        titleContainer.addSubview(titleCell)
        titleCell.snp.makeConstraints { make in
            titleCell.textAlignment = .left
            make.left.equalToSuperview().offset(15)
            make.top.equalToSuperview().offset(20)
        }

        titleContainer.addSubview(subTitleCell)
        subTitleCell.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(15)
            make.right.equalToSuperview()
            make.top.equalTo(titleCell.snp.bottom).offset(20)
        }

        titleContainer.addSubview(enterButton)
        enterButton.snp.makeConstraints { make in
            make.self.height.equalTo(55)
            make.left.equalToSuperview().offset(15)
            make.right.equalToSuperview().offset(-15)
            make.bottom.equalToSuperview().offset(-20)
        }
    
        addSubview(potImageCell)
        potImageCell.snp.makeConstraints { make in
            make.self.height.equalTo(132)
            make.centerX.equalTo(titleContainer.snp.right).offset(-22)
            make.centerY.equalTo(titleContainer.snp.top)
        }
    }
    
    func fill(model: OnBoardModel, delegate: OnBoardCellDelegate) {
        titleCell.text = model.title
        
        let text = NSAttributedString(string: model.subTitle).withLineSpacing(5)
        
        subTitleCell.attributedText = text
        imageCell.image = UIImage(named: model.image)
        
        if (model.isLast) {
            enterButton.isHidden = false
            potImageCell.isHidden = false
        } else {
            enterButton.isHidden = true
            potImageCell.isHidden = true
        }
        
        self.delegate = delegate
    }
}
