//
//  UIOnBoardPageView.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation
import UIKit
import AdvancedPageControl

class UIOnBoardPageView: BaseView {
    
    private lazy var onBoardCollection: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        
        let view = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        view.delegate = self
        view.dataSource = self
        view.isPagingEnabled = true
        view.backgroundColor = .mainColor
        view.indicatorStyle = .white
        view.showsHorizontalScrollIndicator = false
        view.delaysContentTouches = false
        view.register(OnBoardCell.self, forCellWithReuseIdentifier: "OnBoardCell")
        return view
    }()
    
    private lazy var pageControlView: AdvancedPageControlView = {
        let view = AdvancedPageControlView()
        let drawer = WormDrawer(
            numberOfPages: 4,
            height: 8,
            width: 8,
            space: 7,
            raduis: 4,
            currentItem: 0,
            indicatorColor: .mainTextColor,
            dotsColor: .mainTextColor,
            isBordered: false,
            borderColor: .clear,
            borderWidth: 0,
            indicatorBorderColor: .clear,
            indicatorBorderWidth: 0)
        view.drawer = drawer
        return view
    }()
    
    override func setupSubViews() {
        addSubview(pageControlView)
        pageControlView.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.left.right.equalToSuperview()
        }
        
        addSubview(onBoardCollection)
        onBoardCollection.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalToSuperview()
            make.bottom.equalTo(pageControlView.snp.top)
        }
    }
    
    private var models: [OnBoardModel] = []
    
    func fill(models: [OnBoardModel]) {
        self.models = models
    }
    
    func skip() {
        DispatchQueue.main.async { [self] in
            onBoardCollection.clipsToBounds = true
            onBoardCollection.setContentOffset(CGPoint(x: frame.width * 3, y: 0), animated: true)
        }
    }
    
    private var onClickFinish: () -> Void = { }

    func onClickFinishListener(onClickFinish: @escaping () -> Void) {
        self.onClickFinish = onClickFinish
    }
}

extension UIOnBoardPageView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return models.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OnBoardCell", for: indexPath) as! OnBoardCell
        
        cell.fill(model: models[indexPath.row], delegate: self)
                
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offSet = scrollView.contentOffset.x
        let width = scrollView.frame.width
        let page = Int(round(offSet / width))
        pageControlView.setPage(page)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width, height: frame.height)
    }
}

extension UIOnBoardPageView: OnBoardCellDelegate {
    func clickEnd() {
        onClickFinish()
    }
}
