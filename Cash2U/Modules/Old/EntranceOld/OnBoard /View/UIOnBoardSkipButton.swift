//
//  UIOnBoardSkipButton.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation
import UIKit

class UIOnBoardSkipButton: BaseView{
    
    private lazy var label: UILabel = {
        let view = UILabel()
        view.text = "Пропустить"
        view.textColor = .white
        view.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        return view
    }()
    
    override func setupView() {
        backgroundColor = .clear
        borderWidth = 1
        borderColor = .white
        layer.cornerRadius = 16
    }
    
    override func setupSubViews() {
        addSubview(label)
        label.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.centerX.equalToSuperview()
        }
    }
}
