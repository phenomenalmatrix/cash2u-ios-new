//
//  OnBoardModel.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation

struct OnBoardModel {
    var title: String
    var subTitle: String
    var image: String
    var isLast: Bool
}
