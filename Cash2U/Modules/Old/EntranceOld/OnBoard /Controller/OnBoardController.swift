//
//  OnBoardController.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation
import UIKit
import AdvancedPageControl

class OnBoardController: BaseModelController<OnBoardViewModel>{
    
    private lazy var uiOnBoardPageView = UIOnBoardPageView()
    
    private lazy var skipButton = UIOnBoardSkipButton()
    
    override func setupUI() {
        view.backgroundColor = .init(named: "OnBoardBackround")
        
        skipButton.onClickListener { UIView in
            self.uiOnBoardPageView.skip()
        }
        uiOnBoardPageView.onClickFinishListener {
            self.viewModel.openMain()
        }
    }
    
    override func setupConstraint() {
        view.addSubview(uiOnBoardPageView)
        uiOnBoardPageView.snp.makeConstraints { make in
            make.height.equalTo(700)
            make.bottom.equalToSuperview().offset(-10)
            make.top.equalToSuperview().offset(10)
            make.left.right.equalToSuperview()
        }
        
        view.addSubview(skipButton)
        skipButton.snp.makeConstraints { make in
            make.height.equalTo(30)
            make.width.equalTo(110)
            make.right.equalToSuperview().offset(-20)
            make.top.equalToSuperview().offset(52)
        }
        
        viewModel.showOnBoardState()
    }
}

extension OnBoardController: OnBoardDelegate {
    func fillOnBoard(models: [OnBoardModel]) {
        uiOnBoardPageView.fill(models: models)
    }
}
