//
//  OnBoardViewModel.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation

protocol OnBoardDelegate: AnyObject {
    func fillOnBoard(models: [OnBoardModel])
}

class OnBoardViewModel: BaseViewModel<OnBoardDelegate> {
    
    func openMain(){
        router.showNumber()
    }
    
    func showOnBoardState() {
        var models: [OnBoardModel] = []
        
        models.append(OnBoardModel(title: "Удобный способ оплаты \nпокупок частями",subTitle: "🔥 Без первичного взноса \n🤝 Без скрытых комиссий \n😎 Без процентов \n🚀 Без переплат",image: "OnBoardImage1", isLast: false))
        models.append(OnBoardModel(title: "Оплачивайте \nс помощью cash2u", subTitle:"Не нужно носить с собой \nналичные — оплачивайте покупки \nна кассе с помощь QR-кода",image: "OnBoardImage2", isLast: false))
        models.append(OnBoardModel(title: "Быстрое оформление \nонлайн",subTitle: "Вам не нужно идти в офис для \nоформления, рассмотрение заявки \nзаймет примерно 15 минут",image: "OnBoardImage3", isLast: false))
        models.append(OnBoardModel(title: "Погашение только \nпосле оплаты",subTitle:"Вносить оплату по графику нужно  только после совершения покупки", image: "OnBoardImage4", isLast: true))
        
        delegate?.fillOnBoard(models: models)
    }
}
