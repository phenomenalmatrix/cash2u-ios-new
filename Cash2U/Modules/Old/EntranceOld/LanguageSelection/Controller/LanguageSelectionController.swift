//
//  LanguageSelectionController.swift
//  cash2u
//
//  Created by Eldar Akkozov on 12/4/22.
//

import Foundation
import UIKit
import SnapKit

class LanguageSelectionController: BaseModelController<LanguageSelectionViewModel> {
    
    private lazy var imageLogo: UIImageView = {
        let view = UIImageView(image: UIImage(named: "AppLogo"))
        view.contentMode = .center
        return view
    }()
    
    private lazy var imageMessage: UIImageView = {
        let view = UIImageView(image: UIImage(named: "MessageLogo"))
        view.contentMode = .center
        return view
    }()
    
    private lazy var russian = UIButtonView("Русский")
    private lazy var kurgiz = UIButtonView("Кыргызча")

    override func setupConstraint() {
        view.addSubview(imageLogo)
        imageLogo.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(view.safeArea.top).offset(35)
            make.width.equalTo(100)
            make.height.equalTo(30)
        }
        
        view.addSubview(kurgiz)
        kurgiz.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.bottom.equalTo(view.safeArea.bottom).offset(-32)
        }
        
        view.addSubview(russian)
        russian.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.bottom.equalTo(kurgiz.snp.top).offset(-14)
        }
        
        view.addSubview(imageMessage)
        imageMessage.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.bottom.equalTo(russian.snp.top)
            make.left.equalToSuperview().offset(view.frame.width / 4.45)
            make.right.equalToSuperview().offset((view.frame.width / 4.45) * -1)
        }
    }
    
    override func setupUI() {
        kurgiz.onClickListener { [self] _ in
            viewModel.selectLang()
        }
        
        russian.onClickListener { [self] _ in
            viewModel.selectLang()
        }
    }
}

extension LanguageSelectionController: LanguageSelectionDelegate {
    
}
