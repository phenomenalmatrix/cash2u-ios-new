//
//  LanguageSelectionViewModel.swift
//  cash2u
//
//  Created by Eldar Akkozov on 12/4/22.
//

import Foundation

protocol LanguageSelectionDelegate: AnyObject {
    
}

class LanguageSelectionViewModel: BaseViewModel<LanguageSelectionDelegate> {
    
    func selectLang() {
        UserDefault<Bool>(key: .IS_FIRST_OPEN).wrappedValue = false
        
        router.showOnBoard()
    }
}
