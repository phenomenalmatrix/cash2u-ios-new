//
//  ValidPinController.swift
//  cash2u
//
//  Created by Eldar Akkozov on 18/4/22.
//

import Foundation
import UIKit
import SnapKit

class ValidPinController: BaseModelController<ValidPinViewModel> {
    
    private lazy var imagePinCode = UIImageView(image: UIImage(named: "ValidPin"))
    
    private lazy var titleView = UILabelView("Повторите PIN-код", font: .systemFont(ofSize: 28, weight: .bold))
    private lazy var subTitleView = UILabelView("Защитите свой аккаунт с помощью\nPIN-кода", font: .systemFont(ofSize: 17))
    
    private lazy var otpView: DPOTPView = {
        let view = DPOTPView()
        view.dismissOnLastEntry = true
        view.isCursorHidden = true
        view.count = 4
        view.fontTextField = .systemFont(ofSize: 24, weight: .bold)
        view.placeholder = "0000"
        view.spacing = 8
        view.dpOTPViewDelegate = self
        view.placeholderTextColor = .init(named: "NumberGrayColorText")!
        view.backGroundColorTextField = .init(named: "NumberBackground")!
        view.cornerRadiusTextField = 11
        return view
    }()
    
    private lazy var forgotPinBatton = UILabelView("Забыли PIN-код?", font: .systemFont(ofSize: 15), textColor: .init(hex: "#747789"))
    
    override func setupUI() {
        imagePinCode.contentMode = .center
        forgotPinBatton.textAlignment = .center
        
        forgotPinBatton.onClickListener { view in
            self.viewModel.showRegister()
        }
    }
    
    override func setupConstraint() {
        view.addSubview(forgotPinBatton)
        forgotPinBatton.snp.makeConstraints { make in
            make.height.equalTo(51)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.bottom.equalTo(view.safeArea.bottom).offset(-16)
        }
        
        view.addSubview(subTitleView)
        subTitleView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.bottom.equalToSuperview().offset((view.frame.height / 2.1) * -1)
        }
        
        view.addSubview(titleView)
        titleView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.bottom.equalTo(subTitleView.snp.top).offset(-10)
        }
        
        view.addSubview(imagePinCode)
        imagePinCode.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top).offset(80)
            make.left.equalToSuperview().offset(view.frame.width / 3.29)
            make.right.equalToSuperview().offset((view.frame.width / 3.29) * -1)
            make.bottom.equalToSuperview().offset((view.frame.height / 1.6) * -1)
        }
        
        view.addSubview(otpView)
        otpView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.width.equalTo(222)
            make.height.equalTo(54)
            make.bottom.equalToSuperview().offset((view.frame.height / 2.66) * -1)
        }
    }
    
    override func showKeyBoard(height: CGFloat) {
        imagePinCode.showHidden(hidden: true)
        
        UIView.animate(withDuration: 2.0) { [self] in
            forgotPinBatton.transform = CGAffineTransform(translationX: 0, y: height * -1)
            otpView.transform = CGAffineTransform(translationX: 0, y: (height - 90) * -1)
            
            titleView.transform = CGAffineTransform(translationX: 0, y: (height - 90) * -1)
            subTitleView.transform = CGAffineTransform(translationX: 0, y: (height - 90) * -1)
            
        }
    }
    
    override func hideKeyBoard(height: CGFloat) {
        imagePinCode.showHidden(hidden: false)
        
        UIView.animate(withDuration: 2.0) { [self] in
            forgotPinBatton.transform = CGAffineTransform(translationX: 0, y: 0)
            otpView.transform = CGAffineTransform(translationX: 0, y: 0)
            
            titleView.transform = CGAffineTransform(translationX: 0, y: 0)
            subTitleView.transform = CGAffineTransform(translationX: 0, y: 0)
        }
    }
}

extension ValidPinController: ValidPinDelegate {
    
}

extension ValidPinController: DPOTPViewDelegate {
    
    func returnButtonTapped() {
        
    }
    
    func mainButtonTapped() {
        
    }
    
    func faceIdTapped() {
        
    }
    
    func dpOTPViewAddText(_ text: String, at position: Int) {
        if text.count == 4 {
            DispatchQueue.main.async {
                self.imagePinCode.showHidden(hidden: false)
            }
        }
    }
    
    func dpOTPViewRemoveText(_ text: String, at position: Int) {
        
    }
    
    func dpOTPViewChangePositionAt(_ position: Int) {
        
    }
    
    func dpOTPViewBecomeFirstResponder() {

    }
    
    func dpOTPViewResignFirstResponder() {

    }
}
