//
//  ValidPinViewModel.swift
//  cash2u
//
//  Created by Eldar Akkozov on 18/4/22.
//

import Foundation

protocol ValidPinDelegate: AnyObject {
    
}

class ValidPinViewModel: BaseViewModel<ValidPinDelegate> {
    
    
    func showRegister() {
        router.showNumber()
    }
}
