//
//  SmsController.swift
//  cash2u
//
//  Created by Eldar Akkozov on 5/4/22.
//

import Foundation
import UIKit

class SmsController: BaseModelController<SmsViewModel> {
    
    private lazy var backButton = UIBackButton()
    
    private lazy var imageMessage = UIImageView(image: UIImage(named: "Message"))
    
    private lazy var titleView = UILabelView("Введите смс-код", font: .systemFont(ofSize: 38, weight: .bold))
    private lazy var subTitleView = UILabelView("Мы отправили код сюда:\n+996 700 700 700", font: .systemFont(ofSize: 17, weight: .bold))
    
    private lazy var timerLabel = UILabelView("Повторно отправить код 00:54", font: .systemFont(ofSize: 15, weight: .bold), textColor: .init(named: "NumberGrayColorText")!)
    
    private lazy var shadowView: UIView = {
        let view = UIView()
        view.isHidden = true
        view.backgroundColor = .init(hex: "#000000", alpha: 0.5)
        return view
    }()
    
    private lazy var otpView: DPOTPView = {
        let view = DPOTPView()
        view.dismissOnLastEntry = true
        view.isCursorHidden = true
        view.count = 6
        view.dpOTPViewDelegate = self
        view.spacing = 8
        view.fontTextField = .systemFont(ofSize: 24, weight: .bold)
        view.placeholder = "000000"
        view.placeholderTextColor = .init(named: "NumberGrayColorText")!
        view.backGroundColorTextField = .init(named: "NumberBackground")!
        view.cornerRadiusTextField = 11
        return view
    }()
    
    private lazy var сonfirmBatton = UIButtonView("Подтвердить")
    private lazy var alertSuccet = UISuccestAlert()
    
    private lazy var bottomSheet = UISecurityBottom()
    
    override func setupUI() {
        imageMessage.contentMode = .center
        

        сonfirmBatton.onClickListener { view in
            self.hideKeyboard()
            
            self.shadowView.showHidden(hidden: false)
        }
        
        alertSuccet.nextButton.onClickListener { view in
            self.alertSuccet.showHidden(hidden: true)
            
            self.bottomSheet.show()
        }
        
        backButton.onClickListener { [self] _ in
            self.viewModel.back()
        }
        
        bottomSheet.nextBatton.onClickListener { view in
            self.bottomSheet.dismiss()
            
            self.viewModel.showPin()
            
//            self.shadowView.showHidden(hidden: true) {
//                self.alertSuccet.isHidden = false
//                self.alertSuccet.alpha = 1
//            }
        }
    }
    
    override func setupConstraint() {
        view.addSubview(сonfirmBatton)
        сonfirmBatton.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.bottom.equalTo(view.safeArea.bottom).offset(-16)
        }
        
        view.addSubview(backButton)
        backButton.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top).offset(16)
            make.left.equalToSuperview().offset(20)
            make.width.equalTo(74)
            make.height.equalTo(28)
        }
        
        view.addSubview(subTitleView)
        subTitleView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.bottom.equalToSuperview().offset((view.frame.height / 2.1) * -1)
        }
        
        view.addSubview(titleView)
        titleView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.bottom.equalTo(subTitleView.snp.top).offset(-10)
        }
        
        view.addSubview(imageMessage)
        imageMessage.snp.makeConstraints { make in
            make.top.equalTo(backButton.snp.bottom)
            make.left.equalToSuperview().offset(view.frame.width / 4.5)
            make.right.equalToSuperview().offset((view.frame.width / 4.5) * -1)
            make.bottom.equalToSuperview().offset((view.frame.height / 1.6) * -1)
        }
        
        view.addSubview(otpView)
        otpView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(54)
            make.bottom.equalToSuperview().offset((view.frame.height / 2.66) * -1)
        }
        
        view.addSubview(timerLabel)
        timerLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset((view.frame.height / 3.13) * -1)
        }
        
        view.addSubview(shadowView)
        shadowView.snp.makeConstraints { make in
            make.top.bottom.left.right.equalToSuperview()
        }
        
        shadowView.addSubview(alertSuccet)
        alertSuccet.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
        }
        
//        present(bottomSheet)
    }
    
    override func showKeyBoard(height: CGFloat) {
        imageMessage.showHidden(hidden: true)
        
        UIView.animate(withDuration: 2.0) { [self] in
            сonfirmBatton.transform = CGAffineTransform(translationX: 0, y: height * -1)
            otpView.transform = CGAffineTransform(translationX: 0, y: (height - 90) * -1)
            
            timerLabel.transform = CGAffineTransform(translationX: 0, y: (height - 100) * -1)
            
            titleView.transform = CGAffineTransform(translationX: 0, y: (height - 90) * -1)
            subTitleView.transform = CGAffineTransform(translationX: 0, y: (height - 90) * -1)

        }
    }
    
    override func hideKeyBoard(height: CGFloat) {
        imageMessage.showHidden(hidden: false)

        UIView.animate(withDuration: 2.0) { [self] in
            сonfirmBatton.transform = CGAffineTransform(translationX: 0, y: 0)
            otpView.transform = CGAffineTransform(translationX: 0, y: 0)

            timerLabel.transform = CGAffineTransform(translationX: 0, y: 0)
            
            titleView.transform = CGAffineTransform(translationX: 0, y: 0)
            subTitleView.transform = CGAffineTransform(translationX: 0, y: 0)
        }
    }
}

extension SmsController: SmsDelegate {
    func showProgress() {
        
    }
    
    func hideProgress() {
        
    }
    
    func showAlert(_ title: String, _ message: String) {
        
    }
}

extension SmsController: DPOTPViewDelegate {
    
    func returnButtonTapped() {
        
    }
    
    func mainButtonTapped() {
        
    }
    
    func faceIdTapped() {
        
    }
    
    func dpOTPViewAddText(_ text: String, at position: Int) {
        if text.count == 6 {
            DispatchQueue.main.async { [self] in
                imageMessage.showHidden(hidden: false)
            }
        }
    }
    
    func dpOTPViewRemoveText(_ text: String, at position: Int) {
        
    }
    
    func dpOTPViewChangePositionAt(_ position: Int) {
        
    }
    
    func dpOTPViewBecomeFirstResponder() {
        
    }
    
    func dpOTPViewResignFirstResponder() {
        
    }
    
    
}
