//
//  SmsViewModel.swift
//  cash2u
//
//  Created by Eldar Akkozov on 5/4/22.
//

import Foundation

protocol SmsDelegate: BaseDelegate {
    
}

class SmsViewModel: BaseViewModel<SmsDelegate> {
    
    private var number: String
    
    required init(number: String, view: SmsDelegate, router: RouterProtocol) {
        self.number = number
        
        super.init(view: view, router: router)
    }
    
    func showPin() {
        router.showPinCode()
    }
    
    required init(view: SmsDelegate, router: RouterProtocol) {
        fatalError("init(view:router:) has not been implemented")
    }
}

