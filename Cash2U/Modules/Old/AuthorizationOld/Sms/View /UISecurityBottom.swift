//
//  UISecurityBottom.swift
//  cash2u
//
//  Created by Eldar Akkozov on 6/4/22.
//

import Foundation
import UIKit

class UISecurityBottom: UIBottomSheet {
    
    private lazy var title = UILabelView("Защитите свой аккаунт с помощью биометрики или PIN-кода", font: .systemFont(ofSize: 20, weight: .bold))
    
    private lazy var swihtView = UIView()
    
    lazy var nextBatton = UIButtonView("Пропустить")

    override func setupConstrants() {
        shadowView.backgroundColor = nil
        shadowView.onClickListener { _ in }
        
        viewContaner.addSubview(title)
        title.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(46)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
        }
        
        viewContaner.addSubview(swihtView)
        swihtView.snp.makeConstraints { make in
            make.top.equalTo(title.snp.bottom).offset(28)
            make.left.right.equalToSuperview()
            make.height.equalTo(44)
        }
        
        viewContaner.addSubview(nextBatton)
        nextBatton.snp.makeConstraints { make in
            make.height.equalTo(56)
            make.top.equalTo(swihtView.snp.bottom).offset(40)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.bottom.equalTo(view.safeArea.bottom).offset(-16)
        }
    }
}
