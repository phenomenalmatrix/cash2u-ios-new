//
//  UISavePinAlert.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation
import UIKit

class UISavePinAlert: BaseView {
    
    private lazy var imageAlert = UIImageView(image: UIImage(named: "AlertSucces"))
    private lazy var title = UILabelView("PIN-код сохранен", font: .systemFont(ofSize: 28, weight: .bold))
    
    lazy var nextButton = UIButtonView("Продолжить")
    
    override func setupView() {
        title.textAlignment = .center
        
        backgroundColor = .init(named: "BackgroundWindowColor")
        layer.cornerRadius = 26
    }
    
    override func setupSubViews() {
        addSubview(imageAlert)
        imageAlert.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(40)
            make.width.equalTo(83)
            make.height.equalTo(59)
        }
        
        addSubview(title)
        title.snp.makeConstraints { make in
            make.top.equalTo(imageAlert.snp.bottom).offset(32)
            make.left.equalToSuperview().offset(18)
            make.right.equalToSuperview().offset(-18)
        }
        
        addSubview(nextButton)
        nextButton.snp.makeConstraints { make in
            make.height.equalTo(56)
            make.left.equalToSuperview().offset(18)
            make.right.equalToSuperview().offset(-18)
            make.top.equalTo(title.snp.bottom).offset(32)
            make.bottom.equalToSuperview().offset(-18)
        }
    }
}
