//
//  UIReferalAlert.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation
import UIKit

class UIReferalAlert: BaseView {
    
    private lazy var imageAlert = UIImageView(image: UIImage(named: "ReferalImage"))
    private lazy var title = UILabelView("Есть ли у вас реферальный код?", font: .systemFont(ofSize: 28, weight: .bold))
    
    lazy var yesButton = UIButtonView("Да")
    lazy var noButton = UIButtonView("Нет")

    override func setupView() {
        title.textAlignment = .left
        imageAlert.contentMode = .center
        
        backgroundColor = .init(named: "BackgroundWindowColor")
        layer.cornerRadius = 26
    }
    
    override func setupSubViews() {
        addSubview(title)
        title.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(24)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
        }
        
        addSubview(imageAlert)
        imageAlert.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(title.snp.bottom).offset(30)
            make.left.right.equalToSuperview()
            make.height.equalTo(128)
        }
        
        addSubview(noButton)
        noButton.snp.makeConstraints { make in
            make.height.equalTo(56)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.top.equalTo(imageAlert.snp.bottom).offset(24)
        }
        
        addSubview(yesButton)
        yesButton.snp.makeConstraints { make in
            make.height.equalTo(56)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.top.equalTo(noButton.snp.bottom).offset(10)
            make.bottom.equalToSuperview().offset(-18)
        }
    }
}
