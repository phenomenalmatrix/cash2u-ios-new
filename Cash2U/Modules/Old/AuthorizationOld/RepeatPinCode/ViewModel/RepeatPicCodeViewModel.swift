//
//  RepeatPicCodeViewModel.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation

protocol RepeatPicCodeDelegate: AnyObject {
    
}

class RepeatPicCodeViewModel: BaseViewModel<RepeatPicCodeDelegate> {
    
    func showReferalEdit() {
        router.showReferalEditCode()
    }
    
    func showRegistered() {
        router.showHomeRegistered()
    }
}
