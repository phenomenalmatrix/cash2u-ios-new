//
//  AuthorizationController.swift
//  cash2u
//
//  Created by Eldar Akkozov on 4/4/22.
//

import Foundation
import UIKit

class NumberController: BaseModelController<NumberViewModel> {
    
    private lazy var nextBatton = UIButtonView("Далее")
    private lazy var numberEdit = UINumberField()
    private lazy var termsOfUseView = UITermsOfUseView()
    
    private lazy var titleView = UILabelView("Вход", font: .systemFont(ofSize: 38, weight: .bold))
    private lazy var subTitleView = UILabelView("Войдите в приложение с помощью своего номера телефона:", font: .systemFont(ofSize: 17))

    private lazy var skipTitleView = UILabelView("Пропустить", font: .systemFont(ofSize: 15))

    private lazy var termsOfUseBottom = UITermsOfUseBottom()
    
    private lazy var cristalView = UIImageView(image: UIImage(named: "Crystal"))
    
    override func setupConstraint() {
        view.addSubview(nextBatton)
        nextBatton.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.bottom.equalTo(view.safeArea.bottom).offset(-16)
        }
        
        view.addSubview(numberEdit)
        numberEdit.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.bottom.equalToSuperview().offset((view.frame.height / 2.6) * -1)
        }
        
        view.addSubview(termsOfUseView)
        termsOfUseView.snp.makeConstraints { make in
            make.left.right.equalToSuperview()
            make.bottom.equalToSuperview().offset((view.frame.height / 3.9) * -1)
        }
        
        view.addSubview(subTitleView)
        subTitleView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.bottom.equalToSuperview().offset((view.frame.height / 2) * -1)
        }
        
        view.addSubview(titleView)
        titleView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.bottom.equalTo(subTitleView.snp.top).offset(-4)
        }
        
        view.addSubview(skipTitleView)
        skipTitleView.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top).offset(16)
            make.right.equalToSuperview().offset(-20)
        }
        
        view.addSubview(cristalView)
        cristalView.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top)
            make.bottom.equalToSuperview().offset((view.frame.height / 2) * -1)
            make.left.equalToSuperview().offset(view.frame.width / 3.7)
            make.right.equalToSuperview().offset((view.frame.width / 3.7) * -1)
        }
        
//        present(termsOfUseBottom)
    }
    
    override func setupUI() {
        cristalView.contentMode = .center
        
        nextBatton.onClickListener { view in
            self.hideKeyboard()
            
            self.viewModel.sendNumber("test", false)
        }
        
        skipTitleView.onClickListener { view in
            self.viewModel.skipAuthorization()
        }
        
        termsOfUseView.descriptionSubTitle.onClickListener { view in
            self.hideKeyboard()
            
            self.termsOfUseBottom.dismiss()
        }
    }
    
    override func showKeyBoard(height: CGFloat) {
        cristalView.showHidden(hidden: true)
        
        UIView.animate(withDuration: 2.0) { [self] in
            nextBatton.transform = CGAffineTransform(translationX: 0, y: height * -1)
            numberEdit.transform = CGAffineTransform(translationX: 0, y: (height - 90) * -1)
            
            termsOfUseView.transform = CGAffineTransform(translationX: 0, y: (height - 70) * -1)
            
            titleView.transform = CGAffineTransform(translationX: 0, y: (height - 90) * -1)
            subTitleView.transform = CGAffineTransform(translationX: 0, y: (height - 90) * -1)

        }
    }
    
    override func hideKeyBoard(height: CGFloat) {
        cristalView.showHidden(hidden: false)

        UIView.animate(withDuration: 2.0) { [self] in
            nextBatton.transform = CGAffineTransform(translationX: 0, y: 0)
            numberEdit.transform = CGAffineTransform(translationX: 0, y: 0)

            termsOfUseView.transform = CGAffineTransform(translationX: 0, y: 0)
            
            titleView.transform = CGAffineTransform(translationX: 0, y: 0)
            subTitleView.transform = CGAffineTransform(translationX: 0, y: 0)
        }
    }
}

extension NumberController: NumberDelegate {
    func showError(message: String) {
        
    }
    
    func showProgress() {
        
    }
    
    func hideProgress() {
        
    }
    
    func showAlert(_ title: String, _ message: String) {
        
    }
}
