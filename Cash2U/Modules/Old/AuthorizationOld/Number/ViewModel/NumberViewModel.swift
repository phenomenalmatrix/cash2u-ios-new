//
//  AuthorizationViewModel.swift
//  cash2u
//
//  Created by Eldar Akkozov on 4/4/22.
//

import Foundation

protocol NumberDelegate: BaseDelegate {
    func showError(message: String)
}

class NumberViewModel: BaseViewModel<NumberDelegate> {
    
    func sendNumber(_ number: String, _ termsOfUse: Bool) {
        router.showSMS(number: number)
    }
    
    func skipAuthorization() {
        router.showHomeGuest()
    }
    
}
