//
//  UITermsOfUse View.swift
//  cash2u
//
//  Created by Eldar Akkozov on 4/4/22.
//

import Foundation

class UITermsOfUseView: BaseView {
    
    private lazy var descriptionTitle = UILabelView("Нажимая «Далее», вы принимаете", font: .systemFont(ofSize: 15), textColor: .mainTextColor)
    lazy var descriptionSubTitle = UILabelView("пользовательское соглашение", font: .systemFont(ofSize: 15), textColor: .grayColor)
    
    override func setupView() {
        backgroundColor = .clear
    }
    
    override func setupSubViews() {
        
        addSubview(descriptionTitle)
        descriptionTitle.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(9)
            make.left.equalToSuperview()
            make.right.equalToSuperview()

        }
        
        addSubview(descriptionSubTitle)
        descriptionSubTitle.snp.makeConstraints { make in
            make.top.equalTo(descriptionTitle.snp.bottom).offset(-1)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview().offset(-9)
        }
    }
}
