//
//  UINumberEdit.swift
//  cash2u
//
//  Created by Eldar Akkozov on 4/4/22.
//

import Foundation
import UIKit

class UINumberField: BaseView {
    
    private lazy var numberMaskLabel = UILabelView("+996", font: .systemFont(ofSize: 24, weight: .bold), textColor: .grayColor)
    
    lazy var numberField: UITextField = {
        let view = UITextField()
        view.attributedPlaceholder = NSAttributedString(
            string: "000 00 00 00",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.grayColor ?? .darkText]
        )
        view.delegate = self
        view.keyboardType = .numberPad
        view.font = .systemFont(ofSize: 24, weight: .bold)
        return view
    }()
    
    private lazy var separator: UIView = {
        let view = UIView()
        view.backgroundColor = .grayColor
        return view
    }()
    
    override func setupView() {
        backgroundColor = .inputColor
        
        layer.cornerRadius = 12
    }
    
    private lazy var contener = UIView()
    
    override func setupSubViews() {
        
        addSubview(contener)
        contener.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.bottom.equalToSuperview()
        }
        
        contener.addSubview(numberMaskLabel)
        numberMaskLabel.snp.makeConstraints { make in
            make.left.equalToSuperview()
            make.top.equalToSuperview().offset(15)
            make.bottom.equalToSuperview().offset(-15)
        }
        
        contener.addSubview(separator)
        separator.snp.makeConstraints { make in
            make.left.equalTo(numberMaskLabel.snp.right).offset(8)
            make.width.equalTo(1)
            make.top.equalToSuperview().offset(21)
            make.bottom.equalToSuperview().offset(-21)
        }
        
        contener.addSubview(numberField)
        numberField.snp.makeConstraints { make in
            make.left.equalTo(separator.snp.right).offset(8)
            make.top.equalToSuperview().offset(15)
            make.bottom.equalToSuperview().offset(-15)
            make.right.equalToSuperview()
        }
    }
}

extension UINumberField: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return false }
        let newString = (text as NSString).replacingCharacters(in: range, with: string)
        textField.text = format(with: "XXX XX XX XX", phone: newString)
                
        return false
    }
    
    func format(with mask: String, phone: String) -> String {
        let numbers = phone.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
        var result = ""
        var index = numbers.startIndex

        for ch in mask where index < numbers.endIndex {
            if ch == "X" {
                result.append(numbers[index])
                index = numbers.index(after: index)

            } else {
                result.append(ch)
            }
        }
        return result
    }
}
