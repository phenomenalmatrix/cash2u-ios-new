//
//  PinCodeViewModel.swift
//  cash2u
//
//  Created by Eldar Akkozov on 6/4/22.
//

import Foundation

protocol PinCodeDelegate: AnyObject {
    
}

class PinCodeViewModel: BaseViewModel<PinCodeDelegate> {
    
    func showRepeatPinCode() {
        router.showRepeatPinCode()
    }
}
