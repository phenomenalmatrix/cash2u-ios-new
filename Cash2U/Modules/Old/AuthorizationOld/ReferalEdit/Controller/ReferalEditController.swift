//
//  ReferalEditController.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation
import UIKit

class ReferalEditController: BaseModelController<ReferalEditViewModel> {
    
    private lazy var skipTitleView = UILabelView("Пропустить", font: .systemFont(ofSize: 15))
    private lazy var titleView = UILabelView("Реферальный\nкод друга", font: .systemFont(ofSize: 38, weight: .bold))

    private lazy var otpView: DPOTPView = {
        let view = DPOTPView()
        view.dismissOnLastEntry = true
        view.isCursorHidden = true
        view.count = 6
        view.spacing = 8
        view.fontTextField = .systemFont(ofSize: 24, weight: .bold)
        view.placeholder = "_"
        view.placeholderTextColor = .init(named: "BasicColorText")!
        view.backGroundColorTextField = .init(named: "NumberBackground")!
        view.cornerRadiusTextField = 11
        return view
    }()
    
    private lazy var сonfirmBatton = UIButtonView("Подтвердить")
    
    override func setupConstraint() {
        view.addSubview(skipTitleView)
        skipTitleView.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top).offset(16)
            make.right.equalToSuperview().offset(-20)
        }
        
        view.addSubview(titleView)
        titleView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.bottom.equalTo(view.safeArea.bottom).offset((view.frame.height / 1.4) * -1)
        }
        
        view.addSubview(otpView)
        otpView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(54)
            make.bottom.equalToSuperview().offset((view.frame.height / 1.6) * -1)
        }
        
        view.addSubview(сonfirmBatton)
        сonfirmBatton.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.bottom.equalTo(view.safeArea.bottom).offset(-16)
        }
    }
    
    override func showKeyBoard(height: CGFloat) {        
        UIView.animate(withDuration: 2.0) { [self] in
            сonfirmBatton.transform = CGAffineTransform(translationX: 0, y: height * -1)
        }
    }
    
    override func hideKeyBoard(height: CGFloat) {
        UIView.animate(withDuration: 2.0) { [self] in
            сonfirmBatton.transform = CGAffineTransform(translationX: 0, y: 0)
        }
    }
    
    override func setupUI() {
        skipTitleView.onClickListener { [self] _ in
            viewModel.showRegistered()
        }
        
        titleView.textAlignment = .left
    }
}

extension ReferalEditController: ReferalEditDelegate {
    func showProgress() {
        
    }
    
    func hideProgress() {
        
    }
    
    func showAlert(_ title: String, _ message: String) {
        
    }
}
