//
//  ReferalEditViewModel.swift
//  cash2u
//
//  Created by Eldar Akkozov on 11/4/22.
//

import Foundation

protocol ReferalEditDelegate: BaseDelegate {
    
}

class ReferalEditViewModel: BaseViewModel<ReferalEditDelegate> {
    
    func showRegistered() {
        router.showHomeRegistered()
    }

}
