//
//  PartnerPageProtocols.swift
//  Cash2U
//
//  Created by talgar osmonov on 7/6/22.
//  
//

import Foundation

protocol PartnerPageViewProtocol: AnyObject {
    func reciveReviews(reviews: PartnerFeedbackModel)
    func reciveError(error: Error)
}

protocol PartnerPageViewToPresenterProtocol: AnyObject {
    func getReviews()
}

protocol PartnerPageInteractorProtocol: AnyObject {
    func getReviews(pdrtnerId: Int)
}

protocol PartnerPageInteractorToPresenterProtocol: AnyObject {
    func reciveReviewSuccess(reviews: PartnerFeedbackModel)
    func reciveError(error: Error)
}

protocol PartnerPageRouterProtocol: AnyObject {
    
}
