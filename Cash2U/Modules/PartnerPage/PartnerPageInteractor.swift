//
//  PartnerPageInteractor.swift
//  Cash2U
//
//  Created by talgar osmonov on 7/6/22.
//  
//

import Foundation

final class PartnerPageInteractor {
    weak var presenter: PartnerPageInteractorToPresenterProtocol?
    private let partnerService = PartnerService.shared
}

extension PartnerPageInteractor: PartnerPageInteractorProtocol {
    func getReviews(pdrtnerId: Int) {
        partnerService.getFeedbackForPartner(partnerId: pdrtnerId) { result in
            switch result {
            case .success(let success):
                if let data = success?.data {
                    let reviews: PartnerFeedbackModel = DecodeHelper.decodeDataToObject(data: data)!
                    self.presenter?.reciveReviewSuccess(reviews: reviews)
                }
            case .failure(let failure):
                self.presenter?.reciveError(error: failure)
            }
        }
    }
}
