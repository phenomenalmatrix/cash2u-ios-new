//
//  LeaveFeedbackViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 2/9/22.
//  
//

import UIKit
import Cosmos

final class LeaveFeedbackViewController: CUViewController {
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private var panModalHeight: CGFloat = 510 {
        didSet {
            panModalSetNeedsLayoutUpdate()
            panModalTransition(to: .shortForm)
        }
    }
    
    private lazy var navTitle: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Оставить отзыв", size: 28, numberOfLines: 1, fontType: .bold, isSizeToFit: true)
        return view
    }()
    
    private lazy var backView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .thirdaryColor
        view.cornerRadius = 18
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        return view
    }()
    
    private lazy var textView: UITextView = {
        let view = UITextView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .mainDarkColor
        view.cornerRadius = 18
        view.textContainerInset = .init(top: 20, left: 10, bottom: 20, right: 10)
        view.font = .systemFont(ofSize: 18)
        view.text = "Оставьте отзыв"
        view.textColor = .grayColor
        view.delegate = self
        return view
    }()
    
    private lazy var rateTitle: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Поставьте свою оценку", size: 18, numberOfLines: 1, fontType: .medium, titleColor: .grayColor)
        return view
    }()
    
    private lazy var raitingView: CosmosView = {
        let view = CosmosView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.settings.starSize = 40
        view.settings.starMargin = 10
        view.settings.emptyImage = UIImage(named: "empty_star")
//        cosmosView.settings.filledImage = UIImage(named: "GoldStarFilled")
        return view
    }()
    
    private lazy var sendButton: CUMainButton = {
        let view = CUMainButton()
        view.setup(title: "Отправить")
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(onSendButtonTapped), for: .touchUpInside)
        return view
    }()
    
    override func loadView() {
        super.loadView()
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    //MARK: - UIActions
    
    @objc private func onSendButtonTapped() {
        SwiftEntryKitService.shared.sendFeedbackSuccess {
            self.dismiss(animated: true) {
                SwiftEntryKitService.shared.dissmiss()
            }
        }
    }
}

extension LeaveFeedbackViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        panModalHeight = 680
        if textView.textColor == .grayColor {
            textView.text = nil
            textView.textColor = .mainTextColor
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        panModalHeight = 510
        if textView.text.isEmpty {
            textView.text = "Оставьте отзыв"
            textView.textColor = .grayColor
        }
    }
}

extension LeaveFeedbackViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        
        view.addSubview(backView)
        view.addSubview(navTitle)
        view.addSubview(textView)
        view.addSubview(rateTitle)
        view.addSubview(raitingView)
        view.addSubview(sendButton)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        backView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(82)
            make.leading.trailing.bottom.equalToSuperview()
        }
        
        navTitle.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(32)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        textView.snp.makeConstraints { make in
            make.top.equalTo(backView.snp.top).offset(20)
            make.leading.trailing.equalToSuperview().inset(20)
            make.height.equalTo(200)
        }
        
        rateTitle.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.top.equalTo(textView.snp.bottom).offset(20)
        }
        
        raitingView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.top.equalTo(rateTitle.snp.bottom).offset(10)
        }
        
        sendButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.top.equalTo(raitingView.snp.bottom).offset(35)
            make.height.equalTo(ScreenHelper.mainButtonHeight)
        }
        
    }
}

// MARK: - PanModalPresentable

extension LeaveFeedbackViewController: PanModalPresentable {
    func panModalDidDismiss() {
        didDismissDelegate?.didDismiss()
    }
    var panScrollable: UIScrollView? {
        return nil
    }
    
    var longFormHeight: PanModalHeight {
        .contentHeight(panModalHeight)
    }
    
    var shortFormHeight: PanModalHeight {
        .contentHeight(panModalHeight)
    }
}
