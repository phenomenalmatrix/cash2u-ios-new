//
//  PartnerPageModuleBuilder.swift
//  Cash2U
//
//  Created by talgar osmonov on 7/6/22.
//  
//

import UIKit

final class PartnerPageModuleBuilder {
    /// Собрать модуль
    class func build(partnerId: Int?) -> UIViewController {
        let view = PartnerPageViewController()
        let presenter = PartnerPagePresenter()
        presenter.partnerId = partnerId
        let interactor = PartnerPageInteractor()
        let router = PartnerPageRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
