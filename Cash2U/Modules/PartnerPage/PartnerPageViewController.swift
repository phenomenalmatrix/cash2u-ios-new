//
//  PartnerPageViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 7/6/22.
//  
//

import UIKit
import IGListKit

final class PartnerPageViewController: CUViewController {
    
    var presenter: PartnerPageViewToPresenterProtocol?
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private lazy var adapter: ListAdapter = {
        let adapter = ListAdapter(updater: ListAdapterUpdater(), viewController: self)
        adapter.collectionView = mainCollection
        adapter.dataSource = self
        return adapter
    }()
    
    private var reviewModel = ReviewsModelIG(name: "Всего отзывов:", reviews: ["Хороший магазин, но цены пздц какие космические, в целом все понравилось", "Классный магазин! Купила мужу новые лыжи – сразу укатил в Каракол к свекрови! Обожаю вас!", "Обули по полной! красавчики!", "Классный магазин! Купила мужу новые лыжи – сразу укатил в Каракол к свекрови! Обожаю вас!", "Хороший магазин, но цены пздц какие космические, в целом все понравилось"], items: [])
    
    private func createItemsData(type: CUPartnerPageButtonType) -> [ListDiffable] {
        var aa = [
        ] as [ListDiffable]
        aa.append(PartnerPageTopModelIG(name: "Top"))
        aa.append(PartnerPageButtonModelIG(name: "Buttons"))
        aa.append(InstallmentAvailableModelIG(name: "Available"))
        switch type {
        case .info:
            aa.append(DescriptionModelIG(name: "Desc", description: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, `making it over 2000 years old. \n\nRichard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of de Finibus Bonorum et Malorum (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, Lorem ipsum dolor sit amet.., comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from de Finibus Bonorum et Malorum by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.\n\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from de Finibus Bonorum et Malorum by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham."))
            aa.append(GalleryModelIG(name: "Галерея"))
            aa.append(ButtonSectionModelIG(title: "Поделиться", action: "info_share"))
        case .goods:
            aa.append(GoodsModelIG(name: "Goods", goods: ["Спортивная одежда", "Спортивная обувь", "Спортивные аксессуары", "Аксессуары", "Спортивная одежда", "Спортивная обувь", "Спортивное питание", "Спортивные аксессуары", "Аксессуары", "Спортивная одежда", "Спортивная обувь", "Спортивные аксессуары", "Аксессуары", "Спортивная одежда", "Спортивная обувь", "Спортивное питание", "Спортивные аксессуары", "Аксессуары", "Спортивная одежда", "Спортивная обувь"]))
        case .reviews:
            aa.append(self.reviewModel)
            aa.append(ButtonSectionModelIG(title: "Оставить отзыв", action: "feedback"))
        case .contacts:
            aa.append(ContactsModelIG(name: "asdf", title: "ТЦ Дордой плаза", description: "ул. Ибраимова 190", schedule: "ASD", phoneNumbers: ["+996 708 567 545", "+996 708 567 545"], socialContacts: ["topsport_discort", "topsport_behance", "topsport.kg", "topsport_dribble", "topsport_tt", "topsport_fb"]))
            aa.append(ContactsModelIG(name: "asdf", title: "ТЦ Дордой плаза", description: "ул. Ибраимова 190", schedule: "ASD", phoneNumbers: ["+996 708 567 545"], socialContacts: ["topsport.kg", "topsport_fb", "topsport_discort"]))
            aa.append(ContactsModelIG(name: "asdf", title: "ТЦ Дордой плаза", description: "ул. Ибраимова 190", schedule: "ASD", phoneNumbers: ["+996 708 567 545", "+996 708 567 545"], socialContacts: ["topsport_discort"]))
            aa.append(ContactsModelIG(name: "asdf", title: "ТЦ Дордой плаза", description: "ул. Ибраимова 190", schedule: "ASD", phoneNumbers: ["+996 708 567 545"], socialContacts: ["topsport_discort", "topsport_fb"]))
            aa.append(ButtonSectionModelIG(title: "Поделиться", action: "contact_share"))
        default:
            break
        }
        
        return aa
    }
    
    private lazy var itemsData: [ListDiffable] = createItemsData(type: .info)
    
    private lazy var mainCollection: CUCollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        let mainCollection = CUCollectionView(
            frame: CGRect.zero,
            collectionViewLayout: flowLayout 
        )
        mainCollection.translatesAutoresizingMaskIntoConstraints = false
        mainCollection.alwaysBounceVertical = true
        mainCollection.contentInset = .init(top: -25, left: 0, bottom: 0, right: 0)
        return mainCollection
    }()
    
    override func loadView() {
        super.loadView()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        navigationItem.rightBarButtonItem = .init(image: UIImage(named: "favorite"), style: .plain, target: self, action: #selector(onFavoriteTapped))
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adapter.performUpdates(animated: true, completion: nil)
    }
    
    // MARK: - UIActions
    
    @objc private func onFavoriteTapped() {
        print("Favorite")
    }
    
    private func pageButtonChanged(type: CUPartnerPageButtonType){
        switch type {
        case .info:
            break
        case .goods:
            break
        case .reviews:
            presenter?.getReviews()
            break
        case .contacts:
            break
        case .purchases:
            break
        case .repayment:
            break
        case .history:
            break
        }
    }
}

extension PartnerPageViewController: PartnerPageButtonSectionControllerDelegate {
    func didSelect(type: CUPartnerPageButtonType) {
        itemsData = createItemsData(type: type)
        self.pageButtonChanged(type: type)
        adapter.performUpdates(animated: true)
    }
}

extension PartnerPageViewController: ButtonSectionControllerDelegate {
    func onButtonTapped(action: String) {
        switch action {
        case "info_share":
            print("info")
        case "feedback":
            let vc = LeaveFeedbackViewController()
            self.navigationController?.presentPanModal(vc)
            presenter?.getReviews()
        case "contact_share":
            print("contact")
        default:
            break
        }
    }
}

extension PartnerPageViewController: MallsSectionControllerDelegate {
    func didSelectMall() {
        let vc = MallsDetailModuleConfigurator.build()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension PartnerPageViewController: CategorySectionControllerDelegate {
    func didSelectCategory() {
        let vc = CategoryDetailModuleConfigurator.build()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}



// MARK: - ListAdapterDataSource

extension PartnerPageViewController: ListAdapterDataSource {
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        itemsData
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        if object is CurrencyModelIG {
            let section = CurrencySectionController()
            return section
        } else if object is ContactsModelIG {
            let section = ContactsSectionController()
//            section.delegate = self
            return section
        } else if object is GoodsModelIG {
            let section = GoodsSectionController()
//            section.delegate = self
            return section
        } else if object is ReviewsModelIG {
            let section = ReviewsSectionController()
//            section.delegate = self
            return section
        } else if object is ButtonSectionModelIG {
            let section = ButtonSectionController()
            section.delegate = self
            return section
        } else if object is GalleryModelIG {
            let section = GallerySectionController()
//            section.delegate = self
            return section
        } else if object is DescriptionModelIG {
            let section = DescriptionSectionController()
//            section.delegate = self
            return section
        } else if object is InstallmentAvailableModelIG {
            let section = InstallmentAvailableSectionController()
//            section.delegate = self
            return section
        } else if object is PartnerPageButtonModelIG {
            let section = PartnerPageButtonSectionController()
            section.delegate = self
            return section
        } else if object is PartnerPageTopModelIG {
            let section = PartnerPageTopSectionController()
//            section.delegate = self
            return section
        } else if object is CategoryTopSectionModelIG {
            let section = CategoryTopSectionController()
//            section.delegate = self
            return section
        } else if object is CategorySelectionModelIG {
            let section = CategorySelectionSectionController()
//            section.delegate = self
            return section
        } else if object is PartnersModelIG {
            let section = PartnersSectionController()
//            section.delegate = self
            return section
        } else if object is BestPartnersModelIG {
            let section = RecomendationSectionController()
//            section.delegate = self
            return section
        } else if object is TagsModelIG {
            let section = HorizontalTagsSectionController()
//            section.delegate = self
            return section
        } else if object is LocationModelIG {
            let section = LocationSectionController()
//            section.delegate = self
            return section
        } else if object is SearchModelIG {
            let section = SearchSectionController()
//            section.delegate = self
            return section
        } else if object is NewPartnersModelIG {
            let section = NewPartnersSectionController()
//            section.delegate = self
            return section
        } else if object is MallsModelIG {
            let section = MallsSectionController(isHeaderButtonHidden: false)
            section.delegate = self
            return section
        } else if object is DiscountModelIG {
            let section = DiscountSectionController()
//            section.delegate = self
            return section
        } else if object is SupportModelIG {
            return SupportSectionController()
        } else if object is VersionModelIG {
            return VersionSectionController()
        } else if object is GreetingsSectionModelIG {
            let section = GreetingsSectionController()
//            section.delegate = self
            return section
        } else if object is BazaarModelIG {
            let section = BazaarsSectionController(isHeaderButtonHidden: false)
//            section.delegate = self
            return section
        } else if object is CategoryModelIG {
            let section = CategorySectionController()
            section.delegate = self
            return section
        } else {
            return ListSectionController()
        }
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }
}

extension PartnerPageViewController: PartnerPageViewProtocol {
    
    func reciveReviews(reviews: PartnerFeedbackModel) {
        guard let index = self.itemsData.firstIndex(where: {$0.diffIdentifier().description == "Reviews"}) else {return}
        self.reviewModel = ReviewsModelIG(name: "Всего отзывов:", reviews: ["Хороший магазин, но цены пздц какие космические, в целом все понравилось", "Классный магазин! Купила мужу новые лыжи – сразу укатил в Каракол к свекрови! Обожаю вас!", "Обули по полной! красавчики!", "Классный магазин! Купила мужу новые лыжи – сразу укатил в Каракол к свекрови! Обожаю вас!", "Хороший магазин, но цены пздц какие космические, в целом все понравилось"], items: reviews.mapDtoListToUI())
        self.itemsData[index] = self.reviewModel
        self.adapter.performUpdates(animated: true)
    }
    
    func reciveError(error: Error) {
        print(error.localizedDescription)
    }
    
}

extension PartnerPageViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        view.addSubview(mainCollection)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        mainCollection.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
