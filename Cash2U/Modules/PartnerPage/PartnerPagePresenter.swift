//
//  PartnerPagePresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 7/6/22.
//  
//

import Foundation

final class PartnerPagePresenter {
    
    weak var view: PartnerPageViewProtocol?
    var interactor: PartnerPageInteractorProtocol?
    var router: PartnerPageRouterProtocol?
    var partnerId: Int!
}

extension PartnerPagePresenter: PartnerPageViewToPresenterProtocol {
    func getReviews() {
        if let partnerId {
            self.interactor?.getReviews(pdrtnerId: partnerId)
        }
    }
}

extension PartnerPagePresenter: PartnerPageInteractorToPresenterProtocol {
    func reciveReviewSuccess(reviews: PartnerFeedbackModel) {
        view?.reciveReviews(reviews: reviews)
    }
    
    func reciveError(error: Error) {
        view?.reciveError(error: error)
    }
    
}
