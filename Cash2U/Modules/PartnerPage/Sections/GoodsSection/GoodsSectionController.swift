//
//  GoodsSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 31/8/22.
//

import UIKit
import IGListKit

class GoodsSectionController: ListSectionController, ListSupplementaryViewSource {
    
    weak var delegate: SectionControllerHeaderDelegate? = nil
    
    private var item: GoodsModelIG?
    
    private var expanded = false

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 20, left: 0, bottom: 32, right: 0)
        supplementaryViewSource = self
    }
    
    override func numberOfItems() -> Int {
        return item?.goods.count ?? 0
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width) - 40, height: 50)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUGoodsView = collectionContext!.dequeueReusableCell(of: CUGoodsView.self, for: self, at: index) as? CUGoodsView else {
            return UICollectionViewCell()
        }
        
        if let item = item {
            cell.setup(text: item.goods[index])
        }
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? GoodsModelIG
    }
    
}

// MARK: - SuplementaryViews

extension GoodsSectionController {
    func supportedElementKinds() -> [String] {
        return [UICollectionView.elementKindSectionHeader]
    }

    func viewForSupplementaryElement(ofKind elementKind: String, at index: Int) -> UICollectionReusableView {
        switch elementKind {
        case UICollectionView.elementKindSectionHeader:
            return userHeaderView(atIndex: index)
        default:
            return UICollectionReusableView()
        }
    }
    
    func sizeForSupplementaryView(ofKind elementKind: String, at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: context.containerSize.width - 40, height: 150)
    }
    
    // private funcs
    
    private func userHeaderView(atIndex index: Int) -> UICollectionReusableView {
        guard let headerView = collectionContext?.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, for: self, class: CUWarningHeaderView.self, at: index) as? CUWarningHeaderView else {
            return UICollectionReusableView()
        }
        return headerView
    }
}
