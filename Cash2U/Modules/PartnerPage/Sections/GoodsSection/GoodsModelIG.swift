//
//  GoodsModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 31/8/22.
//

import UIKit
import IGListKit

final class GoodsModelIG: ListDiffable {
    
    let id = "Goods"
    let name: String
    let goods: [String]
    
    init(name: String, goods: [String]) {
        self.name = name
        self.goods = goods
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? GoodsModelIG else { return false }
        return id == object.id && name == object.name && goods == object.goods
    }
}
