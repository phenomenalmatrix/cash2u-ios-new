//
//  asdViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 30/8/22.
//

import UIKit
import IGListKit

final class DescriptionModelIG: ListDiffable {
    
    let id = "Description"
    let name: String
    let description: String
    
    init(name: String, description: String) {
        self.name = name
        self.description = description
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? DescriptionModelIG else { return false }
        return id == object.id && name == object.name
    }
}
