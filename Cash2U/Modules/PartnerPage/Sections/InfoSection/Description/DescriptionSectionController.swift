//
//  DescriptionSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 30/8/22.
//

import UIKit
import IGListKit
import Atributika

class DescriptionSectionController: ListSectionController {
    
    weak var delegate: GreetingsSectionControllerDelegate? = nil
    
    private var item: DescriptionModelIG?
    
    private var expanded = false

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 0, left: 0, bottom: 32, right: 0)
    }
    
    override func numberOfItems() -> Int {
        return 1
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        
        guard let item = item else {
            return .zero
        }

        var attribitedTitle: NSAttributedString {
            let style = Style.mainTextStyle(
                size: 18,
                fontType: .regular,
                color: .mainTextColor,
                alignment: .left
            )
            return item.description.styleAll(style).attributedString
        }
        
        return CGSize(width: (context.containerSize.width) - 40, height: expanded ? attribitedTitle.getHeight(withConstrainedWidth: (context.containerSize.width) - 40) + 50 : 200)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUDescriptionView = collectionContext!.dequeueReusableCell(of: CUDescriptionView.self, for: self, at: index) as? CUDescriptionView else {
            return UICollectionViewCell()
        }
        
        cell.delegate = self
        cell.setup(text: item?.description ?? "", isExpand: expanded)
        
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? DescriptionModelIG
    }
    
}

extension DescriptionSectionController: CUDescriptionViewDelegate {
    func moreButtonTapped() {
        collectionContext!.performBatch(animated: true) { batchContext in
            self.expanded.toggle()
            batchContext.reload(self)
        } completion: { _ in
            
        }
    }
}


