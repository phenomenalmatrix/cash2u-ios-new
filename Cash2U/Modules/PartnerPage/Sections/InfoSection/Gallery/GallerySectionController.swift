//
//  GallerySectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 30/8/22.
//

import UIKit
import IGListKit

class GallerySectionController: ListSectionController, ListSupplementaryViewSource {
    
    weak var delegate: SectionControllerHeaderDelegate? = nil
    
    private var item: GalleryModelIG?
    
    private var expanded = false

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 20, left: 0, bottom: 32, right: 0)
        supplementaryViewSource = self
    }
    
    override func numberOfItems() -> Int {
        return 1
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width) , height: (context.containerSize.width) / 2.3)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUGalleryView = collectionContext!.dequeueReusableCell(of: CUGalleryView.self, for: self, at: index) as? CUGalleryView else {
            return UICollectionViewCell()
        }
        
        
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? GalleryModelIG
    }
    
}

// MARK: - SuplementaryViews

extension GallerySectionController {
    func supportedElementKinds() -> [String] {
        return [UICollectionView.elementKindSectionHeader]
    }

    func viewForSupplementaryElement(ofKind elementKind: String, at index: Int) -> UICollectionReusableView {
        switch elementKind {
        case UICollectionView.elementKindSectionHeader:
            return userHeaderView(atIndex: index)
        default:
            return UICollectionReusableView()
        }
    }
    
    func sizeForSupplementaryView(ofKind elementKind: String, at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: context.containerSize.width - 40, height: 40)
    }
    
    // private funcs
    
    private func userHeaderView(atIndex index: Int) -> UICollectionReusableView {
        guard let headerView = collectionContext?.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, for: self, class: CUSectionHeaderView.self, at: index) as? CUSectionHeaderView else {
            return UICollectionReusableView()
        }
        headerView.delegate = self
        if let item = item {
            headerView.setup(titleText: item.name, isRightButtonHidden: false)
        }
        return headerView
    }
}

extension GallerySectionController: CUSectionHeaderViewDelegate {
    func viewAllTapped() {
        delegate?.viewAllTapped(type: .BestPartners)
    }
}
