//
//  GalleryModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 30/8/22.
//

import UIKit
import IGListKit

final class GalleryModelIG: ListDiffable {
    
    let id = "Gallery"
    let name: String
    
    init(name: String) {
        self.name = name
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? GalleryModelIG else { return false }
        return id == object.id && name == object.name
    }
}
