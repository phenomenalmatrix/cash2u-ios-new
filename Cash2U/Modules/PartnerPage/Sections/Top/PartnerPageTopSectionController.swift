//
//  PartnerPageTopSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 26/8/22.
//

import UIKit
import IGListKit

class PartnerPageTopSectionController: ListSectionController {
    
    weak var delegate: GreetingsSectionControllerDelegate? = nil
    
    private var item: PartnerPageTopModelIG?

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    override func numberOfItems() -> Int {
        return 1
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width) - 40, height: 235)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUPartnerPageTopView = collectionContext!.dequeueReusableCell(of: CUPartnerPageTopView.self, for: self, at: index) as? CUPartnerPageTopView else {
            return UICollectionViewCell()
        }
        
        if let item = item {
            cell.setup()
        }
        
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? PartnerPageTopModelIG
    }
    
}
