//
//  InstallmentAvailableSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 29/8/22.
//

import UIKit
import IGListKit

class InstallmentAvailableSectionController: ListSectionController {
    
    weak var delegate: GreetingsSectionControllerDelegate? = nil
    
    private var item: InstallmentAvailableModelIG?

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 0, left: 0, bottom: 32, right: 0)
    }
    
    override func numberOfItems() -> Int {
        return 1
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width), height: 60)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUInstallmentAvailableView = collectionContext!.dequeueReusableCell(of: CUInstallmentAvailableView.self, for: self, at: index) as? CUInstallmentAvailableView else {
            return UICollectionViewCell()
        }
        
        if let item = item {
            cell.setup()
        }
        
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? InstallmentAvailableModelIG
    }
    
}
