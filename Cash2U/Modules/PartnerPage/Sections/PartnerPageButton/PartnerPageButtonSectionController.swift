//
//  PartnerPageButtonSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 29/8/22.
//

import UIKit
import IGListKit

protocol PartnerPageButtonSectionControllerDelegate: AnyObject {
    func didSelect(type: CUPartnerPageButtonType)
}

class PartnerPageButtonSectionController: ListSectionController {
    
    
    weak var delegate: PartnerPageButtonSectionControllerDelegate? = nil
    
    private var item: PartnerPageButtonModelIG?

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
    }
    
    override func numberOfItems() -> Int {
        return 1
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width) - 40, height: 100)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUPartnerPageButtonView = collectionContext!.dequeueReusableCell(of: CUPartnerPageButtonView.self, for: self, at: index) as? CUPartnerPageButtonView else {
            return UICollectionViewCell()
        }
        cell.delegate = self
        cell.setup()
        
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? PartnerPageButtonModelIG
    }
    
}

extension PartnerPageButtonSectionController: CUPartnerPageButtonViewDelegate {
    func didSelect(type: CUPartnerPageButtonType) {
        delegate?.didSelect(type: type)
    }
}
