//
//  ReviewsSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 31/8/22.
//

import UIKit
import IGListKit
import Atributika

class ReviewsSectionController: ListSectionController, ListSupplementaryViewSource {
    
    weak var delegate: SectionControllerHeaderDelegate? = nil
    
    private var item: ReviewsModelIG?
    
    private var expanded = false

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 10, left: 0, bottom: 32, right: 0)
        supplementaryViewSource = self
    }
    
    override func numberOfItems() -> Int {
        return item?.items.count ?? 0
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        
        guard let item = item else {
            return .zero
        }

        var attribitedTitle: NSAttributedString {
            let style = Style.mainTextStyle(
                size: 16,
                fontType: .regular,
                color: .mainTextColor,
                alignment: .left
            )
            return item.items[index].comment.styleAll(style).attributedString
        }
        
        return CGSize(width: (context.containerSize.width) - 40, height: attribitedTitle.getHeight(withConstrainedWidth: (context.containerSize.width) - 160) + 63)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUReviewsView = collectionContext!.dequeueReusableCell(of: CUReviewsView.self, for: self, at: index) as? CUReviewsView else {
            return UICollectionViewCell()
        }
        
        if let item = item {
//            cell.setup(text: item.reviews[index])
            cell.setup(item: item.items[index])
        }
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? ReviewsModelIG
    }
    
}

// MARK: - SuplementaryViews

extension ReviewsSectionController {
    func supportedElementKinds() -> [String] {
        return [UICollectionView.elementKindSectionHeader]
    }

    func viewForSupplementaryElement(ofKind elementKind: String, at index: Int) -> UICollectionReusableView {
        switch elementKind {
        case UICollectionView.elementKindSectionHeader:
            return userHeaderView(atIndex: index)
        default:
            return UICollectionReusableView()
        }
    }
    
    func sizeForSupplementaryView(ofKind elementKind: String, at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: context.containerSize.width - 40, height: 40)
    }
    
    // private funcs
    
    private func userHeaderView(atIndex index: Int) -> UICollectionReusableView {
        guard let headerView = collectionContext?.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, for: self, class: CUSectionHeaderView.self, at: index) as? CUSectionHeaderView else {
            return UICollectionReusableView()
        }
        headerView.delegate = self
        if let item = item {
            headerView.setup(titleText: "\(item.name) \(item.items.count)", isRightButtonHidden: true)
        }
        return headerView
    }
}

extension ReviewsSectionController: CUSectionHeaderViewDelegate {
    func viewAllTapped() {
        delegate?.viewAllTapped(type: .BestPartners)
    }
}
