//
//  ReviewsModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 31/8/22.
//

import UIKit
import IGListKit

final class ReviewsModelIG: ListDiffable {
    
    let id = "Reviews"
    let name: String
    let reviews: [String]
    let items: [ReviewsItemModelIG]
    
    init(name: String, reviews: [String], items: [ReviewsItemModelIG]) {
        self.name = name
        self.reviews = reviews
        self.items = items
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? ReviewsModelIG else { return false }
        return id == object.id && name == object.name && reviews == object.reviews && items == object.items
    }
}

final class ReviewsItemModelIG: ListDiffable, Equatable {
    
    static func == (lhs: ReviewsItemModelIG, rhs: ReviewsItemModelIG) -> Bool {
        return lhs.id == rhs.id
    }
    
    let id: Int
    let comment: String
    let rate: Int
    let clientId: Int
    let partnerId: Int
    let createDateTime: String
    
    init(id: Int, comment: String, rate: Int, clientId: Int, partnerId: Int, createDateTime: String) {
        self.id = id
        self.comment = comment
        self.rate = rate
        self.clientId = clientId
        self.partnerId = partnerId
        self.createDateTime = createDateTime
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? ReviewsItemModelIG else { return false }
        return id == object.id && comment == object.comment
    }
}
