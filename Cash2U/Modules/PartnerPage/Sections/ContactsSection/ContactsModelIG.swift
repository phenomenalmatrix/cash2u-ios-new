//
//  ContactsModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 1/9/22.
//

import UIKit
import IGListKit

final class ContactsModelIG: ListDiffable {
    
    let id = UUID().uuidString
    let name: String
    let title: String
    let description: String
    let schedule: String
    let phoneNumbers: [String]
    let socialContacts: [String]
    
    init(name: String, title: String, description: String, schedule: String, phoneNumbers: [String], socialContacts: [String]) {
        self.name = name
        self.title = title
        self.description = description
        self.schedule = schedule
        self.phoneNumbers = phoneNumbers
        self.socialContacts = socialContacts
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? ContactsModelIG else { return false }
        return id == object.id && name == object.name && phoneNumbers == object.phoneNumbers && socialContacts == object.socialContacts
    }
}
