//
//  ContactsSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 1/9/22.
//

import UIKit
import IGListKit
import Atributika

class ContactsSectionController: ListSectionController {
    
    weak var delegate: SectionControllerHeaderDelegate? = nil
    
    private var item: ContactsModelIG?
    
    private let cellsBeforeContacts = 2

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 10, left: 0, bottom: 32, right: 0)
    }
    
    override func numberOfItems() -> Int {
        guard let item = item else {
            return 0
        }
        
        return cellsBeforeContacts + item.phoneNumbers.count + item.socialContacts.count
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        
        let width = context.containerSize.width
        
        guard let item = item else {
            return .zero
        }
        
        if index == 0 {
            
            var attribitedTitle: NSAttributedString {
                let style = Style.mainTextStyle(
                    size: 22,
                    fontType: .medium,
                    color: .mainTextColor,
                    alignment: .left
                )
                return item.title.styleAll(style).attributedString
            }
            
            var attribitedSubtitle: NSAttributedString {
                let style = Style.mainTextStyle(
                    size: 18,
                    fontType: .regular,
                    color: .mainTextColor,
                    alignment: .left
                )
                return item.description.styleAll(style).attributedString
            }
            
            let height = attribitedTitle.getHeight(withConstrainedWidth: width - 140) +
            attribitedSubtitle.getHeight(withConstrainedWidth: width - 140)
            
            return CGSize(width: width - 40, height: height + 50)
            
        } else if index == 1 {
            
            return CGSize(width: width - 40, height: 80)
            
            
        } else if index < cellsBeforeContacts + item.phoneNumbers.count {
            
            return CGSize(width: width - 40, height: 50)
            
        } else {
            
            return CGSize(width: width - 40, height: 50)
            
        }
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        
        guard let item = item else {return UICollectionViewCell()}
        
        if index == 0 {
            
            guard let cell: CUContactsTitleView = collectionContext!.dequeueReusableCell(of: CUContactsTitleView.self, for: self, at: index) as? CUContactsTitleView else {
                return UICollectionViewCell()
            }
            
            cell.setup(title: item.title, subtitle: item.description, isFirst: true)
            
            return cell
            
        } else if index == 1 {
            
            guard let cell: CUScheduleView = collectionContext!.dequeueReusableCell(of: CUScheduleView.self, for: self, at: index) as? CUScheduleView else {
                return UICollectionViewCell()
            }
            
            
            return cell
            
        } else if index < cellsBeforeContacts + item.phoneNumbers.count {
            
            guard let cell: CUPhoneNumberView = collectionContext!.dequeueReusableCell(of: CUPhoneNumberView.self, for: self, at: index) as? CUPhoneNumberView else {
                return UICollectionViewCell()
            }
            
            if index == (cellsBeforeContacts + item.phoneNumbers.count) - 1 {
                cell.setup(phone: item.phoneNumbers[index - cellsBeforeContacts], isLast: true)
            } else {
                cell.setup(phone: item.phoneNumbers[index - cellsBeforeContacts])
            }
                
            
            return cell
            
        } else {
            
            guard let cell: CUSocialsView = collectionContext!.dequeueReusableCell(of: CUSocialsView.self, for: self, at: index) as? CUSocialsView else {
                return UICollectionViewCell()
            }
            
            if index == (cellsBeforeContacts + item.phoneNumbers.count + item.socialContacts.count) - 1 {
                cell.setup(title: item.socialContacts[index - (cellsBeforeContacts + item.phoneNumbers.count)], isLast: true)
            } else {
                cell.setup(title: item.socialContacts[index - (cellsBeforeContacts + item.phoneNumbers.count)])
            }
            
            return cell
            
        }
    }

    override func didUpdate(to object: Any) {
        item = object as? ContactsModelIG
    }
    
}
