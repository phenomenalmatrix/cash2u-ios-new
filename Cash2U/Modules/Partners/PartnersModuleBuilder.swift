//
//  PartnersModuleBuilder.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/5/22.
//  
//

import UIKit

final class PartnersModuleConfigurator {
    /// Собрать модуль
    class func build() -> UIViewController {
        let view = PartnersViewController()
        let presenter = PartnersPresenter()
        let interactor = PartnersInteractor()
        let router = PartnersRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
    
    class func buildGuest() -> UIViewController {
        let view = GuestPartnersViewController()
        let presenter = PartnersPresenter()
        let interactor = PartnersInteractor()
        let router = PartnersRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
