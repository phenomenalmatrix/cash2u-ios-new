//
//  GuestPartnersViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 22/8/22.
//

import UIKit
import IGListKit

final class GuestPartnersViewController: CUBellViewController  {
    
    var presenter: PartnersViewToPresenterProtocol?
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private lazy var partnerMallCollectionView = UIPartnersMallCollectionView()
    private lazy var catigoriesCollectionView = UICatigoriesCollectionView()
    
    private lazy var adapter: ListAdapter = {
        let adapter = ListAdapter(updater: ListAdapterUpdater(), viewController: self)
        adapter.collectionView = mainCollection
        adapter.dataSource = self
        return adapter
    }()
    
    private var bestPartnersModel = BestPartnersModelIG(name: "Рекомендуем", isLoading: false, items: [])
    private var offersAndPromotionsModel = OffersAndPromotionsModelIG(name: "Offers", isLoading: false)
    private var categoriesModel = CategoryModelIG(name: "Cat", count: 14, type: 0, array: [])
    private var bonusCardsModel = BonusCardsModelIG(name: "Bonus", isLoading: false)
    private var discountModel = DiscountModelIG(name: "discountModel", items: [])
    
    private func createItemsData() -> [ListDiffable] {
        var aa = [
        ] as [ListDiffable]
        aa.append(SearchModelIG(name: "Search2"))
        aa.append(LocationModelIG(name: "Location", type: 0))
        aa.append(OffersAndPromotionsModelIG(name: "Offers", isLoading: false))
        aa.append(self.discountModel)
        aa.append(NewPartnersModelIG(name: "Новые партнеры", items: []))
        aa.append(self.bestPartnersModel)
        aa.append(MallsModelIG(name: "Торговые центры", items: []))
        aa.append(BazaarModelIG(name: "Рынки и базары", items: []))
        aa.append(self.categoriesModel)
        aa.append(SupportModelIG())
        aa.append(VersionModelIG())
        return aa
    }
    
    private lazy var itemsData: [ListDiffable] = createItemsData()
    
    private lazy var mainCollection: CUCollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        let mainCollection = CUCollectionView(
            frame: CGRect.zero,
            collectionViewLayout: flowLayout
        )
        mainCollection.translatesAutoresizingMaskIntoConstraints = false
        mainCollection.alwaysBounceVertical = true
        return mainCollection
    }()
    
    override func loadView() {
        super.loadView()
        self.navigationItem.title = "Партнеры"
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
        presenter?.getCategories()
        presenter?.getPartners()
        presenter?.getMalls()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adapter.performUpdates(animated: true, completion: nil)
    }
}

extension GuestPartnersViewController: PartnersViewProtocol {
    func recivePartnersPromo(promo: PartnersPromoModel) {
        
    }
    
    func reciveMarketsToView(markets: MallResponse) {
        print(markets)
    }
    
    func reciveMallsToView(malls: MallResponse) {
        print(malls)
    }
    
    func recivePartnersToView(partners: PartnerResponse) {
        print(partners)
    }
    
    func reciveError(error: Error) {
        print(error)
    }
    
    func reciveCategoriesToView(categories: CategoriesModel) {
        guard let index = self.itemsData.firstIndex(where: {$0.diffIdentifier().description == "Category"}) else {return}
        self.categoriesModel = CategoryModelIG(name: "", count: categories.items.count , type: 0, array: categories.mapDtoListToUIList())
        self.itemsData[index] = self.categoriesModel
        self.adapter.performUpdates(animated: true)
    }
}

extension GuestPartnersViewController: UISearchResultsUpdating, UISearchControllerDelegate {
    func updateSearchResults(for searchController: UISearchController) {
        
    }
}

extension GuestPartnersViewController: SearchSectionControllerDelegate {
    func sectionTapped() {
        let vc = SearchModuleConfigurator.build()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func filterButtonTapped() {
        let vc = FiltersViewController()
        self.presentPanModal(vc)
    }
}

extension GuestPartnersViewController: MallsSectionControllerDelegate {
    func didSelectMall() {
        let vc = MallsDetailModuleConfigurator.build()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension GuestPartnersViewController: CategorySectionControllerDelegate {
    func didSelectCategory() {
        let vc = CategoryDetailModuleConfigurator.build()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


// MARK: - ListAdapterDataSource

extension GuestPartnersViewController: ListAdapterDataSource {
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        itemsData
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        if object is CurrencyModelIG {
            let section = CurrencySectionController()
            return section
        } else if object is OffersAndPromotionsModelIG {
            return OffersAndPromotionsSectionController(isHeaderHidden: true)
        } else if object is BestPartnersModelIG {
            let section = RecomendationSectionController()
//            section.delegate = self
            return section
        } else if object is LocationModelIG {
            let section = LocationSectionController()
//            section.delegate = self
            return section
        } else if object is SearchModelIG {
            let section = SearchSectionController()
            section.delegate = self
            return section
        } else if object is NewPartnersModelIG {
            let section = NewPartnersSectionController()
//            section.delegate = self
            return section
        } else if object is MallsModelIG {
            let section = MallsSectionController(isHeaderButtonHidden: false)
            section.delegate = self
            return section
        } else if object is DiscountModelIG {
            let section = DiscountSectionController()
//            section.delegate = self
            return section
        } else if object is SupportModelIG {
            return SupportSectionController()
        } else if object is VersionModelIG {
            return VersionSectionController()
        } else if object is GreetingsSectionModelIG {
            let section = GreetingsSectionController()
//            section.delegate = self
            return section
        } else if object is BazaarModelIG {
            let section = BazaarsSectionController(isHeaderButtonHidden: false)
//            section.delegate = self
            return section
        } else if object is CategoryModelIG {
            let section = CategorySectionController()
            section.delegate = self
            return section
        } else {
            return ListSectionController()
        }
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }
}

// MARK: - UICollectionViewDelegate

extension GuestPartnersViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}

// MARK: - UICollectionViewDataSource

extension GuestPartnersViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 40
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(cellType: PartnersCategoryCell.self, for: indexPath)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: CUPartnersHeaderView.className, for: indexPath) as? CUPartnersHeaderView else {return UICollectionReusableView()}
        return header
    }
}

// MARK: - UICollectionViewDelegate

extension GuestPartnersViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.width / 1.28)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemPerRow: CGFloat = 2
        let paddingWidth = 20 * (itemPerRow + 1) - 10
        let awailableWidth = collectionView.frame.width - paddingWidth
        let widthPerItem = awailableWidth / itemPerRow
        return CGSize(width: widthPerItem, height: widthPerItem + 40)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
       return 10
    }
}

extension GuestPartnersViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        view.addSubview(mainCollection)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        mainCollection.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
    }
}
