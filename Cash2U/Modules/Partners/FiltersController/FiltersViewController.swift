//
//  FiltersViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 5/9/22.
//  
//

import UIKit


final class FiltersViewController: CUViewController {
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private lazy var mainCollection: CUTableView = {
        let mainCollection = CUTableView(frame: .zero, style: .insetGrouped)
        mainCollection.translatesAutoresizingMaskIntoConstraints = false
        mainCollection.alwaysBounceVertical = true
        mainCollection.showsVerticalScrollIndicator = false
        mainCollection.register(CUFiltersCategoryCell.self)
        mainCollection.rowHeight = 50
        return mainCollection
    }()
    
    private lazy var bottomView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .mainColor
        return view
    }()
    
    private lazy var bottomLineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .grayColor
        view.alpha = 0.25 
        return view
    }()
    
    private lazy var applyButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Применить")
        view.addTarget(self, action: #selector(onApplyButtonTapped), for: .touchUpInside)
        return view
    }()
    
    private lazy var items: [OutlineItem] = DataManager.shared.mockData.items
    private typealias TreeItemType = ListTreeDataSource<OutlineItem>.TreeItemType
    
    private lazy var listTreeDataSource: ListTreeDataSource<OutlineItem> = {
        var dataSource = ListTreeDataSource<OutlineItem>()
        addItems(items, to: dataSource)
        return dataSource
    }()
    
    private lazy var diffableDataSource: UITableViewDiffableDataSource<Section, TreeItemType> = {
        return self.createDiffableDataSource()
    }()
    
    enum Section {
        case main
    }
    
    override func loadView() {
        super.loadView()
        mainCollection.delegate = self
        mainCollection.dataSource = self.diffableDataSource
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reloadUI()
    }
    
    // MARK: - UIActions
    
    @objc private func onApplyButtonTapped() {
        dismiss(animated: true)
    }
    
    // MARK: - Helpers
    
    private func createDiffableDataSource() -> UITableViewDiffableDataSource<Section, TreeItemType> {
        let dataSource = UITableViewDiffableDataSource<Section, TreeItemType>(
            tableView: mainCollection,
            cellProvider: { tableView, indexPath, _ in
                let cell = tableView.dequeueCell(cellType: CUFiltersCategoryCell.self, for: indexPath)
                let item = self.listTreeDataSource.items[indexPath.row]
                cell.setup(item: item)
                return cell
            }
        )
        return dataSource
    }
    
    func reloadUI(animating: Bool = true) {
        var diffableSnaphot = NSDiffableDataSourceSnapshot<Section, TreeItemType>()
        diffableSnaphot.appendSections([.main])
        diffableSnaphot.appendItems(listTreeDataSource.items, toSection: .main)
        self.diffableDataSource.apply(diffableSnaphot, animatingDifferences: animating)
    }
}

extension FiltersViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = CUSectionHeaderView()
        view.setup(titleText: "Фильтры", titleSize: 28, isRightButtonHidden:  true)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        65
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listTreeDataSource.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(cellType: CUFiltersCategoryCell.self, for: indexPath)
        let item = listTreeDataSource.items[indexPath.row]
        cell.setup(item: item)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = listTreeDataSource.items[indexPath.row]
        listTreeDataSource.toggleExpand(item: item)
        if let cell = tableView.cellForRow(at: indexPath) as? CUFiltersCategoryCell {
            UIView.animate(withDuration: 0.2) {
                let transform = CGAffineTransform.init(rotationAngle: item.isExpanded ? CGFloat.pi / 2.0 : 0)
                cell.icon.transform = transform
                if item.subitems.isEmpty {
                    item.value.isSelected.toggle()
                    cell.checkbox.checkmarkIsSelected.toggle()
                }
            }
        }
        self.reloadUI()
    }
}

extension FiltersViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        
        view.addSubview(mainCollection)
        view.addSubview(bottomView)
        view.addSubview(bottomLineView)
        view.addSubview(applyButton)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        mainCollection.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(30)
            make.leading.trailing.equalToSuperview().inset(5)
            make.bottom.equalTo(view.safeArea.bottom).offset(-64)
        }
        
        bottomView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(view.safeArea.bottom)
            make.height.equalTo(96)
        }
        
        bottomLineView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(bottomView.snp.top)
            make.height.equalTo(1)
        }
        
        applyButton.snp.makeConstraints { make in
            make.bottom.equalTo(view.safeArea.bottom).offset(-16)
            make.leading.trailing.equalToSuperview().inset(20)
            make.height.equalTo(ScreenHelper.mainButtonHeight)
        }
    }
}

// MARK: - PanModalPresentable

extension FiltersViewController: PanModalPresentable {
    func panModalDidDismiss() {
        didDismissDelegate?.didDismiss()
    }
    var panScrollable: UIScrollView? {
        return mainCollection
    }
    
    var allowsExtendedPanScrolling: Bool {
        true
    }
    
    var shouldShowScrollIndicator: Bool {
        false
    }
    
    var longFormHeight: PanModalHeight {
        .maxHeight
    }
    
    var shortFormHeight: PanModalHeight {
        .maxHeight
    }
}
