//
//  DataManager.swift
//  TreeView
//
//  Created by Dzmitry Antonenka on 24/04/21.
//

import Foundation

enum MockData {
    case small
    case large88K
    case large350K
    case doubleLarge797K
    case extraLarge7_2M
    
    var items: [OutlineItem] {
        switch self {
        case .small: return OutlineItemDataSet.mockDataSmall
        case .large88K: return OutlineItemDataSet.mockData88K
        case .large350K: return OutlineItemDataSet.mockData350K
        case .doubleLarge797K: return OutlineItemDataSet.mockData797K
        case .extraLarge7_2M: return OutlineItemDataSet.mockData7_2M
        }
    }
}

class DataManager {
    static let shared = DataManager()
        
    var mockData: MockData { self.mockDataSmall }
    
    // specialized data sets
    lazy var mockDataSmall: MockData = .small
    lazy var mockDataLarge88K: MockData = .large88K
    lazy var mockDataLarge350K: MockData = .large350K
    lazy var mockDataDoubleLarge797K: MockData = .doubleLarge797K
    lazy var mockDataExtraLarge7_2M: MockData = .extraLarge7_2M
}

class OutlineItem: Hashable {
    let title: String
    var subitems: [OutlineItem]
    var isSelected: Bool = false

    init(title: String,
         subitems: [OutlineItem] = [],
         isSelected: Bool = false) {
        self.title = title
        self.subitems = subitems
        self.isSelected = isSelected
    }
    func hash(into hasher: inout Hasher) {
        hasher.combine(identifier)
    }
    static func == (lhs: OutlineItem, rhs: OutlineItem) -> Bool {
        return lhs.identifier == rhs.identifier
    }
    private let identifier = UUID()
}

extension OutlineItem: CustomStringConvertible {
    var description: String {
        return "\(title)"
    }
}

struct OutlineItemDataSet {
    static var mockData88K: [OutlineItem] = {
        // Total elements in tree: (Int) $R0 = 88_572, creation time ~0.201 sec on MBP 2019, core i7
        return menuItems(targetNestLevel: 10, currentLevel: 0, itemsInSection: 3)
    }()
    static var mockData350K: [OutlineItem] = {
        // Total elements in tree: (Int) $R0 = 349_524, creation time ~0.94 sec on MBP 2019, core i7
        return menuItems(targetNestLevel: 9, currentLevel: 0, itemsInSection: 4)
    }()
    static var mockData797K: [OutlineItem] = {
        // Total elements in tree: (Int) $R0 = 797_160, creation time ~1.86 sec on MBP 2019, core i7
        return menuItems(targetNestLevel: 12, currentLevel: 0, itemsInSection: 3)
    }()
    static var mockData7_2M: [OutlineItem] = {
        // Total elements in tree: (Int) $R0 = 7_174_452, creation time ~17.5 sec on MBP 2019, core i7
        return menuItems(targetNestLevel: 14, currentLevel: 0, itemsInSection: 3)
    }()
    
    static var mockDataSmall: [OutlineItem] = {
        var items: [OutlineItem] = [
                OutlineItem(title: "Тип", subitems: [
                    OutlineItem(title: "Getting Started", subitems: []),
                    OutlineItem(title: "Advanced Layouts", subitems: []),
                    OutlineItem(title: "Conference App", subitems: [])
                ]),
                OutlineItem(title: "Diffable Data Source", subitems: [
                    OutlineItem(title: "Mountains Search"),
                    OutlineItem(title: "Settings: Wi-Fi"),
                    OutlineItem(title: "Insertion Sort Visualization"),
                    OutlineItem(title: "UITableView: Editing", subitems: [])
                    ]),
                OutlineItem(title: "Lists", subitems: [
                    OutlineItem(title: "Simple List"),
                    OutlineItem(title: "Reorderable List"),
                    OutlineItem(title: "List Appearances"),
                    OutlineItem(title: "List with Custom Cells")
                ]),
                OutlineItem(title: "Outlines", subitems: [
                    OutlineItem(title: "Emoji Explorer - List", subitems: [
                        OutlineItem(title: "Grid"),
                        OutlineItem(title: "Inset Items Grid"),
                        OutlineItem(title: "Two-Column Grid"),
                        OutlineItem(title: "Per-Section Layout", subitems: [])
                        ])
,
                    OutlineItem(title: "Emoji Explorer - List", subitems: [
                        OutlineItem(title: "Grid"),
                        OutlineItem(title: "Inset Items Grid"),
                        OutlineItem(title: "Two-Column Grid"),
                        OutlineItem(title: "Per-Section Layout", subitems: [])
                        ]),
                    OutlineItem(title: "Emoji Explorer - List", subitems: [
                        OutlineItem(title: "Grid"),
                        OutlineItem(title: "Inset Items Grid"),
                        OutlineItem(title: "Two-Column Grid"),
                        OutlineItem(title: "Per-Section Layout", subitems: [])
                        ])
,
                    OutlineItem(title: "Emoji Explorer - List", subitems: [
                        OutlineItem(title: "Grid"),
                        OutlineItem(title: "Inset Items Grid"),
                        OutlineItem(title: "Two-Column Grid"),
                        OutlineItem(title: "Per-Section Layout", subitems: [])
                        ]),
                    OutlineItem(title: "Emoji Explorer - List", subitems: [
                        OutlineItem(title: "Grid"),
                        OutlineItem(title: "Inset Items Grid"),
                        OutlineItem(title: "Two-Column Grid"),
                        OutlineItem(title: "Per-Section Layout", subitems: [])
                        ])
,
                    OutlineItem(title: "Emoji Explorer - List", subitems: [
                        OutlineItem(title: "Grid"),
                        OutlineItem(title: "Inset Items Grid"),
                        OutlineItem(title: "Two-Column Grid"),
                        OutlineItem(title: "Per-Section Layout", subitems: [])
                        ]),
                    OutlineItem(title: "Emoji Explorer - List", subitems: [
                        OutlineItem(title: "Grid"),
                        OutlineItem(title: "Inset Items Grid"),
                        OutlineItem(title: "Two-Column Grid"),
                        OutlineItem(title: "Per-Section Layout", subitems: [])
                        ])
,
                    OutlineItem(title: "Emoji Explorer - List", subitems: [
                        OutlineItem(title: "Grid"),
                        OutlineItem(title: "Inset Items Grid"),
                        OutlineItem(title: "Two-Column Grid"),
                        OutlineItem(title: "Per-Section Layout", subitems: [])
                        ])
                ])
            ]
        return items
    }()
    
    static func menuItems(targetNestLevel: Int, currentLevel: Int, itemsInSection: Int = 3) -> [OutlineItem] {
        return (1...itemsInSection).compactMap { (index) in
            guard currentLevel < targetNestLevel else { return nil }
            return OutlineItem(title: "Level \(currentLevel), item: \(index)", subitems: menuItems(targetNestLevel: targetNestLevel,
                                                                                                    currentLevel: currentLevel + 1, itemsInSection: itemsInSection) )
            
        }
    }
}

func addItems(_ items: [OutlineItem], to dataSource: ListTreeDataSource<OutlineItem>) {
    addItems(items, itemChildren: { $0.subitems }, to: dataSource)
}

