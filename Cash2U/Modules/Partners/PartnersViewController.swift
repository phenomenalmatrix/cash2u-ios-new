//
//  PartnersViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/5/22.
//  
//

import UIKit
import IGListKit

final class PartnersViewController: CUBellViewController  {
    
    var presenter: PartnersViewToPresenterProtocol?
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private lazy var qrButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Оплатить QR", titleSize: 16, cornerRadius: 45  / 2)
        view.addTarget(self, action: #selector(onOpenQR), for: .touchUpInside)
        return view
    }()
    
    private lazy var adapter: ListAdapter = {
        let adapter = ListAdapter(updater: ListAdapterUpdater(), viewController: self)
        adapter.collectionView = mainCollection
        adapter.dataSource = self
        return adapter
    }()
    
    private var categoriesArray: [CategoryModelItemIG]? = nil
    private var partnersArray: [PartnersItemModelIG]? = nil
    
    private var bestPartnersModel = BestPartnersModelIG(name: "Рекомендуем", isLoading: false, items: [])
    private var categoriesModel = CategoryModelIG(name: "Cat", count: 14, type: 0, array: [])
    private var offersAndPromotionsModel = OffersAndPromotionsModelIG(name: "Offers", isLoading: false)
    private var bonusCardsModel = BonusCardsModelIG(name: "Bonus", isLoading: false)
    private var discountModel = DiscountModelIG(name: "discountModel", items: [])
    private var newPartnerModel = NewPartnersModelIG(name: "Новые партнеры", items: [])
    private var mallsModel = MallsModelIG(name: "Торговые центры", items: [])
    private var marketsModel = BazaarModelIG(name: "Рынки и базары", items: [])
    
    private func createItemsData() -> [ListDiffable] {
        var aa = [
        ] as [ListDiffable]
        aa.append(SearchModelIG(name: "Search2"))
        aa.append(LocationModelIG(name: "Location", type: 1))
        aa.append(self.discountModel)
        aa.append(self.newPartnerModel)
        aa.append(self.bestPartnersModel)
        aa.append(self.mallsModel)
        aa.append(self.marketsModel)
        aa.append(self.categoriesModel)
        aa.append(SupportModelIG())
        aa.append(VersionModelIG())
        return aa
    }
    
    private lazy var itemsData: [ListDiffable] = createItemsData()
    
    private lazy var mainCollection: CUCollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        let mainCollection = CUCollectionView(
            frame: CGRect.zero,
            collectionViewLayout: flowLayout
        )
        mainCollection.translatesAutoresizingMaskIntoConstraints = false
        mainCollection.alwaysBounceVertical = true
        return mainCollection
    }()
    
    override func loadView() {
        super.loadView()
        self.navigationItem.title = "Партнеры"
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
        presenter?.getCategories()
        presenter?.getPartners()
        presenter?.getMalls()
        presenter?.getMarkets()
        presenter?.getPartnersPromo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.getCategories()
        presenter?.getPartners()
        presenter?.getMalls()
        presenter?.getMarkets()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adapter.performUpdates(animated: true, completion: nil)
//        let vc = MallsDetailModuleConfigurator.build()
//        vc.hidesBottomBarWhenPushed = true
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - UIActions
    
}
 
extension PartnersViewController: PartnersViewProtocol {
    func recivePartnersPromo(promo: PartnersPromoModel) {
        guard let index = self.itemsData.firstIndex(where: {$0.diffIdentifier().description == "Dicount"}) else {return}
        self.discountModel = DiscountModelIG(name: "discountModel", items: promo.mapDtoListToUIList())
        self.itemsData[index] = self.discountModel
        self.adapter.performUpdates(animated: true)
    }
    
    
    func reciveMarketsToView(markets: MallResponse) {
        guard let index = self.itemsData.firstIndex(where: {$0.diffIdentifier().description == "Bazaar"}) else {return}
        self.marketsModel = BazaarModelIG(name: "Рынки и базары", items: markets.mapDtoListToUIList())
        self.itemsData[index] = self.marketsModel
        self.adapter.performUpdates(animated: true)
    }
    
    func reciveMallsToView(malls: MallResponse) {
        guard let index = self.itemsData.firstIndex(where: {$0.diffIdentifier().description == "Malls"}) else {return}
        self.mallsModel = MallsModelIG(name: "Торговые центры", items: malls.mapDtoListToUIList() )
        self.itemsData[index] = self.mallsModel
        self.adapter.performUpdates(animated: true)
    }
    
    func recivePartnersToView(partners: PartnerResponse) {
        print(partners)
        guard let index = self.itemsData.firstIndex(where: {$0.diffIdentifier().description == "NewPartners"}) else {return}
        self.partnersArray = partners.mapDtoListToUIList()
        self.newPartnerModel = NewPartnersModelIG(name: "Новые партнеры", items: self.partnersArray ?? [])
        self.itemsData[index] = self.newPartnerModel
        adapter.performUpdates(animated: true, completion: nil)
        
        // bestPartners
        guard let index = self.itemsData.firstIndex(where: {$0.diffIdentifier().description == "BestPartners"}) else {return}
        bestPartnersModel = BestPartnersModelIG(name: "Рекомендуем", isLoading: false, items: partners.mapDtoListToUIList())
        self.itemsData[index] = self.bestPartnersModel
        adapter.performUpdates(animated: true, completion: nil)
    }
    
    func reciveError(error: Error) {
        print(error)
    }
    
    func reciveCategoriesToView(categories: CategoriesModel) {
        guard let index = self.itemsData.firstIndex(where: {$0.diffIdentifier().description == "Category"}) else {return}
        self.categoriesArray = categories.mapDtoListToUIList()
        self.categoriesModel = CategoryModelIG(name: "", count: categoriesArray?.count ?? 0, type: 0, array: categoriesArray ?? [])
        self.itemsData[index] = self.categoriesModel
        self.adapter.performUpdates(animated: true)
    }
    
}

extension PartnersViewController: UISearchResultsUpdating, UISearchControllerDelegate {
    func updateSearchResults(for searchController: UISearchController) {
        
    }
}

extension PartnersViewController: SearchSectionControllerDelegate {
    func sectionTapped() {
        let vc = SearchModuleConfigurator.build()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func filterButtonTapped() {
        let vc = FiltersViewController()
        self.presentPanModal(vc)
    }
}

extension PartnersViewController: MallsSectionControllerDelegate {
    func didSelectMall() {
        let vc = MallsDetailModuleConfigurator.build()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension PartnersViewController: CategorySectionControllerDelegate {
    func didSelectCategory() {
        let vc = CategoryDetailModuleConfigurator.build()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension PartnersViewController: PartnerSelectionDelegate {
    func didSelectPartner(partnerId: Int) {
        let vc = PartnerPageModuleBuilder.build(partnerId: partnerId)
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: - ListAdapterDataSource

extension PartnersViewController: ListAdapterDataSource {
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        itemsData
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        if object is CurrencyModelIG {
            let section = CurrencySectionController()
            return section
        } else if object is BestPartnersModelIG {
            let section = RecomendationSectionController()
            section.delegate = self
            return section
        } else if object is LocationModelIG {
            let section = LocationSectionController()
//            section.delegate = self
            return section
        } else if object is SearchModelIG {
            let section = SearchSectionController()
            section.delegate = self
            return section
        } else if object is NewPartnersModelIG {
            let section = NewPartnersSectionController()
            section.delegate = self
            return section
        } else if object is MallsModelIG {
            let section = MallsSectionController(isHeaderButtonHidden: false)
            section.delegate = self
            return section
        } else if object is DiscountModelIG {
            let section = DiscountSectionController()
//            section.delegate = self
            return section
        } else if object is SupportModelIG {
            return SupportSectionController()
        } else if object is VersionModelIG {
            return VersionSectionController()
        } else if object is GreetingsSectionModelIG {
            let section = GreetingsSectionController()
//            section.delegate = self
            return section
        } else if object is BazaarModelIG {
            let section = BazaarsSectionController(isHeaderButtonHidden: false)
//            section.delegate = self
            return section
        } else if object is CategoryModelIG {
            let section =  CategorySectionController()
            section.delegate = self
            return section
        } else {
            return ListSectionController()
        }
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }
}

extension PartnersViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        view.addSubview(mainCollection)
        view.addSubview(qrButton)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        mainCollection.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        qrButton.snp.makeConstraints { make in
            make.bottom.equalTo(view.safeArea.bottom).offset(-12)
            make.centerX.equalToSuperview()
            make.width.equalTo(ScreenHelper.getWidth / 2.5)
            make.height.equalTo(45)
        }
    }
}
