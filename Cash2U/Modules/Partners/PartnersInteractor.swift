//
//  PartnersInteractor.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/5/22.
//  
//

import Foundation

final class PartnersInteractor {
    private let authService = AuthService.shared
    private let categoryService = CategoriesService.shared
    private let partnerService = PartnerService.shared
    private let mallService = MallsService.shared
    weak var presenter: PartnersInteractorToPresenterProtocol?
}

extension PartnersInteractor: PartnersInteractorProtocol {
    func getPartnersPromo() {
        partnerService.getPartnerPromos { result in
            switch result {
            case .success(let success):
                if let data = success?.data {
                    let promos: PartnersPromoModel = DecodeHelper.decodeDataToObject(data: data)!
                    self.presenter?.recivePartnersPromoSuccess(promo: promos)
                }
            case .failure(let failure):
                self.presenter?.reciveError(error: failure)
            }
        }
    }
    
    func getMarkets() {
        mallService.getMarkets { result in
            switch result {
            case .success(let success):
                if let dataResult = success?.data {
                    let markets : MallResponse = DecodeHelper.decodeDataToObject(data: dataResult)!
                    self.presenter?.reciveMarketsSuccess(markets: markets)
                }
            case .failure(let failure):
                self.presenter?.reciveError(error: failure)
            }
        }
    }
    
    func getMalls() {
        mallService.getMalls { result in
            switch result {
            case .success(let success):
                if let dataResult = success?.data {
                    let malls: MallResponse = DecodeHelper.decodeDataToObject(data: dataResult)!
                    self.presenter?.reciveMallsSuccess(malls: malls)
                }
            case .failure(let failure):
                self.presenter?.reciveError(error: failure)
            }
        }
    }
    
    func getPartners() {
        partnerService.getPartners { result in
            switch result {
            case .success(let success):
                if let dataResult = success?.data {
                    let partners: PartnerResponse = DecodeHelper.decodeDataToObject(data: dataResult)!
                    self.presenter?.recivePartnersSuccess(partners: partners)
                }
            case .failure(let failure):
                self.presenter?.reciveError(error: failure)
            }
        }
    }
    
    func getCategories() {
        categoryService.getCategories { result in
            switch result {
            case .success(let success):
                if let dataResult = success?.data {
                    let categories: CategoriesModel = DecodeHelper.decodeDataToObject(data: dataResult)!
                    self.presenter?.reciveCategoriesSuccess(categories: categories)
                }
            case .failure(let failure):
                self.presenter?.reciveError(error: failure)
            }
        }
    }
}
