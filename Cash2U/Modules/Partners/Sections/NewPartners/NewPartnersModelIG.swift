//
//  NewPartnersModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/8/22.
//

import Foundation
import IGListKit

final class NewPartnersModelIG: ListDiffable {
    
    let id = "NewPartners"
    let name: String
    let items: [PartnersItemModelIG]
    
    init(name: String, items: [PartnersItemModelIG]) {
        self.name = name
        self.items = items
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? NewPartnersModelIG else { return false }
        return id == object.id && name == object.name && items.first?.id == object.items.first?.id
    }
}

final class PartnersItemModelIG: ListDiffable, Equatable {
    static func == (lhs: PartnersItemModelIG, rhs: PartnersItemModelIG) -> Bool {
        return lhs.id == rhs.id
    }
    
    let id: Int
    let name: String?
    let logo: String?
    
    
    init(id: Int, name: String, logo: String) {
        self.id = id
        self.name = name
        self.logo = logo
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? PartnersItemModelIG else { return false }
        return id == object.id && name == object.name
    }
}
