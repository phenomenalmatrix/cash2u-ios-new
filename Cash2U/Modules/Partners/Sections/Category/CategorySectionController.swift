//
//  CategorySectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 18/8/22.
//

import UIKit
import IGListKit

protocol CategorySectionControllerDelegate: AnyObject {
    func didSelectCategory()
}

class CategorySectionController: ListSectionController, ListSupplementaryViewSource {
    
    weak var sectionDelegate: SectionControllerHeaderDelegate? = nil
    weak var delegate: CategorySectionControllerDelegate? = nil
    
    private var item: CategoryModelIG?

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 20, left: 20, bottom: 32, right: 20)
        supplementaryViewSource = self
        minimumLineSpacing = 8
        minimumInteritemSpacing = 7
    }
    
    override func numberOfItems() -> Int {
        return item?.array.count ?? 0
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        
        guard let item = item else {
            return .zero
        }

        if item.type == 0 { 
            if index == 0 || index == 1 {
                return CGSize(width: (context.containerSize.width / 2) - 25, height: (context.containerSize.width / 3) - 19)
            } else {
                return CGSize(width: (context.containerSize.width / 3) - 19, height: (context.containerSize.width / 3) - 19)
            }
        } else {
            return CGSize(width: (context.containerSize.width / 3) - 19, height: (context.containerSize.width / 3) - 19)
        }
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUCategoryView = collectionContext!.dequeueReusableCell(of: CUCategoryView.self, for: self, at: index) as? CUCategoryView else {
            return UICollectionViewCell()
        }
        
        cell.setup(model: (self.item?.array[index])!)
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? CategoryModelIG
    }
    
    override func didSelectItem(at index: Int) {
        delegate?.didSelectCategory()
    }
}

// MARK: - SuplementaryViews

extension CategorySectionController {
    func supportedElementKinds() -> [String] {
        return [UICollectionView.elementKindSectionHeader]
    }

    func viewForSupplementaryElement(ofKind elementKind: String, at index: Int) -> UICollectionReusableView {
        switch elementKind {
        case UICollectionView.elementKindSectionHeader:
            return userHeaderView(atIndex: index)
        default:
            return UICollectionReusableView()
        }
    }
    
    func sizeForSupplementaryView(ofKind elementKind: String, at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: context.containerSize.width - 40, height: 40)
    }
    
    // private funcs
    
    private func userHeaderView(atIndex index: Int) -> UICollectionReusableView {
        guard let headerView = collectionContext?.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, for: self, class: CUSectionHeaderView.self, at: index) as? CUSectionHeaderView else {
            return UICollectionReusableView()
        }
        headerView.delegate = self
        
        headerView.setup(titleText: "Категории")
        return headerView
    }
}

extension CategorySectionController: CUSectionHeaderViewDelegate {
    func viewAllTapped() {
        sectionDelegate?.viewAllTapped(type: .BonusCards)
    }
}
