//
//  CategoryModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 18/8/22.
//

import Foundation
import IGListKit

final class CategoryModelIG: ListDiffable {
    
    let id = "Category"
    let name: String
    let count: Int
    let type: Int
    let array: [CategoryModelItemIG]
    
    init(name: String, count: Int, type: Int, array: [CategoryModelItemIG]) {
        self.name = name
        self.count = count
        self.type = type
        self.array = array
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? CategoryModelIG else { return false }
        return id == object.id && name == object.name
    }
}

final class CategoryModelItemIG: ListDiffable {
    
    let id: Int
    let name: String
    let logo: String
    let partnerCount: Int
    let backgroundColor : String
    
    init(id: Int, name: String, logo: String, partnerCount: Int, backgroundColor: String) {
        self.id = id
        self.name = name
        self.logo = logo
        self.partnerCount = partnerCount
        self.backgroundColor = backgroundColor
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? CategoryModelItemIG else { return false }
        return id == object.id && name == object.name
    }
    
}
