//
//  SearchModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 22/8/22.
//

import Foundation
import IGListKit

final class SearchModelIG: ListDiffable {
    
    let id = "Search"
    let name: String
    
    init(name: String) {
        self.name = name
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? SearchModelIG else { return false }
        return id == object.id && name == object.name
    }
}
