//
//  SearchSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 22/8/22.
//

import UIKit
import IGListKit

protocol SearchSectionControllerDelegate: AnyObject {
    func sectionTapped()
    func filterButtonTapped()
}

class SearchSectionController: ListSectionController {
    
    weak var delegate: SearchSectionControllerDelegate? = nil
    
    private var item: SearchModelIG?

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 2, left: 0, bottom: 16, right: 0)
    }
    
    override func numberOfItems() -> Int {
        return 1
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width) - 36, height: 44)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUSearchView = collectionContext!.dequeueReusableCell(of: CUSearchView.self, for: self, at: index) as? CUSearchView else {
            return UICollectionViewCell()
        }
        cell.delegate = self
        if let item = item {
            
        }
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? SearchModelIG
    }
    
    override func didSelectItem(at index: Int) {
        delegate?.sectionTapped()
    }
}

extension SearchSectionController: CUSearchViewDelegate {
    func filterButtonTapped() {
        delegate?.filterButtonTapped()
    }
}
