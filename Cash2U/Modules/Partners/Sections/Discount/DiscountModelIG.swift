//
//  DiscountModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/8/22.
//

import Foundation
import IGListKit

final class DiscountModelIG: ListDiffable {
    
    let id = "Dicount"
    let name: String
    let items: [DiscountItemsModelIG]
    
    init(name: String, items: [DiscountItemsModelIG]) {
        self.name = name
        self.items = items
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? DiscountModelIG else { return false }
        return id == object.id && name == object.name && items == object.items
    }
}

final class DiscountItemsModelIG: ListDiffable, Equatable {
    static func == (lhs: DiscountItemsModelIG, rhs: DiscountItemsModelIG) -> Bool {
        return lhs.id == rhs.id
    }
    
    let id: Int
    let cover, title, hmtlBody: String?
    let partner: PartnerPromoIG?
    let startDateTime, endDateTime: String?
    
    
    init(id: Int, cover: String?, title: String?, htmlBody: String?, partner: PartnerPromoIG?, startDateTime: String?, endDateTime: String?) {
        self.id = id
        self.cover = cover
        self.title = title
        self.hmtlBody = htmlBody
        self.partner = partner
        self.startDateTime = startDateTime
        self.endDateTime = endDateTime
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? DiscountItemsModelIG else { return false }
        return id == object.id && title == object.title
    }
}

final class PartnerPromoIG: ListDiffable {
    
    let id: Int
    let name: String?
    let logo: String?
    
    
    init(id: Int, name: String?, logo: String?) {
        self.id = id
        self.name = name
        self.logo = logo
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? PartnerPromoIG else { return false }
        return id == object.id && name == object.name
    }
}


//// MARK: - Item
//struct PartnersPromoItem: Codable {
//    let id: Int?
//    let cover, title, hmtlBody: String?
//    let partner: Partner?
//    let startDateTime, endDateTime: String?
//}
//
//// MARK: - Partner
//struct Partner: Codable {
//    let id: Int?
//    let name, logo: String?
//}
