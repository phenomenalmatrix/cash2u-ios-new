//
//  DiscountSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/8/22.
//

import UIKit
import IGListKit

class DiscountSectionController: ListSectionController, ListSupplementaryViewSource {
    
    weak var delegate: SectionControllerHeaderDelegate? = nil
    
    private var item: DiscountModelIG?
    
    required init(isHeaderHidden: Bool = true) {
        super.init()
        self.inset = UIEdgeInsets(top: isHeaderHidden ? 0 : 20, left: 0, bottom: 32, right: 0)
        if !isHeaderHidden {
            supplementaryViewSource = self
        }
    }
    
    override func numberOfItems() -> Int {
        return 1
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width), height: ((context.containerSize.width) / 2.3) + 60)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUDiscountView = collectionContext!.dequeueReusableCell(of: CUDiscountView.self, for: self, at: index) as? CUDiscountView else {
            return UICollectionViewCell()
        }
        
        if let item = item {
            cell.setup(items: item.items ?? [])
        }
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? DiscountModelIG
    }
    
    override func didSelectItem(at index: Int) {
//        delegate?.selectSectionController(self)
//        if let cell = self.collectionContext?.cellForItem(at: index, sectionController: self) as? DZBigStickerCell {
//            if item?.isAnimated == true {
//                cell.animatedLogoImageView.startAnimating()
//            } else {}
//        }
    }
}

// MARK: - SuplementaryViews

extension DiscountSectionController {
    func supportedElementKinds() -> [String] {
        return [UICollectionView.elementKindSectionHeader]
    }

    func viewForSupplementaryElement(ofKind elementKind: String, at index: Int) -> UICollectionReusableView {
        switch elementKind {
        case UICollectionView.elementKindSectionHeader:
            return userHeaderView(atIndex: index)
        default:
            return UICollectionReusableView()
        }
    }
    
    func sizeForSupplementaryView(ofKind elementKind: String, at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: context.containerSize.width - 40, height: 40)
    }
    
    // private funcs
    
    private func userHeaderView(atIndex index: Int) -> UICollectionReusableView {
        guard let headerView = collectionContext?.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, for: self, class: CUSectionHeaderView.self, at: index) as? CUSectionHeaderView else {
            return UICollectionReusableView()
        }
        headerView.delegate = self
        if let item = item {
            headerView.setup(titleText: item.name)
        }
        return headerView
    }
}

extension DiscountSectionController: CUSectionHeaderViewDelegate {
    func viewAllTapped() {
        
    }
}
