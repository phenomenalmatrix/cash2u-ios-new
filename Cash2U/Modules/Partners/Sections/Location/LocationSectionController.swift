//
//  LocationSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 22/8/22.
//

import UIKit
import IGListKit

protocol LocationSectionControllerDelegate: AnyObject {
    func settingsTapped()
}

class LocationSectionController: ListSectionController {
    
    weak var delegate: LocationSectionControllerDelegate? = nil
    
    private var item: LocationModelIG?

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 0, left: 0, bottom: 25, right: 0)
    }
    
    override func numberOfItems() -> Int {
        return 1
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width) - 40, height: 50)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CULocationView = collectionContext!.dequeueReusableCell(of: CULocationView.self, for: self, at: index) as? CULocationView else {
            return UICollectionViewCell()
        }
        
        if let item = item {
            cell.setup(type: item.type)
        }
        
//        cell.delegate = self
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? LocationModelIG
    }
    
}

extension LocationSectionController: CUGreetingsViewDelegate {
    func settingsTapped() {
        delegate?.settingsTapped()
    }
}
