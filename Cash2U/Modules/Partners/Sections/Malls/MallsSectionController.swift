//
//  MallsSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/8/22.
//

import UIKit
import IGListKit

protocol MallsSectionControllerDelegate: AnyObject {
    func didSelectMall()
}

class MallsSectionController: ListSectionController, ListSupplementaryViewSource {
    
    weak var sectionDelegate: SectionControllerHeaderDelegate? = nil
    weak var delegate: MallsSectionControllerDelegate? = nil
    
    private var item: MallsModelIG?
    private var isHeaderButtonHidden: Bool = true

    required init(isHeaderButtonHidden: Bool = true) {
        self.isHeaderButtonHidden = isHeaderButtonHidden
        super.init()
        self.inset = UIEdgeInsets(top: 20, left: 0, bottom: 32, right: 0)
        supplementaryViewSource = self
    }
    
    override func numberOfItems() -> Int {
        return 1
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width), height: 145)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUMallsView = collectionContext!.dequeueReusableCell(of: CUMallsView.self, for: self, at: index) as? CUMallsView else {
            return UICollectionViewCell()
        }
        
        cell.delegate = self
        
        if let item = item {
            cell.setup(items: item.items ?? [])
        }
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? MallsModelIG
    }
}

// MARK: - SuplementaryViews

extension MallsSectionController: CUMallsViewDelegate {
    func didSelectMall() {
        delegate?.didSelectMall()
    }
}

// MARK: - SuplementaryViews

extension MallsSectionController {
    func supportedElementKinds() -> [String] {
        return [UICollectionView.elementKindSectionHeader]
    }

    func viewForSupplementaryElement(ofKind elementKind: String, at index: Int) -> UICollectionReusableView {
        switch elementKind {
        case UICollectionView.elementKindSectionHeader:
            return userHeaderView(atIndex: index)
        default:
            return UICollectionReusableView()
        }
    }
    
    func sizeForSupplementaryView(ofKind elementKind: String, at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: context.containerSize.width - 40, height: 40)
    }
    
    // private funcs
    
    private func userHeaderView(atIndex index: Int) -> UICollectionReusableView {
        guard let headerView = collectionContext?.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, for: self, class: CUSectionHeaderView.self, at: index) as? CUSectionHeaderView else {
            return UICollectionReusableView()
        }
        headerView.delegate = self
        if let item = item {
            headerView.setup(titleText: item.name, isRightButtonHidden: isHeaderButtonHidden)
        }
        return headerView
    }
}

extension MallsSectionController: CUSectionHeaderViewDelegate {
    func viewAllTapped() {
        sectionDelegate?.viewAllTapped(type: .BestPartners)
    }
}
