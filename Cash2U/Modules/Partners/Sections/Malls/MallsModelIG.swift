//
//  MallsModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/8/22.
//

import Foundation
import IGListKit

final class MallsModelIG: ListDiffable {
    
    let id = "Malls"
    let name: String
    let items: [MallItemsIG]?
    
    init(name: String, items: [MallItemsIG]?) {
        self.name = name
        self.items = items
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? MallsModelIG else { return false }
        return id == object.id && name == object.name && items?.first?.id == object.items?.first?.id
    }
}

final class MallItemsIG: ListDiffable {
    let id: Int
    let type: String
    let name: String
    let logo: String
    let partnersCount: Int
    
    init(id: Int, type: String, name: String, logo: String, partnersCount: Int) {
        self.id = id
        self.type =  type
        self.name = name
        self.logo = logo
        self.partnersCount = partnersCount
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? MallItemsIG else { return false }
        return id == object.id && name == object.name
    }
}
