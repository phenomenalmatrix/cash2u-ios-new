//
//  BazaarModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/8/22.
//

import Foundation
import IGListKit

final class BazaarModelIG: ListDiffable {
    
    let id = "Bazaar"
    let name: String
    let items: [MallItemsIG]
    
    init(name: String, items: [MallItemsIG]) {
        self.name = name
        self.items = items
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? BazaarModelIG else { return false }
        return id == object.id && name == object.name && items.first?.id == object.items.first?.id
    }
}


