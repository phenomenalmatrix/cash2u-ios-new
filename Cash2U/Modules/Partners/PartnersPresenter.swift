//
//  PartnersPresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/5/22.
//  
//

import Foundation

final class PartnersPresenter {
    
    weak var view: PartnersViewProtocol?
    var interactor: PartnersInteractorProtocol?
    var router: PartnersRouterProtocol?
    
}

extension PartnersPresenter: PartnersViewToPresenterProtocol {
    func getPartnersPromo() {
        interactor?.getPartnersPromo()
    }
    
    func getMarkets() {
        interactor?.getMarkets()
    }
    
    func getMalls() {
        interactor?.getMalls()
    }
    
    func getPartners() {
        interactor?.getPartners()
    }
    
    func getCategories() {
        interactor?.getCategories()
    }
}

extension PartnersPresenter: PartnersInteractorToPresenterProtocol {
    func recivePartnersPromoSuccess(promo: PartnersPromoModel) {
        view?.recivePartnersPromo(promo: promo)
    }
    
    func reciveMarketsSuccess(markets: MallResponse) {
        view?.reciveMarketsToView(markets: markets)
    }
    
    func reciveMallsSuccess(malls: MallResponse) {
        view?.reciveMallsToView(malls: malls)
    }
    
    func recivePartnersSuccess(partners: PartnerResponse) {
        view?.recivePartnersToView(partners: partners)
    }
    
    func reciveError(error: Error) {
        view?.reciveError(error: error)
    }
    
    func reciveCategoriesSuccess(categories: CategoriesModel) {
        view?.reciveCategoriesToView(categories: categories)
    }
    
}
