//
//  PartnersProtocols.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/5/22.
//  
//

import Foundation

protocol PartnersViewProtocol: AnyObject {
    func reciveCategoriesToView(categories: CategoriesModel)
    func recivePartnersToView(partners: PartnerResponse)
    func reciveMallsToView(malls: MallResponse)
    func reciveMarketsToView(markets: MallResponse)
    func recivePartnersPromo(promo: PartnersPromoModel)
    func reciveError(error: Error)
}

protocol PartnersViewToPresenterProtocol: AnyObject {
    func getCategories()
    func getPartners()
    func getMalls()
    func getMarkets()
    func getPartnersPromo()
}

protocol PartnersInteractorProtocol: AnyObject {
    func getCategories()
    func getPartners()
    func getMalls()
    func getMarkets()
    func getPartnersPromo()
}

protocol PartnersInteractorToPresenterProtocol: AnyObject {
    func reciveCategoriesSuccess(categories: CategoriesModel)
    func recivePartnersSuccess(partners: PartnerResponse)
    func reciveMallsSuccess(malls: MallResponse)
    func reciveMarketsSuccess(markets: MallResponse)
    func recivePartnersPromoSuccess(promo: PartnersPromoModel)
    func reciveError(error: Error)
}

protocol PartnersRouterProtocol: AnyObject {
}
