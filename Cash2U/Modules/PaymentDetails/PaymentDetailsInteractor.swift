//
//  PaymentDetailsInteractor.swift
//  Cash2U
//
//  Created by talgar osmonov on 7/10/22.
//  
//

import Foundation

final class PaymentDetailsInteractor {
    weak var presenter: PaymentDetailsInteractorToPresenterProtocol?
}

extension PaymentDetailsInteractor: PaymentDetailsInteractorProtocol {
}
