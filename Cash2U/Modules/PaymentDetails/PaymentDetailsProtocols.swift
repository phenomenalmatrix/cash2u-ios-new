//
//  PaymentDetailsProtocols.swift
//  Cash2U
//
//  Created by talgar osmonov on 7/10/22.
//  
//

import Foundation

protocol PaymentDetailsViewProtocol: AnyObject {
}

protocol PaymentDetailsViewToPresenterProtocol: AnyObject {
}

protocol PaymentDetailsInteractorProtocol: AnyObject {
}

protocol PaymentDetailsInteractorToPresenterProtocol: AnyObject {
}

protocol PaymentDetailsRouterProtocol: AnyObject {
}
