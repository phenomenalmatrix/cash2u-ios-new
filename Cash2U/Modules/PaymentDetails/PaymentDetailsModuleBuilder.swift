//
//  PaymentDetailsModuleBuilder.swift
//  Cash2U
//
//  Created by talgar osmonov on 7/10/22.
//  
//

import UIKit

final class PaymentDetailsModuleConfigurator {
    /// Собрать модуль
    class func build() -> UIViewController {
        let view = PaymentDetailsViewController()
        let presenter = PaymentDetailsPresenter()
        let interactor = PaymentDetailsInteractor()
        let router = PaymentDetailsRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
