//
//  PaymentDetailsViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 7/10/22.
//  
//

import UIKit

final class PaymentDetailsViewController: CUViewController {
    
    var presenter: PaymentDetailsViewToPresenterProtocol?
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private lazy var titleLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Детали оплаты", size: 28, numberOfLines: 2, fontType: .bold)
        return view
    }()
    
    private lazy var backView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .thirdaryColor
        view.cornerRadius = 18
        return view
    }()
    
    private lazy var carrierLablel: CUBlankView = {
        let view = CUBlankView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(placeholder: "Вноситель:", text: "Бекболиев А. А.", placeholderSize: 16, textSize: 16, textColor: .mainTextColor)
        return view
    }()
    
    private lazy var walletNumberLablel: CUBlankView = {
        let view = CUBlankView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(placeholder: "Номер кошелька:", text: "+996 999 999 999", placeholderSize: 16, textSize: 16, textColor: .mainTextColor)
        return view
    }()
    
    private lazy var amountLablel: CUBlankView = {
        let view = CUBlankView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(placeholder: "Сумма:", text: "1 000,00 с", placeholderSize: 16, textSize: 16, textColor: .mainTextColor, isBottomLineHidden: true)
        return view
    }()
    
    private lazy var commissionLablel: CUBlankView = {
        let view = CUBlankView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(placeholder: "Комиссия:", text: "10,00 c", placeholderSize: 16, textSize: 17, textColor: .mainTextColor, isBottomLineHidden: true, isTopLineHidden: true)
        return view
    }()
    
    private lazy var totalLablel: CUBlankView = {
        let view = CUBlankView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(placeholder: "Итого:", text: "12000,00 c", placeholderSize: 16, textSize: 17, textColor: .mainTextColor, isBottomLineHidden: true, isTopLineHidden: false)
        return view
    }()
    
    private lazy var confirmButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Подтвердить платеж")
        view.addTarget(self, action: #selector(onConfirmButtonTapped), for: .touchUpInside)
        return view
    }()
    
    override func loadView() {
        super.loadView()
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - UIActions
    
    @objc private func onConfirmButtonTapped() {
        SwiftEntryKitService.shared.success {
            let vc = CheckModuleConfigurator.build()
            self.navigationController?.pushViewController(vc, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                SwiftEntryKitService.shared.dissmiss()
            }
        }
    }
}

extension PaymentDetailsViewController: PaymentDetailsViewProtocol {
}

extension PaymentDetailsViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        
        view.addSubview(titleLabel)
        view.addSubview(backView)
        view.addSubviews([carrierLablel, walletNumberLablel, amountLablel, commissionLablel, totalLablel, confirmButton])
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top).offset(20)
            make.leading.trailing.equalToSuperview().inset(20)
        }
        
        backView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(20)
            make.leading.trailing.equalToSuperview().inset(20)
            make.height.equalTo(165)
        }
        
        carrierLablel.snp.makeConstraints { make in
            make.top.equalTo(backView.snp.top).offset(8)
            make.leading.trailing.equalToSuperview().inset(35)
            make.height.equalTo(50)
        }
        
        walletNumberLablel.snp.makeConstraints { make in
            make.top.equalTo(carrierLablel.snp.bottom)
            make.leading.trailing.equalToSuperview().inset(35)
            make.height.equalTo(50)
        }
        
        amountLablel.snp.makeConstraints { make in
            make.top.equalTo(walletNumberLablel.snp.bottom)
            make.leading.trailing.equalToSuperview().inset(35)
            make.height.equalTo(50)
        }
        
        commissionLablel.snp.makeConstraints { make in
            make.top.equalTo(backView.snp.bottom).offset(20)
            make.leading.trailing.equalToSuperview().inset(20)
            make.height.equalTo(50)
        }
        
        totalLablel.snp.makeConstraints { make in
            make.top.equalTo(commissionLablel.snp.bottom)
            make.leading.trailing.equalToSuperview().inset(20)
            make.height.equalTo(50)
        }
        
        confirmButton.snp.makeConstraints { make in
            make.bottom.equalTo(view.safeArea.bottom).offset(-16)
            make.leading.trailing.equalToSuperview().inset(20)
            make.height.equalTo(ScreenHelper.mainButtonHeight)
        }
    }
}
