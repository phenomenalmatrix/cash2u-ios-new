//
//  PaymentDetailsPresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 7/10/22.
//  
//

import Foundation

final class PaymentDetailsPresenter {
    
    weak var view: PaymentDetailsViewProtocol?
    var interactor: PaymentDetailsInteractorProtocol?
    var router: PaymentDetailsRouterProtocol?
    
}

extension PaymentDetailsPresenter: PaymentDetailsViewToPresenterProtocol {
}

extension PaymentDetailsPresenter: PaymentDetailsInteractorToPresenterProtocol {
}
