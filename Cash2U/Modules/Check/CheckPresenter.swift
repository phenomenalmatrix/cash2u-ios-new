//
//  CheckPresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 7/10/22.
//  
//

import Foundation

final class CheckPresenter {
    
    weak var view: CheckViewProtocol?
    var interactor: CheckInteractorProtocol?
    var router: CheckRouterProtocol?
    
}

extension CheckPresenter: CheckViewToPresenterProtocol {
}

extension CheckPresenter: CheckInteractorToPresenterProtocol {
}
