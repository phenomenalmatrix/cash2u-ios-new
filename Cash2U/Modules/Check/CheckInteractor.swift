//
//  CheckInteractor.swift
//  Cash2U
//
//  Created by talgar osmonov on 7/10/22.
//  
//

import Foundation

final class CheckInteractor {
    weak var presenter: CheckInteractorToPresenterProtocol?
}

extension CheckInteractor: CheckInteractorProtocol {
}
