//
//  CheckViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 7/10/22.
//  
//

import UIKit

final class CheckViewController: CUViewController {
    
    var presenter: CheckViewToPresenterProtocol?
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private lazy var snapshotView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var titleLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "№1231823", size: 28, numberOfLines: 1, fontType: .bold)
        return view
    }()
    
    private lazy var dateLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "16.06.2022 12:34", size: 18, numberOfLines: 1, fontType: .semibold, titleColor: .grayColor)
        return view
    }()
    
    private lazy var carrierLablel: CUBlankView = {
        let view = CUBlankView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(placeholder: "Вноситель:", text: "Бекболиев А. А.", placeholderSize: 16, textSize: 16, textColor: .mainTextColor)
        return view
    }()
    
    private lazy var walletNumberLablel: CUBlankView = {
        let view = CUBlankView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(placeholder: "Номер кошелька:", text: "+996 999 999 999", placeholderSize: 16, textSize: 16, textColor: .mainTextColor)
        return view
    }()
    
    private lazy var amountLablel: CUBlankView = {
        let view = CUBlankView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(placeholder: "Сумма:", text: "1 010,00 с", placeholderSize: 16, textSize: 16, textColor: .mainTextColor, isBottomLineHidden: true)
        return view
    }()
    
    private lazy var confirmButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Ок")
        view.addTarget(self, action: #selector(onConfirmButtonTapped), for: .touchUpInside)
        return view
    }()
    
    private lazy var saveButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Сохранить", titleColor: .grayColor, backgroundColor: .clear, borderWidth: 1, borderColor: .grayColor)
        view.addTarget(self, action: #selector(onSaveButtonTapped), for: .touchUpInside)
        return view
    }()
    
    override func loadView() {
        super.loadView()
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - UIActions
    
    @objc private func onSaveButtonTapped() {
        DispatchQueue.main.async {
            self.snapshotView.backgroundColor = .mainDarkColor
            let image = self.snapshotView.asImage()
            let data = [image] as [Any]
            UIApplication.share(data)
        }
    }
    
    @objc private func onConfirmButtonTapped() {
        for controller in self.navigationController!.viewControllers as Array {
            if controller is Nol3InstallmentViewController {
                self.navigationController?.popToViewController(controller, animated: true)
            }
        }
    }
}

extension CheckViewController: CheckViewProtocol {
}

extension CheckViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        
        view.addSubview(snapshotView)
        snapshotView.addSubviews([titleLabel, dateLabel])
        snapshotView.addSubviews([carrierLablel, walletNumberLablel, amountLablel, confirmButton, saveButton])
        view.addSubviews([confirmButton, saveButton])
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        snapshotView.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(260)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(20)
            make.leading.trailing.equalToSuperview().inset(20)
        }
        
        dateLabel.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(5)
            make.leading.trailing.equalToSuperview().inset(20)
        }
        
        carrierLablel.snp.makeConstraints { make in
            make.top.equalTo(dateLabel.snp.bottom).offset(20)
            make.leading.trailing.equalToSuperview().inset(20)
            make.height.equalTo(50)
        }
        
        walletNumberLablel.snp.makeConstraints { make in
            make.top.equalTo(carrierLablel.snp.bottom)
            make.leading.trailing.equalToSuperview().inset(20)
            make.height.equalTo(50)
        }
        
        amountLablel.snp.makeConstraints { make in
            make.top.equalTo(walletNumberLablel.snp.bottom)
            make.leading.trailing.equalToSuperview().inset(20)
            make.height.equalTo(50)
        }
        
        confirmButton.snp.makeConstraints { make in
            make.bottom.equalTo(view.safeArea.bottom).offset(-16)
            make.leading.trailing.equalToSuperview().inset(20)
            make.height.equalTo(ScreenHelper.mainButtonHeight)
        }
        
        saveButton.snp.makeConstraints { make in
            make.bottom.equalTo(confirmButton.snp.top).offset(-10)
            make.leading.trailing.equalToSuperview().inset(20)
            make.height.equalTo(ScreenHelper.mainButtonHeight)
        }
    }
}
