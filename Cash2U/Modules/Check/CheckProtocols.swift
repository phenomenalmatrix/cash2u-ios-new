//
//  CheckProtocols.swift
//  Cash2U
//
//  Created by talgar osmonov on 7/10/22.
//  
//

import Foundation

protocol CheckViewProtocol: AnyObject {
}

protocol CheckViewToPresenterProtocol: AnyObject {
}

protocol CheckInteractorProtocol: AnyObject {
}

protocol CheckInteractorToPresenterProtocol: AnyObject {
}

protocol CheckRouterProtocol: AnyObject {
}
