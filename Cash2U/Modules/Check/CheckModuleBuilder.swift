//
//  CheckModuleBuilder.swift
//  Cash2U
//
//  Created by talgar osmonov on 7/10/22.
//  
//

import UIKit

final class CheckModuleConfigurator {
    /// Собрать модуль
    class func build() -> UIViewController {
        let view = CheckViewController()
        let presenter = CheckPresenter()
        let interactor = CheckInteractor()
        let router = CheckRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
