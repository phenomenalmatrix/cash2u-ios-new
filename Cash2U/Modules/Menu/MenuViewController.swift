//
//  MenuViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 15/8/22.
//  
//

import UIKit

final class MenuViewController: CUBellViewController {
    
    var presenter: MenuViewToPresenterProtocol?
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private lazy var navLabel: CULabelView = {
        let view = CULabelView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Оплата частями", size: 26, fontType: .semibold, isSizeToFit: true)
        return view
    }()
    
    private lazy var headerView = UIHeaderView()
    
    private lazy var menuTableView: CUTableView = {
        let view = CUTableView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.delegate = self
        view.dataSource = self
        view.alwaysBounceVertical = true
        view.contentInset = .init(top: 195, left: 0, bottom: 0, right: 0)
        view.register(MenuCell.self)
        return view
    }()
    
    
    
    override func loadView() {
        super.loadView()
        setNavBar()
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    // MARK: - Helpers
    
    func setNavBar() {
        let cityItem = UIBarButtonItem.init(customView: navLabel)
        navigationItem.leftBarButtonItems = [cityItem]
    }
}

extension MenuViewController: MenuViewProtocol {
}

extension MenuViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        KeychainService.shared.deleteAll()
        UserDefaultsService.shared.removeAll()
        self.tabBarController?.dismiss(animated: true) {
            let loginResponse = ["becomeFirstResponder": true]
            NotificationCenter.default
                .post(name: .phoneBecomeFirstResponder, object: nil, userInfo: loginResponse)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(cellType: MenuCell.self, for: indexPath)
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
}

extension MenuViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        
        view.addSubview(menuTableView)
//        view.addSubview(headerView)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        menuTableView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
//        headerView.snp.makeConstraints { make in
//            make.top.equalToSuperview()
//            make.leading.trailing.equalToSuperview()
//            make.height.equalTo(220)
//        }
    }
}
