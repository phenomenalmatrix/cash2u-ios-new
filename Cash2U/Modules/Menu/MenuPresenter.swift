//
//  MenuPresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 15/8/22.
//  
//

import Foundation

final class MenuPresenter {
    
    weak var view: MenuViewProtocol?
    var interactor: MenuInteractorProtocol?
    var router: MenuRouterProtocol?
    
}

extension MenuPresenter: MenuViewToPresenterProtocol {
}

extension MenuPresenter: MenuInteractorToPresenterProtocol {
}
