//
//  MenuModuleBuilder.swift
//  Cash2U
//
//  Created by talgar osmonov on 15/8/22.
//  
//

import UIKit

final class MenuModuleBuilder {
    /// Собрать модуль
    class func build() -> UIViewController {
        let view = MenuViewController()
        let presenter = MenuPresenter()
        let interactor = MenuInteractor()
        let router = MenuRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
