//
//  MenuStatusModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 18/10/22.
//

import UIKit
import IGListKit

enum IdentificationStatus: String {
    case nol3 = "nol3"
    case nol6 = "nol6"
    case fuel = "fuel"
    case product = "product"
}

final class MenuStatusModelIG: ListDiffable {
    
    let id = UUID().uuidString
    let type: IdentificationStatus
    
    init(type: IdentificationStatus) {
        self.type = type
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? MenuStatusModelIG else { return false }
        return id == object.id && type == object.type
    }
}
