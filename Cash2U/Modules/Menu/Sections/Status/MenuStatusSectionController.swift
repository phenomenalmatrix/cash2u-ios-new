//
//  MenuStatusSectionController.swift
//  Cash2U
//
//  Created by talgar osmonov on 18/10/22.
//

import UIKit
import IGListKit

protocol MenuStatusSectionControllerDelegate: AnyObject {
    
}

class MenuStatusSectionController: ListSectionController {
    
    weak var delegate: MenuStatusSectionControllerDelegate? = nil
    
    private var item: MenuStatusModelIG?
    
    required override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
    }
    
    override func numberOfItems() -> Int {
        return 1
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width) - 40, height: ((context.containerSize.width) / 3))
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUInstallmentCell = collectionContext!.dequeueReusableCell(of: CUInstallmentCell.self, for: self, at: index) as? CUInstallmentCell else {
            return UICollectionViewCell()
        }
        
        if let item = item {
            
        }
        return cell
    }

    override func didUpdate(to object: Any) {
        item = object as? MenuStatusModelIG
    }
    
    override func didSelectItem(at index: Int) {
        if let item {
            
        }
    }
}
