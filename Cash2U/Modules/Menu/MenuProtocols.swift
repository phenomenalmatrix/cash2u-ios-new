//
//  MenuProtocols.swift
//  Cash2U
//
//  Created by talgar osmonov on 15/8/22.
//  
//

import Foundation

protocol MenuViewProtocol: AnyObject {
}

protocol MenuViewToPresenterProtocol: AnyObject {
}

protocol MenuInteractorProtocol: AnyObject {
}

protocol MenuInteractorToPresenterProtocol: AnyObject {
}

protocol MenuRouterProtocol: AnyObject {
}
