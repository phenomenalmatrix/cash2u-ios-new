//
//  PayQRViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 15/8/22.
//  
//

import UIKit

final class PayQRViewController: CUViewController {
    
    var presenter: PayQRViewToPresenterProtocol?
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = .secondaryColor
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension PayQRViewController: PayQRViewProtocol {
}

extension PayQRViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
    }
}

// MARK: - PanModalPresentable

extension PayQRViewController: PanModalPresentable {
    func panModalDidDismiss() {
        didDismissDelegate?.didDismiss()
    }
    var panScrollable: UIScrollView? {
        return nil
    }
    
    var longFormHeight: PanModalHeight {
        .contentHeight(ScreenHelper.getHeight / 2)
    }
    
    var shortFormHeight: PanModalHeight {
        .contentHeight(ScreenHelper.getHeight / 2)
    }
}

