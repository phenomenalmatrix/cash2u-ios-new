//
//  PayQRPresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 15/8/22.
//  
//

import Foundation

final class PayQRPresenter {
    
    weak var view: PayQRViewProtocol?
    var interactor: PayQRInteractorProtocol?
    var router: PayQRRouterProtocol?
    
}

extension PayQRPresenter: PayQRViewToPresenterProtocol {
}

extension PayQRPresenter: PayQRInteractorToPresenterProtocol {
}
