//
//  PayQRProtocols.swift
//  Cash2U
//
//  Created by talgar osmonov on 15/8/22.
//  
//

import Foundation

protocol PayQRViewProtocol: AnyObject {
}

protocol PayQRViewToPresenterProtocol: AnyObject {
}

protocol PayQRInteractorProtocol: AnyObject {
}

protocol PayQRInteractorToPresenterProtocol: AnyObject {
}

protocol PayQRRouterProtocol: AnyObject {
}
