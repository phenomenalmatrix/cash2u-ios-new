//
//  PayQRModuleBuilder.swift
//  Cash2U
//
//  Created by talgar osmonov on 15/8/22.
//  
//

import UIKit

final class PayQRModuleBuilder {
    /// Собрать модуль
    class func build() -> UIViewController & PanModalPresentable {
        let view = PayQRViewController()
        let presenter = PayQRPresenter()
        let interactor = PayQRInteractor()
        let router = PayQRRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
