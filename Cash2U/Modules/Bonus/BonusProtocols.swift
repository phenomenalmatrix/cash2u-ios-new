//
//  BonusProtocols.swift
//  Cash2U
//
//  Created by Oroz on 30/9/22.
//  
//

import Foundation

protocol BonusViewProtocol: AnyObject {
}

protocol BonusViewToPresenterProtocol: AnyObject {
}

protocol BonusInteractorProtocol: AnyObject {
}

protocol BonusInteractorToPresenterProtocol: AnyObject {
}

protocol BonusRouterProtocol: AnyObject {
}
