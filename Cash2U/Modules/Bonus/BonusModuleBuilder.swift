//
//  BonusModuleBuilder.swift
//  Cash2U
//
//  Created by Oroz on 30/9/22.
//  
//

import UIKit

final class BonusModuleConfigurator {
    /// Собрать модуль
    class func build() -> UIViewController {
        let view = BonusViewController()
        let presenter = BonusPresenter()
        let interactor = BonusInteractor()
        let router = BonusRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
