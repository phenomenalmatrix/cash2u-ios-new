//
//  BonusSectionController.swift
//  Cash2U
//
//  Created by Oroz on 4/10/22.
//

import Foundation
import IGListKit

class BonusTodaySectionController: ListSectionController, ListSupplementaryViewSource {
    private var item: BonusesTodayModelIG?
    
    weak var delegate: SectionControllerDelegate? = nil

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.supplementaryViewSource = self
    }
    
    override func numberOfItems() -> Int {
        return item?.history.count ?? 0
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width) - 40, height: ((context.containerSize.width) / 2.3))
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUBonusView = collectionContext!.dequeueReusableCell(of: CUBonusView.self, for: self, at: index) as? CUBonusView else {
            return UICollectionViewCell()
        }
        
        cell.setup()
        cell.addBottomBorder(with: .gray, andWidth: 1)
        return cell
    }
    
    override func didSelectItem(at index: Int) {
        delegate?.selectSection(self)
    }

    override func didUpdate(to object: Any) {
        item = object as? BonusesTodayModelIG
    }
    
}

class BonusYesterdaySectionController: ListSectionController, ListSupplementaryViewSource  {
    private var item: BonusesYesterdayModelIG?
    
    weak var delegate: SectionControllerDelegate? = nil

    override init() {
        super.init()
        self.inset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.supplementaryViewSource = self
    }
    
    override func numberOfItems() -> Int {
        return item?.history.count ?? 0
    }

    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: (context.containerSize.width) - 40, height: ((context.containerSize.width) / 2.3))
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell: CUBonusView = collectionContext!.dequeueReusableCell(of: CUBonusView.self, for: self, at: index) as? CUBonusView else {
            return UICollectionViewCell()
        }
        
        cell.setup()
        cell.addBottomBorder(with: .gray, andWidth: 1)
        return cell
    }
    
    override func didSelectItem(at index: Int) {
        delegate?.selectSection(self)
    }

    override func didUpdate(to object: Any) {
        item = object as? BonusesYesterdayModelIG
    }
    
}

extension BonusTodaySectionController {
    func supportedElementKinds() -> [String] {
        return [UICollectionView.elementKindSectionHeader]
    }

    func viewForSupplementaryElement(ofKind elementKind: String, at index: Int) -> UICollectionReusableView {
        switch elementKind {
        case UICollectionView.elementKindSectionHeader:
            return userHeaderView(atIndex: index)
        default:
            return UICollectionReusableView()
        }
    }
    
    func sizeForSupplementaryView(ofKind elementKind: String, at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: context.containerSize.width - 40, height: 60)
    }
    
    // private funcs
    
    private func userHeaderView(atIndex index: Int) -> UICollectionReusableView {
        guard let headerView = collectionContext?.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, for: self, class: CUSectionHeaderView.self, at: index) as? CUSectionHeaderView else {
            return UICollectionReusableView()
        }
        headerView.setup(titleText: "Сегодня", isRightButtonHidden: true)
        return headerView
    }
}

extension BonusYesterdaySectionController {
    func supportedElementKinds() -> [String] {
        return [UICollectionView.elementKindSectionHeader]
    }

    func viewForSupplementaryElement(ofKind elementKind: String, at index: Int) -> UICollectionReusableView {
        switch elementKind {
        case UICollectionView.elementKindSectionHeader:
            return userHeaderView(atIndex: index)
        default:
            return UICollectionReusableView()
        }
    }

    func sizeForSupplementaryView(ofKind elementKind: String, at index: Int) -> CGSize {
        guard let context = collectionContext else {
            return .zero
        }
        return CGSize(width: context.containerSize.width - 40, height: 60)
    }

    // private funcs

    private func userHeaderView(atIndex index: Int) -> UICollectionReusableView {
        guard let headerView = collectionContext?.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, for: self, class: CUSectionHeaderView.self, at: index) as? CUSectionHeaderView else {
            return UICollectionReusableView()
        }
        headerView.setup(titleText: "Вчера", isRightButtonHidden: true)
        return headerView
    }
}
