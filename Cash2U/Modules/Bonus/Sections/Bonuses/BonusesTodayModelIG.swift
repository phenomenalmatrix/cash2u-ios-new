//
//  BonusesTodayModelIG.swift
//  Cash2U
//
//  Created by Oroz on 4/10/22.
//

import Foundation
import IGListDiffKit

final class BonusesTodayModelIG: ListDiffable {
    
    let id: String
    let history: [BonusItem]
    
    init (id: String, history: [BonusItem]){
        self.id = id
        self.history = history
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? BonusesTodayModelIG else { return false }
        return id == object.id && history.last?.bonus == object.history.last?.bonus
    }
    
}

final class BonusesYesterdayModelIG: ListDiffable {
    
    let id: String
    let history: [BonusItem]
    
    init (id: String, history: [BonusItem]){
        self.id = id
        self.history = history
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? BonusesTodayModelIG else { return false }
        return id == object.id && history.last?.bonus == object.history.last?.bonus
    }
    
}

enum WichDayType {
    case TODAY
    case YESTERDAY
}
