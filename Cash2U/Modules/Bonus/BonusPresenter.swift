//
//  BonusPresenter.swift
//  Cash2U
//
//  Created by Oroz on 30/9/22.
//  
//

import Foundation

final class BonusPresenter {
    
    weak var view: BonusViewProtocol?
    var interactor: BonusInteractorProtocol?
    var router: BonusRouterProtocol?
    
}

extension BonusPresenter: BonusViewToPresenterProtocol {
}

extension BonusPresenter: BonusInteractorToPresenterProtocol {
}
