//
//  BonusInteractor.swift
//  Cash2U
//
//  Created by Oroz on 30/9/22.
//  
//

import Foundation

final class BonusInteractor {
    weak var presenter: BonusInteractorToPresenterProtocol?
}

extension BonusInteractor: BonusInteractorProtocol {
}
