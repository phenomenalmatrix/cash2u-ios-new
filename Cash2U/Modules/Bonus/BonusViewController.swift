//
//  BonusViewController.swift
//  Cash2U
//
//  Created by Oroz on 30/9/22.
//  
//

import UIKit
import IGListDiffKit
import IGListKit

final class BonusViewController: CUViewController {
    
    var presenter: BonusViewToPresenterProtocol?
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private lazy var viewBlur: UIView = {
        let view = UIView()
        return view
    }()
    
    private lazy var bonusTitle: CULabelView = {
        let view = CULabelView()
        view.setup(title: "Начислено баллов:", size: 13, fontType: .medium)
        return view
    }()
    
    private lazy var bonusTitleCount: CUDoubleAmountLabelView = {
       let view = CUDoubleAmountLabelView()
        view.setup(summ: 478.00,
                   colorForInteger: .mainTextColor ?? .white,
                   colorForTheRest: UIColor.init(hex: "#5F5AFF"),
                   fontForInteger: UIFont.systemFont(ofSize: 38, weight: .bold),
                   fontForTheRemainder: UIFont.systemFont(ofSize: 38, weight: .bold))
        return view
    }()
    
    private lazy var datePickerFrom: UIDatePicker = {
        let view = UIDatePicker()
        view.datePickerMode = .date
        view.addTarget(self, action: #selector(pressDatePrickerFrom(sender:)), for: UIControl.Event.valueChanged)
        view.frame.size = CGSize(width: 0, height: 250)
        if #available(iOS 13.4, *) {
            view.preferredDatePickerStyle = .wheels
        }
        view.locale = NSLocale(localeIdentifier: "en_GB") as Locale
        return view
    }()
    
    private lazy var datePickerTo: UIDatePicker = {
        let view = UIDatePicker()
        view.datePickerMode = .date
        view.addTarget(self, action: #selector(pressDatePrickerTo(sender:)), for: UIControl.Event.valueChanged)
        view.frame.size = CGSize(width: 0, height: 250)
        if #available(iOS 13.4, *) {
            view.preferredDatePickerStyle = .wheels
        }
        view.locale = NSLocale(localeIdentifier: "en_GB") as Locale
        return view
    }()
    
    private lazy var fromDateSelctor: CUDateSelectorView = {
        let view = CUDateSelectorView()
        view.setup(type: .FROM, date: Date())
        return view
    }()
    
    private lazy var toDateSelctor: CUDateSelectorView = {
        let view = CUDateSelectorView()
        view.setup(type: .TO, date: Date())
        return view
    }()
    
    private lazy var adapter: ListAdapter = {
        let adapter = ListAdapter(updater: ListAdapterUpdater(), viewController: self)
        adapter.collectionView = newCollection
        adapter.dataSource = self
        adapter.scrollViewDelegate = self
        return adapter
    }()

    private lazy var newCollection: CUCollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        let mainCollection = CUCollectionView(
            frame: CGRect.zero,
            collectionViewLayout: flowLayout
        )
        mainCollection.translatesAutoresizingMaskIntoConstraints = false
        mainCollection.alwaysBounceVertical = true
        return mainCollection
    }()
    
    private lazy var animator: UIViewPropertyAnimator = {
       let view =  UIViewPropertyAnimator(duration: 1.5, curve: .easeInOut, animations: {
             self.viewBlur.snp.remakeConstraints { make in
                 make.top.equalTo(self.view.safeArea.top)
                 make.size.equalTo(CGSize(width: self.view.frame.width, height: 0))
             }
         })
        return view
    }()
    
    var height = 100.0

    ///date
    ///items{
        
    ///}
    ///
    ///
    ///
    ///
    
    private lazy var itemsData: [ListDiffable] = createItemsData()

    private func createItemsData() -> [ListDiffable] {
        var aa = [
        ] as [ListDiffable]
        aa.append(
            BonusesTodayModelIG(id: "0", history: [
                BonusItem(date: "22:12", bonus: 23.1, type: "today", comment: "asdasdasdasdas"),
                BonusItem(date: "22:12", bonus: 23.1, type: "today", comment: "asdasdasdasdas"),
                BonusItem(date: "22:12", bonus: 23.1, type: "today", comment: "asdasdasdasdas"),
                BonusItem(date: "22:12", bonus: 23.1, type: "today", comment: "asdasdasdasdas"),
                BonusItem(date: "22:12", bonus: 23.1, type: "today", comment: "asdasdasdasdas"),
                BonusItem(date: "22:12", bonus: 23.1, type: "today", comment: "asdasdasdasdas")
            ]))
        aa.append(
            BonusesYesterdayModelIG(id: "1", history: [
                BonusItem(date: "22:12", bonus: 23.1, type: "today", comment: "asdasdasdasdas"),
                BonusItem(date: "22:12", bonus: 23.1, type: "today", comment: "asdasdasdasdas"),
                BonusItem(date: "22:12", bonus: 23.1, type: "today", comment: "asdasdasdasdas"),
                BonusItem(date: "22:12", bonus: 23.1, type: "today", comment: "asdasdasdasdas"),
                BonusItem(date: "22:12", bonus: 23.1, type: "today", comment: "asdasdasdasdas"),
                BonusItem(date: "22:12", bonus: 23.1, type: "today", comment: "asdasdasdasdas")
            ])
        )
        return aa
    }

    @objc private func pressDatePrickerFrom(sender: UIDatePicker){
        fromDateSelctor.setup(type: .FROM, date: sender.date)
    }
    
    @objc private func pressDatePrickerTo(sender: UIDatePicker){
        toDateSelctor.setup(type: .TO, date: sender.date)
    }
    
    override func loadView() {
        super.loadView()
        title = "Бонусы"
        initNavBar()
        initDatePickers()
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.async {
            self.viewBlur.createGradientBlur()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adapter.performUpdates(animated: true)
    }
    
    private func initDatePickers(){
        fromDateSelctor.dateTitle.inputView = datePickerFrom
        toDateSelctor.dateTitle.inputView = datePickerTo
    }
    
    private func initNavBar(){
        if #available(iOS 15, *) {
            let appearanceEnd = UINavigationBarAppearance()
            appearanceEnd.backgroundColor = UIColor.init(hex: "#3530FA")
            appearanceEnd.shadowColor = .clear
            // setup title font color
            let titleAttribute = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15, weight: .bold), NSAttributedString.Key.foregroundColor: UIColor.white]
            appearanceEnd.titleTextAttributes = titleAttribute
            let appearance = UINavigationBarAppearance()
            appearance.backgroundColor = UIColor.clear
            appearance.shadowColor = .clear
            navigationController?.navigationBar.subviews[2].backgroundColor = .clear
            navigationController?.navigationBar.standardAppearance = appearance
            navigationController?.navigationBar.scrollEdgeAppearance = appearanceEnd
        } else {
            navigationController?.navigationBar.backgroundColor = UIColor.init(hex: "#3530FA")
            navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            navigationController?.navigationBar.shadowImage = UIImage()
            let keyWindow = UIApplication.shared.connectedScenes
                    .filter({$0.activationState == .foregroundActive})
                    .compactMap({$0 as? UIWindowScene})
                    .first?.windows
                    .filter({$0.isKeyWindow}).first
            let statusBar = UIView(frame: (keyWindow?.windowScene?.statusBarManager?.statusBarFrame)!)
                  statusBar.backgroundColor = UIColor.init(hex: "#3530FA")
            keyWindow?.addSubview(statusBar)
        }
    }
}

extension BonusViewController: BonusViewProtocol {
    
}

extension BonusViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        view.addSubviews([viewBlur, bonusTitle, bonusTitleCount, fromDateSelctor, toDateSelctor, newCollection])
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        viewBlur.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top)
            make.size.equalTo(CGSize(width: view.frame.width, height: 100))
        }
        
        bonusTitle.snp.makeConstraints { make in
            make.top.equalTo(viewBlur.snp.top).offset(10)
            make.left.equalToSuperview().offset(20)
        }
        
        bonusTitleCount.snp.makeConstraints { make in
            make.centerY.equalTo(viewBlur)
            make.left.equalToSuperview().offset(20)
        }
        
        fromDateSelctor.snp.makeConstraints { make in
            make.top.equalTo(viewBlur.snp.bottom).offset(30)
            make.leading.equalToSuperview().offset(23)
            make.height.equalTo(34)
            make.width.equalTo(view.frame.width / 2.59)
        }

        toDateSelctor.snp.makeConstraints { make in
            make.top.equalTo(viewBlur.snp.bottom).offset(30)
            make.trailing.equalToSuperview().offset(-23)
            make.height.equalTo(34)
            make.width.equalTo(view.frame.width / 2.59)
        }
        
        newCollection.snp.makeConstraints { make in
            make.top.equalTo(toDateSelctor.snp.bottom).offset(10)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
    }
}

extension BonusViewController: SectionControllerDelegate {
    func selectSection(_ sectionController: ListSectionController) {

    }

}

extension BonusViewController: ListAdapterDataSource {
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return itemsData
    }

    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        if object is BonusesTodayModelIG {
            let section = BonusTodaySectionController()
            return section
        } else if object is BonusesYesterdayModelIG {
            let section = BonusYesterdaySectionController()
            return section
        } else {
            return BonusTodaySectionController()
        }
    }

    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }

}

extension BonusViewController: UIScrollViewDelegate {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y >= 0 {
            if (scrollView.contentOffset.y) <= 20 {
                self.height = 100
                UIView.animate(withDuration: 0.1) {
                    self.viewBlur.snp.remakeConstraints { make in
                        make.top.equalTo(self.view.safeArea.top)
                        make.size.equalTo(CGSize(width: self.view.frame.width, height: 100))
                    }
                    
                
                    self.bonusTitle.isHidden = false
                    self.bonusTitleCount.isHidden = false
                    
                    self.view.layoutIfNeeded()
                    self.view.layoutSubviews()
                }
                print("mensheNulya: \(height)")
            } else {
                self.height = self.height - scrollView.contentOffset.y
                print("bolsheNulya: \(scrollView.contentOffset.y)")
                UIView.animate(withDuration: 0.2) {
                    
                    self.bonusTitle.isHidden = true
                    self.bonusTitleCount.isHidden = true
                }
            }
            print("allSum: \(height)")
            
            UIView.animate(withDuration: 0.1) {
                self.viewBlur.snp.remakeConstraints { make in
                    make.top.equalTo(self.view.safeArea.top)
                    make.size.equalTo(CGSize(width: self.view.frame.width, height: self.height))
                }
                
                self.view.layoutIfNeeded()
                self.view.layoutSubviews()
            }
        }
    }
    
}
