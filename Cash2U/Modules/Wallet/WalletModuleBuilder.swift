//
//  WalletModuleBuilder.swift
//  Cash2U
//
//  Created by talgar osmonov on 15/8/22.
//  
//

import UIKit

final class WalletModuleBuilder {
    /// Собрать модуль
    class func build() -> UIViewController {
        let view = WalletViewController()
        let presenter = WalletPresenter()
        let interactor = WalletInteractor()
        let router = WalletRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
