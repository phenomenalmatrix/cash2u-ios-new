//
//  WalletPresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 15/8/22.
//  
//

import Foundation

final class WalletPresenter {
    
    weak var view: WalletViewProtocol?
    var interactor: WalletInteractorProtocol?
    var router: WalletRouterProtocol?
    
}

extension WalletPresenter: WalletViewToPresenterProtocol {
}

extension WalletPresenter: WalletInteractorToPresenterProtocol {
}
