//
//  WalletProtocols.swift
//  Cash2U
//
//  Created by talgar osmonov on 15/8/22.
//  
//

import Foundation

protocol WalletViewProtocol: AnyObject {
}

protocol WalletViewToPresenterProtocol: AnyObject {
}

protocol WalletInteractorProtocol: AnyObject {
}

protocol WalletInteractorToPresenterProtocol: AnyObject {
}

protocol WalletRouterProtocol: AnyObject {
}
