//
//  WalletViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 15/8/22.
//  
//

import UIKit

final class WalletViewController: CUViewController {
    
    var presenter: WalletViewToPresenterProtocol?
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private lazy var qrButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Оплатить QR", titleSize: 16, cornerRadius: 45 / 2)
        view.addTarget(self, action: #selector(onOpenQR), for: .touchUpInside)
        return view
    }()
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = .systemPink
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - UIActions
    
}

extension WalletViewController: WalletViewProtocol {
}

extension WalletViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        
        view.addSubview(qrButton)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        qrButton.snp.makeConstraints { make in
            make.bottom.equalTo(view.safeArea.bottom).offset(-12)
            make.centerX.equalToSuperview()
            make.width.equalTo(ScreenHelper.getWidth / 2.5)
            make.height.equalTo(45)
        }
    }
}
