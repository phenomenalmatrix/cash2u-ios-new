//
//  WalletInteractor.swift
//  Cash2U
//
//  Created by talgar osmonov on 15/8/22.
//  
//

import Foundation

final class WalletInteractor {
    weak var presenter: WalletInteractorToPresenterProtocol?
}

extension WalletInteractor: WalletInteractorProtocol {
}
