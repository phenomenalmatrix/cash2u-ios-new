//
//  MallsopSectionModelIG.swift
//  Cash2U
//
//  Created by talgar osmonov on 5/9/22.
//

import UIKit
import IGListKit

final class MallsTopSectionModelIG: ListDiffable {
    
    let id = "MallsTopSection"
    let name: String
    
    init(name: String) {
        self.name = name
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard self !== object else { return true }
        guard let object = object as? MallsTopSectionModelIG else { return false }
        return id == object.id && name == object.name
    }
}
