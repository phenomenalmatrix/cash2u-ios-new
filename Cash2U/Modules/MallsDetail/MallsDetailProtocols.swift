//
//  MallsDetailProtocols.swift
//  Cash2U
//
//  Created by talgar osmonov on 5/9/22.
//  
//

import Foundation

protocol MallsDetailViewProtocol: AnyObject {
}

protocol MallsDetailViewToPresenterProtocol: AnyObject {
}

protocol MallsDetailInteractorProtocol: AnyObject {
}

protocol MallsDetailInteractorToPresenterProtocol: AnyObject {
}

protocol MallsDetailRouterProtocol: AnyObject {
}
