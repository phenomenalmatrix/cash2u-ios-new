//
//  MallsDetailPresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 5/9/22.
//  
//

import Foundation

final class MallsDetailPresenter {
    
    weak var view: MallsDetailViewProtocol?
    var interactor: MallsDetailInteractorProtocol?
    var router: MallsDetailRouterProtocol?
    
}

extension MallsDetailPresenter: MallsDetailViewToPresenterProtocol {
}

extension MallsDetailPresenter: MallsDetailInteractorToPresenterProtocol {
}
