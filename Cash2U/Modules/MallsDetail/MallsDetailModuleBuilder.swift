//
//  MallsDetailModuleBuilder.swift
//  Cash2U
//
//  Created by talgar osmonov on 5/9/22.
//  
//

import UIKit

final class MallsDetailModuleConfigurator {
    /// Собрать модуль
    class func build() -> UIViewController {
        let view = MallsDetailViewController()
        let presenter = MallsDetailPresenter()
        let interactor = MallsDetailInteractor()
        let router = MallsDetailRouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
