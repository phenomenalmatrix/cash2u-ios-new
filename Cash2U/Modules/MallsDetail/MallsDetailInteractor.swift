//
//  MallsDetailInteractor.swift
//  Cash2U
//
//  Created by talgar osmonov on 5/9/22.
//  
//

import Foundation

final class MallsDetailInteractor {
    weak var presenter: MallsDetailInteractorToPresenterProtocol?
}

extension MallsDetailInteractor: MallsDetailInteractorProtocol {
}
