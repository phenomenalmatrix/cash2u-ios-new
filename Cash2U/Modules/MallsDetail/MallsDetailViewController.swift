//
//  MallsDetailViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 5/9/22.
//  
//

import UIKit
import IGListKit

final class MallsDetailViewController: CUViewController  {
    
    var presenter: MallsDetailViewToPresenterProtocol?
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private lazy var adapter: ListAdapter = {
        let adapter = ListAdapter(updater: ListAdapterUpdater(), viewController: self)
        adapter.collectionView = mainCollection
        adapter.dataSource = self
        return adapter
    }()
    
    private var bestPartnersModel = BestPartnersModelIG(name: "Рекомендуем", isLoading: false, items: [])
    private var offersAndPromotionsModel = OffersAndPromotionsModelIG(name: "Offers", isLoading: false)
    private var bonusCardsModel = BonusCardsModelIG(name: "Bonus", isLoading: true)
    private var discountModel = DiscountModelIG(name: "discountModel", items: [])
    
    private func createItemsData() -> [ListDiffable] {
        var aa = [
        ] as [ListDiffable]
        aa.append(MallsTopSectionModelIG(name: "Top"))
        aa.append(self.discountModel)
        aa.append(CategoryModelIG(name: "Cat", count: 6, type: 1, array: []))
        aa.append(self.bestPartnersModel)
        aa.append(TagsModelIG(name: "Партнеры", tags: [""]))
        aa.append(PartnersModelIG(name: "Partners"))
        aa.append(SupportModelIG())
        return aa
    }
    
    private lazy var itemsData: [ListDiffable] = createItemsData()
    
    private lazy var mainCollection: CUCollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        let mainCollection = CUCollectionView(
            frame: CGRect.zero,
            collectionViewLayout: flowLayout
        )
        mainCollection.translatesAutoresizingMaskIntoConstraints = false
        mainCollection.alwaysBounceVertical = true
        return mainCollection
    }()
    
    override func loadView() {
        super.loadView()
        navigationItem.rightBarButtonItem = .init(image: UIImage(named: "search_icon"), style: .plain, target: self, action: #selector(onSearchTapped))
        self.navigationItem.title = "Beta Stores"
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        adapter.performUpdates(animated: true, completion: nil)
        
    }
    
    // MARK: - UIActions
    
    @objc private func  onSearchTapped() {
        
    }
}

extension MallsDetailViewController: MallsDetailViewProtocol {
}

extension MallsDetailViewController: UISearchResultsUpdating, UISearchControllerDelegate {
    func updateSearchResults(for searchController: UISearchController) {
        
    }
}

extension MallsDetailViewController: PartnersSectionControllerDelegate {
    func didSelect() {
        let vc = PartnerPageModuleBuilder.build(partnerId: 0)
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension MallsDetailViewController: CategorySectionControllerDelegate {
    func didSelectCategory() {
        let vc = CategoryDetailModuleConfigurator.build()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension MallsDetailViewController: PartnerSelectionDelegate {
    func didSelectPartner(partnerId: Int) {
        let vc = PartnerPageModuleBuilder.build(partnerId: partnerId)
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


// MARK: - ListAdapterDataSource

extension MallsDetailViewController: ListAdapterDataSource {
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        itemsData
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        if object is CurrencyModelIG {
            let section = CurrencySectionController()
            return section
        } else if object is MallsTopSectionModelIG {
            let section = MallsTopSectionController()
//            section.delegate = self
            return section
        } else if object is TagsModelIG {
            let section = HorizontalTagsSectionController()
//            section.delegate = self
            return section
        } else if object is PartnersModelIG {
            let section = PartnersSectionController()
            section.delegate = self
            return section
        } else if object is BestPartnersModelIG {
            let section = RecomendationSectionController()
            section.delegate = self
            return section
        } else if object is LocationModelIG {
            let section = LocationSectionController()
//            section.delegate = self
            return section
        } else if object is NewPartnersModelIG {
            let section = NewPartnersSectionController()
//            section.delegate = self
            return section
        } else if object is MallsModelIG {
            let section = MallsSectionController(isHeaderButtonHidden: false)
//            section.delegate = self
            return section
        } else if object is DiscountModelIG {
            let section = DiscountSectionController()
//            section.delegate = self
            return section
        } else if object is SupportModelIG {
            return SupportSectionController()
        } else if object is VersionModelIG {
            return VersionSectionController()
        } else if object is GreetingsSectionModelIG {
            let section = GreetingsSectionController()
//            section.delegate = self
            return section
        } else if object is BazaarModelIG {
            let section = BazaarsSectionController(isHeaderButtonHidden: false)
//            section.delegate = self
            return section
        } else if object is CategoryModelIG {
            let section = CategorySectionController()
            section.delegate = self
            return section
        } else {
            return ListSectionController()
        }
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }
}

extension MallsDetailViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        view.addSubview(mainCollection)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        mainCollection.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
    }
}
