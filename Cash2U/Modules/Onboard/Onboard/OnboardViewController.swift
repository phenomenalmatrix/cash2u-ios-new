//
//  OnboardViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 20/7/22.
//  
//

import UIKit

final class OnboardViewController: CUViewController {
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    var models: [OnBoardModel] = [
        OnBoardModel(title: "Удобный способ оплаты \nпокупок частями",subTitle: "🔥 Без первичного взноса \n🤝 Без скрытых комиссий \n😎 Без процентов \n🚀 Без переплат",image: "OnBoardImage1", isLast: false),
        OnBoardModel(title: "Быстрое оформление \nонлайн",subTitle: "Вам не нужно идти в офис для \nоформления, рассмотрение заявки \nзаймет примерно 15 минут",image: "OnBoardImage3", isLast: false),
        OnBoardModel(title: "Мобильный кошелек на \nвсе случаи жизни!", subTitle: "💵 Мультивалютные счета\n🔁 Конвертация валют\n🌏 Переводы заграницу и внутри страны \n🏧 Вывод средств ",image: "OnBoardImage2", isLast: false),
        OnBoardModel(title: "Каталог партнеров",subTitle:"Оплачивайте покупки или услуги у наших партнеров с помощью cash2u", image: "OnBoardImage4", isLast: true)
    ]
    
    private lazy var uiOnBoardPageView = UIOnBoardPageView()
    
    private lazy var skipButton: CUMainButton = {
        let view = CUMainButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setup(title: "Пропустить", titleSize: 14, titleColor: .grayColor, backgroundColor: .thirdaryColor, cornerRadius: 30 / 2)
        view.addTarget(self, action: #selector(onSkipTapped), for: .touchUpInside)
        return view
    }()
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = .mainColor
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        let item = UIBarButtonItem.init(customView: skipButton)
        let currWidth = item.customView?.widthAnchor.constraint(equalToConstant: 120)
        currWidth?.isActive = true
        let currHeight = item.customView?.heightAnchor.constraint(equalToConstant: 30)
        currHeight?.isActive = true
        navigationItem.rightBarButtonItems = [item]
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        uiOnBoardPageView.fill(models: models)
        
        uiOnBoardPageView.onClickFinishListener {
            UserDefaultsService.shared.isFirstLaunch = false
            let vc = InputPhoneNumberModuleBuilder.build()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    // MARK: - UIActions
    
    @objc private func onSkipTapped() {
        UserDefaultsService.shared.isFirstLaunch = false
        let vc = InputPhoneNumberModuleBuilder.build()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension OnboardViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        
        view.addSubview(uiOnBoardPageView)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        uiOnBoardPageView.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(view.safeArea.bottom)
        }
    }
}
