//
//  OnboardingViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 20/9/22.
//  
//

import UIKit
import AVFoundation


struct OnboardingModel {
    let text: String
    let description: String
    let subdescription: String?
    let video: String
}

final class OnboardingViewController: CUViewController {
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    
    @objc dynamic var currentIndex = 0
    
    private var data = [
        OnboardingModel(text: "Оплата частями",
                        description: "Удобный способ оплаты покупок частями в магазинах наших партнеров",
                        subdescription: "🔥 Без первичного взноса\n🤝 Без скрытых комиссий\n😎 Без процентов\n🚀 Без переплат",
                        video: "instraction"),
        OnboardingModel(text: "Мобильный кошелек",
                        description: "Мультивалютные счета, переводы, обмен валют, оплата услуг и многое другое",
                        subdescription: "💸 Мультивалютные счета \n🔃 Конвертация валют \n🌍 Переводы заграницу и внутри страны \n🏧 Вывод средств",
                        video: "instraction"),
        OnboardingModel(text: "Партнеры cash2u",
                        description: "Оплачивайте покупки или услуги у наших партнеров с помощью cash2u",
                        subdescription: nil,
                        video: "instraction")
    ]
    
    private var indexPathArray = [IndexPath]()
    
    private var isSoundMuted = false
    
    private lazy var mainCollection: CUCollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        let mainCollection = CUCollectionView(
            frame: CGRect.zero,
            collectionViewLayout: flowLayout
        )
        mainCollection.translatesAutoresizingMaskIntoConstraints = false
        mainCollection.delegate = self
        mainCollection.dataSource = self
        mainCollection.isPagingEnabled = true
        mainCollection.register(CUOnboardingCell.self)
        return mainCollection
    }()
    
    private lazy var linePageControll: CUOnboardPageControll = {
        let view = CUOnboardPageControll()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.pageCount = data.count
        return view
    }()
    
    override func loadView() {
        super.loadView()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Пропустить", style: .plain, target: self, action: #selector(onSkipTapped))
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "sound_icon"), style: .plain, target: self, action: #selector(onMuteTapped))
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let vc = JWTDecoderService.shared.getUserViewController(isAfterAuthorization: false)
        self.navigationController?.present(vc, animated: false, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let cell = mainCollection.visibleCells.first as? CUOnboardingCell {
            cell.player?.play()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let cell = mainCollection.visibleCells.first as? CUOnboardingCell {
            cell.player?.pause()
        }
    }
    
    @objc private func onSkipTapped() {
        UserDefaultsService.shared.isFirstLaunch = false
        let vc = InputPhoneNumberModuleBuilder.build(isAfterAuthorization: false, isAfterOnboarding: true)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @objc private func onMuteTapped() {
        isSoundMuted.toggle()
        if isSoundMuted {
            navigationItem.rightBarButtonItem?.image = UIImage(named: "touch_id")
            if let cell = mainCollection.visibleCells.first as? CUOnboardingCell {
                cell.player?.volume = 0
            }
        } else {
            navigationItem.rightBarButtonItem?.image = UIImage(named: "sound_icon")
            if let cell = mainCollection.visibleCells.first as? CUOnboardingCell {
                cell.player?.volume = 1
            }
        }
    }
}

extension OnboardingViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = data[indexPath.row]
        let cell = collectionView.dequeueCell(cellType: CUOnboardingCell.self, for: indexPath)
        cell.setup(model: item, isMuted: isSoundMuted)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? CUOnboardingCell {
            cell.player?.pause()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? CUOnboardingCell {
            currentIndex = indexPath.row
            cell.player?.pause()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width), height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension OnboardingViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let page = scrollView.contentOffset.x / scrollView.bounds.width
        let progressInPage = scrollView.contentOffset.x - (page * scrollView.bounds.width)
        let progress = CGFloat(page) + progressInPage
        linePageControll.progress = progress
        
        if let cell = self.mainCollection.cellForItem(at: IndexPath(row: self.currentIndex, section: 0)) as? CUOnboardingCell {
            cell.player?.volume = isSoundMuted ? 0 : 1
            cell.player?.seek(to: .zero)
            cell.player?.play()
        }
    }
    
}

extension OnboardingViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        
        view.addSubview(linePageControll)
        view.addSubview(mainCollection)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        linePageControll.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top).offset(10)
            make.leading.trailing.equalToSuperview()
        }
        
        mainCollection.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(linePageControll.snp.bottom).offset(16)
            make.bottom.equalTo(view.safeArea.bottom)
        }
    }
}
