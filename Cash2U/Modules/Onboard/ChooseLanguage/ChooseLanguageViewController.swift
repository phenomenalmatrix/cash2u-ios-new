//
//  ChooseLanguageViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 20/7/22.
//  
//

import UIKit

final class ChooseLanguageViewController: CUViewController {
    
    private var isViewHieararchyCreated = false
    private var isConstraintsInstalled = false
    
    private lazy var imageMessage: UIImageView = {
        let view = UIImageView(image: UIImage(named: "MessageLogo"))
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private lazy var russian: CUMainButton = {
        let view = CUMainButton()
        view.setup(title: "Продолжить на Русском", backgroundColor: .mainButtonColor)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(onLanguageButtonTapped), for: .touchUpInside)
        return view
    }()
    
    private lazy var kurgiz: CUMainButton = {
        let view = CUMainButton()
        view.setup(title: "Кыргызча уланта берүү", backgroundColor: .mainButtonColor)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(onLanguageButtonTapped), for: .touchUpInside)
        return view
    }()
    
    private lazy var uzbek: CUMainButton = {
        let view = CUMainButton()
        view.setup(title: "O'zbek tilida davom eting", backgroundColor: .mainButtonColor)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(onLanguageButtonTapped), for: .touchUpInside)
        return view
    }()
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = .mainColor
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.addLogoToNavigationBarItem()
        
        createViewHierarchyIfNeeded()
        setupConstrainstsIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - UIActions
    
    @objc private func onLanguageButtonTapped() {
        self.navigationController?.pushViewController(OnboardingViewController(), animated: true)
    }
}

extension ChooseLanguageViewController {
    func createViewHierarchyIfNeeded() {
        guard !isViewHieararchyCreated else { return }
        isViewHieararchyCreated = true
        
        view.addSubview(kurgiz)
        view.addSubview(russian)
        view.addSubview(uzbek)
        view.addSubview(imageMessage)
    }

    func setupConstrainstsIfNeeded() {
        guard !isConstraintsInstalled else { return }
        isConstraintsInstalled = true
        
        uzbek.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.bottom.equalTo(view.safeArea.bottom).offset(-32)
            make.height.equalTo(ScreenHelper.mainButtonHeight).priority(999)
        }
        
        kurgiz.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.bottom.equalTo(uzbek.snp.top).offset(-10)
            make.height.equalTo(ScreenHelper.mainButtonHeight).priority(999)
        }
        
        russian.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.bottom.equalTo(kurgiz.snp.top).offset(-10)
            make.height.equalTo(ScreenHelper.mainButtonHeight).priority(999)
        }
        
        imageMessage.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top)
            make.bottom.equalTo(russian.snp.top)
            make.leading.equalToSuperview().offset(60)
            make.trailing.equalToSuperview().offset(-60)
        }
    }
}
