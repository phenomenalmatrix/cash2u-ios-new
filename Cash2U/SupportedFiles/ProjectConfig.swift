//
//  ProjectConfig.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/5/22.
//

import Foundation

final class ProjectConfig {
    
    static let appName = "cash2u"
    static let bundleId = Bundle.main.bundleIdentifier ?? ""
    static let appVersion = Bundle.main.version ?? ""
    static let appBuild = Bundle.main.buildNumberString
    
}

// MARK: - URLs

extension ProjectConfig {
    
    static var baseAuthUrl = "\(baseUrl):1001"
    static let baseCreditUrl = "\(baseUrl):2001"
    static let baseFuelUrl = "\(baseUrl):8030"
    static let baseUserUrl = "\(baseUrl):8001"
    static let baseBonusUrl = "\(baseUrl):7777"
    static let baseCatalogUrl = "\(baseUrl):28001"
    
    static var baseUrl: String {
        get {
            if Bundle.main.bundleIdentifier == "kg.client.mobile.cash2u" {
                return "https://cash2u.io"
            } else {
                return "https://stage.c2u.io"
            }
        }
    }

    static var baseMeetUrl: String {
        get {
            if Bundle.main.bundleIdentifier == "kg.client.mobile.cash2u" {
                return "meet.cash2u.io"
            } else {
                return "meet.cash2u.io"
            }
        }
    }
}

// MARK: - Versions

extension Bundle {
    
    // номер версии релиза
    var releaseVersionNumber: String {
        (infoDictionary?["CFBundleShortVersionString"] as? String) ?? ""
    }

    // номер версии билда
    var buildVersionNumber: UInt64 {
        guard let buildString = infoDictionary?["CFBundleVersion"] as? String
        else {
            return 0
        }
        return NumberFormatter.default.number(from: buildString)?
            .uint64Value ?? 0
    }

    // номер версии билда
    var buildNumberString: String {
        String(buildVersionNumber)
    }

    var version: String? {
        let version =
            object(
                forInfoDictionaryKey: "CFBundleShortVersionString"
            ) as? String
        return version
    }
}
