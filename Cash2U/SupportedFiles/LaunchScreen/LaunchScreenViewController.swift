//
//  LaunchScreenViewController.swift
//  Cash2U
//
//  Created by talgar osmonov on 18/7/22.
//

import Foundation
import UIKit
import SnapKit

final class LaunchScreenViewController: CUViewController {
    
    var logoIsHidden = false
    
    lazy var logoImageView: UIImageView = {
        let view = UIImageView(image: UIImage(named: "Cash2uLogo"))
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = .mainDarkColor
        view.addSubview(logoImageView)
        logoImageView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        logoImageView.isHidden = logoIsHidden
    }
}
