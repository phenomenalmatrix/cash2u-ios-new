//
//  LaunchScreenAnimator.swift
//  Cash2U
//
//  Created by talgar osmonov on 18/7/22.
//

import Foundation
import UIKit

protocol LaunchScreenAnimatorProtcol {
    func animateAppearance()
    func animateDisappearance(completion: @escaping (() -> Void?))
}

final class LaunchScreenAnimator: LaunchScreenAnimatorProtcol {
    
    // MARK: - Properties
    
    private unowned let foregroundLaunchScreenWindow: UIWindow
    private unowned let backgroundLaunchScreenWindow: UIWindow
    
    private unowned let foregroundLaunchScreenViewController: LaunchScreenViewController
    private unowned let backgroundLaunchScreenViewController: LaunchScreenViewController
    
    // MARK: - Initialization
    
    init(foregroundLaunchScreenWindow: UIWindow, backgroundLaunchScreenWindow: UIWindow) {
        self.foregroundLaunchScreenWindow = foregroundLaunchScreenWindow
        self.backgroundLaunchScreenWindow = backgroundLaunchScreenWindow
        
        guard
            let foregroundLaunchScreenViewController = foregroundLaunchScreenWindow.rootViewController as? LaunchScreenViewController,
            let backgroundLaunchScreenViewController = backgroundLaunchScreenWindow.rootViewController as? LaunchScreenViewController else {
                fatalError("Splash window doesn't have splash root view controller!")
            }
        
        self.foregroundLaunchScreenViewController = foregroundLaunchScreenViewController
        self.backgroundLaunchScreenViewController = backgroundLaunchScreenViewController
    }
    
    func animateAppearance() {
        foregroundLaunchScreenWindow.isHidden = false
    }
    
    func animateDisappearance(completion: @escaping (() -> Void?)) {
        UIView.animate(withDuration: 0.3, delay: 0, options: [], animations: { [weak self] in
            guard let self = self else {return}
            self.foregroundLaunchScreenWindow.alpha = 0
            self.backgroundLaunchScreenWindow.alpha = 0
            
        }) { [weak self] _ in
            guard let self = self else {return}
            self.foregroundLaunchScreenWindow.isHidden = true
            self.backgroundLaunchScreenWindow.isHidden = true
        }
    }
}
