//
//  LaunchScreenPresenter.swift
//  Cash2U
//
//  Created by talgar osmonov on 18/7/22.
//

import Foundation
import UIKit

protocol LaunchScreenPresenterProtcol {
    func present(scene: UIWindowScene)
    func dismiss(completion: @escaping (() -> Void?))
}

final class LaunchScreenPresenter: LaunchScreenPresenterProtcol {
    
    private lazy var animator: LaunchScreenAnimatorProtcol = LaunchScreenAnimator(foregroundLaunchScreenWindow: foregroundLaunchScreenWindow, backgroundLaunchScreenWindow: backgroundLaunchScreenWindow)
    
    private lazy var foregroundLaunchScreenWindow: UIWindow = {
        let vc = launchScreenViewController(logoIsHidden: false)
        return launchScreenWindow(windowLevel: .normal + 1, rootViewController: vc)
    }()
    
    private lazy var backgroundLaunchScreenWindow: UIWindow = {
        let vc = launchScreenViewController(logoIsHidden: false)
        return launchScreenWindow(windowLevel: .normal - 1, rootViewController: vc)
    }()
    
    func present(scene: UIWindowScene) {
        
        foregroundLaunchScreenWindow.frame = scene.coordinateSpace.bounds
        foregroundLaunchScreenWindow.windowScene = scene
        
        backgroundLaunchScreenWindow.frame = scene.coordinateSpace.bounds
        backgroundLaunchScreenWindow.windowScene = scene
        
        animator.animateAppearance()
    }
    
    func dismiss(completion: @escaping (() -> Void?)) {
        animator.animateDisappearance(completion: completion)
    }
    
    // MARK: - Helpers
    
    private func launchScreenWindow(windowLevel: UIWindow.Level, rootViewController: LaunchScreenViewController?) -> UIWindow {
        let launchScreenWindow = UIWindow()
        
        launchScreenWindow.overrideUserInterfaceStyle = UserDefaultsService.shared.isDarkMode ? .dark : .light
        launchScreenWindow.windowLevel = windowLevel
        launchScreenWindow.rootViewController = rootViewController
        launchScreenWindow.isUserInteractionEnabled = false
        
        return launchScreenWindow
    }
    
    private func launchScreenViewController(logoIsHidden: Bool) -> LaunchScreenViewController? {
        let launchScreenViewController = LaunchScreenViewController()
        launchScreenViewController.logoIsHidden = logoIsHidden
        return launchScreenViewController
    }
}
