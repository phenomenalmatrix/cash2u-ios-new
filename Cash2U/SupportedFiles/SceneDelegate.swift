//
//  SceneDelegate.swift
//  Cash2U
//
//  Created by talgar osmonov on 18/5/22.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    private var launchPresenter: LaunchScreenPresenterProtcol? = LaunchScreenPresenter()
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene = windowScene
        window?.overrideUserInterfaceStyle = UserDefaultsService.shared.isDarkMode ? .dark : .light
        
        if UserDefaultsService.shared.isFirstLaunch {
            window?.rootViewController = UINavigationController(rootViewController: ChooseLanguageViewController())
        } else {
//            window?.rootViewController = UINavigationController(rootViewController: IdentityApplicationModuleConfigurator.build())
            window?.rootViewController = UINavigationController(rootViewController: InputPhoneNumberModuleBuilder.build(isAfterAuthorization: false))
        }
        
        // MARK: - CallObserver
        
        addCallObserver()
        
        // MARK: - Splash Screen
        
        launchPresenter?.present(scene: windowScene)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.launchPresenter?.dismiss(completion: { [weak self] in
                self?.launchPresenter = nil
            })
        }
        
        window?.makeKeyAndVisible()
    }
    
    // MARK: - Helpers
    
    private func addCallObserver() {
        NotificationCenter.default.addObserver(
            self,
            selector:#selector(onCallListener(notification:)),
            name: .callResponder,
            object: nil)
    }
    
    @objc private func onCallListener(notification: NSNotification) {
        guard let data = notification.userInfo else {return}
        let model = CallModel.getModel(data: data)
        guard let rootViewController = UIViewController.topMostController else {return}
        rootViewController.modalPresentationStyle = .formSheet
        rootViewController.present(EnterPinCodeViewController(), animated: true)
    }

}

