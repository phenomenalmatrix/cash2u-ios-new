//
//  SplashController.swift
//  cash2u
//
//  Created by Eldar Akkozov on 2/4/22.
//

import Foundation
import UIKit

class SplashController: BaseModelController<SplashViewModel>, SplashDelegate {
    
    private lazy var imageSplash: UIImageView = {
        let view = UIImageView(image: UIImage(named: "Cash2uLogo"))
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func setupConstraint() {
        view.addSubview(imageSplash)
        imageSplash.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }
    
    override func setupUI() {
        view.backgroundColor = .black

        viewModel.check()        
    }
}
