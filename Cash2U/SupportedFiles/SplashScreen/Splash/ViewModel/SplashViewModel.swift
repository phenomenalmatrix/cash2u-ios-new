//
//  SplashViewModel.swift
//  cash2u
//
//  Created by Eldar Akkozov on 2/4/22.
//

import Foundation

protocol SplashDelegate: AnyObject {  }

class SplashViewModel: BaseRepositoryViewModel<SplashDelegate, SplashRepository> {
    
    func check(){
        switch(repository.getUserInfo()){
        case .SHOW_ENTRANCE:
            
            router.showLanguageSelection()
            break
        case .SHOW_AUTHORIZATION:
            
            router.showNumber()
            break
        case .SHOW_HOME:
            
            router.showValidPinCode()
            break
        }
    }
    
}
