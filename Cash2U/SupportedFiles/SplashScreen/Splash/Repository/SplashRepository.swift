//
//  SplashRepository.swift
//  cash2u
//
//  Created by Eldar Akkozov on 5/4/22.
//

import Foundation

enum UserState {
    case SHOW_ENTRANCE
    case SHOW_AUTHORIZATION
    case SHOW_HOME
}

protocol SplashRepositoryService {
    func getIsFirstLaunch() -> Bool
    func getToken() -> String?
}

class SplashRepository: BaseRepository<SplashService> {
    
    func getUserInfo() -> UserState {
        if service.getIsFirstLaunch() {
            return .SHOW_ENTRANCE
        } else{
            if service.getToken() == nil {
                return .SHOW_AUTHORIZATION
            } else {
                return .SHOW_HOME
            }
        }
    }
}
