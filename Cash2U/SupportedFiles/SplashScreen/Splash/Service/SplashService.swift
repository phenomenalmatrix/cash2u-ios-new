//
//  SplashService.swift
//  cash2u
//
//  Created by Eldar Akkozov on 5/4/22.
//

import Foundation

class SplashService: SplashRepositoryService {
    func getIsFirstLaunch() -> Bool {
        return UserDefault<Bool>(key: .IS_FIRST_OPEN, defaultValue: true).wrappedValue ?? true
    }
    
    func getToken() -> String? {
        return UserDefault<String>(key: .TOKEN).wrappedValue
    }
}

