//
//  File.swift
//  Cash2U
//
//  Created by talgar osmonov on 5/8/22.
//

import UIKit

final class JWTDecoderService {
    
    static let shared = JWTDecoderService()
    
    var isIdentified: Bool {
        if KeychainService.shared.hasToken {
            let jwt = try? decode(jwt: KeychainService.shared.accessToken ?? "")
            if jwt?.isIdentified == "True" {
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    }
    
    func isAccessTokenExpired() -> Bool {
        let jwt = try? decode(jwt: KeychainService.shared.accessToken ?? "")
        
        if let jwt = jwt, let expired = jwt.expiresAt {
            let jwtDate = Calendar.current.date(byAdding: .minute, value: -3, to: expired) ?? Date()
            let date = Date()
//            print(date, jwtDate, expired)
            return jwtDate < date
        } else {
            return false
        }
        
    }
    
    func getUserViewController(isAfterAuthorization: Bool = true) -> UIViewController {
        
        if KeychainService.shared.hasToken {
            
            let jwt = try? decode(jwt: KeychainService.shared.accessToken ?? "")
            
            if jwt?.isIdentified == "True" {
                let main = MainTabBar()
                main.isAfterAuthorization = isAfterAuthorization
                let vc = UINavigationController(rootViewController: main)
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                return vc
            } else {
                let vc = HomeModuleConfigurator.buildRegistered(isAfterAuthorization: isAfterAuthorization)
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                return vc
            }
            
        } else {
            let vc = HomeModuleConfigurator.buildGuest()
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            return vc
        }
    }
}
