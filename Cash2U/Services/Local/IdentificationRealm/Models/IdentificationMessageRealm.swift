//
//  IdentificationMessageRealmModel.swift
//  Cash2U
//
//  Created by talgar osmonov on 24/10/22.
//

import Foundation
import RealmSwift


class IdentificationMessageRealm: Object, Codable {
    
    @objc dynamic var type = ""
    @objc dynamic var id = ""
    @objc dynamic var date = Date()
    @objc dynamic var senderName = ""
    @objc dynamic var senderId = ""
    @objc dynamic var status: String = ""
    @objc dynamic var message = ""
    @objc dynamic var pictureUrl = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    static func createModel(text: String = "", photo: String = "", type: SendMessageType, id: String, sender: CUSender, status: IdentificationMessageStatus) -> IdentificationMessageRealm {
        let model = IdentificationMessageRealm()
        model.type = type.rawValue
        model.id = id
        model.date = Date()
        model.senderName = sender.displayName
        model.senderId = sender.senderId
        model.status = status.rawValue
        model.message = text
        model.pictureUrl = photo
        return model
    }
    
    static func convertToArray(data: Results<IdentificationMessageRealm>) -> [IdentificationMessageRealm] {
        return data.map { model in
            let item = IdentificationMessageRealm()
            item.type = model.type
            item.id = model.id
            item.date = model.date
            item.senderName = model.senderName
            item.senderId = model.senderId
            item.status = model.status
            item.message = model.message
            item.pictureUrl = model.pictureUrl
            return item
        }
    }
}

enum IdentificationMessageStatus: String {
    case create, needToEdit
}

enum SendMessageType: String {
    case text, photo, textPhoto
}

struct CUSender: SenderType {
    var senderId: String
    var displayName: String
}

class CUMessage: NSObject, MessageType {
    var type: SendMessageType
    var sender: SenderType
    var messageId: String
    var sentDate: Date
    var kind: MessageKind
    var status: IdentificationMessageStatus = .create
    
    init(message: IdentificationMessageRealm) {
        self.type = SendMessageType.init(rawValue: message.type)!
        self.sender = CUSender(senderId: message.senderId, displayName: message.senderName)
        self.messageId = message.id
        self.sentDate = message.date
        self.status = IdentificationMessageStatus(rawValue: message.status)!
        switch SendMessageType(rawValue: message.type) {
        case .text:
            self.kind = MessageKind.text(message.message)
        case .photo:
            self.kind = MessageKind.photo(ImageMediaItem(image: (FileManagerService.shared.getSavedImage(named: message.pictureUrl) ?? UIImage(named: message.pictureUrl))!))
        case .textPhoto:
            self.kind = MessageKind.text(message.message)
        default:
            self.kind = MessageKind.text(message.message)
        }
    }
}

struct ImageMediaItem: MediaItem {
  var url: URL?
  var image: UIImage?
  var placeholderImage: UIImage
  var size: CGSize

  init(image: UIImage) {
    self.image = image
      size = CGSize(width: ScreenHelper.getWidth - 80, height: 160)
    placeholderImage = UIImage()
  }

  init(imageURL: URL) {
    url = imageURL
    size = CGSize(width: ScreenHelper.getWidth - 80, height: 160)
    placeholderImage = UIImage()
  }
}
