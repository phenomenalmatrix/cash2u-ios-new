//
//  IdentificationRealmService.swift
//  Cash2U
//
//  Created by talgar osmonov on 24/10/22.
//

import Foundation
import RealmSwift

enum IdentificationRealmServiceError: Error {
    case cannotFetch, cannotAdd
}

class IdentificationRealmService {
    
    static let shared = IdentificationRealmService()
    
    func fetchMyList(completion: @escaping (Result<Results<IdentificationMessageRealm>?, Error>) -> Void) {
        DispatchQueue.main.async {
            let result: Result<Results<IdentificationMessageRealm>?, Error>
            defer {
                DispatchQueue.main.async {
                    completion(result)
                }
            }
            
            let realm = try? Realm()
            let list = realm?.objects(IdentificationMessageRealm.self)
                .sorted(byKeyPath: "date", ascending: true)
            
            if let list = list {
                result = .success(list)
            } else {
                result = .failure(IdentificationRealmServiceError.cannotFetch)
            }
        }
    }
    
    func addToMyList(model: IdentificationMessageRealm, completion: @escaping (Result<Void?, Error>) -> Void) {
        DispatchQueue.dbThread.async {
            autoreleasepool {
                let realm = try? Realm()
                do {
                    try realm?.write {
                        realm?.add(model, update: .all)
                    }
                    DispatchQueue.main.async {
                        completion(.success(nil))
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(.failure(IdentificationRealmServiceError.cannotAdd))
                    }
                }

            }
        }
    }
    
    func updateModel(id: String, message: String? = nil, photo: String? = nil, completion: @escaping (Result<Void?, Error>) -> Void) {
        DispatchQueue.dbThread.async {
            autoreleasepool {
                
                let realm = try? Realm()
                
                guard let data = realm?.objects(IdentificationMessageRealm.self).first(where: { item in
                    return item.id == id
                }) else {
                    completion(.failure(IdentificationRealmServiceError.cannotAdd))
                    return
                }
                
                do {
                    try realm?.write {
                        if let message {
                            data.message = message
                        } else if let photo {
                            data.pictureUrl = photo
                        }
                    }
                    DispatchQueue.main.async {
                        completion(.success(nil))
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(.failure(IdentificationRealmServiceError.cannotAdd))
                    }
                }

            }
        }
    }
    
    func clearAll(completion: @escaping (Result<Void?, Error>) -> Void) {
        DispatchQueue.dbThread.async {
            autoreleasepool {
                let realm = try? Realm()
                do {
                    try realm?.write {
                        realm?.deleteAll()
                    }
                    DispatchQueue.main.async {
                        completion(.success(nil))
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(.failure(IdentificationRealmServiceError.cannotAdd))
                    }
                }

            }
        }

    }
//
//    func deleteFromMyList(model: StickerPackModelRealm, completion: @escaping (Result<Void?, Error>) -> Void) {
//        DispatchQueue.dbThread.async {
//            autoreleasepool {
//                let realm = try? Realm()
//                guard let file = realm?.objects(StickerPackModelRealm.self).first(where: { item in
//                    item.id == model.id
//                }) else {return}
//
//                do {
//                    try realm?.write {
//                        realm?.delete(file)
//                    }
//                    DispatchQueue.main.async {
//                        completion(.success(nil))
//                    }
//                } catch {
//                    DispatchQueue.main.async {
//                        completion(.failure(RealmStickerServiceError.cannotAdd))
//                    }
//                }
//
//            }
//        }
//    }
}
