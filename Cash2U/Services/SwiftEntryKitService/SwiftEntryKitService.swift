//
//  SwiftEntryKitService.swift
//  Cash2U
//
//  Created by talgar osmonov on 26/7/22.
//

import Foundation
import SwiftEntryKit

final class SwiftEntryKitService {
    
    static let shared = SwiftEntryKitService()
    
    private func setupWarningAttributes() -> EKAttributes {
        var attributes = EKAttributes()
        attributes.displayDuration = 2
        attributes.statusBar = UserDefaultsService.shared.isDarkMode ? .light : .dark
        attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
        attributes.hapticFeedbackType = .error
        attributes.entranceAnimation = .init(translate: .init(duration: 0.3, anchorPosition: .automatic, delay: 0, spring: .init(damping: 0.65, initialVelocity: 0)), scale: nil, fade: .init(from: 0, to: 1, duration: 0.3))
        attributes.exitAnimation = .init(translate: .init(duration: 0.3, anchorPosition: .automatic, delay: 0, spring: .init(damping: 0.65, initialVelocity: 0)), scale: nil, fade: .init(from: 1, to: 0, duration: 0.3))
        attributes.position = .center
        return attributes
    }
    
    private func setupRegistrationSuccessAttributes() -> EKAttributes {
        var attributes = EKAttributes()
        attributes.displayDuration = .infinity
        attributes.screenBackground = .color(color: EKColor.init(UIColor(hex: "#000000", alpha: 0.5)))
        attributes.statusBar = UserDefaultsService.shared.isDarkMode ? .light : .dark
        attributes.scroll = .enabled(swipeable: false, pullbackAnimation: .jolt)
        attributes.hapticFeedbackType = .success
        attributes.entryInteraction = .absorbTouches
        attributes.screenInteraction = .absorbTouches
        attributes.entranceAnimation = .init(translate: .init(duration: 0.3, anchorPosition: .automatic, delay: 0, spring: .init(damping: 0.65, initialVelocity: 0)), scale: nil, fade: .init(from: 0, to: 1, duration: 0.3))
        attributes.exitAnimation = .init(translate: .init(duration: 0.3, anchorPosition: .automatic, delay: 0, spring: .init(damping: 0.65, initialVelocity: 0)), scale: nil, fade: .init(from: 1, to: 0, duration: 0.3))
        attributes.position = .center
        return attributes
    }
    
    private func setupActivityIndicatorAttributes() -> EKAttributes {
        var attributes = EKAttributes()
        attributes.displayDuration = .infinity
        attributes.popBehavior = .overridden
        attributes.screenBackground = .color(color: EKColor.init(UIColor(hex: "#000000", alpha: 0.5)))
        attributes.statusBar = UserDefaultsService.shared.isDarkMode ? .dark : .dark
        attributes.scroll = .disabled
        attributes.hapticFeedbackType = .none
        attributes.entryInteraction = .absorbTouches
        attributes.screenInteraction = .absorbTouches
        attributes.entranceAnimation = .init(translate: nil, scale: nil, fade: .init(from: 0, to: 1, duration: 0.3))
        attributes.exitAnimation = .init(translate: nil, scale: nil, fade: .init(from: 1, to: 0, duration: 0.3))
        attributes.position = .center
        return attributes
    }
    
    func registrationSuccess(completion: @escaping(() -> Void)) {
        let view = CURegistrationSuccessView()
        view.setup(title: "Регистрация прошла \nуспешно!")
        view.nextButtonClick = completion
        view.setDimensions(height: 210, width: ScreenHelper.getWidth - 60)
        SwiftEntryKit.display(entry: view, using: setupRegistrationSuccessAttributes())
        
    }
    
    func sendFeedbackSuccess(completion: @escaping(() -> Void)) {
        let view = CURegistrationSuccessView()
        view.setup(title: "Отзыв будет опубликован\nпосле модерации", image: UIImage(named: "voskl_icon"))
        view.nextButtonClick = completion
        view.setDimensions(height: 210, width: ScreenHelper.getWidth - 60)
        SwiftEntryKit.display(entry: view, using: setupRegistrationSuccessAttributes())
        
    }
    
    func referralCodeSuccess(completion: @escaping(() -> Void)) {
        let view = CURegistrationSuccessView()
        view.setup(title: "Код успешно активирован!")
        view.nextButtonClick = completion
        view.setDimensions(height: 210, width: ScreenHelper.getWidth - 60)
        SwiftEntryKit.display(entry: view, using: setupRegistrationSuccessAttributes())
        
    }
    
    func success(title: String = "Успешно!", completion: @escaping(() -> Void)) {
        let view = CURegistrationSuccessView()
        view.setup(title: title)
        view.nextButtonClick = completion
        view.setDimensions(height: 210, width: ScreenHelper.getWidth - 60)
        SwiftEntryKit.display(entry: view, using: setupRegistrationSuccessAttributes())
        
    }
    
    func pinSaveSuccess(completion: @escaping(() -> Void)) {
        let view = CURegistrationSuccessView()
        view.setup(title: "Защита приложения \nвключена")
        view.nextButtonClick = completion
        view.setDimensions(height: 210, width: ScreenHelper.getWidth - 60)
        SwiftEntryKit.display(entry: view, using: setupRegistrationSuccessAttributes())
    }
    
    func hasReferralCode(confirmClick: @escaping(() -> Void), declineClick: @escaping(() -> Void)) {
        let view = CUHasReferralCodeView()
        view.confirmButtonClick = confirmClick
        view.declineButtonClick = declineClick
        view.setDimensions(height: 385, width: ScreenHelper.getWidth - 60)
        SwiftEntryKit.display(entry: view, using: setupRegistrationSuccessAttributes())
    }
    
    func checkInternetConnectivity(completion: @escaping(() -> Void)) {
        let view = CURegistrationSuccessView()
        view.setup(title: "Не можем подключиться \nпроверьте интернет соединение", buttonTitle: "Retry")
        view.nextButtonClick = completion
        view.setDimensions(height: 210, width: ScreenHelper.getWidth - 60)
        SwiftEntryKit.display(entry: view, using: setupRegistrationSuccessAttributes())
    }
    
    func showActivityIndicator(_ isShown: Bool) {
        let view = CUAcvivityIndicatorView()
        view.setDimensions(height: 250, width: 250)
        if isShown {
            view.showLoader(true)
            SwiftEntryKit.display(entry: view, using: setupActivityIndicatorAttributes())
        } else {
            view.showLoader(false)
            SwiftEntryKit.dismiss()
        }
    }
    
    func showWarningView(title: String, image: String) {
        let view = CUWarningView()
        view.setup(title: title, image: image)
        view.setDimensions(height: 210, width: ScreenHelper.getWidth - 60)
        SwiftEntryKit.display(entry: view, using: setupWarningAttributes())
    }
    
    func dissmiss() {
        SwiftEntryKit.dismiss()
    }
}
