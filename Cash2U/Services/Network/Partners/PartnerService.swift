//
//  PartnerService.swift
//  Cash2U
//
//  Created by Oroz on 28/10/22.
//

import Foundation
import Moya

final class PartnerService {
    
    static let shared = PartnerService()
    
    private let provider = NetworkProvider<PartnerAPI>()
    
    func getPartners(complection: @escaping(Result<Response?, MoyaError>) -> Void) {
        provider.request(.getPartners) { result in
            switch result {
            case .success(let data):
                complection(.success(data))
            case .failure(let error):
                complection(.failure(error))
            }
        }
    }
    
    func getPartnerPromos(complection: @escaping(Result<Response?, MoyaError>) -> Void) {
        provider.request(.getPartnersPromo) { result in
            switch result {
            case .success(let data):
                complection(.success(data))
            case .failure(let error):
                complection(.failure(error))
            }
        }
    }
    
    func getFeedbackForPartner(partnerId: Int, complection: @escaping(Result<Response?, MoyaError>) -> Void) {
        provider.request(.getPartnerFeedback(partnerId: partnerId)) { result in
            switch result {
            case .success(let data):
                complection(.success(data))
            case .failure(let error):
                complection(.failure(error))
            }
        }
    }
    
}
