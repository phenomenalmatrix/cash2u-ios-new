//
//  PartnerAPI.swift
//  Cash2U
//
//  Created by Oroz on 28/10/22.
//

import Foundation
import Moya

enum PartnerAPI {
    case getPartners
    case getPartnersPromo
    case getPartnerFeedback(partnerId: Int)
//    case getPartnerById
}

extension PartnerAPI: TargetType {
    
    var parameters: [String : Any]? {
        return nil
    }
    
    var baseURL: URL {
        return URL(string: ProjectConfig.baseCatalogUrl)!
    }
    
    var path: String {
        switch self {
        case .getPartners:
            return "/Client/api/v1/partners"
        case .getPartnersPromo:
            return "/Client/api/v1/proms"
        case .getPartnerFeedback(partnerId: let partnerId):
            return "/Client/api/v1/\(partnerId)/feedbacks"
        }
//        case .getPartnerById:
//            return
        
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var task: Moya.Task {
        switch self {
        case .getPartners:
            return .requestPlain
        case .getPartnersPromo:
            return .requestPlain
        case .getPartnerFeedback(partnerId: _):
            return .requestPlain
        }
//        case .getPartnerById
//            return
    }
    
    var headers: [String : String]? {
        let head = [
            "Content-Type":"application/x-www-form-urlencoded",
            "Accept":"application/json",
            "Authorization":"Bearer \(KeychainService.shared.accessToken ?? "")"
        ]
        
        switch self {
        case .getPartners:
            return head
        case .getPartnersPromo:
            return head
//        case .getPartnerById:
//            return
        case .getPartnerFeedback(partnerId: _):
            return head
        }
    }
}
