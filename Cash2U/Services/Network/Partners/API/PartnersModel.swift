//
//  PartnersModel.swift
//  Cash2U
//
//  Created by Oroz on 28/10/22.
//

import Foundation

struct PartnerResponse: Codable {
    let items: [PartnerItems]
    let pageCount: Int?
    let totalItemCount: Int?
    let pageNumber: Int?
    let pageSize: Int?
    let hasPreviousPage: Bool?
    let hasNextPage: Bool?
}

struct PartnerItems: Codable {
    let id: Int?
    let name: String?
    let logo: String?
}

extension PartnerResponse {
    func mapDtoListToUIList() -> [PartnersItemModelIG] {
        return self.items.map { model in
            PartnersItemModelIG(
                id: model.id ?? Int(),
                name: model.name ?? String(),
                logo: model.logo ?? String()
            )
        }
    }
}

extension PartnerItems {
    func mapDtoToUI() -> PartnersItemModelIG {
        return PartnersItemModelIG(
            id: self.id ?? Int(),
            name: self.name ?? String(),
            logo: self.name ?? String()
        )
    }
}
