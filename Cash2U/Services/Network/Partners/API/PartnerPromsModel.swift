//
//  PartnerPromsModel.swift
//  Cash2U
//
//  Created by Oroz on 7/11/22.
//

import Foundation

// MARK: - PartnersPromoModel
struct PartnersPromoModel: Codable {
    let items: [PartnersPromoItem]
    let pageCount, totalItemCount, pageNumber, pageSize: Int?
    let hasPreviousPage, hasNextPage: Bool?
}

// MARK: - Item
struct PartnersPromoItem: Codable {
    let id: Int?
    let cover, title, hmtlBody: String?
    let partner: PartnerInPromoModel?
    let startDateTime, endDateTime: String?
}

// MARK: - Partner
struct PartnerInPromoModel: Codable {
    let id: Int?
    let name, logo: String?
}


extension PartnersPromoModel {
    func mapDtoListToUIList() -> [DiscountItemsModelIG] {
        return self.items.map { model in
            DiscountItemsModelIG(
                id: model.id ?? Int(),
                cover: model.cover,
                title: model.title,
                htmlBody: model.hmtlBody,
                partner: model.partner?.mapDtoToUI(),
                startDateTime: model.startDateTime,
                endDateTime: model.endDateTime
            )
        }
    }
}

extension PartnersPromoItem {
    func mapDtoToUI() -> DiscountItemsModelIG {
        return DiscountItemsModelIG(
            id: self.id ?? Int(),
            cover: self.cover,
            title: self.title,
            htmlBody: self.hmtlBody,
            partner: self.partner?.mapDtoToUI(),
            startDateTime: self.startDateTime,
            endDateTime: self.endDateTime
        )
    }
}

extension PartnerInPromoModel {
    func mapDtoToUI() -> PartnerPromoIG {
        return PartnerPromoIG(id: self.id ?? Int(), name: self.name, logo: self.logo)
    }
}
