//
//  PartnerFeedbackModel.swift
//  Cash2U
//
//  Created by Oroz on 8/11/22.
//

import Foundation
// MARK: - PartnersFeedback
struct PartnerFeedbackModel: Codable {
    let items: [PartnersFeedbackItem]
    let pageCount, totalItemCount, pageNumber, pageSize: Int?
    let hasPreviousPage, hasNextPage: Bool?
}

// MARK: - Item
struct PartnersFeedbackItem: Codable {
    let id: Int?
    let comment: String?
    let rate, clientID, partnerID: Int?
    let createDateTime: String?

    enum CodingKeys: String, CodingKey {
        case id, comment, rate
        case clientID = "clientId"
        case partnerID = "partnerId"
        case createDateTime
    }
}

extension PartnerFeedbackModel {
    func mapDtoListToUI() -> [ReviewsItemModelIG]{
        return self.items.map { model in
            ReviewsItemModelIG(
                id: model.id ?? Int(),
                comment: model.comment ?? String(),
                rate: model.rate ?? Int(),
                clientId: model.clientID ?? Int(),
                partnerId: model.partnerID ?? Int(),
                createDateTime: model.createDateTime ?? String()
            )
        }
    }
}

extension PartnersFeedbackItem {
    func mapDtoToUI() -> ReviewsItemModelIG {
        return ReviewsItemModelIG(
            id: self.id ?? Int(),
            comment: self.comment ?? String(),
            rate: self.rate ?? Int(),
            clientId: self.clientID ?? Int(),
            partnerId: self.partnerID ?? Int(),
            createDateTime: self.createDateTime ?? String()
        )
    }
}
