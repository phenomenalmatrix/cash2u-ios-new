//
//  CategoriesModel.swift
//  Cash2U
//
//  Created by Oroz on 25/10/22.
//

import Foundation

struct CategoriesModel: Codable {
    let items: [CategoriesItemsModel]
    let pageCount: Int?
    let totalItemCount: Int?
    let pageNumber: Int?
    let pageSize: Int?
    let hasPreviousPage: Bool?
    let hasNextPage: Bool?
    
    enum CodingKeys: String, CodingKey {
        case items
        case pageCount
        case totalItemCount
        case pageNumber
        case pageSize
        case hasPreviousPage
        case hasNextPage
    }
}

struct CategoriesItemsModel: Codable {
    let id: Int?
    let name: String?
    let logo: String?
    let partnerCount: Int?
    let backgroundColor : String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case logo
        case partnerCount
        case backgroundColor
    }
}

extension CategoriesModel {
    func mapDtoListToUIList() -> [CategoryModelItemIG] {
        return self.items.map { model in
            CategoryModelItemIG(
                id: model.id ?? 0,
                name: model.name ?? "",
                logo: model.logo ?? "",
                partnerCount: model.partnerCount ?? 0,
                backgroundColor : model.backgroundColor ?? ""
            )
        }
    }
}

extension CategoriesItemsModel {
    func mapDtoToUI() -> CategoryModelItemIG {
        return CategoryModelItemIG(
            id: self.id ?? Int(),
            name: self.name ?? String(),
            logo: self.logo ?? String(),
            partnerCount: self.partnerCount ?? Int(),
            backgroundColor: self.backgroundColor ?? String()
        )
    }
}
