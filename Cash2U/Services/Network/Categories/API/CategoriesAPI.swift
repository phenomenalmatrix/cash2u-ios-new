//
//  CategoriesAPI.swift
//  Cash2U
//
//  Created by Oroz on 25/10/22.
//

import Foundation
import Moya

enum CategoriesAPI {
    case getCategories
}

extension CategoriesAPI: TargetType {
    var baseURL: URL {
        return URL(string: ProjectConfig.baseCatalogUrl)!
    }
    
    var path: String {
        switch self {
        case .getCategories:
            return "/Client/api/v1/categories"
        }
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var task: Moya.Task {
        switch self {
        case .getCategories:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .getCategories:
            return [
                "Content-Type":"application/x-www-form-urlencoded",
                "Accept":"application/json",
                "Authorization":"Bearer \(KeychainService.shared.accessToken ?? "")"
            ]
        }
    }
}


