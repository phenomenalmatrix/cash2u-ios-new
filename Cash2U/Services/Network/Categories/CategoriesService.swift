//
//  CategoriesService.swift
//  Cash2U
//
//  Created by Oroz on 25/10/22.
//

import Foundation
import Moya

final class CategoriesService {
    
    static let shared = CategoriesService()
    
    private let provider = NetworkProvider<CategoriesAPI>()
    
    func getCategories(complection: @escaping(Result<Response?, MoyaError>) -> Void) {
        provider.request(.getCategories) { result in
            switch result {
            case .success(let data):
                complection(.success(data))
            case .failure(let error):
                complection(.failure(error))
            }
        }
    }
    
}
