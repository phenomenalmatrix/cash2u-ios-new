//
//  Bonus.swift
//  Cash2U
//
//  Created by Oroz on 4/10/22.
//

import Foundation

struct BonusList: Codable {
    let items: [BonusItem]?
    let pageCount, totalItemCount, pageNumber, pageSize: Int
    let hasPreviousPage, hasNextPage: Bool
}

struct BonusItem: Codable {
    let date: String
    let bonus: Double
    let type: String
    let comment: String?
}

enum BonusType {
    case GET
    case USED
}
