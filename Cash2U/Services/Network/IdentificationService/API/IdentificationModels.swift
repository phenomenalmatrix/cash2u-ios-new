//
//  IdentificationModels.swift
//  Cash2U
//
//  Created by talgar osmonov on 21/10/22.
//

import Foundation

struct IdentificationImageModel: Codable {
    
    var key: String
    
    enum CodingKeys: String, CodingKey {
        case key = "key"
    }
}

struct IdentificationRequestsModel: Codable {
    
    var pin: String = ""
    var documentNumber: String = ""
    var documentType: String = ""
    
    var passportTmpImage0Key: String = UserDefaultsService.shared.frontPassportKey
    var passportTmpImage1Key: String = UserDefaultsService.shared.backPassportKey
    var passportTmpImage2Key: String = UserDefaultsService.shared.selfiePassportKey

    var isPublicOfferRead: Bool = true
    var invitationCode: String = ""

    enum CodingKeys: String, CodingKey {
        case pin = "pin"
        case documentNumber = "documentNumber"
        case documentType = "documentType"

        case passportTmpImage0Key = "passportTmpImage0Key"
        case passportTmpImage1Key = "passportTmpImage1Key"
        case passportTmpImage2Key = "passportTmpImage2Key"
        
        case isPublicOfferRead = "isPublicOfferRead"
        case invitationCode = "invitationCode"
    }
}
