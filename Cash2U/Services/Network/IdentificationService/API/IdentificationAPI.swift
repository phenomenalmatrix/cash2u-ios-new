//
//  IdentificationAPI.swift
//  Cash2U
//
//  Created by talgar osmonov on 21/10/22.
//

import Foundation
import Moya

enum IdentificationAPI {
    case requestIdentification(IdentificationRequestsModel)
    case sendIdentificationImage(UIImage)
}

extension IdentificationAPI: TargetType {
    var baseURL: URL {
        switch self {
        case .requestIdentification(_):
            return URL(string: ProjectConfig.baseCreditUrl)!
        default:
            return URL(string: ProjectConfig.baseCreditUrl)!
        }
    }
    
    var path: String {
        switch self {
        case .requestIdentification(_):
            return "api/v1/IdentificationRequests/Create"
        case .sendIdentificationImage(_):
            return "api/v1/TmpFile/UploadImage"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .requestIdentification(_):
            return .post
        case .sendIdentificationImage(_):
            return .post
        }
    }
    
    var task: Task {
        switch self {
        case .requestIdentification(let data):
            return .requestJSONEncodable(data)
        case .sendIdentificationImage(let image):
            let imageData = image.jpegData(compressionQuality: 0.7) ?? Data()
            let imageMultipartFormData = MultipartFormData(provider: .data(imageData), name: "image", fileName: "\(Int.random(in: Int.min...Int.max)).jpeg", mimeType: "image/jpeg")
            return .uploadMultipart([imageMultipartFormData])
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .requestIdentification(_):
            return [
                "Content-Type":"application/json",
                "Accept":"application/json",
                "Authorization":"Bearer \(KeychainService.shared.accessToken ?? "")"
            ]
        case .sendIdentificationImage(_):
            return [
                "Content-type":"multipart/form-data",
                "Authorization":"Bearer \(KeychainService.shared.accessToken ?? "")"
            ]
        }
    }
}

