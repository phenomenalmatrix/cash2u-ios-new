//
//  IdentificationService.swift
//  Cash2U
//
//  Created by talgar osmonov on 21/10/22.
//

import Foundation
import Moya
import AppAuth

enum IdentificationImageType {
    case frontPassport
    case backPassport
    case selfiePassport
}

final class IdentificationService {
    
    static let shared = IdentificationService()
    
    private let provider = NetworkProvider<IdentificationAPI>()
    
    func sendIdentificationRequest(model: IdentificationRequestsModel) {
        provider.request(.requestIdentification(model)) { result in
            switch result {
            case .success(let success):
                print(success.response)
            case .failure(let failure):
                print(failure.response)
            }
        }
    }
    
    func sendIdentificationImage(image: UIImage, imageType: IdentificationImageType, completion: @escaping (Result<Response, MoyaError>) -> Void) {
        
        SwiftEntryKitService.shared.showActivityIndicator(true)
        provider.request(.sendIdentificationImage(image)) { result in
            switch result {
            case .success(let success):
                
                do {
                    let data = try JSONDecoder().decode(IdentificationImageModel.self, from: success.data)
                    
                    switch imageType {
                    case .frontPassport:
                        UserDefaultsService.shared.identificationModel.passportTmpImage0Key = data.key
                    case .backPassport:
                        UserDefaultsService.shared.identificationModel.passportTmpImage1Key = data.key
                    case .selfiePassport:
                        UserDefaultsService.shared.identificationModel.passportTmpImage2Key = data.key
                    }
                    
                } catch let error {
                    print(error.localizedDescription)
                }
                
                completion(.success(success))
                SwiftEntryKitService.shared.showActivityIndicator(false)
                
            case .failure(let failure):
                completion(.failure(failure))
                SwiftEntryKitService.shared.showActivityIndicator(false)
            }
        }
    }
}
