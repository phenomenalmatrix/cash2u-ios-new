//
//  AuthService.swift
//  Cash2U
//
//  Created by talgar osmonov on 20/7/22.
//

import Foundation
import Moya
import AppAuth

final class AuthService {
    
    static let shared = AuthService()
    
    private let provider = NetworkProvider<AuthAPI>()
    
    func requestVideoIdentification(completion: @escaping(Result<Void?, MoyaError>) -> Void) {
        provider.request(.requestVideoIdentification) { result in
            switch result {
            case .success(_):
                completion(.success(nil))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func refreshToken(completion: @escaping(() -> Void)) {
        provider.request(.refreshToken(KeychainService.shared.refreshToken ?? "")) { result in
            switch result {
            case .success(let data):
                let decoder = JSONDecoder()
                do {
                    let data = try decoder.decode(UserTokenResponseModel.self, from: data.data)
                    KeychainService.shared.saveToken(token: data)
                    completion()
                } catch let error {
                    print(error.localizedDescription)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func sendNumber(number: String, comletion: @escaping (Result<Response?, MoyaError>) -> Void) {
        provider.request(.sendNumber(number)) { result in
            switch result {
            case .success(let data):
                comletion(.success(data))
            case .failure(let error):
                comletion(.failure(error))
            }
        }
    }
    
    func sendOtp(number: String, otp: String, comletion: @escaping (Result<Response?, MoyaError>) -> Void) {
        provider.request(.sendOtp(number, otp)) { result in
            switch result {
            case .success(let data):
                comletion(.success(data))
                
            case .failure(let error):
                comletion(.failure(error))
            }
        }
    }
}
