//
//  AuthAPI.swift
//  Cash2U
//
//  Created by talgar osmonov on 26/7/22.
//

import Foundation
import Moya

enum AuthAPI {
    
    static let clientId = "ropc"
    static let clientSecret = "secret"
    static let password = "password"
    static let refreshTokenPost = "refresh_token"
    
    case sendNumber(String)
    case sendOtp(String, String)
    case refreshToken(String)
    case requestVideoIdentification
}

extension AuthAPI: TargetType {
    var parameters: [String : Any]? {
        return nil
    }
    
    var baseURL: URL {
        switch self {
        case .requestVideoIdentification:
            return URL(string: ProjectConfig.baseCreditUrl)!
        default:
            return URL(string: ProjectConfig.baseAuthUrl)!
        }
        
    }
    
    var path: String {
        switch self {
        case .sendNumber(_):
            return "passwordless/v1/start"
        case .sendOtp:
            return "connect/token"
        case .refreshToken(_):
            return "connect/token"
        case .requestVideoIdentification:
            return "api/v1/IdentificationRequests/RequestVideoIdentification"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .sendNumber(_):
            return .post
        case .sendOtp(_, _):
            return .post
        case .refreshToken(_):
            return .post
        case .requestVideoIdentification:
            return .post
        }
    }
    
    var task: Task {
        switch self {
        case .sendNumber(let number):
            let data: Data = "phone_number=\(number)&is_public_offer_read=\(true)&application_signature=\(String())".data(using: .utf8)!
            return .requestData(data)
        case .sendOtp(let number, let otp):
            let data: Data = "client_id=\(AuthAPI.clientId)&client_secret=\(AuthAPI.clientSecret)&grant_type=\(AuthAPI.password)&username=\(number)&password=\(otp)".data(using: .utf8)!
            return .requestData(data)
        case .refreshToken(let token):
            let data: Data = "client_id=\(AuthAPI.clientId)&client_secret=\(AuthAPI.clientSecret)&grant_type=\(AuthAPI.refreshTokenPost)&refresh_token=\(token)".data(using: .utf8)!
            return .requestData(data)
        case .requestVideoIdentification:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .requestVideoIdentification:
            return [
                "Content-Type":"application/x-www-form-urlencoded",
                "Accept":"application/json",
                "Authorization":"Bearer \(KeychainService.shared.accessToken ?? "")"
            ]
        default:
            return [
                "Content-Type":"application/x-www-form-urlencoded"
            ]
        }
    }
}
