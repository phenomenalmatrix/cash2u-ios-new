//
//  AuthModels.swift
//  Cash2U
//
//  Created by talgar osmonov on 26/7/22.
//

import Foundation

// MARK: - Token

struct UserTokenResponseModel: Codable {
    
    var accessToken: String
    var refreshToken: String
    
    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case refreshToken = "refresh_token"
    }
    
}

// MARK: - PhoneNumberError

struct InputPhoneNumberErrorModel: Codable {
    let status: Int
    let errors: InputPhoneNumberErroDetail
    let title, traceID: String
    let type: String
    
    enum CodingKeys: String, CodingKey {
        case status, errors, title
        case traceID = "traceId"
        case type
    }
}

struct InputPhoneNumberErroDetail: Codable {
    let lockoutEnd : [String]?
    let phoneNumber: [String]
    
    enum CodingKeys: String, CodingKey {
        case lockoutEnd = "LockoutEnd"
        case phoneNumber = "PhoneNumber"
    }
}

// MARK: - InputSmsError

struct InputSmsErrorModel: Codable {
    let error, errorDescription: String
    let lockoutEnd: String?
    
    enum CodingKeys: String, CodingKey {
        case error
        case errorDescription = "error_description"
        case lockoutEnd = "LockoutEnd"
    }
}
