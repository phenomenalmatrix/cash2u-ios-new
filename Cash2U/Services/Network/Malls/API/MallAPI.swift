//
//  MallAPI.swift
//  Cash2U
//
//  Created by Oroz on 31/10/22.
//

import Foundation
import Moya

enum MallAPI {
    case getMalls
    case getMarkets
}

extension MallAPI: TargetType {
    var baseURL: URL {
        return URL(string: ProjectConfig.baseCatalogUrl)!
    }
    
    var path: String {
        switch self {
        case .getMalls:
            return "Client/api/v1/malls"
        case .getMarkets:
            return "Client/api/v1/malls"
        }
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var task: Moya.Task {
        switch self {
        case .getMalls:
            return .requestParameters(parameters: [
                "Type" : "Mall",
                "Page" : "1",
                "PageSize" : "1000"
            ], encoding: URLEncoding.queryString)
        case .getMarkets:
            return .requestParameters(parameters: [
                "Type" : "Market",
                "Page" : "1",
                "PageSize" : "1000"
            ], encoding: URLEncoding.queryString)
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .getMalls:
            return [
                "Content-Type":"application/x-www-form-urlencoded",
                "Accept":"application/json",
                "Authorization":"Bearer \(KeychainService.shared.accessToken ?? "")"
            ]
        case .getMarkets:
            return [
                "Content-Type":"application/x-www-form-urlencoded",
                "Accept":"application/json",
                "Authorization":"Bearer \(KeychainService.shared.accessToken ?? "")"
            ]
        }
    }
}
