//
//  MallModels.swift
//  Cash2U
//
//  Created by Oroz on 31/10/22.
//

import Foundation

struct MallResponse: Codable {
    let items: [MallItems]
    let pageCount: Int?
    let totalItemCount: Int?
    let pageNumber: Int?
    let pageSize: Int?
    let hasPreviousPage: Bool?
    let hasNextPage: Bool?
}

struct MallItems: Codable {
    let id: Int?
    let type: String?
    let name: String?
    let logo: String?
    let partnersCount: Int?
}


extension MallResponse {
    func mapDtoListToUIList() -> [MallItemsIG] {
        return self.items.map { model in
            MallItemsIG(
                id: model.id ?? Int(),
                type: model.type ?? String(),
                name: model.name ?? String(),
                logo: model.logo ?? String(),
                partnersCount: model.partnersCount ?? Int()
            )
        }
    }
}

extension MallItems {
    func mapDtoToUI() -> MallItemsIG {
        return MallItemsIG(
            id: self.id ?? Int(),
            type: self.type ?? String(),
            name: self.name ?? String(),
            logo: self.logo ?? String(),
            partnersCount: self.partnersCount ?? Int()
        )
    }
}
