//
//  MallsService.swift
//  Cash2U
//
//  Created by Oroz on 31/10/22.
//

import Foundation
import Moya

final class MallsService {
    
    static let shared = MallsService()
    
    private let provider = NetworkProvider<MallAPI>()
    
    func getMalls(complection: @escaping(Result<Response?, MoyaError>) -> Void) {
        provider.request(.getMalls) { result in
            switch result {
            case .success(let data):
                complection(.success(data))
            case .failure(let error):
                complection(.failure(error))
            }
        }
    }
    
    func getMarkets(complection: @escaping(Result<Response?, MoyaError>) -> Void) {
        provider.request(.getMarkets) { result in
            switch result {
            case .success(let data):
                complection(.success(data))
            case .failure(let error):
                complection(.failure(error))
            }
        }
    }
    
    
}
