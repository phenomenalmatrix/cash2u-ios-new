//
//  NetworkProvider.swift
//  cash2u
//
//  Created by Eldar Akkozov on 2/4/22.
//

import Foundation
import Moya
import Alamofire
import JGProgressHUD


class NetworkProvider<Target: TargetType> {
    
    private lazy var sessionManager: Session = {
        let configuration = URLSessionConfiguration.af.default
        configuration.waitsForConnectivity = true
        configuration.timeoutIntervalForResource = 20
        return Session(
            configuration: configuration,
            startRequestsImmediately: false
        )
    }()
    
    private lazy var provider: MoyaProvider<Target> = {
        #if DEBUG
        return MoyaProvider<Target>(session: self.sessionManager, plugins: [NetworkLoggingPlugin(verbose: true, cURL: true)])
        #else
        return MoyaProvider<Target>(session: self.sessionManager)
        #endif
    }()
    
    private lazy var tokenProvider: MoyaProvider<AuthAPI> = {
        #if DEBUG
        return MoyaProvider<AuthAPI>(session: self.sessionManager, plugins: [NetworkLoggingPlugin(verbose: true, cURL: true)])
        #else
        return MoyaProvider<AuthAPI>(session: self.sessionManager)
        #endif
    }()
    
    func request(_ target: Target,
                 comletion: @escaping (Result<Response, MoyaError>) -> Void) {
       
        if JWTDecoderService.shared.isAccessTokenExpired() {
            
            tokenProvider.request(.refreshToken(KeychainService.shared.refreshToken ?? "")) { [weak self] result in
                switch result {
                case .success(let data):
                    let decoder = JSONDecoder()
                    do {
                        let data = try decoder.decode(UserTokenResponseModel.self, from: data.data)
                        KeychainService.shared.saveToken(token: data)
                        
                        self?.provider.request(target) { result in
                            switch result {
                            case .success(let response):
                                if (200...299).contains(response.statusCode) {
                                    comletion(.success(response))
                                } else {
                                    comletion(.failure(.statusCode(response)))
                                }
                            case .failure(let error):
                                if error.errorCode == 6 {
                                    SwiftEntryKitService.shared.checkInternetConnectivity {
                                        SwiftEntryKitService.shared.showActivityIndicator(true)
                                        self?.request(target, comletion: comletion)
                                    }
                                } else {
                                    comletion(.failure(error))
                                }
                            }
                        }
                        
                    } catch let error {
                        print(error.localizedDescription)
                    }
                case .failure(let error):
                    comletion(.failure(error))
                }
            }
            
        } else {
            
            provider.request(target) { result in
                switch result {
                case .success(let response):
                    if (200...299).contains(response.statusCode) {
                        comletion(.success(response))
                    } else {
                        comletion(.failure(.statusCode(response)))
                    }
                case .failure(let error):
                    if error.errorCode == 6 {
                        SwiftEntryKitService.shared.checkInternetConnectivity {
                            SwiftEntryKitService.shared.showActivityIndicator(true)
                            self.request(target, comletion: comletion)
                        }
                    } else {
                        comletion(.failure(error))
                    }
                }
            }
        }
    }
}
