//
//  CallServiceSocket.swift
//  Cash2U
//
//  Created by talgar osmonov on 6/9/22.
//

import Foundation
import SwiftSignalRClient

struct CallModel: Codable {
    var sessionId: String
    var token: String

    enum CodingKeys: String, CodingKey {
        case sessionId = "sessionId"
        case token = "token"
    }
    
    func getDictionary() -> [String : Any] {
        return [
            "sessionId" : sessionId,
            "token" : token
        ]
    }
    
    static func getModel(data: [AnyHashable : Any]) -> CallModel {
        guard let sessionId = data["sessionId"] as? String, let token = data["token"] as? String else {return CallModel(sessionId: "", token: "")}
        return CallModel(sessionId: sessionId, token: token)
    }
}

protocol SocketDelegate: AnyObject {
    func connectionDidOpen(hubConnection: HubConnection)
    func connectionDidFailToOpen(error: Error)
    func connectionDidClose(error: Error?)
}


public final class CallSocket {
    
    static let shared = CallSocket()
    
    weak var delegate: SocketDelegate? = nil
    
    private let urlVideo = ":2001/api/v1/videoIdentificationHub"
    
    private var connection: HubConnection? = nil
    
    public init() {
        connection = HubConnectionBuilder(url: URL(string: ProjectConfig.baseUrl + urlVideo)!).withHttpConnectionOptions(configureHttpOptions: { httpConnectionOptions in
            httpConnectionOptions.headers = ["Authorization": "Bearer \(KeychainService.shared.accessToken ?? "")"]
        })
        .withLogging(minLogLevel: .debug)
        .build()
             
        connection?.delegate = self
        
        connection?.on(method: "ReceiveVideoIdentificationToken") { argumentExtractor in
            do {
                self.handleMessage(try argumentExtractor.getArgument(type: CallModel.self))
            } catch {
                print(error)
            }
        }
    }
    
    func startConnection() {
        connection?.start()
    }
    
    func stopConnection() {
        connection?.stop()
    }
    
    private func handleMessage(_ arguments: CallModel) {
        NotificationCenter.default
            .post(name: .callResponder, object: nil, userInfo: arguments.getDictionary())
    }
}

extension CallSocket: HubConnectionDelegate {
    public func connectionDidOpen(hubConnection: HubConnection) {
        delegate?.connectionDidOpen(hubConnection: hubConnection)
    }
    
    public func connectionDidFailToOpen(error: Error) {
        delegate?.connectionDidFailToOpen(error: error)
    }
    
    public func connectionDidClose(error: Error?) {
        delegate?.connectionDidClose(error: error)
    }
}
