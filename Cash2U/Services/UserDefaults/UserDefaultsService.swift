//
//  UserDefaultsService.swift
//  Cash2U
//
//  Created by talgar osmonov on 19/5/22.
//

import Foundation
import SwiftyUserDefaults


extension DefaultsKeys {
    var isDarkMode: DefaultsKey<Bool> { .init("isDarkMode", defaultValue: true)}
    var isFirstLaunch: DefaultsKey<Bool> { .init("isFirstLaunch", defaultValue: true)}
    var isSecured: DefaultsKey<Bool> { .init("isSecured", defaultValue: false)}
    var otpTimestamp: DefaultsKey<Double> { .init("otpTimestamp", defaultValue: Date().currentTimeMillis())}
    var callOtpTimestamp: DefaultsKey<Double> { .init("callOtpTimestamp", defaultValue: Date().currentTimeMillis())}
    var phoneNumber: DefaultsKey<String> { .init("phoneNumber", defaultValue: "")}
    var hasFaceAndTouchPermission: DefaultsKey<Bool> { .init("hasFaceAndTouchPermission", defaultValue: false)}
    var homeCellTypes: DefaultsKey<[String]> { .init("homeCellTypes", defaultValue: [
        HomeCellType.Currency.rawValue,
        HomeCellType.BestPartners.rawValue,
        HomeCellType.OffersAndPromotions.rawValue,
        HomeCellType.BonusCards.rawValue,
        HomeCellType.News.rawValue])}
    var homeRemoveCellTypes: DefaultsKey<[String]> { .init("homeRemoveCellTypes", defaultValue: [])}
    var isAutoPayEnabled: DefaultsKey<Bool> { .init("isAutoPayEnabled", defaultValue: false)}
    var identificationInputType: DefaultsKey<Int> { .init("identificationInputType", defaultValue: InputBarType.empty.rawValue)}
    var savedIdentificationInputType: DefaultsKey<Int> { .init("savedIdentificationInputType", defaultValue: InputBarType.empty.rawValue)}
    var editIdentificationInputType: DefaultsKey<Int> {.init("editIdentificationInputType", defaultValue: -1)}
    var frontPassportKey: DefaultsKey<String> {.init("frontPassportKey", defaultValue: "")}
    var backPassportKey: DefaultsKey<String> {.init("backPassportKey", defaultValue: "")}
    var selfiePassportKey: DefaultsKey<String> {.init("selfiePassportKey", defaultValue: "")}
    var identificationModel: DefaultsKey<Data> {.init("identificationModel", defaultValue: Data())}
}

final class UserDefaultsService {
    
    static let shared = UserDefaultsService()
    
    var identificationModel: IdentificationRequestsModel {
        get {
            let data = try? JSONDecoder().decode(IdentificationRequestsModel.self, from: Defaults[\.identificationModel])
            return data ?? IdentificationRequestsModel()
        }
        set {
            let data = try? JSONEncoder().encode(newValue)
            return Defaults[\.identificationModel] = data ?? Data()
        }
    }
    // MARK: - Images
    
    var selfiePassportKey: String {
        get {
            return Defaults[\.selfiePassportKey]
        }
        set {
            return Defaults[\.selfiePassportKey] = newValue
        }
    }
    
    var backPassportKey: String {
        get {
            return Defaults[\.backPassportKey]
        }
        set {
            return Defaults[\.backPassportKey] = newValue
        }
    }
    
    var frontPassportKey: String {
        get {
            return Defaults[\.frontPassportKey]
        }
        set {
            return Defaults[\.frontPassportKey] = newValue
        }
    }
    
    // MARK: - EndImages
    
    var editIdentificationInputType: InputBarType? {
        set {
            if let newValue {
                Defaults[\.editIdentificationInputType] = newValue.rawValue
            } else {
                Defaults[\.editIdentificationInputType] = -1
            }
        }
        get {
            if Defaults[\.editIdentificationInputType] != -1 {
                return InputBarType.init(rawValue: Defaults[\.editIdentificationInputType])!
            } else {
                return nil
            }
        }
    }
    
    var identificationInputType: InputBarType {
        set {
            Defaults[\.identificationInputType] = newValue.rawValue
        }
        get {
            InputBarType.init(rawValue: Defaults[\.identificationInputType])!
        }
    }
    
    var savedIdentificationInputType: InputBarType {
        set {
            Defaults[\.savedIdentificationInputType] = newValue.rawValue
        }
        get {
            InputBarType.init(rawValue: Defaults[\.savedIdentificationInputType])!
        }
    }
    
    var homeCellTypes: [String] {
        set {
            Defaults[\.homeCellTypes] = newValue
        }
        get {
            Defaults[\.homeCellTypes]
        }
    }
    
    var homeRemoveCellTypes: [String] {
        set {
            Defaults[\.homeRemoveCellTypes] = newValue
        }
        get {
            Defaults[\.homeRemoveCellTypes]
        }
    }
    
    var isDarkMode: Bool {
        set {
            Defaults[\.isDarkMode] = newValue
        }
        get {
            Defaults[\.isDarkMode]
        }
    }
    
    var isFirstLaunch: Bool {
        set {
            Defaults[\.isFirstLaunch] = newValue
        }
        get {
            Defaults[\.isFirstLaunch]
        }
    }
    
    var isSecured: Bool {
        set {
            Defaults[\.isSecured] = newValue
        }
        get {
            Defaults[\.isSecured]
        }
    }
    
    var callOtpTimestamp: Double {
        set {
            Defaults[\.callOtpTimestamp] = newValue
        }
        get {
            Defaults[\.callOtpTimestamp]
        }
    }
    
    var otpTimestamp: Double {
        set {
            Defaults[\.otpTimestamp] = newValue
        }
        get {
            Defaults[\.otpTimestamp]
        }
    }
    
    var phoneNumber: String {
        set {
            Defaults[\.phoneNumber] = newValue
        }
        get {
            Defaults[\.phoneNumber]
        }
    }
    
    var hasFaceAndTouchPermission: Bool {
        set {
            Defaults[\.hasFaceAndTouchPermission] = newValue
        }
        get {
            Defaults[\.hasFaceAndTouchPermission]
        }
    }
    
    var isAutoPayEnabled: Bool {
        set {
            Defaults[\.isAutoPayEnabled] = newValue
        }
        get {
            Defaults[\.isAutoPayEnabled]
        }
    }
    
    
    func removeAll() {
        Defaults.defaults.removeObject(forKey: "hasFaceAndTouchPermission")
        Defaults.defaults.removeObject(forKey: "phoneNumber")
        Defaults.defaults.removeObject(forKey: "otpTimestamp")
        Defaults.defaults.removeObject(forKey: "isSecured")
        Defaults.defaults.removeObject(forKey: "homeCellTypes")
        Defaults.defaults.removeObject(forKey: "registeredHomeCellTypes")
        Defaults.defaults.removeObject(forKey: "callOtpTimestamp")
        Defaults.defaults.removeObject(forKey: "isAutoPayEnabled")
        Defaults.defaults.removeObject(forKey: "identificationInputType")
        Defaults.defaults.removeObject(forKey: "savedIdentificationInputType")
        Defaults.defaults.removeObject(forKey: "editIdentificationInputType")
        Defaults.defaults.removeObject(forKey: "frontPassportKey")
        Defaults.defaults.removeObject(forKey: "backPassportKey")
        Defaults.defaults.removeObject(forKey: "selfiePassportKey")
        Defaults.defaults.removeObject(forKey: "identificationModel")
    }
    
    func clearKeychainIfWillUnistall() {
        let freshInstall = !UserDefaults.standard.bool(forKey: "alreadyInstalled")
        if freshInstall {
            KeychainService.shared.deleteAll()
            UserDefaults.standard.set(true, forKey: "alreadyInstalled")
        }
    }
}
