//
//  FileManagerService.swift
//  Cash2U
//
//  Created by talgar osmonov on 1/11/22.
//

import Foundation
import UIKit


final class FileManagerService {
    static let shared = FileManagerService()
    
    func saveImage(fileName: String, image: UIImage, completion: (() -> Void)? = nil) {
        DispatchQueue.main.async {
            guard let data = image.jpegData(compressionQuality: 0.7) else {
                return
            }
            guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
                return
            }
            do {
                try data.write(to: directory.appendingPathComponent("\(fileName).png")!)
                if let completion {
                    completion()
                }
            } catch {
                print(error.localizedDescription)
                return
            }
        }
    }
    
    func getSavedImage(named: String) -> UIImage? {
        if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
            return UIImage(contentsOfFile: URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(named).path)
        }
        return nil
    }
}
