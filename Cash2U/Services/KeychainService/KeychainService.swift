//
//  KeychainService.swift
//  Cash2U
//
//  Created by talgar osmonov on 20/7/22.
//

import Foundation
import KeychainSwift

final class KeychainService {
    
    static let shared = KeychainService()
    
    private var provider: KeychainSwift {
        let i = KeychainSwift()
        i.synchronizable = true
        return i
    }
    
    // MARK: - Token
    
    var accessToken: String? {
        get {
            provider.get("access_token")
        }
        set {
            provider.set(newValue ?? "", forKey: "access_token")
        }
    }
    
    var refreshToken: String? {
        get {
            provider.get("refresh_token")
        }
        set {
            provider.set(newValue ?? "", forKey: "refresh_token")
        }
    }
    
    func saveToken(token: UserTokenResponseModel?) {
        if let token = token {
            accessToken = token.accessToken
            refreshToken = token.refreshToken
        }
    }
    
    // MARK: - Pin Code
    
    var pinCode: String? {
        get {
            provider.get("pin_code")
        }
        set {
            provider.set(newValue ?? "", forKey: "pin_code")
        }
    }
    
    var hasToken: Bool {
        return self.accessToken != "" &&  self.accessToken != nil
    }
    
    func deleteAll() {
        provider.clear()
    }

}
